var webpack = require('webpack');
var HardSourceWebpackPlugin = require('hard-source-webpack-plugin');

module.exports = {

  entry     : {'script' : './src/js/index.js'},
  output    : {
    filename          : '[name].min.js',
    sourceMapFilename : '[name].min.map'
  },
  module    : {
    loaders : [
      {
        test    : /.jsx?$/,
        loader  : 'babel-loader',
        exclude : /node_modules/,

        query : {
          compact : false,
          presets : ['es2015', 'react']
        }
      }
    ]
  },
  devtool   : 'source-map',
  externals : {
    'react-dom' : 'ReactDOM',
    'react'     : 'React',
    'pikaday'   : 'Pikaday',
    'loader'    : 'Loader',
    'stations'  : 'Stations'
  },
  plugins   : [
    new webpack.optimize.UglifyJsPlugin({
      compress : {
        warnings : true
      }
    }),
    new HardSourceWebpackPlugin({
      cacheDirectory : '.cache/[confighash]',
      configHash     : require('node-object-hash')({sort : false}).hash,
      recordsPath    : '.cache/[confighash]/records.json',
    })
  ],
  resolve   : {
    extensions : ['', '.js', '.jsx']
  }
};