const gulp = require('gulp');

function parsePropertyType(property) {
  if (Array.isArray(property))
    return 'array';
  else if (Number.isInteger(property))
    return 'int';
  else if (typeof property === 'number')
    return 'number';
  else if (typeof property === "string")
    return 'string';
  else if(typeof property === "object")
    return 'object';
  return 'any';
}

function parseProperty(unknown, propertyName) {

}

function parseObject(obj, propertyName) {
  let tempObj = {};

  for (let property in obj) {
    if (obj.hasOwnProperty(property)) {
      tempObj[property] = parsePropertyType(obj[property]);
    }
  }

  console.log(tempObj);
}

gulp.task('test:jsdoc', function (done) {
  let json;

  try {
    json = require('./../dest/test.json');
  } catch (e) {
    return console.error(e);
  }
  console.log(json.id);
  if (Array.isArray(json))
    console.log('Array');
  else
    parseObject(json, ['Main']);

  done();
});