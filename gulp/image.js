var gulp = require('gulp')
  , image = require('gulp-image')
  , watch = require('gulp-watch')
  , livereload = require('gulp-livereload')
  , rename = require('gulp-rename')
  , fs = require('fs')
  , clean = require('gulp-clean');

var debug = require('gulp-debug');

var plumber = require('gulp-plumber');

var imagemin = require('gulp-imagemin');
var resize = require('gulp-image-resize');
var svg2png = require('gulp-svg2png');

var chaos = require('gulp-chaos');

gulp.task('image:convert::svg-png', function () {
  return gulp.src('./dest/public/images/map/*.svg')
             .pipe(chaos('./dest/public/images/map/png', {taskName : 'image:convert::svg-png'}))
             .pipe(svg2png({
               width  : 100,
               height : 100
             }, true, 10))
             .pipe(debug({title : 'convert file'}))
             .pipe(gulp.dest('./dest/public/images/map/png'))
             .pipe(livereload());
});

gulp.task('image:clean', function (done) {

  try {
    fs.accessSync('./dest/public/images', fs.F_OK);
    return gulp.src('./dest/public/images')
               .pipe(clean());
  } catch (e) {
  }
  return done()

});

var imageMinConfigSVG = {verbose : true}
  , imageMinConfigNotSVG = {
  pngquant       : true,
  optipng        : true,
  zopflipng      : false,
  // zopflipng      : true,
  jpegRecompress : false,
  jpegoptim      : true,
  mozjpeg        : true,
  gifsicle       : true,
  svgo           : false,
  concurrent     : 10
};

gulp.task('image:optimize::svg', function () {
  return gulp.src('./src/images/**/*.svg')
             .pipe(chaos('./dest/public/images', {taskName : 'image:optimize::svg'}))
             .pipe(imagemin(imageMinConfigSVG))
             .pipe(gulp.dest('./dest/public/images'))
             .pipe(livereload());
});

gulp.task('image:optimize::notsvg', function () {
  return gulp.src(['./src/images/**/*.*', '!./src/images/**/*.svg'])
             .pipe(chaos('./dest/public/images', {taskName : 'image:optimize::notsvg'}))
             .pipe(gulp.dest('./dest/public/images'))
             .pipe(plumber())
             .pipe(resize({
               width : 1600
             }))
             .pipe(gulp.dest('./dest/public/images'))
             .pipe(image(imageMinConfigNotSVG))
             .pipe(plumber.stop())
             .pipe(gulp.dest('./dest/public/images'));
});

gulp.task('image:minify', gulp.parallel('image:optimize::notsvg', 'image:optimize::svg'));

gulp.task('image:watch::svg', function () {
  return watch('./src/images/**/*.svg', {
    name    : 'image::svg',
    verbose : true
  }, function (file) {
    let index = file.path.indexOf('/src/images/'), src = '.' + file.path.slice(index);
    fs.access(src, fs.F_OK, function (err) {
      if (!err) {
        return gulp.src(src)
                   .pipe(imagemin(imageMinConfigSVG))
                   .pipe(gulp.dest('./dest/public/images/' + src.replace('./src/images/', '').replace(file.basename, '')))
                   .pipe(livereload());
      }
    });
  });
});

gulp.task('image:watch::notsvg', function () {
  return watch('./src/images/**/*.{png|jpg}', {
    name    : 'image::notsvg',
    verbose : true
  }, function (file) {
    let index = file.path.indexOf('/src/images/'), src = '.' + file.path.slice(index);
    fs.access(src, fs.F_OK, function (err) {
      if (!err) {

        return gulp.src(src)
                   .pipe(resize({
                     width : 1600
                   }))
                   .pipe(image(imageMinConfigNotSVG))
                   .pipe(gulp.dest('./dest/public/images/' + src.replace('./src/images/', '').replace(file.basename, '')))
                   .pipe(livereload());

      }
    });
  });
});

gulp.task('image:watch', gulp.parallel('image:watch::notsvg', 'image:watch::svg'));

module.exports = {
  build : gulp.series(/*'image:clean', */'image:minify', 'image:convert::svg-png'),
  watch : gulp.parallel('image:watch::notsvg', 'image:watch::svg')
};
