const gulp = require('gulp')
  , watch = require('gulp-watch')
  , webpack = require('gulp-webpack')
  , debug = require('gulp-debug')
  , replace = require('gulp-replace')
  , rename = require('gulp-rename')
  , livereload = require('gulp-livereload')
  , uglifyjs = require('gulp-uglifyjs')
  , gulpIf = require('gulp-if')
  , concat = require('gulp-concat');

const _config = require('./_config');
const gulpIgnore = require('gulp-ignore');

function js_build__index(isWatch) {

  let webpackConfig = require('./webpack.conf');
  webpackConfig.watch = isWatch;
  webpackConfig.debug = isWatch;

  return gulp.src('./src/js/index.js')
    .pipe(webpack(webpackConfig))
    .pipe(replace('%gulp-mainPath%', _config.main))
    .pipe(replace('%gulp-imagePath%', _config.path.images))
    .pipe(gulp.dest('./dest/public/js'))
    .pipe(livereload());
}

function js_build__admin(isWatch) {

  let webpackConfig = require('./webpack.admin.conf');
  webpackConfig.watch = isWatch;
  webpackConfig.debug = isWatch;

  return gulp.src('./src/admin/js/index.js')
    .pipe(webpack(webpackConfig))
    .pipe(replace('%gulp-mainPath%', _config.main))
    .pipe(replace('%gulp-imagePath%', _config.path.images))
    .pipe(replace('%gulp-adminPath%', _config.admin.main))
    .pipe(gulp.dest('./dest/public/' + _config.adminDir + '/js'))
    .pipe(livereload());
}

gulp.task('js:build::index', js_build__index.bind(gulp, false));
gulp.task('js:build::admin', js_build__admin.bind(gulp, false));

gulp.task('js:watch::index', js_build__index.bind(gulp, true));
gulp.task('js:watch::admin', js_build__admin.bind(gulp, true));

module.exports = {
  build : gulp.parallel('js:build::index', 'js:build::admin'),
  watch : gulp.parallel('js:watch::index'),
  admin : {
    build : gulp.parallel('js:build::admin'),
    watch : gulp.parallel('js:watch::admin'),
  }
};