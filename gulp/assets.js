var gulp = require('gulp')
  , _config = require('./_config');

gulp.task('assets:js', function () {
  return gulp.src('./src/assets/js/**/*.js')
    .pipe(gulp.dest('./dest/public/js'))
});

gulp.task('assets:admin::js', function () {
  return gulp.src('./src/admin/assets/js/**/*.js')
    .pipe(gulp.dest('./dest/public/' + _config.adminDir + '/js/assets'))
});

gulp.task('assets:admin::css', function () {
  return gulp.src('./src/admin/assets/css/**/*.*')
    .pipe(gulp.dest('./dest/public/' + _config.adminDir + '/css/assets'))
});

gulp.task('assets:admin::fonts', function () {
  return gulp.src('./src/admin/fonts/**/*.*')
    .pipe(gulp.dest('./dest/public/' + _config.adminDir + '/fonts/'))
});

module.exports = {
  build : gulp.parallel('assets:js'),
  admin : {
    build : gulp.parallel('assets:admin::js', 'assets:admin::css', 'assets:admin::fonts')
  }
};