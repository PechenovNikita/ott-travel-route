var webpack = require('webpack');
var ReplacePlugin = require('webpack-plugin-replace');
var _config = require('./_config');

module.exports = {

  entry        : {'admin' : './src/admin/js/index.js'},
  output       : {
    filename          : '[name].min.js',
    sourceMapFilename : '[name].min.map',
    publicPath        : '/public/' + _config.adminDir + '/js/',
    path              : path.resolve(__dirname, '../dest/public/' + _config.adminDir + '/js'),
  },
  module       : {
    loaders : [
      {
        test    : /.jsx?$/,
        loader  : 'babel-loader',
        exclude : /(node_modules|bower_components|dest|gulp)/,

        query : {
          compact : false,
          presets : ['es2015', 'stage-0', 'react']
        }
      }
    ]
  },
  devtool      : 'source-map',
  externals    : {
    'react-dom'  : 'ReactDOM',
    'react'      : 'React',
    'pikaday'    : 'Pikaday',
    'loader'     : 'Loader',
    'jsoneditor' : 'JSONEditor',
    'ref-data'   : 'window.tw.refData.ru',
    'stations'   : 'Stations'
  },
  resolve      : {
    extensions : ['', '.js', '.jsx']
  },
  watch        : false,
  watchOptions : {
    aggregateTimeout : 300,
    poll             : 1000,
    ignored          : /node_modules/
  },

  devServer : {
    contentBase : path.join(__dirname, './dest'),
    compress    : true,
    port        : 9000,

    headers : {
      'Access-Control-Allow-Origin'  : '*',
      'Access-Control-Allow-Methods' : 'GET, POST, PUT, DELETE, PATCH, OPTIONS',
      'Access-Control-Allow-Headers' : 'X-Requested-With, content-type, Authorization'
    }
  }
};
