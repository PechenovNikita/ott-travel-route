var path = '';
var domain = 'http://travelexpert.onetwotrip.com';
// var googleMapApi = 'AIzaSyBrQYQaWyBCzqu7Fn_lSYZPw-DMTPCnRT4';
var googleMapApi = 'AIzaSyBNpNAkpzG1ZGj0GfzPYxajZZl61s4JfBs';

console.log('googleMapApi', googleMapApi);

var fs = require('fs');

if (fs.existsSync('./local/_config.js')) {
  path = require('./../local/_config').path;
  domain = require('./../local/_config').domain;
  googleMapApi = require('./../local/_config').googleMapApi;
}

function rubCreate(str) {
  str = str.replace(new RegExp('%rub-bold%', 'g'), '<span class=\'icon-rub bold\'></span>');
  return str.replace(new RegExp('%rub%', 'g'), '<span class=\'icon-rub regular\'></span>');
}

var adminDir = 'admin_43953/';

module.exports = {
  domain       : domain,
  main         : path + '/ru/',
  adminDir     : adminDir,
  admin        : {
    path   : path + '/public/',
    main   : path + '/public/' + adminDir + '',
    data   : path + '/public/' + adminDir + 'data/',
    css    : path + '/public/' + adminDir + 'css/',
    images : path + '/public/' + adminDir + 'images/',
    js     : path + '/public/' + adminDir + 'js/'
  },
  path         : {
    main   : path + '/public/',
    css    : path + '/public/css/',
    images : path + '/public/images/',
    js     : path + '/public/js/'
  },
  content      : '',
  googleMapApi : googleMapApi
};
