'use strict';

const axios = require('axios');

const chat_id = '-235099588';
const botID = '463338497:AAHerO8hx-w1CobWM26dwgdYsnuqeiL9Jeg';

/**
 *
 * @param {string} text
 * @return {Promise<*>}
 */
module.exports = function (text) {
  const data = {
    chat_id,
    text,
    disable_notification : true
  };

  return axios.get(`https://api.telegram.org/bot${botID}/sendMessage`, {params:data})
              .then(response => {
                if (response.status === 200)
                  return response.data;
                return null;
              })
              .catch((err) => true);
};
/**
 *
 * @param {string|number} message_id
 * @param {string} text
 * @return {Promise<*>}
 */
module.exports.update = function (message_id, text) {
  const data = {
    chat_id,
    text,
    message_id
  };

  return axios.get(`https://api.telegram.org/bot${botID}/editMessageText`, {params:data})
              .then(response => {
                if (response.status === 200)
                  return response.data;
                return null;
              })
              .catch(() => true);
};
