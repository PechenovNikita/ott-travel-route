var gulp = require('gulp')
  , less = require('gulp-less')
  , rename = require('gulp-rename')
  , livereload = require('gulp-livereload')
  , lessData = require('gulp-less-data')
  , cleanCSS = require('gulp-clean-css')
  , watch = require('gulp-watch');

var _config = require('./_config');
var chaos = require('gulp-chaos');

gulp.task('less:build::reset', function () {
  return gulp.src('./src/less/assets/reset.less')
             .pipe(chaos('./dest/public/css', {taskName : 'less:build::reset'}))
             .pipe(less())
             .pipe(gulp.dest('./dest/public/css'))
             .pipe(cleanCSS())
             .pipe(rename('reset.min.css'))
             .pipe(gulp.dest('./dest/public/css'));
});

function less_build__index() {
  return gulp.src('./src/less/index.less')
             .pipe(chaos('./dest/public/css', {
               taskName : 'less_build__index',
               dirs     : ['./src/less/**/*.less']
             }))
             .pipe(lessData({
               imgPath : '"' + _config.path.images + '"'
             }))
             .pipe(less())
             .pipe(rename('style.css'))
             .pipe(gulp.dest('./dest/public/css'))
             .pipe(livereload())
             .pipe(cleanCSS())
             .pipe(rename('style.min.css'))
             .pipe(gulp.dest('./dest/public/css'))
             .pipe(livereload());
}

function less_build__admin() {
  return gulp.src('./src/admin/less/index.less')
             .pipe(chaos('./dest/public/' + _config.adminDir + '/css', {
               taskName : 'less_build__admin',
               dirs     : ['./src/admin/less/**/*.less']
             }))
             .pipe(lessData({
               imgPath : '"' + _config.path.images + '"'
             }))
             .pipe(less())
             .pipe(rename('admin.css'))
             .pipe(gulp.dest('./dest/public/' + _config.adminDir + '/css'))
             .pipe(livereload())
             .pipe(cleanCSS())
             .pipe(rename('admin.min.css'))
             .pipe(gulp.dest('./dest/public/' + _config.adminDir + '/css'))
             .pipe(livereload());
}

function less_build__print() {
  return gulp.src('./src/less/print.less')
             .pipe(lessData({
               imgPath : '"' + _config.path.images + '"'
             }))
             .pipe(less())
             .pipe(rename('print.css'))
             .pipe(gulp.dest('./dest/public/css'))
             .pipe(livereload())
             .pipe(cleanCSS())
             .pipe(rename('print.min.css'))
             .pipe(gulp.dest('./dest/public/css'))
             .pipe(livereload());
}

gulp.task('less:build::index', gulp.parallel(less_build__index, less_build__print));
gulp.task('less:build::admin', less_build__admin);

gulp.task('less:watch::index', function () {
  return watch('./src/less/**/*.less', {
    name    : 'less::index',
    verbose : true
  }, gulp.parallel(less_build__index, less_build__print));
});
gulp.task('less:watch::admin', function () {
  return watch('./src/admin/less/**/*.less', {
    name    : 'less::admin',
    verbose : true
  }, less_build__admin);
});

module.exports = {
  build : gulp.parallel('less:build::reset', 'less:build::index', 'less:build::admin'),
  watch : gulp.parallel('less:watch::index'),
  admin : {
    build : gulp.series('less:build::admin'),
    watch : gulp.series('less:watch::admin'),
  }
};