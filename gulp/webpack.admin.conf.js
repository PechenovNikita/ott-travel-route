var webpack = require('webpack');

module.exports = {

  entry     : {'admin' : './src/admin/js/index.js'},
  output    : {
    filename          : '[name].min.js',
    sourceMapFilename : '[name].min.map'
  },
  module    : {
    loaders : [
      {
        test    : /.jsx?$/,
        loader  : 'babel-loader',
        exclude: /(node_modules|bower_components|dest|gulp)/,

        query : {
          compact : false,
          presets : ['es2015', 'stage-0','react']
        }
      }
    ]
  },
  devtool   : 'source-map',
  externals : {
    'react-dom'  : 'ReactDOM',
    'react'      : 'React',
    'pikaday'    : 'Pikaday',
    'loader'     : 'Loader',
    'jsoneditor' : 'JSONEditor',
    'ref-data'   : 'window.tw.refData.ru',
    'stations'   : 'Stations'
  },
  resolve   : {
    extensions : ['', '.js', '.jsx']
  }
};
