const gulp = require('gulp')
  , pug = require('gulp-pug')
  , debug = require('gulp-debug')
  , rename = require('gulp-rename')
  , watch = require('gulp-watch')
  , livereload = require('gulp-livereload');

var plumber = require('gulp-plumber');
var chaos = require('gulp-chaos');
var middleware = require('gulp-middleware');

var fs = require('fs');
var argv = require('yargs').argv;

var config = require('./_config');

function rubCreate(str) {
  str = str.replace(new RegExp('%rub-bold%', 'g'), '<span class=\'icon-rub bold\'></span>');
  return str.replace(new RegExp('%rub%', 'g'), '<span class=\'icon-rub regular\'></span>');
}

function configRub(configData) {
  configData = JSON.stringify(configData);
  configData = rubCreate(configData);
  return JSON.parse(configData);
}

function dateFormat() {
  let d = new Date();
  return '[\033[42m\033[30m' + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds() + '\033[0m]';
}

let dateNow = Date.now();
let emptyFileDataContent = '{"data":[], "update":"' + dateNow + '"}';

function createRec(pathes, index, callback) {
  let path = pathes.filter(function (p, i) {return i <= index;}).join('')
    , done = (pathes.length == index + 1);

  let func = function () {
    if (!done)
      createRec(pathes, index + 1, callback);
    else
      callback();
  };

  fs.stat(path, function (err, stat) {

    if (err) {
      if (path[path.length - 1] == '/')
        fs.mkdir(path, func);

      else {
        fs.open(path, 'w', function (err, file) {
          fs.writeSync(file, emptyFileDataContent);
          fs.closeSync(file);

          func();
        });
      }

    } else {
      func();
    }

  });
}

gulp.task('json:create::data.json', function (done) {
  createRec(['./dest/', 'public/', config.adminDir, 'data.json'], 0, function () {
    done();
  });
});

function addAdminJSONData(jsonName) {

  fs.access('./dest/public/' + config.adminDir + 'data.json', function (err) {
    if (err) {
      console.log('no file data.json', './dest/public/' + config.adminDir + 'data.json');
      let file = fs.openSync('./dest/public/' + config.adminDir + '/data.json', 'w');
      fs.writeSync(file, emptyFileDataContent);
      fs.closeSync(file);
    }
    let adminData = require('./../dest/public/' + config.adminDir + '/data.json');
    if (!adminData.update) {
      adminData = {
        data   : [],
        update : dateNow
      }
    } else {
      adminData.update = dateNow;
    }
    if (adminData.data.indexOf(jsonName) < 0) {
      adminData.data.push(jsonName);
      let file = fs.openSync('./dest/public/' + config.adminDir + '/data.json', 'w');
      fs.writeSync(file, JSON.stringify(adminData));
      fs.closeSync(file);
    }
  });

  gulp.src('./src/data/' + jsonName)
      .pipe(gulp.dest('./dest/public/' + config.adminDir + '/data/'));
}

/**
 *
 * @param jsonFile
 * @return {Promise<any>}
 */
function createIndex(jsonFile) {
  if (jsonFile.indexOf('.json') < 1)
    return Promise.resolve();

  addAdminJSONData(jsonFile);

  let name = jsonFile.replace('.json', '');
  let dir = './dest/ru/' + name;

  let PUGsettings = {
    pretty : true,
    locals : {
      url      : name + '/index.html',
      config   : JSON.parse(JSON.stringify(require('./_config'))),
      topImage : 'top/back00.png'
    }
  };

  return new Promise((resolve, reject) => {

    fs.readFile('./src/data/' + jsonFile, 'utf8', function (err, data) {
      // if (err) throw err;
      let t = data.replace(/\n/g, '').replace(/^\s+\{/, '{');

      PUGsettings.locals.config.content = configRub(JSON.parse(t));

      gulp.src('./src/pug/index.pug')
          .on('finish', () => {
            // console.log(dateFormat() + 'Create index.html from ' + jsonFile + ': \'\033[35m' + dir + '\033[0m');
            resolve()
          })
          .on('error', () => reject())
          // .pipe(chaos('./dest/ru/', {
          //   taskName : jsonFile,
          //   dirs     : ['./src/pug/**/*.pug', './src/data/' + jsonFile],
          // }))
          .pipe(middleware((file, next) => {
            console.log(dateFormat() + ' Creating index.html from \'\033[36m' + jsonFile + '\033[0m\' in directory \'\033[35m' + dir + '\033[0m');
            next();
          }))
          .pipe(pug(PUGsettings))
          .pipe(gulp.dest(dir))
          .pipe(middleware((file, next) => {
            console.log(dateFormat() + ' File index.html from \'\033[36m' + jsonFile + '\033[0m\' was created');
            next();
          }))
          .pipe(livereload());
    });

  })
  // PUGsettings.locals.config.content = configRub(require('./../src/data/' + jsonFile));

  // gulp.src('./src/pug/index.pug')
  //  .pipe(pug(PUGsettings))
  //  .pipe(gulp.dest(dir))
  //  .pipe(debug({title : 'Create index.html from ' + jsonFile + ':'}));

}

gulp.task('pug:build::all', function (done) {

  if (argv.name) {
    fs.access('./src/data/' + argv.name + '.json', (err) => {
      if (!err) {
        createIndex(argv.name + '.json')
          .then(() => done())
          .catch(() => done());
      } else {
        console.error('File \'\033[36m' + argv.name + '.json' + '\033[0m\' doesn\'t exist!');
        done();
      }
    });
  } else {
    fs.readdir('./src/data', function (err, files) {
      Promise.all(files.map(createIndex))
             .then(() => done())
             .catch(() => done());
    });
  }

});

gulp.task('json:watch::index', function () {
  return watch('./src/data/*.json', {verbose : true}, function (file) {
    let fileName = file.basename;
    fs.access('./src/data/' + file.basename, (err) => {
      if (!err) {
        createIndex(fileName)
      } else {
        console.error('File \'\033[36m' + fileName + '\033[0m\' doesn\'t exist!')
      }
    });
  });
});

gulp.task('pug:watch::index', function () {
  return watch('./src/pug/**/*.pug', {verbose : true}, function () {
    fs.readdir('./src/data', function (err, files) {
      files.forEach(createIndex);
    });
  });
});

gulp.task('pug:build::admin', function () {
  return gulp.src('./src/admin/pug/index.pug')
             .pipe(pug({
               pretty : true,
               locals : {
                 config   : JSON.parse(JSON.stringify(require('./_config'))),
                 topImage : 'top/back00.png'
               }
             }))
             .pipe(gulp.dest('./dest/public/' + config.adminDir + '/'));
});

gulp.task('pug:watch::admin', function () {
  return watch('./src/admin/pug/**/*.pug', {verbose : true}, function () {
    return gulp.src('./src/admin/pug/index.pug')
               .pipe(pug({
                 pretty : true,
                 locals : {
                   config   : JSON.parse(JSON.stringify(require('./_config'))),
                   topImage : 'top/back00.png'
                 }
               }))
               .pipe(gulp.dest('./dest/public/' + config.adminDir + '/'));
  });
});

module.exports = {
  build : gulp.series('json:create::data.json', 'pug:build::all'),
  watch : gulp.series('json:create::data.json', gulp.parallel('pug:watch::index', 'json:watch::index')),
  admin : {
    build : gulp.series('json:create::data.json', 'pug:build::admin'),
    watch : gulp.parallel('pug:watch::admin'),
  }
};
