const gulp = require('gulp')
  /*, fontgen = require('gulp-fontgen')*/;

gulp.task('fontgen:build::PFDin-Black', function () {
  // try {
  //
  //   return gulp.src("./src/fonts/PFDinTextCompPro-XBlack.ttf")
  //     .pipe(fontgen({
  //       dest : "./dest/public/fonts/PFDin"
  //     }));
  // } catch (e) {
  //   console.log(e);

    return gulp.src("./src/fonts/compiled/PFDin/*.*")
      .pipe(gulp.dest("./dest/public/fonts/PFDin"));
  // }
});

gulp.task('fontgen:build::Rouble', function () {
  return gulp.src("./src/fonts/Rouble/*.*")
    .pipe(gulp.dest("./dest/public/fonts/Rouble"));
});

gulp.task('fontgen:copy::social-icons', function () {
  return gulp.src("./src/fonts/social-icons/*.*")
    .pipe(gulp.dest("./dest/public/fonts/social-icons"));
});

module.exports = {
  build : gulp.parallel('fontgen:build::PFDin-Black', 'fontgen:build::Rouble', 'fontgen:copy::social-icons')
};