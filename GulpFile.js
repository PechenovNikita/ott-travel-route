var gulp = require('gulp')
  , livereload = require('gulp-livereload');

var pug = require('./gulp/pug')
  , less = require('./gulp/less')
  , js = require('./gulp/js')
  , fonts = require('./gulp/fonts')
  , image = require('./gulp/image')
  , assets = require('./gulp/assets');

var telegram = require('./gulp/telegram')
  , telegramUpdate = require('./gulp/telegram').update;
// require('./gulp/test');

// MAIN
let messageID_image;
let messageID_static;
let messageID_page;
gulp.task('build:image', gulp.series(
  function (done) {
    telegram(`🏎️ 📷 конвертируем картинки`)
      .then((data) => {
        if (data) {
          if (data.ok) {
            messageID_image = data.result.message_id;
          }
        }
        done()
      });
  },
  gulp.parallel(image.build),
  function (done) {
    telegramUpdate(messageID_image, `🏁 📷 картинки готовы`)
      .then((data) => {
        done()
      });
  }));

gulp.task('build:static', gulp.series(
  function (done) {
    telegram(`🏎️ 🈴 создаем скрипты и стили`)
      .then((data) => {
        if (data) {
          if (data.ok) {
            messageID_static = data.result.message_id;
          }
        }
        done()
      });
  }, gulp.parallel(assets.build, less.build, js.build, fonts.build, js.admin.build, less.admin.build, assets.admin.build),
  function (done) {
    telegramUpdate(messageID_static, `🏁 🈴 скрипты и стили готовы`)
      .then((data) => {
        done()
      });
  }));

gulp.task('build:page', gulp.series(
  function (done) {
    telegram(`🏎️ 🗺️ создаем сраницы городов`)
      .then((data) => {
        if (data) {
          if (data.ok) {
            messageID_page = data.result.message_id;
          }
        }
        done()
      });
  }, gulp.series(pug.build, pug.admin.build),
  function (done) {
    telegramUpdate(messageID_page, `🏁 🗺️ страницы городов готовы`)
      .then((data) => {
        done()
      });
  }));

// ADMIN
gulp.task('build:admin', gulp.series(pug.admin.build, js.admin.build, less.admin.build, assets.admin.build));
gulp.task('watch:admin', gulp.parallel(less.admin.watch, js.admin.watch, pug.admin.watch));

// WATCH ALL
gulp.task('watch', gulp.parallel(function live() {
  livereload.listen();
}, image.watch, less.watch, js.watch, pug.watch, 'watch:admin'));

// ALL
gulp.task('default', gulp.series(
  function (done) {
    telegram('start build local')
      .then(() => done());
  },
  gulp.parallel('build:page', 'build:image', 'build:static')),
  function (done) {
    telegram('end build local')
      .then(() => done());
  }
);
