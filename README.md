# README #



### Что надо сделать чтобы запустить? ###

```
#!javascript
> npm install
```
```
#!javascript
> gulp
```

### Задачи ###

Задача, которая собирает все css и js файлы в папку ./dest/public
```
#!javascript

> gulp build:static
```

Задача, которая оптимизирует все картинки из папки ./src/images в папку ./dest/public/images
```
#!javascript

> gulp build:image
```

Задача по компиляции всех страниц по json файлам в папке ./src/data/%id%.json в соответствующие папки ./dest/ru/%id%/index.html 
```
#!javascript

> gulp build:page
```

Задача по компиляции одной страницы по json файлу в папке ./src/data/%имя файла%.json в соответствующие папки ./dest/ru/%имя файла%/index.html 
```
#!javascript

> gulp build:page --name %имя файла%
```