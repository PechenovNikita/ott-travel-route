const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const http = require('http');
const app = express();
// url encoding
app.use(bodyParser.urlencoded({extended : false}));
// gzip
// redirect all html requests to `index.html`
app.use(function (req, res, next) {

  if (path.extname(req.path).length > 0) {
    // console.log('>0', req.path);
    // normal static file request
    if (req.path == '/test.json') {
      res.json(require('./../dest/test.json'));
    } else {
      // console.log(req.path == '/test.json', 'req.path');
      req.url = req.path.replace('/ott/travel-route', '');
      // console.log(req.path == '/test.json', req.url);
      next();
    }
  }
  else {
    // console.log('<0', req.path);
    // should force return `index.html` for angular.js

    if (req.path == "/_api/searching/startSync2/") {
      // console.log('json');
      let request = http.request(
        {
          port   : 80,
          host   : 'www.onetwotrip.com',
          method : 'GET',
          path   : '/_api/searching/startSync2/?ad=1&cn=0&in=0&cs=E&route=0102MOWLED0502&currency=RUB&source=YOURAPIIDENTIFIER&callback=test'
        });

      request.on('response', function (response) {
        // console.log('json response');
        response.on('data', function (data) {
          // console.log('json response data');

          // console.log(data.toString());
          res.json(data.toString());
          // next();
        });
      });
      request.end();
    } else {
      req.url = req.path + '/index.html';
      next();
    }

  }
});
// static file serve
app.use(express.static('./dest'));
console.log('start virtual server on port 3000');
app.listen(3000);

