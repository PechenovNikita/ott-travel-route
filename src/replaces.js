const fs = require('fs');
const url = require('url');
const querystring = require('querystring');

const sourcePath = './src/data';
const regexpFind = /"([^"]+ru\/hotel\/[^"]+)"/gmi;
const request = require('request');

const testData = require('./test.json');
const testMapData = new Map();

testData.forEach((url) => {
  testMapData.set(url.original, url.redirect)
});

function unique(arr) {
  var obj = {};

  for (var i = 0; i < arr.length; i++) {
    var str = arr[i];
    obj[str] = true; // запомнить строку в виде свойства объекта
  }

  return Object.keys(obj); // или собрать ключи перебором для IE8-
}

fs.readdir(sourcePath, (err, files) => {
  if (!err) {
    files.forEach(file => {
      if (file.indexOf('.json') > 0) {
        fs.readFile(`${sourcePath}/${file}`, 'utf8', (err, data) => {
          if (!err) {

            const match = data.match(regexpFind);
            let newData = data;
            if (match) {
              match.forEach((m) => {
                const withoutQuotes = m.split('"').join('');
                if(testMapData.has(withoutQuotes)){
                  newData = newData.replace(withoutQuotes, testMapData.get(withoutQuotes));
                }
              });

              fs.writeFile(`${sourcePath}/${file}`, newData, function (err) {
                if (err) {
                  return console.log(err);
                }
                console.log("The file json was saved!");
              });


            }
          }
        });
      }
    });
  }
});

if (true) {
  fs.readdir(sourcePath, (err, files) => {

    let count = 0, lines = [];
    let linesData = [];

    let promises = [];
    files.forEach(file => {
      if (file.indexOf('.json') > 0) {
        count++;
        fs.readFile(`${sourcePath}/${file}`, 'utf8', function (err, data) {
          count--;
          const temp_data = {
            file    : file,
            matches : []
          };
          const match = data.match(regexpFind);
          if (match) {
            match.forEach((m) => {
              lines.push(m);
              temp_data.matches.push(m.split('"').join(''));
            });
            temp_data.matches = unique(temp_data.matches);
            // if (match[1] === `"${raplaced[0][0]}"`)
            //   console.log(match, '____', raplaced[0][0]);
            // // raplaced.forEach((links) => {
            // //   console.log(links[0],'____', match[1]);
            // //   if (match[1] == links[0]) {
            // //     console.log(links[0]);
            // //   }
            // // });
            // // console.log(raplaced);
            // if (raplaced[match[1]]) {
            //   // console.log(match[1]);
            // }
          }

          if (count === 0) {

            console.log('Loaded', lines.length, unique(lines).length);

            unique(lines).sort().map((line) => {
              return line.trim().substring(1, line.length - 1);
            }).reduce((promise, line, index, arr) => {

              return promise.then(() => {
                return new Promise((resolve, reject) => {

                  if (testMapData.has(line)) {
                    console.log('index - ', index);

                    const replaceLine = testMapData.get(line);

                    const obj = url.parse(decodeURI(replaceLine));
                    // https://www.onetwotrip.com/ru/hotels/hotel/tamarit-apartments-503877
                    const newMatch = replaceLine.match(/hotel\/[^?]+-([\d]+)/i);

                    if (!newMatch) {
                      console.log(line);

                      // request.get('https://www.onetwotrip.com/_hotels/api/hotelRequest?id=400307&lang=ru&locale=ru&currency=RUB', (err, res, body) => {
                      //   console.log(JSON.parse(body).result);
                      // })
                      linesData.push({
                        original : line,
                        redirect : replaceLine
                      });
                      console.log('_____');

                      resolve();
                    } else {
                      const hotelID = newMatch[1];
                      console.log('id : ', hotelID);

                      let parameters = querystring.parse(obj.query);

                      parameters.utm_source = 'travel_expert_page';
                      parameters.utm_medium = 'travel-expert';
                      parameters.utm_campaign = 'travel_expert_page';
                      parameters.srcmarker2 = 'travel_expert_page';
                      parameters.scp = '14,12trip_travel-expert_ru,travel_expert_page';
                      parameters.s = 'true';

                      if (parameters.persons) {
                        const persons = parameters.persons.split(',');

                        switch (persons.length) {
                          case 0:
                            parameters.adults = 1;
                            break;
                          case 2:
                            parameters.children = persons[1];
                          case 1:
                            parameters.adults = persons[0];
                            break;
                          default:
                            parameters.adults = persons[0];
                            parameters.children = persons[1];
                            parameters.children_ages = persons.filter((num, index) => index > 1).join(',');
                        }

                        delete parameters.persons;

                      }
                      delete parameters.display_period;

                      if (!parameters.id || !parameters.sc || !parameters.countryName || !parameters.cityName || !parameters.to) {

                        console.log('get from Api');
                        request.get(`https://www.onetwotrip.com/_hotels/api/hotelRequest?id=${hotelID}&lang=ru&locale=ru&currency=RUB`, (err, res, body) => {
                          const getData = JSON.parse(body);

                          if (!getData.error && getData.result) {

                            const hotel = getData.result.hotel;

                            parameters.id = parameters.sc = hotel.city_id;
                            parameters.to = hotel.city;
                            parameters.countryName = hotel.country_slug;
                            parameters.cityName = hotel.city_slug;

                            // console.log(parameters);
                            // console.log();
                            const replacedLineWithNewParams = `${obj.protocol}//${obj.host}${obj.pathname}?${querystring.stringify(parameters)}`;

                            linesData.push({
                              original : line,
                              redirect : replacedLineWithNewParams
                            });
                          } else {
                            console.log('ERROR IN:', line);
                            console.log('ERROR:', getData.error);
                            linesData.push({
                              original : line,
                              redirect : replaceLine
                            });
                          }

                          console.log('_____');
                          resolve();

                        })
                      } else {
                        linesData.push({
                          original : line,
                          redirect : replaceLine
                        });
                        console.log('_____');
                        resolve();
                      }

                    }

                  } else {
                    const r = request.get(line, function (err, res, body) {
                      linesData.push({
                        original : line,
                        redirect : res.request.uri.href
                      });
                      console.log(line.substr(29, 40), res.request.uri.href.substr(29, 40));
                      resolve();
                    });
                  }

                })
              });

            }, Promise.resolve()).then(() => {

              fs.writeFile("./src/test.json", JSON.stringify(linesData), function (err) {
                if (err) {
                  return console.log(err);
                }
                console.log("The file json was saved!");
              });
            });

            // fs.writeFile("./src/test.txt", `${inner}`, function (err) {
            //   if (err) {
            //     return console.log(err);
            //   }
            //   console.log("The file was saved!");
            // });
            //
            // fs.writeFile("./src/test.json", JSON.stringify(linesData), function (err) {
            //   if (err) {
            //     return console.log(err);
            //   }
            //   console.log("The file json was saved!");
            // });
          }
        });

      }
    });
  });
}