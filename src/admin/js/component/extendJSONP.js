"use strict";

import JSONP from './../../../js/JSONP';

let data = {
  name : {},
  poi  : {},
  data : {}
};

let currentDate = new Date();

function numUpdate(num) {
  return (num < 10 ? '0' + num : num);
}

let defaultDateStart = (function () {
  return currentDate.getFullYear() + '-' + numUpdate(currentDate.getMonth() + 1) + '-' + numUpdate(currentDate.getDate());
}())
  , defaultDateEnd = (function () {
  currentDate.setDate(currentDate.getDate() + 30);
  return currentDate.getFullYear() + '-' + numUpdate(currentDate.getMonth() + 1) + '-' + numUpdate(currentDate.getDate());
}());

JSONP.admin = {
  getCityId(name, callback, limit = 100, type = 'geo'){
    // console.log('JSONP.admin.getCityId()', name, data.name);
    if (data.name[name]) {
      callback(data.name[name], 'cache');
    } else {
      return JSONP.ott.getCityId(name, function (response, id) {
        if (!response.error && response)
          data.name[name] = response;
        callback(response, id);
      }, limit, type);
    }
  },
  getPOI(city_id, callback){
    if (data.poi[city_id]) {
      callback(data.poi[city_id], 'cache');
    } else {
      return JSONP.ott.getPOI(city_id, function (response) {
        data.poi[city_id] = response;
        callback(response);
      });
    }
  },
  getCityData(dateStart = defaultDateStart, dateEnd = defaultDateEnd, city_id, callback){
    let index = dateStart + dateEnd + city_id;
    if (data.data[index]) {
      callback(data.data[index], 'cache');
    } else {
      return JSONP.ott.getCityData(dateStart, dateEnd, city_id, function (response) {
        data.data[index] = response;
        callback(response);
      });
    }
  },

  findFlights(route, count_ad, count_cn, count_in, cs, callback){
    let url = `https://www.onetwotrip.com/_api/searching/startSync2/?ad=${count_ad}&cn=${count_cn}&in=${count_in}${cs}&route=${route}&currency=RUB`;
    JSONP(url, callback);
  },
  /**
   *
   * {string|number} object_id — geoname_id объекта geo, tourism, airport
   * {string} [object_type=geo] – тип обьекта: geo, tourism, airport
   * {string} date_start — дата заезда (в формате YYYY-MM-DD)
   * {string} date_end — дата выезда (в формате YYYY-MM-DD)
   * {number} adults — количество взрослых
   * {Array.<number>} children — количество взрослых
   * {string=} [currency=RUB] — см. lang, locale, currency
   * {string} [lang=ru] — см. lang, locale, currency
   * {string} [locale=ru] - см. lang, locale, currency
   */
  getCityPolling(object_id, date_start, date_end, adults, children, callback, callbackPolling, object_type = 'geo', currency = 'RUB', lang = 'ru', locale = 'ru'){
    let url = 'https://hapi.onetwotrip.com/api/searchRequest?date_start=' + date_start + '&date_end=' + date_end + '&currency=RUB&adults=' + adults + '&children=' + children.length + children + '&object_id=' + object_id + '&object_type=geo&lang=ru&locale=ru';

    return JSONP(url, function (response, id) {

      callback(response, id);

      if (response.error)
        return;

      let result = response.result;

      JSONP.admin.__sendingPolling(result.request_id, callbackPolling, currency, lang, locale)

    });
  },
  __sendingPolling(request_id, callbackPolling, currency = 'RUB', lang = 'ru', locale = 'ru'){

    let url = `https://hapi.onetwotrip.com/api/searchPolling?request_id=${request_id}&currency=${currency}&lang=${lang}&locale=${locale}`;

    JSONP(url, function (response) {

      callbackPolling(response, request_id);

      if (!response.error) {
        let result = response.result;
        if (result.status == 'progress') {
          setTimeout(JSONP.admin.__sendingPolling.bind(JSONP, request_id, callbackPolling, currency, lang, locale), 3000);
        }
      }
    });
  }
};

export default JSONP;
