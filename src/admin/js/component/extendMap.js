"use strict";

import Map from '../../../js/map';

class ExtendMap extends Map {
  constructor(el) {
    super(el);
  }

  update() {
    return this
      .clear()
      .initData().initDirection().initPoint().initPOI().bound();
  }

  getZoom() {
    return this._map.getZoom();
  }

  getCenter() {
    return this._map.getCenter();
  }

  /**
   *
   * @returns {*}
   */
  get$EL() {
    return this.$el;
  }
}

export default ExtendMap;

