"use strict";
/**
 * @type {Object}
 * @property {Object.<string, string>} Cities
 * @property {Object.<string, string>} Airports
 * @property {Object.<string, string>} Countries
 */
import refData from 'ref-data';

/**
 * @typedef {Object} IATAdata
 * @property {string} name
 * @property {string} iata
 */

/**
 *
 * @param name
 * @returns {Array.<IATAdata>}
 */
export function findItemsByName(name) {
  let cityData = findItemIN(name, refData.Cities)
    , airportData = findItemIN(name, refData.Airports)
    , countryData = findItemIN(name, refData.Countries);

  return [].concat(cityData, airportData, countryData);
}
/**
 *
 * @param {string} search
 * @param {Object.<string, string>} data
 * @returns {Array.<IATAdata>}
 */
function findItemIN(search, data) {
  let first = [], second = [], third = [], fourth = [];

  for (let iata in data) {
    if (data.hasOwnProperty(iata)) {
      let name = data[iata];

      if (name.indexOf(search) == 0) {
        first.push(setItem(name, iata));
      } else if (name.toLowerCase().indexOf(search.toLowerCase()) == 0) {
        second.push(setItem(name, iata));
      } else if (name.indexOf(search) > 0) {
        third.push(setItem(name, iata));
      } else if (name.toLowerCase().indexOf(search.toLowerCase()) > 0) {
        fourth.push(setItem(name, iata));
      }
    }
  }

  return [].concat(first, second, third, fourth);
}

function setItem(name, iata) {
  return ({
    name : name,
    iata : iata
  });
}

/**
 *
 * @param {string} iata
 * @returns {?string}
 */
export function findCityByIATA(iata) {
  return refData.Cities[iata] || null;
}

/**
 *
 * @param {string} iata
 * @returns {*|string|null}
 */
export function findByIATA(iata){
  return refData.Cities[iata] || refData.Airports[iata] || refData.Countries[iata] || null;
}