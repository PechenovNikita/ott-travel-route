"use strict";

function toFixed(num, count) {
  let pow = Math.pow(10, count);
  return Math.round(num * pow) / pow;
}

/**
 *
 * @param {string|number} value
 * @return {?number|string}
 */
export function lat(value) {
  // lat +90 to -90 long +180 to -180
  if (!value)
    return null;
  let valueString = '' + value;
  let replaced = valueString.replace(/[^-,.0-9]/g, '').replace(',', '.')
    , val = parseFloat(replaced);
  if (!replaced || !val)
    return null;

  if (val > 90)
    return 90;
  else if (val < -90)
    return 90;
  return toFixed(val, 6);

}
/**
 *
 * @param {string|number} value
 * @return {?string|number}
 */
export function lng(value) {
  // lat +90 to -90 long +180 to -180
  if (!value)
    return null;
  let valueString = '' + value;
  let replaced = valueString.replace(/[^-,.0-9]/g, '').replace(',', '.')
    , val = parseFloat(replaced);
  if (!replaced || !val)
    return null;

  if (val > 180)
    return 180;
  else if (val < -180)
    return 180;
  return toFixed(val, 6);

}