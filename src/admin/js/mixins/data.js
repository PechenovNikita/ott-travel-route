"use strict";
/**
 *
 * @param {?Object} data
 * @param {string[]} params
 * @param {?*=} def
 * @returns {*}
 */
export function getByKey(data, params, def = null) {
  let d = data, i = 0;

  while (d && d.hasOwnProperty(params[i]) && i < params.length) {
    d = d[params[i]];
    i++;
  }

  if (d && d != data)
    return d;
  return def;
}