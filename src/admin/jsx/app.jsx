'use strict';

import React from 'react';
import {render} from 'react-dom';

import JSONP from './../../js/JSONP';
import config from './../js/_config';

import state from '../js/mixins/state';

import Page from './page';
import PagesList from './pagesList';
import Test from './test';

import Modal from './blocks/modal';

class App extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      page      : false,
      update    : 0,
      pageData  : null,
      pages     : [],
      canUpdate : false
    };
  }

  componentDidMount() {
    if (location.hash) {
      this.setTestPage();
    } else {
      JSONP.json(config.path.dataJSON, this.setPages.bind(this));
    }
    // this.setPage('baku.json');
  }

  setTestPage() {
    this.setState((state) => ({
      ...state,
      test : true
    }));
  }

  setPages(response) {
    if (response.data && response.update) {

      this.setState((state) => ({
        ...state,
        pages  : response.data.sort((pageA, pageB) => pageA > pageB ? 1 : (pageA < pageB ? -1 : 0)),
        update : response.update
      }));
    }
  }

  newPage() {
    this.getPageData(
      {
        'title'        : '',
        'top'          : {},
        'about'        : {},
        'route'        : [],
        'advices'      : [],
        'afterAdvices' : {}
      }
    )
  }

  setPage(name) {
    JSONP.json(config.path.data + name, this.getPageData.bind(this));
  }

  getPageData(pageData) {
    this.setState((state) => ({
      ...state,
      page     : true,
      pageData : pageData
    }));
  }

  setList() {
    state(this, {page : false})
  }

  update() {
    JSONP.json(config.path.dataJSON, this.updateData.bind(this));
  }

  updateData(response) {
    if (response.data && response.update) {
      if (response.update !== this.state.update) {
        state(this, {
          pages  : response.data,
          update : response.update
        });
      }
    }
  }

  render() {
    return (
      <div>
        {this.__render()}
        <Modal/>
      </div>
    )
  }

  __render() {
    if (this.state.test)
      return <Test/>;

    if (this.state.page)
      return (
        <div key={'uni_' + Date.now()}>
          <Page data={this.state.pageData} onBack={this.setList.bind(this)}/>
        </div>
      );

    return (
      <PagesList items={this.state.pages}
                 onCreate={this.newPage.bind(this)}
                 onSelect={this.setPage.bind(this)}/>
    );
  }

}

render(<App/>, document.getElementById('app'));
