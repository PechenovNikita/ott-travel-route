'use strict';

import {Component} from 'react';

class FormCheck extends Component {

  constructor(props) {
    super(props);

    this.state = {
      checked : !!props.checked,
      label   : props.label
    }
  }

  __onChange() {
    this.state = {
      checked : !this.state.checked,
      label   : this.state.label
    };
    if (this.props.onChange)
      this.props.onChange();
  }

  componentWillReceiveProps(newProps) {
    this.state = {
      checked : !!newProps.checked,
      label   : newProps.label
    }
  }

  /**
   *
   * @return {boolean}
   */
  isChecked() {
    return this.state.checked;
  }

  render() {
    return (
      <div className="form-check">
        <label className="form-check-label">
          <input type="checkbox" onChange={this.__onChange.bind(this)}
                 checked={this.state.checked}
                 className="form-check-input"/>&nbsp;{this.state.label}
        </label>
      </div>
    );
  }
}

export default FormCheck;