'use strict';

import {Component} from 'react';
import ReactDOM from 'react-dom';

class Message extends Component {

  constructor(props) {
    super(props);

    this.state = {
      type    : props.type ? props.type : 'danger',
      message : props.message || ''
    }
  }

  componentWillReceiveProps(newProps) {
    this.setState({
      type    : newProps.type ? newProps.type : 'danger',
      message : newProps.message || ''
    });
  }

  render() {
    return (
      <div className={`alert alert-${this.state.type}`}>
        <h5>{this.state.type == 'danger' ? 'Error' : 'Message'}!</h5>
        <p className="mb-0">{this.state.message}</p>
      </div>
    );
  }
}

export default Message;