'use strict';

import {Map} from 'immutable';

import React, {Component} from 'react';
import {getWeather} from './weather/api';
import SearchCoordinate from './weather/SearchCoordinate';
import WeatherPreview from './weather/Preview';

class Weather extends Component {

  constructor(props) {
    super();

    const options = props.options || {};

    this.state = {
      data : Map({
        text    : options.text || '',
        lat     : options.lat || null,
        lng     : options.lng || null,
        weather : null
      })
    };
  }

  select = (text, lat, lng) => {
    this.setState(({data}) => ({
      data : data
        .set('text', text)
        .set('lat', lat)
        .set('lng', lng)
    }));
  };

  render() {
    return (
      <div className="">
        <h5>Поиск места по названию</h5>
        <SearchCoordinate text={this.state.data.get('text', '')}
                          onSelect={this.select}/>
        {this.__renderPlace()}
        {this.__renderPreview()}
      </div>
    );
  }

  weather = (event) => {
    event.stopPropagation();
    event.preventDefault();

    getWeather(this.state.data.get('lat'), this.state.data.get('lng'))
      .catch(err => alert(err))
      .then(response => {
        let weather = [];
        let lastDay = 0;

        response.list.forEach((item) => {
          const date = (new Date(item.dt_txt));
          if (lastDay !== date.getDate()) {
            weather.push([
              item
            ]);
          } else {
            weather[weather.length - 1].push(item);
          }
          lastDay = date.getDate();
        });

        weather = weather.map((day) => {
          if (day.length === 8) {
            return [day[3], day[7]]
          } else if (day.length > 2) {
            return [day[0], day[day.length - 1]];
          }
          return day;
        }).filter((item, index) => index < 4);

        this.setState(({data}) => ({data : data.set('weather', weather)}))
      });
  };

  __renderPlace() {
    const {lat, lng, text} = this.state.data.toJS();
    if (lat && lng && text) {
      return <div className="card mb-3">
        <div className="card-body">
          <h4 className="card-title">{text}</h4>
          <p className="card-text">{lat} {lng}</p>
          <a href="javascript:void(0)" onClick={this.weather} className="btn btn-primary">Проверка погоды</a>
        </div>
      </div>
    }
    return null;
  }

  __renderPreview() {
    if (this.state.data.get('weather')) {
      return <WeatherPreview days={this.state.data.get('weather')}/>
    }
    return <div>Тута будет превью если нажать на "Проверка погоды"</div>;
  }

  getValue() {
    const {lat, lng, text} = this.state.data.toJS();
    return {
      lat,
      lng,
      text
    };
  }
}

export default Weather;