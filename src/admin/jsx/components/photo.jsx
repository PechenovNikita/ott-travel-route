'use strict';

import React from 'react';
import InputLocation from './forms/inputLocation';
import TypeInput from './forms/typeInput';

import state from './../../js/mixins/state';

import BtnUpdate from './forms/btnUpdate';

function parseProps(props) {

  let data = props.data || {};

  return {
    tags        : data.tags || [],
    title       : data.title || '',
    description : data.description || '',
    location    : data.location
  }
}

class Photo extends React.Component {

  constructor(props) {
    super(props);

    this.state = parseProps(props);
  }

  componentWillMount() {}

  componentDidMount() {}

  componentWillUnmount() {}

  componentDidUpdate(prevProps, prevState) {}

  componentWillReceiveProps(newProps) {
    state(this, parseProps(newProps));
  }

  update(){

  }

  render() {
    return (
      <div className="admin-photo">
        <div className="admin-photo__settings">
          <TypeInput title="Тэги" items={this.state.tags}/>
          <InputLocation value={this.state.location}/>
        </div>
        <div className="admin-photo__demo">
          <div className="admin-photo__demo__btn">
            <BtnUpdate prop="map" onClick={this.update.bind(this)}/>
          </div>
          <div className="block-photos"
               data-tags={JSON.stringify(this.state.tags)}
               data-location={JSON.stringify(this.state.location)}>
            <div className="block-photos__back">
              <div className="block-photos__back__thumbs block-photo-back"/>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Photo;