'use strict';

import React from 'react';
import state from './../../../js/mixins/state';

// /**
//  *
//  * @type {Object<string,BtnUpdate[]>}
//  */
// let btns = {};

// /**
//  *
//  * @param {BtnUpdate} btn
//  * @param {string} prop
//  */
// function addBtn(btn, prop) {
//   if (btns.hasOwnProperty(prop))
//     btns[prop].push(btn);
//   else
//     btns[prop] = [btn];
// }
//
// function removeBtn(id, prop) {
//   if (btns.hasOwnProperty(prop)) {
//     let i = -1;
//     btns[prop].forEach(function (btn, index) {
//       if (btn.ident == id) {
//         i = index
//       }
//     });
//     if (i > -1)
//       btns[prop].splice(i, 1);
//   }
// }
//
// function updateBtns(prop) {
//   if (btns.hasOwnProperty(prop))
//     btns[prop].forEach(function (btn) {
//       btn.start();
//     });
// }

class BtnUpdate extends React.Component {

  constructor(props) {
    super(props);

    this._dur = 1000;

    this.state = {
      canUpdate : false,
      timer     : this._dur,
      duration  : this._dur,
      start     : Date.now()
    };

    // if (props.prop)
    //   this._prop = props.prop;
    //
    // this.ident = 'BtnUpdate_' + Date.now();
  }

  componentDidMount() {
    // if (this._prop)
    //   addBtn(this, this._prop);
    this.tic();
  }

  componentWillUnmount() {
    // if (this._prop)
    //   removeBtn(this._id, this._prop);
  }

  tic() {
    let timer = (Date.now() - this.state.start);

    if (timer >= this.state.duration) {
      state(this, {
        timer     : 0,
        canUpdate : true
      });
    } else {
      state(this, {
        timer     : this.state.duration - timer,
        canUpdate : false
      });
      setTimeout(this.tic.bind(this), 10);
    }
  }

  click() {

    if (this.state.canUpdate) {
      // if (this._prop)
      //   updateBtns(this._prop);
      // else
        this.start();
      if (this.props.onClick) {
        this.props.onClick();
      }

    }

  }

  start() {
    state(this, {
      canUpdate : false,
      timer     : this._dur,
      duration  : this._dur,
      start     : Date.now()
    });

    this.tic();

  }

  render() {
    let inner = '';
    if (this.state.canUpdate) {
      inner = (<i className="fa fa-refresh" aria-hidden="true"/>);
    } else {

      let timerView = Math.round(this.state.timer / 100) / 10;
      if (timerView * 10 == Math.round(timerView) * 10)
        timerView = timerView + '.0';

      inner = (<span>{timerView}c</span>);
    }
    return (
      <button onClick={this.click.bind(this)}
              className={"btn admin-btn-update" + (this.state.canUpdate ? ' btn-primary' : ' btn-warning')}
              disabled={!this.state.canUpdate}>{inner}</button>
    );
  }
}

export default BtnUpdate;