'use strict';

import React from 'react';
import Input from './input';
import Select from './select';

import state from './../../../js/mixins/state';
import JSONP from './../../../js/component/extendJSONP';

class InputLocation extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      value      : props.value || {},
      city_name  : '',
      items      : [],
      city_index : ''
    };

    this._id = 'inputLocation_' + Date.now();
  }

  componentWillMount() {}

  componentDidMount() {}

  componentWillUnmount() {}

  componentDidUpdate(prevProps, prevState) {}

  componentWillReceiveProps(newProps) {
    state(this, {
      value      : newProps.value || {},
      items      : [],
      city_name  : '',
      city_index : ''
    });
  }

  selectOttCityId(value) {
    let city = this.state.items[value];

    state(this, {
      value      : {
        lat    : city.lat,
        lng    : city.lng,
        radius : this.state.value.radius || 5
      },
      city_name  : city.name,
      city_index : value
    });

  }

  getCityList(name) {
    state(this, {
      city_name : name
    });
    if (name.length > 1)
      this._getDataId = JSONP.ott.getCityId(name, this.ottCities.bind(this));
  }

  ottCities(response, id) {
    if (this._getDataId != id)
      return;
    if (!response.error) {
      state(this, {
        items      : response.result,
        city_index : ''
      });
    }
  }

  changeSome(){
    state(this, {
      value:{
        lat:this.refs.lat.getValue(),
        lng:this.refs.lng.getValue(),
        radius:this.refs.radius.getValue()
      }
    })
  }

  render() {
    let select = 'Нет результатов';

    let searchResult = this.state.items.map(function (item, index) {
      return {
        name  : item.name + ', ' + item.country,
        value : index
      }
    });

    if (searchResult.length > 0) {
      select = (<Select ref="city_id"
                        onSelect={this.selectOttCityId.bind(this)}
                        title="Выдача OneTwoTrip"
                        items={searchResult}
                        value={this.state.city_index}/>);
    }



    return (
      <div className="admin-input-location">
        <div className="row">
          <div className="col col-5">
            <Input ref="lat" title="Latitude (широта)"
                   onChange={this.changeSome.bind(this)}
                   placeholder="широта" value={this.state.value.lat}/>
          </div>
          <div className="col col-5">
            <Input ref="lng" title="Longitude (долгота)"
                   onChange={this.changeSome.bind(this)}
                   placeholder="долгота" value={this.state.value.lng}/>
          </div>
          <div className="col col-2">
            <Input ref="radius" title="Радиус в км" desc="До 32 км" placeholder="Радиус поиска"
                   onChange={this.changeSome.bind(this)}
                   value={this.state.value.radius || 5}/>
          </div>
        </div>
        <div className="row">
          <div className="col col-6">
            <Input onChange={this.getCityList.bind(this)}
                   title="Найти по городу с ott" placeholder="название"
                   value={this.state.city_name}/>
          </div>
          <div className="col col-6">
            {select}
          </div>
        </div>
      </div>
    );
  }
}

export default InputLocation;
