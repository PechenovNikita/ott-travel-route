"use strict";

import React from 'react';
import Input from './input';

class InputGroup extends React.Component {

  constructor(props) {
    super(props);

    this.state = {inputs : props.inputs};
    this._id = 'inputGroup_' + Date.now();

  }

  getValue() {
    let value = {};

    for (let ref in this.refs) {
      if (this.refs.hasOwnProperty(ref)) {
        let input = this.refs[ref];
        if (input.name) {
          value[input.name] = input.getValue();
        }
      }
    }

    return value;
  }

  render() {

    let cl = 'col-lg-' + this.props.col_lg + ' col-md-' + this.props.col_md + ' col-sm-' + this.props.col_sm;
    let items = this.state.inputs.map(function (data, index) {
      return (
        <div key={'group_' + index} className={cl}>
          <Input ref={this._id + '_' + index} value={data.value || ''}
                 name={data.name}
                 placeholder={data.placeholder || ''}
                 title={data.title}
                 desc={data.desc}/>
        </div>
      );
    }, this);

    return (
      <fieldset className="form-group">
        <label>{this.props.title}</label>
        <div>
          <div className="row">
            {items}
          </div>
        </div>
      </fieldset>
    );
  }
}

export default InputGroup;