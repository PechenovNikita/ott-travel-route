'use strict';

import {Component} from 'react';
import state from './../../../js/mixins/state';
import SelectFromOptions from './selectFromOptions';

function itemNameFunc(item) {
  if (item.city)
    return item.city + ', ' + item.name + ', ' + item.country;
  return item.name + ', ' + item.country;
}

function parseProps(props) {

  let data = props.data || {};

  return {
    value        : props.value || '',
    displayValue : props.value || '',
    error        : false,

    items : [],
    focus : false,

    edit   : !props.data,
    update : false,
    data   : data
  };
}

class SelectFrom extends Component {

  constructor(props) {
    super(props);

    this.state = parseProps(props);
    this._id = 'selectFrom_' + Date.now();
  }

  componentDidUpdate() {

    if (this._hideOptions) {
      this._hideOptions = false;
      state(this, {focus : false});
    }

    if (this._needFocus) {
      this._needFocus = false;
      if (this.refs.input)
        this.refs.input.focus();
    }

    if (this._isSelect) {
      this._isSelect = false;
      this.__whenSelect();
    }
  }

  componentWillReceiveProps(newProps) {
    state(this, parseProps(newProps));
  }

  onInput(event) {
    let val = event.target.value;

    state(this, {
      value : val,
      error : false,
      items : (val.length > 1 ? this.state.items : [])
    });

    this.__getItemsFromValue(val);
  }

  __getItemsFromValue(val) {}

  __valueFromSelectData(data) {
    return data.toString();
  }

  get value() {
    return this.state.data;
  }

  onFocus() {
    state(this, {focus : true});
  }

  onBlur() {
    this._hideOptions = true;
    setTimeout(function () {
      state(this, {update : !this.state.update});
    }.bind(this), 500);
  }

  onSelect(data) {

    this.__setData(data);
    this._isSelect = true;
  }

  edit() {
    this._needFocus = true;
    state(this, {edit : true});
  }

  /**
   *
   * @param {Event} event
   */
  onKeyDown(event) {
    if (this.refs.options) {

      switch (event.keyCode) {
        //up 38
        case 38:
          event.stopPropagation();
          event.preventDefault();
          this.refs.options.activeMove(-1);
          break;
        //down 40
        case 40:
          event.stopPropagation();
          event.preventDefault();
          this.refs.options.activeMove(1);
          break;
        // enter
        case 13:
          event.stopPropagation();
          event.preventDefault();
          this.__setValueFromOptions();
          break;
      }

    }
  }

  __setValueFromOptions() {
    if (!this.refs.options)
      return;

    this.__setData(this.refs.options.value)
  }

  __setData(data) {
    if (data) {
      state(this, {
        displayValue : this.__valueFromSelectData(data) || '',
        error        : false,
        items        : [],
        edit         : false,
        data         : data
      });
      this._isSelect = true;
    }
  }

  __whenSelect() {

    if (this.props.onSelect)
      return this.props.onSelect();

  }

  render() {

    // console.log('render', this.state);

    let options = (this.state.items.length > 0 && this.state.focus ? (
      <div className="admin-select-from__input__options">
        <SelectFromOptions ref="options" items={this.state.items} onSelect={this.onSelect.bind(this)}/>
      </div>
    ) : null);

    return (
      <div className="admin-select-from">
        <div className="form-group">
          <label htmlFor={this._id}>{this.props.label || 'Выбор'}</label>

          {(() => {
            if (this.state.edit) {
              return (
                <div className="admin-select-from__input">
                  <input type="text" className="form-control"
                         id={this._id}
                         ref="input"
                         onFocus={this.onFocus.bind(this)}
                         onBlur={this.onBlur.bind(this)}
                         value={this.state.value}
                         onKeyDown={this.onKeyDown.bind(this)}
                         onChange={this.onInput.bind(this)}
                         placeholder={this.props.placeholder || "Начните писать"}/>
                  {options}
                </div>
              );
            } else {
              return (
                <div className="admin-select-from__data">
                  <button className="btn btn-secondary text-left"
                          style={{
                            width     : '100%',
                            textAlign : 'left'
                          }}
                          onClick={this.edit.bind(this)}>

                    <i className="fa fa-pencil"/>
                    <span>{this.state.displayValue}</span>
                  </button>
                </div>
              );
            }
          })()}
          <div>
            <small className="form-text text-muted">{this.props.small || ''}</small>
          </div>
        </div>
      </div>
    );
  }
}

export default SelectFrom;