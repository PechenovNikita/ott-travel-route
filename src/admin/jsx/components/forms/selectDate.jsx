'use strict';

import {Component} from 'react';
import state from './../../../js/mixins/state';
import Pikaday from 'pikaday';

const currentYear = (new Date()).getFullYear()
  , monthOnScreen = ['янв', 'февр', 'марта', 'апр', 'мая', 'июня', 'июля', 'авг', 'сен', 'окт', 'нояб', 'дек']
  , monthInField = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'];

const i18n = {
  previousMonth : 'Предыдущий месяц',
  nextMonth     : 'Следующий месяц',
  months        : ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
  weekdays      : ['Воскресенье', 'Понедельник', 'Ворник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
  weekdaysShort : ['ВС', 'ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ', 'СБ']
};

/**
 *
 * @param {?Date} date
 * @param {string[]} months
 * @returns {?string}
 */
function stringifyMode(date, months) {
  if (!date || !(date instanceof Date))
    return '';

  let day = date.getDate()
    , month = date.getMonth()
    , year = date.getFullYear();

  let tmp = day + "\u00a0" + months[month];
  if (year != currentYear)
    tmp += ', ' + year + '\u00a0г.';

  return tmp;
}

function parseMinMaxDate(minDate, maxDays = 240) {
  let minD = new Date();
  if (minDate && minDate instanceof Date) {
    minD = minDate
  }

  let maxD = new Date((new Date(minD)).setDate(minD.getDate() + parseInt(maxDays, 10)));

  return {
    min : minD,
    max : maxD
  };
}

class SelectData extends Component {

  constructor(props) {
    super(props);

    this.state = {
      date     : props.value,
      disabled : !!props.disabled,
      helper   : false
    };
  }

  componentWillMount() {}

  componentDidMount() {
    let dates = parseMinMaxDate(this.props.minDate, this.props.maxDays);

    this.picker = new Pikaday({
      minDate  : dates.min,
      maxDate  : dates.max,
      firstDay : 1,
      i18n     : i18n,
      onSelect : this.selectDate.bind(this)
    });

    this.refs.pikaday.appendChild(this.picker.el);

    if (this.state.date) {
      this.picker.setDate(this.state.date);
      this.picker.gotoDate(this.state.date);
    } else if (this.state.minDate) {
      this.picker.gotoDate(this.state.minDate);
    }

    this.picker.hide();
  }

  componentWillUnmount() {}

  componentDidUpdate(prevProps, prevState) {
    if (this._isSelect) {
      this._isSelect = false;
      this.__whenSelect();
    }

    if (this._changeMinDate) {
      this._changeMinDate = false;

      let dates = parseMinMaxDate(this.props.minDate, this.props.maxDays);

      this.picker.setMinDate(dates.min);
      this.picker.setMaxDate(dates.max);

      if (this.state.date && this.state.date < this.props.minDate) {
        state(this, {
          date : this.props.minDate
        });
        this.picker.setDate(dates.min);
        this.picker.gotoDate(dates.min);
      } else if (!this.state.date) {
        this.picker.gotoDate(dates.min);
      }
    }
  }

  componentWillReceiveProps(newProps) {
    if (newProps.value !== this.props.value || this.props.disabled !== newProps.disabled) {

      state(this, {
        date     : newProps.value,
        disabled : !!newProps.disabled,
        helper   : false
      });

      if (newProps.value) {
        this.picker.setDate(newProps.value);
        this.picker.gotoDate(newProps.value);
      }

    }

    if (newProps.minDate != this.props.minDate) {
      this._changeMinDate = true;
    }
  }

  onBlur() {
    state(this, {helper : false});
    this.__hidePikaday();
  }

  onClick() {
    state(this, {helper : true});
    this.__showPikaday();
  }

  selectDate(date) {
    this._isSelect = true;

    state(this, {
      error : false,
      date  : date
    });

  }

  __showPikaday() {
    this.picker.show();
  }

  __hidePikaday() {
    this.picker.hide();
  }

  __whenSelect() {
    if (this.props.onSelect)
      return this.props.onSelect();
  }

  get value() {
    return this.state.date;
  }

  render() {
    let dateString = stringifyMode(this.state.date, monthInField);
    let label = (this.props.label ? (
      <label htmlFor={this._id}>{this.props.label}</label>
    ) : null);
    return (
      <div className="admin-select-date">
        <div className="form-group">
          {label}

          <div className="admin-select-date__input">
            <button className="btn btn-secondary text-left"
                    disabled={this.state.disabled}
                    style={{
                      width     : '100%',
                      textAlign : 'left'
                    }}
                    onClick={this.onClick.bind(this)}
                    onBlur={this.onBlur.bind(this)}>

              <i className="fa fa-calendar"/> <span>{dateString}</span>
            </button>

            <div className="admin-select-date__helper" style={{display : (this.state.helper ? 'block' : 'none')}}>
              <div ref="pikaday" className="admin-select-date__input__pikaday"/>
            </div>
          </div>

        </div>
      </div>
    );
  }
}

export default SelectData;