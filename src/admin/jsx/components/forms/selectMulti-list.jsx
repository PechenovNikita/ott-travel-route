'use strict';

import React from 'react';
import state from './../../../js/mixins/state';


class SelectMultiList extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      items : props.items || []
    }
  }

  componentWillMount() {}

  componentDidMount() {}

  componentWillUnmount() {}

  componentDidUpdate(prevProps, prevState) {}

  componentWillReceiveProps(newProps) {
    state(this, {items : newProps.items});
  }

  select(index){
    let selected = this.state.items[index];
    this.props.onSelect(selected, index);
  }

  render() {
    let d = Date.now();
    let items = this.state.items.map(function (item, index) {
      return (<li key={d + "_" + index} onMouseDown={this.select.bind(this, index)}>{this.props.displayFunc(item)}</li>)
    }, this);
    
    return (
      <div className="admin-select-multi-list">
        <ul>{items}</ul>
      </div>
    );
  }
}

export default SelectMultiList;