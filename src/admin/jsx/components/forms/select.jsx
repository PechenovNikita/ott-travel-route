'use strict';

import React from 'react';
import state from './../../../js/mixins/state';

class Select extends React.Component {

  constructor(props) {
    super(props);

    // let value = (props.items.length > props.selected ? props.items[props.selected].value : '');

    this.state = {
      items : props.items || [],
      value : props.value
    };

    this._id = 'select_' + Date.now();
  }

  componentDidUpdate() {
    if (this._needSend && this.props.onSelect) {
      this._needSend = false;
      this.props.onSelect(this.state.value);
    }

  }

  componentWillReceiveProps(newProps) {
    state(this, {
      items : newProps.items,
      value : newProps.value
    });
  }

  onChange(event) {
    this._needSend = true;
    let val = event.target.value;
    state(this, {value : val});
  }

  getValue() {
    return this.state.value
  }

  render() {

    let label = (this.props.title ? <label htmlFor={this._id}>{this.props.title}</label> : '')
      , desc = (this.props.desc ? <small className="form-text text-muted">{this.props.desc}</small> : '');

    let d = Date.now();
    let options = this.state.items.map(function (item, index) {
      let value = item.value;
      return (<option key={this._id + d + index}
                      value={value}>{item.name}</option>)
    }, this);

    let inner = (options.length == 0
      ? (<div>{this.state.value || "нет данных"}</div>)
      : (<select className="form-control"
                 value={this.state.value}
                 onChange={this.onChange.bind(this)}
                 id={this._id}>{options}</select>));
    console.log(this.state.value);
    return (
      <div className="admin-select">
        <div className="form-group">
          {label}
          {inner}
          {desc}
        </div>
      </div>
    );
  }
}

export default Select;