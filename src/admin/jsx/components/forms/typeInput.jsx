'use strict';

import React from 'react';
import TypeInputItem from './typeInput-Item';

import state from '../../../js/mixins/state';

class TypeInput extends React.Component {
  /**
   *
   * @param {Object} props
   */
  constructor(props) {
    super(props);

    this.state = {
      items : props.items || [],
      value : '',
      focus : false
    };

    this._id = 'typed_input_' + Date.now();

  }

  componentDidUpdate() {
    this._focused = null;

    if(this._changed){
      this._changed = false;
      if(this.props.onChange)
        this.props.onChange();
    }
  }

  /**
   *
   * @param {Event} event
   */
  onChange(event) {
    state(this, {value : event.target.value});
  }

  /**
   *
   * @param {Event} event
   */
  onKeyDown(event) {
    // console.log(event.keyCode);
    switch (event.keyCode) {
      // enter
      case 13:
        this.__addWord();
        break;
      // backspace
      case 8:
        this.__lastWord(event);
        break;
    }
  }

  /**
   *
   * @param {Event} event
   * @private
   */
  __lastWord(event) {
    if (this.state.value.length <= 0 && this.state.items.length > 0) {
      let items = this.state.items;
      state(this, {
        value : items.pop(),
        items : items
      });
      event.stopPropagation();
      event.preventDefault();
    }
  }

  /**
   * @return {string[]}
   */
  __addWord() {
    if (!this.state.value)
      return this.state.items;
    let items = this.state.items;
    items.push(this.state.value);

    this._changed = true;

    state(this, {
      items : items,
      value : ''
    });



    return items;
  }

  onFocus() {
    state(this, {focus : true});
  }

  onBlur() {
    state(this, {focus : false});
  }

  changeWaypoint(i, newValue) {

    let st = this.state.items.map(function (item, index) {
      if (index === i)
        return newValue;
      return item;
    });
    this._changed = true;
    state(this, {items : st});
  }

  deleteWaypoint(index) {
    let st = this.state.items;
    st.splice(index, 1);
    this._changed = true;
    state(this, {items : st});
  }

  /**
   * @return {string[]}
   */
  getValue() {
    return this.__addWord();
  }

  addMiddle(event) {
    let after = parseInt(event.target.getAttribute('data-after'), 10);
    let items = this.state.items;

    this._focused = after + 1;

    items.splice(after + 1, 0, "");

    state(this, {items : items});
  }

  render() {

    let label = (this.props.title ? <label htmlFor={this._id}>{this.props.title}</label> : '')
      , desc = (this.props.desc ? <small className="form-text text-muted">{this.props.desc}</small> : '');

    let self = this, focused = this._focused;

    let items = this.state.items.reduce(function (line, str, index, arr) {
      line.push(
        <TypeInputItem onDelete={self.deleteWaypoint.bind(self)}
                       onChange={self.changeWaypoint.bind(self)}
                       index={index}
                       focus={focused == index}
                       key={'typed_item_' + index + Date.now()} value={str}/>
      );

      if (index < arr.length - 1)
        line.push(<div key={'typed_item_middle_' + index + Date.now()}
                       onClick={self.addMiddle.bind(self)}
                       className="admin-typed__items__middle"
                       data-after={index}/>);

      return line;
    }, []);

    return (
      <div className="admin-type-input">
        <div className="form-group">
          {label}
          <div className={"form-control admin-typed" + (this.state.focus ? ' focus' : '')}>
            <div className="admin-typed__items">{items}</div>
            <input type="text" className="admin-typed__input"
                   onKeyDown={this.onKeyDown.bind(this)}
                   onFocus={this.onFocus.bind(this)}
                   onBlur={this.onBlur.bind(this)}
                   onChange={this.onChange.bind(this)}
                   value={this.state.value} id={this._id}/>
          </div>
          {/*<input type="text" className="form-control"*/}
          {/*id={this._id} value={this.state.value}*/}
          {/*onChange={this.change.bind(this)}*/}
          {/*placeholder={this.props.placeholder}/>*/}
          {desc}
        </div>
      </div>
    );
  }
}

export default TypeInput;