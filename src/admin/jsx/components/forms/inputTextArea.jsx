"use strict";

import React from 'react';
import state from '../../../js/mixins/state';

/**
 * @class Text
 * @property {{input:HTMLElement}} refs
 */
class Text extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      value : props.value
    };
    this._id = 'textarea_' + Date.now();
  }

  // componentDidMount(){
  //   this.__height();
  // }

  // componentDidUpdate(){
  //   this.__height();
  // }

  change(event) {
    // this.__height();
    let val = event.target.value;
    if (this.props.onChange)
      this.props.onChange(val);
    state(this, {value : val});
  }

  // __height() {
  //   this.refs.input.style.height = '1px';
  //   this.refs.input.style.height = this.refs.input.scrollHeight + 20 + 'px';
  // }

  getValue() {
    return this.state.value;
  }

  render() {
    let label = (this.props.title ? <label htmlFor={this._id}>{this.props.title}</label> : '')
      , desc = (this.props.desc ? <small className="form-text text-muted">{this.props.desc}</small> : '');
    return (
      <div className="admin-input-textarea">
        <div className="form-group">
          {label}
          <textarea ref="input" placeholder={this.props.placeholder}
                    className="form-control"
                    id={this._id}
                    rows={this.props.rows ? this.props.rows : "10"}
                    onChange={this.change.bind(this)}
                    value={this.state.value}/>
          {desc}
        </div>
      </div>
    )
  }

}

export default Text;