'use strict';

import React from 'react';
import InputTextArea from './inputTextArea';

import state from './../../../js/mixins/state';

class InputTextAreaGroup extends React.Component {

  constructor(props) {
    super(props);

    let items = (typeof props.value === 'string' ? [props.value] : props.value);

    items = (Array.isArray(items) ? items : []);

    this.state = {
      items : items || []
    };

    this._itemsRefs = [];
    this._id = 'inputTextAreaGroup_' + Date.now();
  }

  componentWillReceiveProps(newProps) {
    let items = (typeof newProps.value === 'string' ? [newProps.value] : newProps.value);
    items = (Array.isArray(items) ? items : []);
    state(this, {
      items : items || []
    });
  }

  onUp(index) {
    let arr = this.getValue();
    let el = arr.splice(index, 1);
    arr.splice(index - 1, 0, el[0]);
    state(this, {items : arr});
  }

  onDown(index) {
    let arr = this.getValue();
    let el = arr.splice(index, 1);
    arr.splice(index + 1, 0, el[0]);
    state(this, {items : arr});
  }

  onDelete(index) {
    if (confirm('Уверены что хотите удалить?')) {
      let arr = this.getValue();
      arr.splice(index, 1);
      state(this, {items : arr});
    }
  };

  add() {
    let arr = this.getValue();
    if (arr.length == 0 || arr[arr.length - 1]) {
      arr.push('');
      state(this, {items : arr});
    }
  }

  getValue() {
    return (this._itemsRefs.map(function (item) {
      return this.refs[item].getValue();
    }, this));
  }

  render() {

    this._itemsRefs = [];

    let items = this.state.items.map(function (advice, index, arr) {

      let disabledUp = true, disabledDown = true;

      if (arr.length > 1) {
        if (index > 0)
          disabledUp = false;
        if (index < arr.length - 1)
          disabledDown = false;
      }

      let ref = Date.now() + index;
      this._itemsRefs.push(ref);

      return (
        <div className="admin-input-textarea-group-item" key={Date.now() + index}>
          <div className="row">
            <div className="col-md-9 col-lg-10">
              <InputTextArea ref={ref} value={advice} rows="3"/>
            </div>
            <div className="col-md-3 col-lg-2">
              <div className="row">
                <div className="col col-6">
                  <button className="btn btn-secondary"
                          onClick={this.onUp.bind(this, index)}
                          style={{opacity : (disabledUp ? .2 : 1)}} disabled={disabledUp}>
                    <i className="fa fa-chevron-up"/></button>
                  <button className="btn btn-secondary"
                          onClick={this.onDown.bind(this, index)}
                          style={{opacity : (disabledDown ? .2 : 1)}}
                          disabled={disabledDown}><i className="fa fa-chevron-down"/>
                  </button>
                </div>
                <div className="col col-6">
                  <button className="btn btn-danger btn-sm"
                          onClick={this.onDelete.bind(this, index)}><i className="fa fa-trash-o"/></button>
                </div>
              </div>
            </div>
          </div>

        </div>);
    }, this);

    return (
      <div className="admin-input-textarea-group">
        {items}
        <div>
          <button className="btn btn-secondary"
                  onClick={this.add.bind(this)}><i className="fa fa-plus"/></button>
        </div>
      </div>
    );
  }
}

export default InputTextAreaGroup;
