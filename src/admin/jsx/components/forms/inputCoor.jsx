'use strict';

import React from 'react';
import state from './../../../js/mixins/state';

function coordinateFormat(coordinate, isLat = false) {

  if (isLat && (coordinate > 90))
    return 90;
  if (isLat && (coordinate < -90))
    return -90;

  if (!isLat && (coordinate > 180))
    return 180;
  if (!isLat && (coordinate < -180))
    return -180;

  const regex = /([-]*)([0-9.]*)/mg;
  let m = regex.exec(
    coordinate.toString()
      .replace(/[,]/g, '.')
  ), val = '';

  if (!m)
    return '';

  m.forEach((match, groupIndex) => {
    if (groupIndex == 0)
      return;
    val += match;
  });

  return val;
}

class InputCoordinate extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      lat : props.lat || '',
      lng : props.lng || '',
    };
    this._id = 'inputCoordinate_' + Date.now();
  }

  componentWillReceiveProps(newProps) {
    state(this, {
      lat : newProps.lat || '',
      lng : newProps.lng || ''
    });
  }

  changeLat(event) {
    state(this, {lat : coordinateFormat(event.target.value, true)});
  }

  changeLng(event) {
    state(this, {lng : coordinateFormat(event.target.value, false)});
  }

  getValue() {
    return {
      lat : this.state.lat,
      lng : this.state.lng
    };
  }

  render() {
    return (
      <div className="admin-input-coor">
        <div className="form-group">
          <div className="input-group">
            <div className="input-group-addon">Координаты:</div>
            <input type="text" value={this.state.lat} onChange={this.changeLat.bind(this)} className="form-control"
                   placeholder="latitude"/>
            <div className="input-group-addon delimiter"></div>
            <input type="text" value={this.state.lng} onChange={this.changeLng.bind(this)} className="form-control"
                   placeholder="longitude"/>
          </div>
        </div>
      </div>
    );
  }
}

export default InputCoordinate;