'use strict';

import React from 'react';
import state from './../../../js/mixins/state';

import SelectMultiList from './selectMulti-list';
import SelectMultiItem from './selectMulti-item';

class SelectMulti extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      items    : props.items || [],
      helper   : [],
      selected : props.selected || [],
      focus    : false,
      value    : '',
      all      : false
    };

    this._id = 'selectMulti_' + Date.now();
  }

  // componentWillMount() {}
  //
  // componentDidMount() {}
  //
  // componentWillUnmount() {}
  //
  // componentDidUpdate(prevProps, prevState) {}

  componentWillReceiveProps(newProps) {
    state(this, {
      items    : newProps.items,
      selected : newProps.selected,
      all      : false
    });
  }

  onFocus() {
    state(this, {focus : true});
  }

  onBlur() {
    state(this, {focus : false});
  }

  onKeyDown(event) {
    // console.log(event.keyCode);
    switch (event.keyCode) {
      // enter
      case 13:
        this.__addFirst();
        break;
    }
  }

  __addFirst() {
    let helper = this.state.helper;
    if (helper.length > 0)
      this.select(helper[0]);
  }

  onChange(event) {
    let selected = this.state.selected;
    let val = event.target.value;

    if (val.length < 2) {
      state(this, {
        value  : event.target.value,
        helper : []
      });
      return;
    }
    let helper = this.props.filterFunc(this.state.items, val);

    helper = helper.filter(function (item) {
      return (selected.indexOf(item) < 0);
    });

    state(this, {
      value  : event.target.value,
      helper : helper
    });
  }

  select(data, index) {
    let selected = this.state.selected;
    selected.push(data);
    state(this, {
      value    : '',
      helper   : [],
      selected : selected
    });
  }

  deleteSelectItem(index) {
    let selected = this.state.selected;
    selected.splice(index, 1);

    state(this, {
      selected : selected
    })
  }

  getValue() {
    return this.state.selected;
  }

  toggleList() {
    state(this, {
      all : !this.state.all
    })
  }

  render() {

    let listMenu = <a href="javascript:void(0);"
                      onClick={this.toggleList.bind(this)}>{this.state.all ? 'Скрыть список' : 'Показать весь список'}</a>;

    let label = (this.props.title ? <label htmlFor={this._id}>{this.props.title}</label> : '')
      , desc = (this.props.desc ? <small className="form-text text-muted">{this.props.desc} {listMenu}</small> :
      <small className="form-text text-muted">Всего элементов: {this.state.items.length},
        выбрано: {this.state.selected.length} {listMenu}</small>);

    let items = this.state.selected.map(function (item, index) {
      return <SelectMultiItem key={index} displayFunc={this.props.displayFunc}
                              value={item} index={index}
                              onClick={this.deleteSelectItem.bind(this)}/>
    }, this);

    return (
      <div className="admin-select-multi">
        <div className="form-group">
          {label}
          <div className={"form-control" + (this.state.focus ? ' focus' : '')}>
            <div className="admin-select-multi__items">{items}</div>
            <div className="admin-select-multi__data">

              <input type="text" className="admin-select-multi__input"
                     onKeyDown={this.onKeyDown.bind(this)}

                     onFocus={this.onFocus.bind(this)}
                     onBlur={this.onBlur.bind(this)}
                     onChange={this.onChange.bind(this)}
                     value={this.state.value} id={this._id}/>

              <div className="admin-select-multi__data__helper">
                <SelectMultiList onSelect={this.select.bind(this)}
                                 items={this.state.helper}
                                 displayFunc={this.props.displayFunc}/>
              </div>

            </div>
          </div>
          {desc}
          {(() => {
            if (this.state.all) {
              return (<div className="admin-select-multi__list">
                {this.state.items.map(function (item, index) {
                  return <button onClick={this.select.bind(this, item, index)} key={this._id + '_' + index}
                                 className="btn btn-sm btn-link admin-select-multi__list__item">{this.props.displayFunc(item)},</button>
                }, this)}
              </div>)
            }
            return null;
          })()}


        </div>
      </div>
    );
  }
}

export default SelectMulti;