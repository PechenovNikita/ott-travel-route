'use strict';

import React from 'react';
import state from './../../../js/mixins/state';
import InputCoordinate from './inputCoor';

function parseProps(props) {
  let name = '', coor = {}, location = props.location;

  if (typeof location == 'string') {
    name = location;
  } else {
    location = location || {};
    coor = (location.lat && location.lng ? location : {});
  }

  return {
    name         : name,
    coor         : coor,
    isCoordinate : !!(coor.lat && coor.lng)
  }
}
class InputPoint extends React.Component {

  constructor(props) {
    super(props);

    this.state = parseProps(props);

    this._id = 'inputPoint_' + Date.now();
  }

  onTypeChange(event) {
    // if (this.refs.coordinate.getValue())
    //   this._changed = true;
    state(this, {isCoordinate : (event.target.value === 'coor')});
  }

  onNameChange(event) {
    this._changed = true;

    state(this, {name : event.target.value});
  }

  componentWillReceiveProps(newProps) {
    state(this, parseProps(newProps));
  }

  componentDidMount() {
  }

  componentDidUpdate(prevProps, prevState) {
    if (this._changed) {
      console.log(this.value);
      this._changed = false;
      if (this.props.onChange)
        this.props.onChange();
    }
  }

  get value() {
    return (this.state.isCoordinate ? this.refs.coordinate.getValue() : this.state.name);
  }

  getValue() {
    console.log('Depricated getValue InputPoint');

    return (this.state.isCoordinate ? this.refs.coordinate.getValue() : this.state.name);
  }

  render() {
    return (
      <div className="admin-input-point">

        <div className="form-group">
          <label>{this.props.title}</label>
          <div className="float-xs-right form-inline">
            <label className="form-check-inline">
              <input type="radio" className="form-check-input"
                     name={this._id + '_type'}
                     onChange={this.onTypeChange.bind(this)}
                     value="name" checked={!this.state.isCoordinate}/> По названию</label>


            <label className="form-check-inline">
              <input type="radio" className="form-check-input"
                     name={this._id + '_type'}
                     onChange={this.onTypeChange.bind(this)}
                     value="coor" checked={this.state.isCoordinate}/> По координатам</label>
          </div>
          <div className={"admin-input-point__name" + (this.state.isCoordinate ? ' hide' : '')}>
            <div className="form-group">
              <div className="input-group">
                <div className="input-group-addon">Название:</div>
                <input type="text"
                       value={this.state.name}
                       onChange={this.onNameChange.bind(this)}
                       className="form-control" placeholder="Название точки"/>
              </div>
            </div>
          </div>
          <div className={"admin-input-point__coordinate" + (!this.state.isCoordinate ? ' hide' : '')}>

            <InputCoordinate ref="coordinate" lat={this.state.coor.lat} lng={this.state.coor.lng}/>

          </div>
        </div>


      </div>
    );
  }
}

export default InputPoint;