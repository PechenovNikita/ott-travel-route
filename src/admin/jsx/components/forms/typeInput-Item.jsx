'use strict';

import React from 'react';
import state from './../../../js/mixins/state';

class TypeInputItem extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      value : props.value,
      edit  : props.focus,
      focus : props.focus
    };

    this._needToFocus = props.focus;
    this._oldValue = props.value;

    this._id = 'typeInputItem_' + Date.now();

  }

  onInput(event) {
    state(this, {value : event.target.value});
  }

  onKeyDown(event) {
    switch (event.keyCode) {
      case 13:
        this._needUpdateChange = true;
        state(this, {
          edit  : false,
          focus : false
        });
        break;
    }
  }

  edit() {
    this._needToFocus = true;
    state(this, {edit : true});
  }

  onFocus() {
    state(this, {focus : true});
  }

  onBlur() {
    this._needUpdateChange = true;

    state(this, {
      focus : false,
      edit  : false
    });
  }

  componentDidMount() {
    if (this._needToFocus) {
      this._needToFocus = false;
      this.refs.input.focus();
    }
  }

  componentDidUpdate() {
    if (this.state.value) {

      if (this._needToFocus) {
        this._needToFocus = false;
        this.refs.input.focus();
      }

      if (this._needUpdateChange) {
        this._needUpdateChange = false;
        if (this.props.onChange) {
          this.props.onChange(this.props.index, this.state.value);
        }
      }

    } else if (this.props.onDelete && this._needUpdateChange) {
      this._needUpdateChange = false;

      if (this.props.onDelete)
        this.props.onDelete(this.props.index);
    }

    // if (this._needUpdateChange) {
    //   this._needUpdateChange = false;
    //   if (this.props.onChange) {
    //     let old = this._oldValue;
    //     this._oldValue = this.state.value;
    //     this.props.onChange(old, this.state.value);
    //   }
    // }
  }

  getValue() {
    return this.state.value;
  }

  // <span key={'typed_item_' + index + Date.now()} className="admin-typed__item">{str}</span>
  render() {

    let inner = (this.state.edit ? (
      <input ref="input" className="admin-typed-item__input"
             onChange={this.onInput.bind(this)}
             onFocus={this.onFocus.bind(this)}
             onBlur={this.onBlur.bind(this)}
             onKeyDown={this.onKeyDown.bind(this)}
             value={this.state.value}/>
    ) : (<span className="admin-typed-item__inner">{this.state.value}</span>));

    return (<div className={"admin-typed-item" + (this.state.focus ? ' focus' : '')}
                 onClick={this.edit.bind(this)}>{inner}</div>);
  }
}

export default TypeInputItem;