'use strict';

import React from 'react';
import state from './../../../js/mixins/state';

class SelectMultiItem extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      index : props.index,
      value : props.value
    }
  }

  componentWillReceiveProps(newProps) {
    state(this, {
      value : newProps.value,
      index : newProps.index
    });
  }

  click() {
    if (this.props.onClick)
      this.props.onClick(this.state.index);
  }

  render() {
    return (
      <div className="admin-select-multi-item" onClick={this.click.bind(this)}>{this.props.displayFunc(this.state.value)}</div>
    );
  }
}

export default SelectMultiItem;