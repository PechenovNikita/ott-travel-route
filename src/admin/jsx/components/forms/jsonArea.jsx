'use strict';

import React from 'react';
import JSONEditor from 'jsoneditor';

class JSONArea extends React.Component {

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    let options = {
      onChange : this.onChange.bind(this)
    };
    this._editor = new JSONEditor(this.refs.editor, options);
    this._editor.set(this.props.value);
    this._editor.expandAll();
  }

  onChange() {
    // if (this.props.onChange)
    //   this.props.onChange(this._editor.get());
  }

  refresh() {
    if (this.props.onRefresh)
      this.props.onRefresh(this._editor.get());
  }

  componentWillUnmount() {}

  componentDidUpdate(prevProps, prevState) {}

  componentWillReceiveProps(newProps) {
    console.log('componentWillReceiveProps', newProps);
    this._editor.set(newProps.value);
    this._editor.expandAll();
  }

  getValue() {
    return this._editor.getText();
  }

  render() {
    return (
      <div className="admin-json">
        <div className="form-group">
          <div ref="editor" style={{
            width  : '100%',
            height : 400
          }}/>
        </div>
        <button className="btn btn-primary" onClick={this.refresh.bind(this)}>Обновить</button>
      </div>
    );
  }
}

export default JSONArea;