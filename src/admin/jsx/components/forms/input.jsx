"use strict";

import React from 'react';
import state from '../../../js/mixins/state';

class Input extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      value : (typeof props.value == 'string' ? props.value : '')
    };

    this._id = 'input_' + Date.now();
  }

  get name() {
    return this.props.name
  }

  componentWillReceiveProps(newProps) {
    state(this, {
      value : (typeof newProps.value == 'string' ? newProps.value : '')
    });
  }

  componentDidUpdate() {
    if (this._changed) {
      this._changed = false;
      if (this.props.onChange)
        this.props.onChange();
    }
  }

  getValue() {
    console.log('Depricated getValue in Input');
    return this.state.value;
  }

  get value(){
    return this.state.value;
  }

  change(event) {

    let value = event.target.value;
    this._changed = true;
    state(this, {value : value});

    // if (this.props.onChange)
    //   this.props.onChange(value);
  }

  render() {

    let label = (this.props.title ? <label htmlFor={this._id}>{this.props.title}</label> : '')
      , desc = (this.props.desc ? <small className="form-text text-muted">{this.props.desc}</small> : '');

    return (
      <div className="admin-input-text">
        <div className="form-group">
          {label}
          <input type="text" className="form-control"
                 id={this._id} value={this.state.value}
                 onChange={this.change.bind(this)}
                 placeholder={this.props.placeholder}/>
          {desc}
        </div>
      </div>
    );
  }
}

export default Input;