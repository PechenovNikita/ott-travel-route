'use strict';

import {Component} from 'react';
import state from './../../../../../js/mixins/state';
import textCount from './../../../../../js/mixins/textCount';
import {findByIATA} from './../../../../../js/component/refData';

function timeString(t) {
  return t[0] + '' + t[1] + ':' + t[2] + '' + t[3];
}

function timeStringMad(t) {
  let h = parseInt(t[0], 10), m = parseInt(t[1], 10);
  let dm = m % 60;
  h = h + (m - dm) / 60;
  if (dm == 0)
    return h + textCount(h, 'час', 'часа', 'часов');
  else {
    if (h == 0)
      return dm + textCount(dm, 'минут', 'минуты', 'минут');
    return h + 'ч ' + dm + 'мин';
  }
}

function parseData(data) {
  if (data && data.frs) {

    return (
      <table className="table">
        <thead className="thead-default">
        <tr>
          <th>Индекс</th>
          <th>Стоимость</th>
          <th>Пересадки</th>
          <th>Трипы</th>
          <th>Ожидание</th>
        </tr>
        </thead>
        <tbody>
        {parseFares(data.frs, data)}
        </tbody>
      </table>
    );
  }

  return null;
}
/**
 *
 * @param dir
 * @param trips
 * @returns {{from: {title: (*|string|null), time: *}, to: {title: (*|string|null), time: *}, across: {title: string, time: string}}}
 */
function parseOneDir(dir, trips) {

  let first = trips[dir.trps[0].id]
    , last = trips[dir.trps[dir.trps.length - 1].id];

  let ret = {
    from   : {
      "title" : findByIATA(first.from),
      "time"  : timeString(first.stTm)
    },
    to     : {
      "title" : findByIATA(last.to),
      "time"  : timeString(last.endTm)
    },
    across : {
      title : 'Прямой',
      time  : ''
    }
  };

  if (dir.trps.length > 1) {

    let time = timeStringMad(dir.trps.reduce(function (time, trp, i) {
      if (!trp.stpTm)
        return time;
      return [
        time[0] + parseInt(trp.stpTm[0] + '' + trp.stpTm[1], 10),
        time[1] + parseInt(trp.stpTm[2] + '' + trp.stpTm[3], 10)
      ]
    }, [0, 0]))

    if (dir.trps.length == 2)
      ret.across.title = 'через ' + findByIATA(first.to);
    else {
      let c = dir.trps.length - 1;
      ret.across.title = c + textCount(c, ' переседка', ' пересадки', ' пересадок');
    }

    ret.across.time = time;
  }

  return ret;
}

function parseFares(fares, data) {
  console.log(fares);

  let id = 'list_' + Date.now();

  // prcInf.amt - цена
  // dirs.trps - trips
  return fares.sort(function (a, b) {
    if (a.prcInf.amt > b.prcInf.amt)
      return 1;
    else if (a.prcInf.amt < b.prcInf.amt)
      return -1;
    return 0;
  }).filter(function (x, index) {
    return index < 10;
  }).map(function (fare, index) {

    // let countDirection = fare.dirs.length;

    if (index == 0)
      console.log(fare);

    return (
      <tr key={index}>
        <td>
          <div className="form-check">
            <label className="form-check-label">
              <input value={index}  type="checkbox" className="form-check-input"/> Бизнесс
            </label>
          </div>
        </td>
        <td>{fare.prcInf.amt}</td>
        <td>
          <div className="list-group">
            {
              fare.dirs.map(function (dir, index) {
                let d = parseOneDir(dir, data.trps);
                return <div key={'test_' + index} className="list-group-item">{d.across.title} - {d.across.time}</div>
              })
            }
          </div>
        </td>
        <td>
          <div className="list-group">
            {
              fare.dirs.map(function (dir, index) {
                return (
                  <div className="list-group-item" key={'dirs_' + index}>
                    {
                      dir.trps.map(function (trp, i) {
                        return parseTrips(trp.id, data, index + '_' + i);
                      })
                    }
                  </div>
                )
              })
            }
          </div>
        </td>
      </tr>
    )
  });
}

function parseTrips(id, data, key) {
  let trip = (data.trps[id]);
  return <div key={id + '_trip_' + key}>{findByIATA(trip.from)}({timeString(trip.stTm)})
    - {findByIATA(trip.to)}({timeString(trip.endTm)})</div>
}

class FormFlightList extends Component {

  constructor(props) {
    super(props);

    this.state = {
      data  : props.data,
      items : []
    };
    console.log(props);
  }

  componentWillMount() {
    // console.log(props.data)
  }

  // componentDidMount() {}

  // componentWillUnmount() {}

  // componentDidUpdate(prevProps, prevState) {}

  componentWillReceiveProps(newProps) {
    state(this, {
      data  : newProps.data,
      items : []
    });

    console.log('componentWillReceiveProps', newProps);
  }

  render() {
    console.log(this.state.data);
    return (
      <div className="admin-form-flight-list">
        { parseData(this.state.data)}
      </div>
    );
  }
}

export default FormFlightList;