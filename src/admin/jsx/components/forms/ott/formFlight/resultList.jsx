'use strict';

import {Component} from 'react';
import state from './../../../../../js/mixins/state';
import textCount from './../../../../../js/mixins/textCount';
import {findByIATA} from './../../../../../js/component/refData';
import {transliterate} from './../../../../../js/component/transliterate';
/**
 *
 * @typedef {{id: number, dirs: [*], gdsInf: number, lstTkDt: string, prcInf: {adtB: number, cnnB: number, infB: number, adtT: number, cnnT: number, infT: number, cur: string, mkup: number, amt: number, markupNew: number, discount: number, adtM: number, adtMarkupNew: number, adtDiscount: number, cnnM: number, cnnMarkupNew: number, cnnDiscount: number, infM: number, infMarkupNew: number, infDiscount: number, adtA: number, cnnA: number, infA: number, adtAmountWC: number, cnnAmountWC: number, infAmountWC: number}, pltCrr: string, stAvl: number, infMsgs: [*], pmtVrnts: {tag: string, transactions: [*], passengersCount: {adtCount: number, cnnCount: number, infCount: number}}, is2OW4RT: boolean, gdsType: string, privateAccountCode: string, srvCls: string, isThrough: boolean, oldId: number, agCnt: string, utmMarker: string, sourceTypes: string, rqId: number, dirsCnt: number, fid: number, bonusPercent: number, tripcoins: number, adtTripcoins: number, cnnTripcoins: number, infTripcoins: number, tripcoinsCurrency: string, isBadFare: boolean, score: number, priceScore: number, timeScore: number, sale: boolean, frKey: string}} OttFare
 */

function timeString(t) {
  return t[0] + '' + t[1] + ':' + t[2] + '' + t[3];
}

function timeStringMad(t) {
  let h = parseInt(t[0], 10), m = parseInt(t[1], 10);
  let dm = m % 60;
  h = h + (m - dm) / 60;
  if (dm == 0)
    return h + textCount(h, ' час', ' часа', ' часов');
  else {
    if (h == 0)
      return dm + textCount(dm, ' минута', ' минуты', ' минут');
    return h + ' ч ' + dm + ' мин';
  }
}

function parsePriceString(num) {
  let price = Math.round(num);
  return price.toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ') + ' %rub%';
}

/**
 *
 * @param dir
 * @param trips
 * @returns {{from: {title: (*|string|null), time: *}, to: {title: (*|string|null), time: *}, across: {title: string, time: string}}}
 */
function parseOneDir(dir, trips) {

  let first = trips[dir.trps[0].id]
    , last = trips[dir.trps[dir.trps.length - 1].id];

  let ret = {
    from   : {
      "title" : findByIATA(first.from),
      "time"  : timeString(first.stTm)
    },
    to     : {
      "title" : findByIATA(last.to),
      "time"  : timeString(last.endTm)
    },
    across : {
      title : 'Прямой',
      time  : ''
    }
  };

  if (dir.trps.length > 1) {

    let time = timeStringMad(dir.trps.reduce(function (time, trp, i) {
      if (!trp.stpTm)
        return time;
      return [
        time[0] + parseInt(trp.stpTm[0] + '' + trp.stpTm[1], 10),
        time[1] + parseInt(trp.stpTm[2] + '' + trp.stpTm[3], 10)
      ]
    }, [0, 0]));

    if (dir.trps.length == 2)
      ret.across.title = 'через ' + findByIATA(first.to);
    else {
      let c = dir.trps.length - 1;
      ret.across.title = c + textCount(c, ' переседка', ' пересадки', ' пересадок');
    }

    ret.across.time = time;
  }

  return ret;
}

function parseTrips(id, data, key) {
  let trip = (data.trps[id]);
  return <div key={id + '_trip_' + key}>{findByIATA(trip.from)}({timeString(trip.stTm)})
    - {findByIATA(trip.to)}({timeString(trip.endTm)})</div>
}

function parseProps(props, currentState) {
  /**
   *
   * @type {Array.<OttFare>}
   */
  let fares = (props.data.frs || []).sort(function (a, b) {

    if (a.prcInf.amt > b.prcInf.amt)
      return 1;
    else if (a.prcInf.amt < b.prcInf.amt)
      return -1;
    return 0;
  });

  if (currentState && (fares.length == currentState.fares.length && (fares[0] && currentState.fares && currentState.fares[0]) && fares[0].prcInf.amt == currentState.fares[0].prcInf.amt))
    return currentState;

  return {
    data   : props.data,
    fares  : fares,
    items  : [],
    page   : 0,
    view   : 10,
    select : -1
  }
}

class FlightResultList extends Component {

  constructor(props) {
    super(props);

    this.state = parseProps(props);
  }

  componentWillReceiveProps(newProps) {
    state(this, parseProps(newProps, this.state));
  }

  onSelect(event) {
    state(this, {
      select : event.target.value
    });

    if (this.props.onSelect) {

      let index = event.target.value
        , fare = this.state.data.frs[index];

      let first = parseOneDir(fare.dirs[0], this.state.data.trps);

      let data = {
        type   : 'flight',
        from   : first.from,
        to     : first.to,
        across : first.across,
        price  : {
          title : "",
          btn   : {
            href  : "#",
            title : parsePriceString(fare.prcInf.amt)
          }
        }
      };

      if (fare.dirs.length > 1) {
        let back = parseOneDir(fare.dirs[1], this.state.data.trps);
        data.back = {
          title      : "Рейс обратно",
          items      : [
            back.from,
            back.to
          ],
          across     : back.across,
          close_text : "+рейс туда"
        };

        data.price.title = "+рейс обратно";
      }

      this.props.onSelect(data);
    }
  }

  /**
   *
   * @param {number} start
   * @param {number} end
   * @private
   */
  __nextPage(start, end) {
    if (end < this.state.fares.length)
      state(this, {
        page : this.state.page + 1
      });
  }

  /**
   *
   * @param {number} start
   * @param {number} end
   * @private
   */
  __prevPage(start, end) {
    if (start > 0)
      state(this, {
        page : this.state.page - 1
      });
  }

  __renderFareTripsAccross(fare) {
    return fare.dirs.map(function (dir, index) {
      let d = parseOneDir(dir, this.state.data.trps);
      if (d.across.time)
        return <div key={'test_' + index} className="list-group-item">{d.across.title} - {d.across.time}</div>;
      return <div key={'test_' + index} className="list-group-item">{d.across.title}</div>;
    }, this)
  }

  /**
   *
   * @param {OttFare} fare
   * @return {Array}
   * @private
   */
  __renderFareTrips(fare){
    return fare.dirs.map(function (dir, index) {
      return (
        <div className="list-group-item" key={'dirs_' + index}>
          {
            dir.trps.map(function (trp, i) {
              return parseTrips(trp.id, this.state.data, index + '_' + i);
            }, this)
          }
        </div>
      )
    }, this)
  }

  __renderFares(start, end) {
    let fares = this.state.fares;

    // prcInf.amt - цена
    // dirs.trps - trips
    return fares.filter(function (x, index) {
      return index < end && index >= start;
    }).map(function (fare, index) {

      // let countDirection = fare.dirs.length;
      let i = start + index;

      return (
        <tr key={i}>
          <td>
            <div className="form-check">
              <label className="form-check-label">
                <input name="select" value={i}
                       checked={this.state.select == i}
                       onChange={this.onSelect.bind(this)} type="radio"
                       className="form-check-input"/>
                &nbsp;Выбрать
              </label>
            </div>
          </td>
          <td>{fare.prcInf.amt}</td>
          <td>
            <div className="list-group">
              {this.__renderFareTripsAccross(fare)}
            </div>
          </td>
          <td>
            <div className="list-group">
              {this.__renderFareTrips(fare)}
            </div>
          </td>
        </tr>
      );

    }, this);
  }

  __renderTable() {

    let start = this.state.view * this.state.page
      , end = start + this.state.view;

    if (this.state.fares && this.state.fares.length > 0) {

      return (
        <table className="table">
          <thead className="thead-default">
          <tr>
            <th>Индекс</th>
            <th>Стоимость</th>
            <th>Пересадки</th>
            <th>Трипы</th>
          </tr>
          </thead>
          <tfoot>
          <tr>
            <td colSpan="4">
              <button onClick={this.__prevPage.bind(this, start, end)} disabled={start == 0}
                      className="btn btn-sm btn-secondary"><i
                className="fa fa-arrow-left"/></button>
              &nbsp;
              <span>Показано {this.state.view} из {this.state.fares.length} ({start} - {end})</span>
              &nbsp;
              <button onClick={this.__nextPage.bind(this, start, end)} disabled={end >= this.state.fares.length}
                      className="btn btn-sm btn-secondary"><i
                className="fa fa-arrow-right"/></button>
            </td>
          </tr>
          </tfoot>
          <tbody>
          {this.__renderFares(start, end)}
          </tbody>
        </table>
      );
    }

    return null;
  }

  render() {
    return (
      <div className="admin-form-flight-list">
        {this.__renderTable()}
      </div>
    );
  }
}

export default FlightResultList;