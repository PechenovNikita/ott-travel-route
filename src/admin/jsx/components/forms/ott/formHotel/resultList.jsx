'use strict';

import React, {Component} from 'react';
import state from './../../../../../js/mixins/state';

function parseProps(props, currentState) {

  let offers = (props.offers || []).sort((a, b) => {
    if (a[2] > b[2])
      return 1;
    else if (a[2] < b[2])
      return -1;

    if (a[0] > b[0])
      return 1;
    else if (a[0] < b[0])
      return -1;

    return 0;
  });

  if (currentState && (offers.length === currentState.offers.length
    && (offers[0] && currentState.offers && currentState.offers[0]) && offers[0][0] === currentState.offers[0][0]))
    return currentState;

  return {
    offers : offers,
    hotels : props.hotels || [],

    page        : 0,
    view        : 10,
    select      : -1,
    multiSelect : [],

    filter : '',
    noId   : [],
  }
}

class HotelResultList extends Component {

  constructor(props) {
    super(props);

    this.state = parseProps(props);
    this._items = [];
  }

  componentWillReceiveProps(newProps) {
    state(this, parseProps(newProps, this.state));
  }

  __selectHotel(data, index) {
    state(this, {
      select : index
    });
    if (this.props.onSelect) {
      this.props.onSelect(data);
    }
  }

  __selectMultiHotel(event, data, id) {
    const checked = event.currentTarget.checked;
    const items = this.state.multiSelect.slice();
    if (checked) {
      items.push(id);
      this._items.push(data);
    } else {
      const index = items.indexOf(id);
      const indexInItem = this._items.reduce((i, item, index) => {
        if (i > -1) {
          return i;
        }
        const temp_id = `${item.hotel.id}/${item.price}/${item.price_nd}`;
        if (id === temp_id) {
          return index;
        }
        return i;
      }, -1);
      items.splice(index, 1);
      this._items.splice(indexInItem, 1);
    }
    state(this, {
      multiSelect : items
    });

    if (this.props.onSelect) {
      this.props.onSelect(this._items);
    }
  }

  __renderHotel(item, index) {
    let i = this.state.page * this.state.view + index;
    return (
      <tr key={i}>

        <td>
          <div className="form-check">
            {(() => {
              if (this.props.multi) {
                const id = `${item.hotel.id}/${item.price}/${item.price_nd}`;
                return (
                  <label className="form-check-label">
                    <input onChange={(event) => {
                      this.__selectMultiHotel(event, item, id)
                    }}
                           checked={this.state.multiSelect.indexOf(id) > -1}
                           name="select"
                           value={i} type="checkbox"
                           className="form-check-input"/> Выбрать
                  </label>
                )
              }
              return (
                <label className="form-check-label">
                  <input onChange={this.__selectHotel.bind(this, item, i)}
                         checked={this.state.select === i}
                         name="select"
                         value={i} type="radio"
                         className="form-check-input"/> Выбрать
                </label>
              )
            })()}
          </div>
        </td>

        <td>
          <img src={item.hotel.img_s} height="50" alt=""/>
        </td>

        <td>
          <span>{item.hotel.name}</span>
        </td>

        <td>
          <span>{item.price} / {item.price_nd}</span>
        </td>

      </tr>
    );
  }

  /**
   *
   * @param  id
   * @return {*}
   * @private
   */
  __findHotelById(id) {
    for (let i = 0; i < this.state.hotels.length; i++) {
      if (parseInt(this.state.hotels[i].id, 10) === parseInt(id)) {
        return this.state.hotels[i];
      }
    }
    return null;
  }
  /**
   *
   * @param {string} filter
   * @return {number[]}
   * @private
   */
  __searchNot(filter) {
    if (!filter)
      return [];
    if (!this.state.hotels || this.state.hotels.length === 0)
      return [];

    let f = filter.toLowerCase();

    return this.state.hotels
               .filter(hotel => {
                 // address
                 // name
                 // translated_name
                 return hotel.name.toLowerCase().indexOf(f) < 0;
               })
               .map(hotel => hotel.id);
  }

  /**
   *
   * @param {*} hotels
   * @return {Array}
   * @private
   */
  __renderList(hotels) {


    return hotels.map(this.__renderHotel, this);
  }

  /**
   *
   * @param {number} start
   * @param {number} end
   * @private
   */
  __nextPage(start, end) {
    if (end < this.state.offers.length)
      state(this, {
        page : this.state.page + 1
      });
  }

  /**
   *
   * @param {number} start
   * @param {number} end
   * @private
   */
  __prevPage(start, end) {
    if (start > 0)
      state(this, {
        page : this.state.page - 1
      });
  }

  timestamp;

  filter = (event) => {
    const value = event.currentTarget.value;
    // console.log(value);

    const time = this.timestamp = Date.now();

    setTimeout(() => {
      if (time !== this.timestamp)
        return;

      const isNot = this.__searchNot(value);

      if (time !== this.timestamp)
        return;

      this.setState(() => {
        return {
          ...this.state,
          noId : isNot
        }
      }, () => {
        console.log('update', this.state);
      });

    }, 100);

  };

  render() {

    let start = this.state.view * this.state.page
      , end = start + this.state.view;

    let hotels = this.state.offers
                     .filter(item => {
                       return this.state.noId.indexOf(item[0]) < 0
                     })
                     .slice(start, end)
                     .map((item) => {
                       return {
                         hotel    : this.__findHotelById([item[0]]),
                         price    : item[1],
                         price_nd : item[2]
                       };
                     }, this);

    return (
      <div>
        <div>
          <input type="text" placeholder="Фильтр" className="form-control mb-3" onChange={this.filter}/>
        </div>
        <table className="table">
          <thead className="thead-default">
          <tr>
            <th/>
            <th>Картинка</th>
            <th>Название</th>
            <th>Цена (без налога / с налогом)</th>
          </tr>
          </thead>
          <tfoot>
          <tr>
            <td colSpan="4">
              <button onClick={this.__prevPage.bind(this, start, end)} disabled={start === 0}
                      className="btn btn-sm btn-secondary"><i
                className="fa fa-arrow-left"/></button>
              &nbsp;
              <span>Показано {this.state.view} из {hotels.length} ({start} - {end})</span>
              &nbsp;
              <button onClick={this.__nextPage.bind(this, start, end)} disabled={end >= hotels.length}
                      className="btn btn-sm btn-secondary"><i
                className="fa fa-arrow-right"/></button>
            </td>
          </tr>
          </tfoot>
          <tbody>
          {this.__renderList(hotels)}
          </tbody>
        </table>
      </div>
    );
  }
}

export default HotelResultList;
