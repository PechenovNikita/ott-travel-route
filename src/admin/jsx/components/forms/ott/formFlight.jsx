'use strict';

import {Component} from 'react';
import SelectCity from './selectCity';
import SelectCityFromRefData from './selectCityFromRefData';
import SelectDate from './../selectDate';

import Loader from './../../standart/loader';
import Message from './../../standart/message';
import FormCheck from './../../standart/form-check';

// import FormFlightList from './formFlightList';

import FlightResultList from './formFlight/resultList';

import state from './../../../../js/mixins/state';
import JSONP from './../../../../js/component/extendJSONP';

import CardViewFlight from './../../../components/route/waypoint/cards/view/cardFlight';

import {transliterate} from './../../../../js/component/transliterate';

let testData = {};

class OttFormFlight extends Component {

  constructor(props) {
    super(props);

    let data = props.data || {};

    this.state = {
      dataFrom : data.from,
      dataTo   : data.to,

      dataDate     : data.date,
      dataDateBack : data.dateBack,

      dataAD : data.ad || 1,
      dataCN : data.cn || 0,
      dataIN : data.in || 0,

      dataTypeE : true,
      dataTypeW : false,
      dataTypeB : false,
      dataTypeF : false,
      loading   : false,

      searchResult : {},
      multi        : !!data.dateBack,

      value : props.value
    };

    this._id = 'ottFromFlight_' + Date.now();
  }

  componentWillMount() {}

  componentDidMount() {
    // JSONP.ajax('/test.json', (function(response){
    //   this.__ottResultFlights(JSON.parse(response));
    // }).bind(this))
  }

  componentWillUnmount() {}

  componentDidUpdate(prevProps, prevState) {}

  componentWillReceiveProps(newProps) {}

  selectFrom() {
    state(this, {
      dataFrom : this.refs.from.value,
    });
  }

  selectTo() {
    state(this, {
      dataTo : this.refs.to.value
    });
  }

  selectDate() {
    state(this, {
      dataDate : this.refs.date.value
    });
  }

  selectDateBack() {
    state(this, {
      dataDateBack : this.refs.date_back.value
    });
  }

  setCountAD(event) {
    state(this, {dataAD : parseInt(event.target.value)})
  }

  setCountCN(event) {
    state(this, {dataCN : parseInt(event.target.value)})
  }

  setCountIN(event) {
    state(this, {dataIN : parseInt(event.target.value)})
  }

  changeMulti(event) {
    state(this, {multi : !!event.target.checked})
  }

  findFlights() {

    let day = this.state.dataDate.getDate()
      , month = this.state.dataDate.getMonth() + 1;

    day = day < 10 ? '0' + day : day;
    month = month < 10 ? '0' + month : month;

    let from = this.state.dataFrom.iata
      , to = this.state.dataTo.iata;

    let str = day + '' + month + from + to;

    if (this.state.multi) {

      let backday = this.state.dataDateBack.getDate()
        , backmonth = this.state.dataDateBack.getMonth() + 1;

      backday = backday < 10 ? '0' + backday : backday;
      backmonth = backmonth < 10 ? '0' + backmonth : backmonth;

      str += backday + '' + backmonth /*+ to + from*/;
    }

    let cs_e = this.refs.form_check_E.isChecked()
      , cs_w = this.refs.form_check_W.isChecked()
      , cs_b = this.refs.form_check_B.isChecked()
      , cs_f = this.refs.form_check_F.isChecked();

    let cs = (cs_e ? 'E' : '') + (cs_w ? 'W' : '') + (cs_b ? 'B' : '') + (cs_f ? 'F' : '');
    cs = (cs ? '&cs=' + cs : '');
    let count_ad = this.refs.ad.value || 0
      , count_cn = this.refs.cn.value || 0
      , count_in = this.refs.in.value || 0;

    // https://www.onetwotrip.com/_api/searching/startSync2/?ad=1&cn=0&in=0&cs=E&route=0102MOWLED0502&currency=RUB&source=YOURAPIIDENTIFIER
    let url = 'https://www.onetwotrip.com/_api/searching/startSync2/?ad=' + count_ad + '&cn=' + count_cn + '&in=' + count_in + cs + '&route=' + str + '&currency=RUB';
    // console.log(url);
    state(this, {loading : true});
    JSONP.admin.findFlights(str, count_ad, count_cn, count_in, cs, this.__ottResultFlights.bind(this));

  }

  __ottResultFlights(response, id) {
    state(this, {
      searchResult : response,
      loading      : false
    });
  }

  test() {
    state(this, {loading : true});
    JSONP.ajax('/test.json?ad=2&cn=2&in=0&route=0101MOWBCN&currency=RUB', function (response) {
      // console.log(response);
      state(this, {
        loading      : false,
        searchResult : JSON.parse(response)
      });
    }.bind(this));
  }

  setType() {
    state(this, {
      dataTypeE : this.refs.form_check_E.isChecked(),
      dataTypeW : this.refs.form_check_W.isChecked(),
      dataTypeB : this.refs.form_check_B.isChecked(),
      dataTypeF : this.refs.form_check_F.isChecked()
    });
  }

  __renderType() {
    return (
      <fieldset>
        <div className="row">
          <div className="col col-6">
            <FormCheck ref="form_check_E" onChange={this.setType.bind(this)} checked={this.state.dataTypeE}
                       label="Эконом"/>
            <FormCheck ref="form_check_W" onChange={this.setType.bind(this)} checked={this.state.dataTypeW}
                       label="Премиум"/>


          </div>
          <div className="col col-6">
            <FormCheck ref="form_check_B" onChange={this.setType.bind(this)} checked={this.state.dataTypeB}
                       label="Бизнесс"/>
            <FormCheck ref="form_check_F" onChange={this.setType.bind(this)} checked={this.state.dataTypeF}
                       label="Первый"/>
          </div>
        </div>
      </fieldset>
    );
  }

  __renderAddParams() {
    let canSearch = !!(this.state.dataFrom && this.state.dataTo);
    canSearch = !!(canSearch && this.state.dataDate && (!this.state.multi || this.state.dataDateBack));
    canSearch = (canSearch && (!!this.state.dataIN || !!this.state.dataAD || !!this.state.dataCN));
    canSearch = canSearch && !this.state.loading;

    return (
      <div className="row">
        <div className="col col-2">

          <div className="form-group">
            <label htmlFor={this._id + '_ad'}>Взрослых</label>
            <input ref="ad"
                   value={this.state.dataAD}
                   onChange={this.setCountAD.bind(this)}
                   type="number" id={this._id + '_ad'} className="form-control"/>
          </div>

        </div>
        <div className="col col-2">

          <div className="form-group">
            <label htmlFor={this._id + '_cn'}>Детей</label>
            <input ref="cn"
                   value={this.state.dataCN}
                   onChange={this.setCountCN.bind(this)}
                   type="number" id={this._id + '_cn'} className="form-control"/>
          </div>

        </div>
        <div className="col col-2">

          <div className="form-group">
            <label htmlFor={this._id + '_in'}>Младенцев</label>
            <input ref="in"
                   value={this.state.dataIN}
                   onChange={this.setCountIN.bind(this)}
                   type="number" id={this._id + '_in'} className="form-control"/>
          </div>

        </div>
        <div className="col col-3">
          {this.__renderType()}
        </div>

        <div className="col col-3">
          <div className="form-group">
            <button className={`btn ${canSearch ? 'btn-primary' : 'btn-secondary'} btn-block`} disabled={!canSearch}
                    id={this._id + '_find'}
                    onClick={this.findFlights.bind(this)}>Найти
            </button>
          </div>
        </div>
      </div>
    );

  }

  __renderInputs() {
    let dataFrom = this.state.dataFrom
      , dataTo = this.state.dataTo;

    return (
      <div className="row">
        <div className="col col-3">
          <SelectCityFromRefData ref="from"
                                 data={dataFrom}
                                 value={valueDirectionName(dataFrom)}
                                 label="Место отправления"
                                 small="Страна/город/аэропорт из базы OneTwoTrip"
                                 onSelect={this.selectFrom.bind(this)}/>
        </div>
        <div className="col col-3">
          <SelectCityFromRefData ref="to"
                                 data={dataTo}
                                 value={valueDirectionName(dataTo)}
                                 label="Место прибытия"
                                 small="Страна/город/аэропорт из базы OneTwoTrip"
                                 onSelect={this.selectTo.bind(this)}/>
        </div>
        <div className="col col-3">
          <SelectDate ref="date"
                      label="Дата"
                      onSelect={this.selectDate.bind(this)} value=''/>
        </div>
        <div className="col col-3">

          <div className="form-check">
            <label className="form-check-label" style={{marginBottom : '.5rem'}}>
              <input type="checkbox"
                     ref="multi"
                     checked={this.state.multi}
                     onChange={this.changeMulti.bind(this)} className="form-check-input"/>&nbsp;
              Туда обратно
            </label>
            <SelectDate
              minDate={(this.state.dataDate ? new Date((new Date(this.state.dataDate)).setDate(this.state.dataDate.getDate() + 1)) : null)}
              ref="date_back"
              disabled={!this.state.multi}
              onSelect={this.selectDateBack.bind(this)} value=''/>
          </div>
        </div>
      </div>
    );
  }

  render() {

    return (
      <div className="admin-form-flight">
        {this.__renderInputs()}
        {this.__renderAddParams()}
        <hr/>

        <div className="form-group">
          {(() => {
            if (this.state.loading) {
              return (<Loader/>);
            } else if (this.state.searchResult && this.state.searchResult.errors) {
              return (<Message message={JSON.stringify(this.state.searchResult.errors)}/>)
            } else if (this.state.searchResult && this.state.searchResult.frsCnt == 0) {
              return (<Message type="warning" message="Ничего не найдено"/>)
            }
            return (
              <div>
                <FlightResultList onSelect={this.selectFromList.bind(this)} data={this.state.searchResult}/>
              </div>
            );
          })()}
        </div>

        <div className="form-group">
          <div style={{width : 600}} className="list-group">
            <div className="list-group-item">
              <CardViewFlight data={this.state.value}/>
            </div>
          </div>
        </div>

      </div>
    );
  }

  selectFromList(data) {

    //https://www.onetwotrip.com/ru/aviabilety/moscow-barcelona_MOW-BCN/?p=2,2,0&s=true#0101MOWBCN

    let day = this.state.dataDate.getDate()
      , month = this.state.dataDate.getMonth() + 1;

    day = day < 10 ? '0' + day : day;
    month = month < 10 ? '0' + month : month;

    let from = this.state.dataFrom.iata
      , to = this.state.dataTo.iata;

    let str = day + '' + month + from + to;

    if (this.state.multi) {

      let backday = this.state.dataDateBack.getDate()
        , backmonth = this.state.dataDateBack.getMonth() + 1;

      backday = backday < 10 ? '0' + backday : backday;
      backmonth = backmonth < 10 ? '0' + backmonth : backmonth;

      str += backday + '' + backmonth /*+ to + from*/;
    }

    data.price.btn.href = 'https://www.onetwotrip.com/ru/aviabilety/' + transliterate(this.state.dataFrom.name) + '-' + transliterate(this.state.dataTo.name) + '_' + this.state.dataFrom.iata + '-' + this.state.dataTo.iata + '/?p=' + (this.state.dataAD || 0) + ',' + (this.state.dataCN || 0) + ',' + (this.state.dataIN || 0) + '&s=true#' + str;

    state(this, {
      value : data,
      // searchResult : {}
    });
  }

  getValue() {
    return this.state.value;
  }
}

function valueDirectionName(data) {
  if (!data)
    return '';
  return data.name + ' (' + data.iata + ')';
}

export default OttFormFlight;
