'use strict';

import {Component} from 'react';

class OTTSelectCityOptions extends Component {

  constructor(props) {
    super(props);

    this.state = {
      items  : props.items || [],
      active : 0
    };
  }

  componentDidUpdate(prevProps, prevState) {
    // console.log(prevState, this.state);
    this.__updateScroll();
  }

  __updateScroll() {
    let active = this.state.active;
    let start, end, length = this.state.items.length, endIndex = length - 1;

    if (length < 7 || active < 5) {

      start = 0;
      end = 5;

    } else {

      if (endIndex - active <= 1) {
        start = endIndex - 5;
        end = endIndex;
      } else {
        start = active - 4;
        end = active + 1;
      }

    }

    jQuery(this.refs.container).stop().animate({
      scrollTop : (start * 50)
    }, 150);
  }

  componentWillReceiveProps(newProps) {
    this.setState({
      items  : newProps.items || [],
      active : 0
    });
  }

  activeMove(delta) {
    let index = this.state.active
      , length = this.state.items.length;

    index += delta;
    if (index < 0)
      index = length - 1;
    else if (index > length - 1)
      index = 0;
    this.setState({
      items  : this.state.items,
      active : index
    });

  }

  select(value, index) {

    this.setState({
      items  : this.state.items,
      active : index
    });

    if (this.props.onSelect)
      this.props.onSelect(value);

  }

  get value(){
    return this.state.items[this.state.active].value;
  }

  render() {

    let options = this.state.items.map(function (item, index) {
      let isActive = index == this.state.active;
      return (<a key={index} href="javascript:void(0);" className={'list-group-item ' + (isActive ? 'active' : '')}
                 onClick={this.select.bind(this, item.value, index)}>{item.title}</a>);
    }, this);

    return (
      <div ref="container" className="admin-ott-select-city-options">
        <div className="list-group">{options}</div>
      </div>
    );
  }
}

export default OTTSelectCityOptions;