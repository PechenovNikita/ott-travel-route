'use strict';

import {Component} from 'react';
import SelectFrom from './../selectFrom';

import state from './../../../../js/mixins/state';
import {findItemsByName} from './../../../../js/component/refData';

class OttSelectCityRefData extends SelectFrom {

  constructor(props) {
    super(props);
  }

  __getItemsFromValue(val) {
    let items = [];
    if (val.length > 2)
      items = (findItemsByName(val)).map(function (item) {
        return {
          title : item.name,
          value : item
        };
      });
    state(this, {items : items});
  }

  __valueFromSelectData(data) {
    return data.name + '(' + data.iata + ')';
  }
}

export default OttSelectCityRefData;