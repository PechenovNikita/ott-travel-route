'use strict';

import {Component} from 'react';
import JSONP from './../../../../js/component/extendJSONP';
import state from './../../../../js/mixins/state';
import SelectFrom from './../selectFrom';

function itemNameFunc(item) {
  if (item.city)
    return item.city + ', ' + item.name + ', ' + item.country;
  return item.name + ', ' + item.country;
}

class OTTSelectCity extends SelectFrom {

  constructor(props) {
    super(props);
  }

  __getItemsFromValue(val) {
    if (val.length > 2)
      this.__getData(val);
    else
      this._requestID = null;
  }

  __valueFromSelectData(data) {
    return data.name + ', ' + data.country + '';
  }

  onInput(event) {
    let val = event.target.value;

    state(this, {
      value : val,
      error : false,
      items : (val.length > 1 ? this.state.items : [])
    });

    this._requestID = null;

    if (val.length > 2) {
      this.__getData(val);
    } else {
      state(this, {items : []});
    }
  }

  __getData(val) {

    this._requestID = 'cache';
    this._requestID = JSONP.admin.getCityId(val, this.onOttResponse.bind(this), this.props.limit || 100, this.props.type || 'geo');

  }

  onOttResponse(response, requestID) {

    let reqID = this._requestID;

    if ((!reqID || !requestID) || (reqID !== requestID) || response.error) {
      state(this, {
        items : [],
        error : true
      });
      return;
    }

    // console.log(response.result, reqID, requestID);

    let items = response.result
      .filter(function (item) {
        return item.type == 'geo';
        // console.log('item', item);
      }, this)
      .map(function (item, idnex) {
        return {
          title : item.name + ', ' + item.country,
          value : item
        }
      }, this);

    // console.log(items, reqID, requestID);
    // console.log('Items', items);

    state(this, {
      items : items,
      error : false
    });

  }
}

export default OTTSelectCity;