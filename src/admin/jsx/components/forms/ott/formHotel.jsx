'use strict';

import React, {Component} from 'react';
import state from './../../../../js/mixins/state';
import JSONP from './../../../../js/component/extendJSONP';

import CardViewHotel from './../../../components/route/waypoint/cards/view/cardHotel';

import OttSelectCity from './selectCity';
import SelectDate from './../selectDate';

import Message from './../../standart/message';
import Loader from './../../standart/loader';
import Resultlist from './formHotel/resultList';

function parsePriceString(num) {
  let price = Math.round(num);
  return price.toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ') + ' %rub%';
}

function formatDate(date) {
  if (!date || !(date instanceof Date))
    return null;

  let year = date.getFullYear()
    , month = date.getMonth() + 1
    , day = date.getDate();

  month = (month < 10) ? '0' + month : month;
  day = (day < 10) ? '0' + day : day;

  return year + '-' + month + '-' + day;

}

function hotelError(error) {
  if (typeof error === 'string')
    return error;
  else if (error.code && error.msg && error.type)
    return `Code #${error.code} ('${error.type}') : "${error.msg}"`;
  else if (error.msg && error.type)
    return `Error ('${error.type}') : "${error.msg}"`;
  return 'Unknown error type';
}

class OttFormHotel extends Component {

  constructor(props) {
    super(props);

    this.state = {
      city      : {},
      startDate : null,
      endDate   : null,
      adults    : 1,
      children  : '',

      resultHotels  : [],
      resultOffers  : [],
      statusPolling : false,

      error   : false,
      loading : false,

      data  : {},
      items : [],
    };

    this._id = 'ottFormHotel_' + Date.now();
  }

  selectStartDate() {
    state(this, {
      startDate : this.refs.startDate.value
    });
  }

  selectEndDate() {
    state(this, {
      endDate : this.refs.endDate.value
    });
  }

  selectCity() {
    state(this, {
      city : this.refs.city.value
    });
  }

  changeAdults(event) {
    state(this, {
      adults : event.target.value
    });
  }

  changeChildren(event) {
    let val = event.target.value;
    val = val.replace(/([бБ.])/mg, ',')
             .replace(/[^0-9,\s]/mg, '')
             .replace(/[,]{2,}/mg, ',')
             .replace(/,([0-9])/mg, ', $1');

    state(this, {
      children : val
    });
  }

  search() {
    let object_id = this.state.city.city_id
      , date_start = formatDate(this.state.startDate)
      , date_end = formatDate(this.state.endDate)
      , adults = this.state.adults
      , children = this.state.children.split(', ');

    children = children.filter((child) => {
      return !(!child || child == '');
    }).map(function (child, index) {
      return '&child_age[' + index + ']=' + child;
    });

    state(this, {
      resultHotels : [],
      resultOffers : [],
      error        : false,
      loading      : true
    });

    this._request_id = JSONP.admin.getCityPolling(object_id, date_start, date_end, adults, children,
      this.__ottSearchResult.bind(this),
      this.__ottPollingResult.bind(this)
    );

  }

  /**
   *
   * @param {Object} response
   * @param {?Object} response.error
   * @param {?Object} response.result
   * @param {string} response.result.request_id
   * @param {Array.<Object>} response.result.hotels
   * @param {string} id
   * @private
   */
  __ottSearchResult(response, id) {
    let request_id = this._request_id;

    if (!id || !request_id || id !== request_id || response.error) {

      state(this, {
        error   : response.error,
        loading : false,

        statusPolling : false,
        resultOffers  : [],
        resultHotels  : [],
      });
      return;
    }

    state(this, {
      resultHotels : response.result.hotels
    });

    this._request_id = response.result.request_id;
  }

  /**
   *
   * @param {Object} response
   * @param {?Object} response.error
   * @param {?Object} response.result
   * @param {string} response.result.status
   * @param {Array} response.result.offers
   * @param {string} id
   * @private
   */
  __ottPollingResult(response, id) {

    let request_id = this._request_id;

    if (!id || !request_id || id != request_id || response.error) {

      state(this, {
        error   : true,
        loading : false,

        statusPolling : false,
        resultOffers  : [],
        resultHotels  : [],
      });

      return;
    }

    let result = response.result
      , status = (result.status == 'done');

    if (status) {

      state(this, {
        statusPolling : status,
        loading       : false,
        resultOffers  : result.offers
      });

    } else {

      state(this, {
        statusPolling : status,
        resultOffers  : result.offers
      });

    }

  }

  __selectHotel(data) {

    console.log('this.__selectHotel()', data);

    if (this.props.multi) {
      state(this, {
        statusPolling : true,
        error         : false,
        loading       : false,
        items         : data,
      });
    } else {
      state(this, {
        statusPolling : true,
        error         : false,
        loading       : false,
        data          : data,
      });
    }

  }

  getValue() {
    if (this.props.multi) {
      return this.multiValue;
    }
    return this.value;
  }

  get multiValue() {
    const data = this.state.items;

    return data.map((item) => {
      const hotel = item.hotel || {};
      console.log(item);
      return {
        'type'  : 'hotel',
        'image' : {
          'title'       : hotel.name,
          'description' : hotel.address,
          'src'         : hotel.img_s
        },
        'price' : {
          'title' : [
            '',
            ''
          ],
          'btn'   : {
            'href'  : hotel.url,
            'title' : parsePriceString(item.price_nd)
          }
        }
      }
    })
  }

  get value() {

    let data = this.state.data || {}
      , hotel = data.hotel || {};

    return {
      'type'  : 'hotel',
      'image' : {
        'title'       : hotel.name,
        'description' : hotel.address,
        'src'         : hotel.img_s
      },
      'price' : {
        'title' : [
          '',
          ''
        ],
        'btn'   : {
          'href'  : hotel.url,
          'title' : parsePriceString(data.price_nd)
        }
      }
    }

  }

  __renderHotels() {

    if (!this.state.statusPolling)
      return null;

    return (
      <Resultlist multi={this.props.multi}
                  offers={this.state.resultOffers}
                  hotels={this.state.resultHotels}
                  onSelect={this.__selectHotel.bind(this)}/>
    );

  }

  __renderForm() {
    let searchEnable = (!this.state.loading && this.state.startDate && this.state.endDate && this.state.city && (this.state.adults || this.state.children));
    return (
      <div className="row">
        <div className="col col-4">
          <OttSelectCity label='Город'
                         ref="city"
                         data={this.state.city}
                         value={(this.state.city ? (this.state.city.name || '') : '')}
                         onSelect={this.selectCity.bind(this)}/>
        </div>
        <div className="col col-4">
          <SelectDate
            ref="startDate"
            onSelect={this.selectStartDate.bind(this)}
            label="Дата заселения"/>
        </div>
        <div className="col col-4">
          <SelectDate
            ref="endDate"
            onSelect={this.selectEndDate.bind(this)}
            minDate={(this.state.startDate ? new Date((new Date(this.state.startDate)).setDate(this.state.startDate.getDate() + 1)) : null)}
            maxDays="30"
            label="Дата выезда"/>
        </div>
        <div className="col col-3">
          <div className="form-group">
            <label htmlFor={'adults_' + this._id}>Взрослых</label>
            <input type="number" className="form-control" id={'adults_' + this._id}
                   onChange={this.changeAdults.bind(this)}
                   value={this.state.adults}/>
          </div>
        </div>
        <div className="col col-3">
          <div className="form-group">
            <label htmlFor={'children_' + this._id}>Детей</label>
            <input type="text" className="form-control" id={'children_' + this._id}
                   onChange={this.changeChildren.bind(this)}
                   value={this.state.children}/>
            <small className="form-text text-muted">Возраст всех детей через запятую</small>
          </div>
        </div>
        <div className="col col-3">
          <button className="btn btn-primary" disabled={!searchEnable} onClick={this.search.bind(this)}>Найти</button>
        </div>

      </div>
    );
  }

  render() {
    return (
      <div className="admin-ott-hotel">
        {this.__renderForm()}
        <hr/>
        <div>
          {(() => {
            if (this.state.error) {
              return (<Message type="danger" message={hotelError(this.state.error)}/>);
            }

            if (this.state.loading) {
              let text = (this.state.statusPolling ? (
                <div>
                  <h5>Найдено {this.state.resultOffers.length} предложений</h5>
                </div>
              ) : (
                <div>
                  <h5>Идет поиск</h5>
                  <div>Найдено {this.state.resultOffers.length} предложений</div>
                </div>
              ));
              return (
                <div className="row">
                  <div className="col col-3">
                    <Loader/>
                  </div>
                  <div className="col col-5">
                    {text}
                  </div>
                </div>
              )
            }
          })()}
        </div>
        <div>
          {this.__renderHotels()}
          <div style={{width : 600}}>
            {(() => {
              if (this.props.multi) {
                return (<div>
                  Выбрано {this.state.items.length} отелей
                </div>)
              } else {
                return <CardViewHotel data={this.value}/>;
              }
            })()}
          </div>
        </div>
      </div>
    );
  }
}

export default OttFormHotel;
