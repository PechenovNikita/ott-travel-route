'use strict';

import React from 'react';
// import state from './../../js/mixins/state';
import InputTextArea from './forms/inputTextArea';
// import Input from './forms/input';
import InputGroup from './forms/inputGroup';

import Box from './../blocks/pageBox';
import {PageBoxInner} from './../blocks/pageBox';
import {PageBoxBody} from './../blocks/pageBox';

import MapBox from './map';
import TypeInput from './forms/typeInput';

import {getByKey} from './../../js/mixins/data';

//cities : Array[6]
// hide : boolean
// disable : boolean
// description : "В цену включен международный перелет, проживание в отелях каждый день, поезд, автобус. Вся информация по перемещениям представлена ниже."
// map : Object
// price : Object

class About extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      hide    : getByKey(props.data, ['hide'], false),
      disable : getByKey(props.data, ['disable'], false),

      description : getByKey(props.data, ['description'], ''),
      cities      : getByKey(props.data, ['cities'], []),
      map         : getByKey(props.data, ['map'], {}),
      price       : getByKey(props.data, ['price'], {}),
    };

  }

  getValue() {
    let val = {
      hide    : this.state.hide,
      disable : this.state.disable,

      description : this.refs.description.getValue(),
      price       : this.refs.price.getValue(),
      cities      : this.refs.cities.getValue(),
      map         : this.refs.map.getValue(),
    };
    return val;
  }

  render() {

    return (
      <Box title="Блок с общим описанием">
        <PageBoxBody>
          <div className="row">
            <div className="col-12 col-sm-6">
              {this.__renderDisabled()}
            </div>
            <div className="col-12 col-sm-6">
              {this.__renderHide()}
            </div>
          </div>
          {this.__renderAboutBody()}
        </PageBoxBody>
        {this.__renderMap()}
      </Box>

    );
  }

  __renderMap() {
    const display = this.state.disable ? 'none' : 'block';
    return (
      <div style={{display}}>
        <PageBoxInner>
          <MapBox ref="map" height="400"
                  direction={this.state.map.direction}
                  point={this.state.map.point}
                  poi={this.state.map.poi}/>
        </PageBoxInner>
      </div>
    );
  }

  changeDisable = () => {
    this.setState((state) => ({
      ...state,
      disable : !state.disable
    }));
  };

  __renderDisabled() {
    return (
      <div className="form-check">
        <label className="form-check-label">
          <input type="checkbox"
                 className="form-check-input"
                 checked={this.state.disable}
                 onChange={this.changeDisable}
                 value="on"/> Скрыть этот блок полностью</label>
      </div>
    )
  }

  changeHide = () => {
    this.setState((state) => ({
      ...state,
      hide : !state.hide
    }));
  };

  __renderHide() {
    return (
      <div className="form-check">
        <label className="form-check-label">
          <input type="checkbox"
                 className="form-check-input"
                 checked={this.state.hide}
                 onChange={this.changeHide}
                 value="on"/> Оставить только карту</label>
      </div>
    )
  }

  __renderAboutBody() {
    const display = (this.state.disable || this.state.hide) ? 'none' : 'block';
    let price = [{
      name        : 'cost',
      placeholder : 'Цена',
      value       : getByKey(this.state.price, ['cost'], ''),
      desc        : 'Вместо знака рубля ввести %rub-bold% (для жирного рубля) или %rub% (для обычного)',
    }, {
      name        : 'description',
      placeholder : 'Описание',
      value       : getByKey(this.state.price, ['description'], ''),
      desc        : 'Для новой строки <br/>'
    }];
    return (
      <div className="admin-page-about" style={{display}}>
        <div className="row">
          <div className="col-lg-6 col-md-8">
            <InputTextArea ref="description" value={this.state.description} rows="5" placeholder="Мини-текст"
                           title="Описание"/>
          </div>
          <div className="col-lg-6 col-md-4">
            <InputGroup ref="price" col_lg="6" col_md="12" col_sm="6" title="Цены" inputs={price}/>
          </div>
          <div className="col-lg-12">
            <TypeInput ref="cities" title="Наборные города" items={this.state.cities}/>
          </div>
        </div>
      </div>
    );
  }

}

export default About;
