'use strict';

import React, {Component} from 'react';

import styled from 'styled-components';

const width = 22;

const $Wrapper = styled.div`
  position: relative;
  margin-bottom: 20px;
  //&:after {
  //  content: "";
  //  display: table;
  //  clear: both;
  //}
  display:flex; 
  flex-flow: row wrap;
  align-items: stretch;
`;

const $WeatherCurrent = styled.div`
  flex: 1 1 50%;
  box-sizing: border-box;
  border: 1px solid #D8D8D8;
  box-shadow: 4px 4px 3px rgba(0,0,0,.1);
  padding-bottom: 40px;
  
  @media(min-width: 768px){
    flex: 1 1 ${100 - width * 3}%;
    box-shadow: 4px 2px 3px rgba(0,0,0,.1);
  }
`;
const $WeatherItem = styled.div`
  flex: 1 1 50%;
  box-sizing: border-box;
  border: 1px solid #D8D8D8;
  background: rgba(216, 230, 234, 0.1);
  padding-bottom: 30px;
  
  @media(min-width: 768px){
    flex: 1 1 ${width}%;
  }
`;

const $WeatherLine = styled.table`
  width: 100%;
`;

const $WeatherDate = styled.td`
  font-size: 20px;
  padding: 10px 20px;
  
  @media(min-width: 768px){
    padding: 10px 20px;
  }
  
  @media(min-width: 1024px){
    font-size: 25px;
    padding: 20px 25px 10px;
  }
`;

const $WeatherDescription = styled.td`
  font-size: 16px;
  padding: 10px 20px;
`;

const $WeatherCellBottom = styled.td`
  vertical-align: bottom;
  text-align: ${props => props.textAlign ? props.textAlign : 'left'};
`;

const $WeatherCellMiddle = styled.td`
  vertical-align: middle;
  text-align: ${props => props.textAlign ? props.textAlign : 'left'};
`;

const $WeatherCellTop = styled.td`
  vertical-align: top;
  text-align: ${props => props.textAlign ? props.textAlign : 'left'};
`;

const $WeatherCube = styled.td`
  vertical-align: bottom;  
`;

const $WeatherTempBig = styled.div`
  font-size: 25px;
  
  @media(min-width: 768px){
    font-size: 35px;
  }
  
  @media(min-width: 1024px){
    font-size: 46px;
  }
`;

const $WeatherTempMedium = styled.div`
  font-size: 18px;
  
  @media(min-width: 768px){
    font-size: 25px;
  }
  
  @media(min-width: 1024px){
    font-size: 32px;
  }
`;

const $WeatherCel = styled.span`
  font-size: .8em;
`;

const $WeatherTempSmall = styled.div`
  font-size: 26px;
`;

const $WeatherDescBig = styled.div`
  font-size: 18px;
`;
const $WeatherDescMedium = styled.div`
  font-size: 14px;
`;

const $WeatherDescSmall = styled.div`
  font-size: 10px;
`;

const $WeatherIconBig = styled.img`
  width: 50px;
  height: 50px;
  
  @media(min-width: 768px){
    width: 70px;
    height: 70px;    
  }
  
  @media(min-width: 1024px){
    width: 100px;
    height: 100px;  
  }
`;

const $WeatherIconMedium = styled.img`
  width: 20px;
  height: 20px;
  
  @media(min-width: 768px){
    width: 35px;
    height: 35px;    
  }
  
  @media(min-width: 1024px){
    width: 50px;
    height: 50px;  
  }
`;

const $WeatherIconSmall = styled.img`
  width: 20px;
  height: 20px;
`;

//days
class WeatherPreview extends Component {

  render() {
    return (
      <$Wrapper>
        {this.props.days.map((day, index) => {
          return this.__renderItem(day, index);
        })}
      </$Wrapper>
    );
  }

  __renderItem(day, index) {
    if (day.length > 1) {
      return this.__renderItemDN(day, index);
    }
    return this.__renderItemOne(day, index);
  }

  __renderItemOne(day, index) {
    if (index === 0) {
      return (
        <$WeatherCurrent key={index}>
          {this.__renderInner(day[0], null, index)}
        </$WeatherCurrent>
      );
    }
    return (
      <$WeatherItem key={index}>
        {this.__renderInner(day[0], null, index)}
      </$WeatherItem>
    )
  }

  __renderItemDN(day, index) {
    const d = day[0];
    const n = day[1];
    if (index === 0) {
      return (
        <$WeatherCurrent key={index}>
          {this.__renderInner(d, n, index)}
        </$WeatherCurrent>
      );
    }
    return (
      <$WeatherItem key={index}>
        {this.__renderInner(d, n, index)}
      </$WeatherItem>
    )
  }

  __renderInner(d, n, index) {
    const date = new Date(d.dt_txt);
    return <$WeatherLine>
      <tbody>
      <tr>
        <$WeatherDate colSpan={n ? 2 : 1}>
          <div>{index === 0 ? 'Сегодня' : dateFormat(date)}</div>
        </$WeatherDate>
      </tr>
      <tr>
        <$WeatherCellBottom textAlign="center">
          <$WeatherIconBig src={`/public/images/weather/${d.weather[0].icon}.svg`} alt=""/>
        </$WeatherCellBottom>
        {(() => {
          if (n) {
            return (
              <$WeatherCellBottom>
                <$WeatherIconMedium src={`/public/images/weather/${n.weather[0].icon}.svg`} alt=""/>
              </$WeatherCellBottom>
            );
          }
          return null
        })()}

      </tr>
      <tr>
        <$WeatherCellTop textAlign="center">
          <$WeatherTempBig>{d.main.temp | 0}&deg;
            <$WeatherCel>С</$WeatherCel>
          </$WeatherTempBig>
        </$WeatherCellTop>
        {(() => {
          if (n) {
            return (
              <$WeatherCellTop>
                <$WeatherTempMedium>{n.main.temp | 0}&deg;
                  <$WeatherCel>С</$WeatherCel>
                </$WeatherTempMedium>
              </$WeatherCellTop>
            );
          }
          return null
        })()}

      </tr>
      <tr>
        <$WeatherDescription colSpan={n ? 2 : 1}>
          <div>{d.weather[0].description}</div>
        </$WeatherDescription>
      </tr>
      </tbody>
    </$WeatherLine>
  }

}

const months = ['янв', 'фев', 'март', 'апр', 'май', 'июнь', 'июль', 'авг', 'сен', 'окт', 'ноя', 'дек']

function dateFormat(date) {
  return date.getDate() + ' ' + months[date.getMonth()];
}

export default WeatherPreview;