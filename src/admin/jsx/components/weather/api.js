//http://samples.openweathermap.org/data/2.5/forecast?q=London,us&appid=b1b15e88fa797225412429c1c50c122a1
'use strict';
import jsonp from 'jsonp';

import axios from 'axios';

export function getCoordinates(address) {
  const geocoder = new google.maps.Geocoder;

  return new Promise((resolve, reject) => {
    geocoder.geocode({'address' : address}, function (results, status) {
      if (status === 'OK') {
        resolve(results/*[0].geometry.location*/);
      } else {
        reject(status);
      }
    });
  })
}

const api = '59cc8ee54bc95bc1b1bb6e79a26db2fc';

export function getWeather(lat, lng) {
  const url = `http://api.openweathermap.org/data/2.5/forecast?lat=${lat}&lon=${lng}&lang=ru&units=metric&APPID=${api}`;
  return new Promise((resolve, reject) => {
    jsonp(url, {/*param : 'jsoncallback'*/}, (err, data) => {
      if (err) {
        return reject(err);
      }
      return resolve(data);
    });
  });
}
