'use strict';

import {Map} from 'immutable';
import React, {Component} from 'react';
import {getCoordinates} from './api';

class SearchCoordinate extends Component {

  constructor(props) {
    super();

    this.state = {
      data : Map({
        value    : props.text || '',
        error    : '',
        results  : Map({
          items : [],
          show  : false,
        }),
        selected : props.text && props.lat && props.lng ? {
          text : props.text,
          lat  : props.lat,
          lng  : props.lng
        } : null
      }),
    };
  }

  changeValue = (event) => {
    event.stopPropagation();
    event.preventDefault();
    const value = event.currentTarget.value;
    this.setState(({data}) => ({data : data.set('value', value).set('error', '')}));
  };

  search = (event) => {
    event.stopPropagation();
    event.preventDefault();

    getCoordinates(this.state.data.get('value'))
      .then(results => {
        const items = results.map((item) => ({
          text : item.formatted_address,
          lat  : item.geometry.location.lat(),
          lng  : item.geometry.location.lng(),
        }));

        const selected = items[0];

        this.setState(({data}) => ({
          data : data
            .setIn(['results', 'items'], items)
            .set('selected', selected)
            .set('value', selected.text)
            .setIn(['results', 'show'], false)
        }), () => {
          if (this.props.onSelect) {
            this.props.onSelect(selected.text, selected.lat, selected.lng);
          }
        });

        console.log(items);
      })
      .catch(err => {
        this.setState(({data}) => ({
          data : data
            .set('error', err)
            .setIn(['results', 'items'], [])
        }))
      });
  };

  render() {
    return (
      <div className="">
        <div className="input-group mb-2">
          <input type="text" className="form-control"
                 value={this.state.data.get('value', '')}
                 onChange={this.changeValue}/>
          {this.__renderResult()}
          <div className="input-group-btn">
            <button className="btn btn-outline-primary" onClick={this.search}>Google!</button>
          </div>
        </div>
        <div className="text-danger mb-1">{this.state.data.get('error')}...</div>
        {this.__renderContent()}
      </div>
    );
  }

  toggleDropDown = (event) => {
    event.stopPropagation();
    event.preventDefault();

    this.setState(({data}) => ({data : data.setIn(['results', 'show'], !this.state.data.getIn(['results', 'show'], false))}));
  };

  select = (event) => {

    event.stopPropagation();
    event.preventDefault();

    const itemIndex = event.currentTarget.getAttribute('data-index');

    const items = this.state.data.getIn(['results', 'items'], []);

    this.__onSelect(items[itemIndex]);
  };

  __onSelect(item) {
    this.setState(({data}) => ({
      data : data
        .set('selected', item)
        .set('value', item.text)
        .setIn(['results', 'show'], false)
    }), () => {
      if (this.props.onSelect) {
        this.props.onSelect(item.text, item.lat, item.lng);
      }
    });
  }

  __renderResult() {
    if (this.state.data.getIn(['results', 'items'], []).length > 1) {
      const show = this.state.data.getIn(['results', 'show'], false) ? 'show' : '';
      return (
        <div className={`input-group-btn ${show}`}>
          <button onClick={this.toggleDropDown} type="button" className="btn btn-outline-secondary dropdown-toggle">
            Все результаты
          </button>
          <div className={`dropdown-menu ${show}`} style={{
            right : 0,
            left  : 'auto'
          }}>
            {this.state.data.getIn(['results', 'items'], []).map((item, index) => (
              <a onClick={this.select} className="dropdown-item" data-index={index} key={index}
                 href="javascript:void(0)">{item.text}</a>
            ))}
          </div>
        </div>
      )
    }

    return null;
  }

  __renderContent() {
    return null;
  }
}

export default SearchCoordinate;