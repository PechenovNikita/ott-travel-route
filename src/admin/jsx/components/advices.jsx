'use strict';

import React from 'react';
import InputTextArea from './forms/inputTextArea';
import InputTextAreaGroup from './forms/inputTextAreaGroup';

import Box from './../blocks/pageBox';
import {PageBoxInner} from './../blocks/pageBox';
import {PageBoxBody} from './../blocks/pageBox';
import state from './../../js/mixins/state';

class Advice extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      advices : props.advices || []
    };
    this.items = [];
  }

  componentDidUpdate(prevProps, prevState) {}

  componentWillReceiveProps(newProps) {
    state(this, {advices : newProps.advices})
  }

  // onUp(index) {
  //   let arr = this.getValue();
  //   let el = arr.splice(index, 1);
  //   arr.splice(index - 1, 0, el);
  //   state(this, {advices : arr});
  // }
  //
  // onDown(index) {
  //   let arr = this.getValue();
  //   let el = arr.splice(index, 1);
  //   arr.splice(index + 1, 0, el);
  //   state(this, {advices : arr});
  // }
  //
  // onDelete(index) {
  //   let arr = this.getValue();
  //   arr.splice(index, 1);
  //   state(this, {advices : arr});
  // };
  //
  // add() {
  //   let arr = this.getValue();
  //   if (arr[arr.length - 1]) {
  //     arr.push('');
  //     state(this, {advices : arr});
  //   }
  // }

  getValue() {
    return this.refs.items.getValue();
  }

  render() {


    return (
      <Box title="Блок с советами">
        <PageBoxBody>
          <div className="admin-advices">
            <InputTextAreaGroup ref="items" value={this.state.advices}/>
          </div>
        </PageBoxBody>
      </Box>
    );
  }
}

export default Advice;