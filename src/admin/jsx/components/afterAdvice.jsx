'use strict';

import React from 'react';
import state from './../../js/mixins/state';
import Box from './../blocks/pageBox';
import {PageBoxInner} from './../blocks/pageBox';
import {PageBoxBody} from './../blocks/pageBox';

import Input from './forms/input';
import InputTextAreaGroup from './forms/inputTextAreaGroup';

class AfterAdvice extends React.Component {

  constructor(props) {
    super(props);

    let text = (typeof props.text === "string" ? [props.text] : props.text);

    this.state = {
      title : props.title || '',
      text  : text || []
    }
  }

  componentWillReceiveProps(newProps) {
    let text = (typeof newProps.text === "string" ? [newProps.text] : newProps.text);

    state(this, {
      title : newProps.title || '',
      text  : text || []
    });
  }

  getValue() {
    return {
      title : this.refs.title.getValue(),
      text  : this.refs.text.getValue()
    }
  }

  render() {

    return (
      <Box title="Блок после советов">
        <PageBoxBody>
          <div className="admin-after_advice">
            <Input ref="title" title="Заголовок"/>
            <InputTextAreaGroup ref="text" value={this.state.text}/>
          </div>
        </PageBoxBody>
      </Box>
    );
  }
}

export default AfterAdvice;