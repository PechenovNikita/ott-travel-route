'use strict';
import {fromJS, List, Map} from 'immutable';
import React, {Component} from 'react';
import styled from 'styled-components';
import {$GalleryWrapper, generateImageUrlFromData} from '../CustomPhoto';

const $PreviewGrid = styled.div`
  display: flex;
  flex: 0 0 25%;
  flex-flow: row wrap;
  position: relative;
  
  > button {
    position: absolute;
    top: 5px;
    left: 5px;
  }
`;

const $PreviewItem = styled.div`
  flex: 0 0 ${props => props.del ? 100 / props.del : 25}%;
  height: ${props => props.del ? 200 / (4 / props.del) : 200}px;
  background: transparent url(${props => props.image}) center center no-repeat;
  background-size: cover;
  box-shadow: inset 0 0 0 ${props => props.over ? 4 : 0}px green;
  
  > button {
    margin: 5px;
  }
`;

const $PreviewAdd = styled.div`

  display:inline-block;
  width: 100px;
  height: 100px;
  background: orangered;
  position: relative;
  
  > div.image {
    position: relative;
    width: 100%;
    height: 100%;
    background-position: center center;
    background-repeat: no-repeat;
    background-size: cover;
    z-index: 0;
    box-shadow: inset 0 0 0 ${props => props.over ? 4 : 0}px green;
  }
  
  > div.options {
    position: absolute;
    width: 0;
    height: 0;
    top: 0;
    right: 0;
    z-index: 1;
  }
`;

class Preview extends Component {

  constructor(props) {
    super();

    this.state = {
      data : Map({
        grid : props.grid ? fromJS(props.grid) : List([1, 1, 1, 1]),
        move : Map({
          from : null,
          to   : null
        })
      })
    }
  }

  get $value() {
    return this.state.data.get('grid').toJS();
  }

  __addGrid(index) {
    this.setState(({data}) => ({
      data : data.setIn(['grid', index], List([1, 1, 1, 1]))
    }));
  }

  __removeGrid(index) {
    this.setState(({data}) => ({
      data : data.setIn(['grid', index], 1)
    }));
  }

  render() {

    if (this.props.items.size < 1) {
      return <div>Нет картинок</div>
    }
    this._index = 0;
    let count = this.props.items.size;

    let list = this.state.data.get('grid').map((item, index) => {
      return this.__renderPreviewImage(item, index);
    });

    let display = this.state.data.get('grid').reduce((last, grid) => {
      if (parseFloat(grid))
        return last + 1;
      return last + 4;
    }, 0);

    const adds = (count - display) || 0;

    return (
      <div>
        <$GalleryWrapper>
          {list}
        </$GalleryWrapper>
        {adds > 0 ? <div>+ {adds} фото</div> : ''}
        {this.__renderAddsPhoto(count - adds)}
      </div>
    )
  }

  __renderAddsPhoto(index) {
    if (index <= 0)
      return null;
    return (
      <div>
        {this.props.items.slice(index).map((photo, i) => {
          return <$PreviewAdd key={photo.id}
                              over={this.state.data.getIn(['move', 'to']) === (index + i)}
                              draggable={true}
                              onDragOver={() => {
                                const to = this.state.data.getIn(['move', 'to']);
                                if ((index + i) !== to) {
                                  this.setState(({data}) => ({
                                    data : data.setIn(['move', 'to'], (index + i))
                                  }))
                                }
                              }}
                              onDragStart={() => {
                                const from = this.state.data.getIn(['move', 'from']);
                                if ((index + i) !== from) {
                                  this.setState(({data}) => ({
                                    data : data.setIn(['move', 'from'], (index + i))
                                  }))
                                }
                              }}
                              onDragEnd={this.__drop.bind(this)}>
            <div className="image" style={{backgroundImage : `url(${generateImageUrlFromData(photo)})`}}/>
            <div className="options">
              <button type="button" className="btn btn-sm btn-danger float-right" onClick={() => {
                if (this.props.onRemove) {
                  this.props.onRemove(photo);
                }
              }}><span className="fa fa-trash"/></button>
            </div>
          </$PreviewAdd>;
        }).toArray()}
      </div>
    );
  }

  __renderPreviewImage(grid, i, children = false) {

    if (typeof grid === 'number') {
      const index = this._index;
      this._index = this._index + 1;
      const item = this.props.items.get(index, null); //this.state.data.getIn(['selected', 'items', index]);

      if (!item)
        return null;

      const src = generateImageUrlFromData(item);

      return (
        <$PreviewItem draggable={true}
                      over={this.state.data.getIn(['move', 'to']) === index}
                      onDragOver={() => {
                        const to = this.state.data.getIn(['move', 'to']);
                        if (index !== to) {
                          this.setState(({data}) => ({
                            data : data.setIn(['move', 'to'], index)
                          }))
                        }
                      }}
                      onDragStart={() => {
                        const from = this.state.data.getIn(['move', 'from']);
                        if (index !== from) {
                          this.setState(({data}) => ({
                            data : data.setIn(['move', 'from'], index)
                          }))
                        }
                      }}
                      onDragEnd={this.__drop.bind(this)}
                      del={children ? 2 : 4} image={src} key={src}>
          <button type="button" className="btn btn-sm btn-danger float-right" onClick={() => {
            if (this.props.onRemove) {
              this.props.onRemove(item);
            }
          }}><span className="fa fa-trash"/></button>
          {(() => {
            if (children) {
              return null;
            }
            return (
              <button type="button" className="btn btn-sm btn-primary" onClick={() => {
                this.__addGrid(i)
              }}>1/8
              </button>
            )
          })()}
        </$PreviewItem>
      );
    }

    return (
      <$PreviewGrid key={Date.now() + '_' + this._index}>
        <button type="button" className="btn btn-sm btn-primary" onClick={() => {
          this.__removeGrid(i)
        }}>1/4
        </button>
        {grid.map((it, index) => {
          return this.__renderPreviewImage(it, index, true)
        }).toArray()}
      </$PreviewGrid>
    );
  }

  __drop() {
    const from = this.state.data.getIn(['move', 'from']);
    const to = this.state.data.getIn(['move', 'to']);

    this.setState(({data}) => ({
      data : data.set('move', Map({
        from : null,
        to   : null
      }))
    }));

    if (from !== to && this.props.onToggle) {
      this.props.onToggle(from, to);
    }

  }
}

export default Preview;
