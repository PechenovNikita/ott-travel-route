'use strict';

import jsonp from 'jsonp';

const url = 'https://api.flickr.com/services/rest/';

const format = 'json'
  , api_key = '2c7d52d0d378d2df47924bbeb0cb2ad8';

const methods = {
  search : 'flickr.photos.search'
};

function serialize(obj) {
  const str = [];
  for (let p in obj)
    if (obj.hasOwnProperty(p)) {
      str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]));
    }
  return str.join('&');
}

export function search(params, user_id = null) {
  const data = {
    ...params,
    method   : methods.search,
    api_key,
    format,
    per_page : 500,
  };

  if (user_id) {
    data.user_id = user_id;
  }

  return new Promise((resolve, reject) => {
    jsonp(`${url}?${serialize(data)}`, {param : 'jsoncallback'}, (err, data) => {
      if (err)
        reject(err);
      resolve(data);
    });
  });
}

export function searchText(text, user_id = null) {
  return search({text}, user_id);
}

