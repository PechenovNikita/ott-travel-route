'use strict';

import {Map, List} from 'immutable';
import React, {Component} from 'react';
import {searchText} from './api';

class Search extends Component {

  constructor(props) {
    super();

    this.state = {
      data : Map({
        text   : Map({
          input : '',
        }),
        tags   : Map({
          input : '',
        }),
        userId : Map({
          id : '151640904@N03',
          on : true
        })
      })
    };

    this.onChange = this.onChange.bind(this);
    this.search = this.search.bind(this);
  }

  render() {
    return (
      <div>
        <h4>Поиск</h4>
        <div className="small">Если поле текст пустое и выбран поиск по пользователю - то покажет все фото пользователя</div>
        {this.__renderSearchForm()}
      </div>
    );
  }

  onChange(event) {
    const value = event.currentTarget.value;
    this.setState(({data}) => ({data : data.setIn(['text', 'input'], value)}));
  }

  __renderSearchForm() {
    return (
      <div className="btn-toolbar mb-2">
        <div className="mr-2">
          <div>Текст для поиска</div>
          <div className="input-group">
            <input type="text" className="form-control mb-2 mb-sm-0"
                   value={this.state.data.getIn(['text', 'input'], '')}
                   onChange={this.onChange}/>
          </div>
        </div>
        <div className="mr-2">
          <div>Пользователь</div>
          <div className="input-group">
            <label className="input-group-addon" id="btnGroupAddon">
              <input type="checkbox"
                     checked={this.state.data.getIn(['userId', 'on'])}
                     onChange={() => {
                       this.setState(({data}) => ({data : data.updateIn(['userId', 'on'], on => !on)}))
                     }}
                     aria-label="Checkbox for following text input"/>
            </label>
            <input className="form-control" value={this.state.data.getIn(['userId', 'id'])}
                   disabled={!this.state.data.getIn(['userId', 'on'])}
                   onChange={(event) => {
                     const value = event.currentTarget.value;

                     this.setState(({data}) => ({
                       data : data.setIn(['userId', 'id'], value)
                     }));
                   }}/>
          </div>
        </div>
        <button type="button" className="btn btn-primary"
                onClick={this.search}>Search
        </button>
      </div>
    );
  }

  search(event) {
    event.stopPropagation();
    event.preventDefault();

    const id = Date.now();

    this.loading(id);

    const user_id = this.state.data.getIn(['userId', 'on']) ? this.state.data.getIn(['userId', 'id']) : null;

    searchText(this.state.data.getIn(['text', 'input']), user_id)
      .then(response => {
        if (response.stat === 'ok') {
          console.log(response.photos.photo);
          this.init(id, response.photos.photo);
        }
      })
      .catch(err => {
        console.log(err);
      })
  }

  loading(id) {
    if (this.props.onLoading) {
      this.props.onLoading(id);
    }
  }

  init(id, items) {
    if (this.props.onSearch) {
      this.props.onSearch(id, items);
    }
  }
}

export default Search;