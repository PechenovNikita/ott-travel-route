'use strict';

import {List, Map} from 'immutable';
import React, {Component} from 'react';
import {$GalleryWrapper, $GalleryItem, generateImageUrlFromData} from '../CustomPhoto';
import styled from 'styled-components';

class Grid extends Component {

  constructor(props) {
    super();

    this.state = {
      data : Map({
        current : 0,
        perPage : 40,
      })
    };

    this.toggle = this.toggle.bind(this);
  }

  toggle(item, isSelected) {
    if (isSelected) {
      this.props.onRemove ? this.props.onRemove(item) : null;
    } else {
      this.props.onAdd ? this.props.onAdd(item) : null;
    }
  }

  render() {
    const {current, perPage} = this.state.data.toJS();
    if (this.props.loading) {
      return <div>Loading...</div>
    }
    return (

      <div>
        <$GalleryWrapper>
          {this.props.items
               .slice(current, current + perPage)
               .map((item, index) => {
                 const id = item.id;
                 const selected = this.props.selectedIds.indexOf(id) > -1;
                 return (
                   <$GalleryItem style={{backgroundImage : `url(${generateImageUrlFromData(item)})`}}
                                 selected={selected}
                                 onClick={() => {
                                   this.toggle(item, selected);
                                 }}
                                 key={current + '_' + id}/>
                 );
               })}
        </$GalleryWrapper>
        {this.__renderPag(current, perPage)}
      </div>
    );
  }

  __renderPag(current, perPage) {
    const itemSize = this.props.items.size;

    if (itemSize > perPage) {
      return <div className="btn-toolbar">
        <div className="btn-group mr-2" role="group">
          <button disabled={current <= 0}
                  onClick={() => {
                    this.changePosition(-perPage);
                  }} type="button"
                  className="btn btn-outline-secondary">Prev
          </button>
          <button disabled={current + perPage >= itemSize}
                  onClick={() => {
                    this.changePosition(perPage);
                  }}
                  type="button"
                  className="btn btn-outline-secondary">Next
          </button>
        </div>
        <div className="input-group">
          <span className="input-group-addon">{current}/{itemSize}</span>
        </div>
      </div>
    }
    return null;
  }

  changePosition(delta) {
    this.setState(({data}) => ({
      data : data.update('current', current => current + delta)
    }));
  }
}

export default Grid;