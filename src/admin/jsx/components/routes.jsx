'use strict';

import React from 'react';
import state from './../../js/mixins/state';
import Box from './../blocks/pageBox';
import {PageBoxInner} from './../blocks/pageBox';
import {PageBoxBody} from './../blocks/pageBox';
import Waypoint from './route/waypoint';

class Routes extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      routes : props.value || []
    };

    this._refs = [];
    this._id = 'routes_' + Date.now();
  }

  componentWillReceiveProps(newProps) {
    state(this, {
      routes : newProps.value || []
    })
  }

  sort(index, delta) {
    let arr = this.getValue()
      , item = arr.splice(index, 1);
    arr.splice(index + delta, 0, item[0]);
    state(this, {routes : arr});
  }

  remove(index) {
    console.log('removeItem');
    let arr = this.getValue();
    arr.splice(index, 1);
    state(this, {routes : arr});
  }

  addItem() {
    console.log('createItem');
    let arr = this.getValue();
    arr.push({focus : true});
    state(this, {routes : arr})
  }

  getValue() {
    return this._refs.map(function (ref) {
      return this.refs[ref].getValue();
    }, this);
  }

  render() {

    this._refs = [];

    let routes = this.state.routes.map(function (route, index, arr) {
      let ref = Date.now() + '_' + index;
      this._refs.push(ref);

      let disableUp = index < 1, disableDown = index >= arr.length - 1;
      return (
        <div key={Date.now() + '_' + index} className="admin-routes__item">

          <div className="admin-routes__edit">


            <div className="btn-group btn-group-sm">
              <button className={"btn" + (!disableUp ? " btn-info" : " btn-secondary")}
                      onClick={disableUp ? ()=> {} : this.sort.bind(this, index, -1)}
                      disabled={disableUp}><i className="fa fa-chevron-up"/></button>
              <button className={"btn" + (!disableDown ? " btn-info" : " btn-secondary")}
                      onClick={disableDown ? ()=> {} : this.sort.bind(this, index, 1)}
                      disabled={disableDown}><i className="fa fa-chevron-down"/></button>
              <button className="btn btn-danger"
                      onClick={this.remove.bind(this, index)}><i className="fa fa-trash-o"/></button>
            </div>


          </div>

          <Waypoint ref={ref} data={route}/>

        </div>
      )
    }, this);

    let inner = (
      <div className="admin-route">
        {routes}
        <div className="admin-box__pad-min">
          <button className="btn btn-secondary"
                  onClick={this.addItem.bind(this)}><i className="fa fa-plus"/> Добавить
          </button>
        </div>
      </div>
    );

    return (
      <Box title="Блок маршрута">
        <PageBoxInner>
          {inner}
        </PageBoxInner>
      </Box>
    );
  }
}

export default Routes;