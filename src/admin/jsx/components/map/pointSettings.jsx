'use strict';

import React from 'react';
import state from './../../../js/mixins/state';

import InputPoint from './../forms/inputPoint';
import PoiSettings from './poiSettings';
import Input from '../forms/input';
import SelectMulti from '../forms/selectMulti';

//"point": {
// "zoom": 10,
//   "location": "Нью Йорк",
//   "poi": {
//   "items": [
//     "hotels",
//     "museums"
//   ],
//     "name": "Нью Йорк"
// }
// }

function parseProps(props) {
  let data = props.data || {};

  let location = (Array.isArray(data.location) ? data.location : [data.location]);

  return {
    location : location || [],
    zoom     : data.zoom || 7
  };
}

class PointSettings extends React.Component {

  constructor(props) {
    super(props);

    this.state = parseProps(props);

    this._id = 'pointSettings_' + Date.now();

  }

  componentWillReceiveProps(newProps) {
    state(this, parseProps(newProps));
  }

  componentDidUpdate() {
    if (this._changed) {
      this._changed = false;
      if (this.props.onChange)
        this.props.onChange();
    }
  }

  getValue() {
    console.log('depricated getValue() use getter value');
    return this.value;
  }

  get value() {

    let val = {
      location : this.pointsValue,
      zoom     : this.refs.zoom.value || 7
    };

    return val;
  }

  get pointsValue() {
    return this._refs.map((ref) => {
      return this.refs[ref].value;
    }, this);
  }

  getFromMap() {
    // if (this.props.fromMap) {
    //   let mapData = this.props.fromMap();
    //   if (mapData)
    //     state(this, mapData);
    // }
  }

  // poi(event) {
  //   state(this, {
  //     hasPoi : event.target.checked
  //   });
  // }

  // togglePoi() {
  //   state(this, {
  //     hasPoi : !this.state.hasPoi
  //   });
  // }

  whenPointChange() {
    this._changed = true;
    state(this, {
      location : this.pointsValue
    });
  }

  whenZoomChange() {
    this._changed = true;
    state(this, {
      zoom : this.refs.zoom.value || 7
    });
  }

  // addPoint(){
  //   let st = this.getValue();
  // }

  __renderPoints() {
    this._refs = [];
    return this.state.location.map((location, index, all) => {
      let ref = this._id + 'point' + index;
      this._refs.push(ref);
      return (
        <div key={this._id + '_' + index}>
          <div className="row">
            <div className="col col-11">
              <InputPoint ref={ref}
                          onChange={this.whenPointChange.bind(this)}
                          title={`Точка №${index + 1}`} location={location}/>
            </div>
            {(() => {
              if (all.length > 1)
                return (
                  <div className="col col-1">
                    <button onClick={this.removePoint.bind(this, index)} className="btn btn-sm btn-danger"><i
                      className="fa fa-trash-o"/></button>
                  </div>);
              return null;
            })()}
          </div>
        </div>
      );
    }, this)
  }

  addPoint() {
    let st = this.value;
    st.location.push('');
    state(this, st);
    this._changed = true;
  }

  removePoint(index) {
    let st = this.value;
    st.location.splice(index, 1);
    state(this, st);
    this._changed = true;
  }

  render() {

    return (
      <div className="admin-map-point">
        <div className="row">

          <div className="col-lg-8">
            {this.__renderPoints()}
            <div className="form-group">
              <button onClick={this.addPoint.bind(this)} className="btn btn-secondary btn-sm"><i
                className="fa fa-plus"/><span>Добавить точку</span></button>
            </div>
          </div>

          <div className="col col-6 col-lg-1 col-xl-2 hide">
            <Input ref="zoom" title="Масштаб"
                   onChange={this.whenZoomChange.bind(this)}
                   placeholder="масштаб" value={this.state.zoom.toString()}/>
          </div>

          <div className="col col-6 col-lg-3 col-xl-2 hide">

            <div className="btn-group-vertical" data-toggle="buttons">

              <button className="btn btn-secondary btn-sm" onClick={this.getFromMap.bind(this)}><i
                className="fa fa-map-marker"/> обновить с карты
              </button>

              {/*<label className={"btn" + (this.state.hasPoi ? ' btn-primary' : ' btn-outline-secondary')}>*/}
              {/*<input onChange={this.togglePoi.bind(this)} type="checkbox" checked={this.state.hasPoi}*/}
              {/*autoComplete="off"/> POI*/}
              {/*</label>*/}

            </div>

          </div>
        </div>
        {/*{poiSet}*/}
      </div>
    );
  }
}

export default  PointSettings;
