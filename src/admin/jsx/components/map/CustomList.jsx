'use strict';

import {Component} from 'react';
import state from './../../../js/mixins/state';
import Updater from './../../../../js/updater';

class CustomList extends Component {

  constructor(props) {
    super(props);

    this.state = {
      items  : props.value,
      google : false
    };

    this._id = 'CustomList_' + Date.now();
  }

  componentWillMount() {}

  componentWillUnmount() {}

  componentDidUpdate(prevProps, prevState) {}

  componentWillReceiveProps(newProps) {
    state(this, {items : newProps.value});
  }

  removeItem(index) {
    // if (confirm('Точно удалить место?')) {
      let items = this.value;
      items.splice(index, 1);
      state(this, {items : items});
    // }
  }
  addItem(){
    let items = this.value;
    items.push({});
    state(this, {items : items});
  }

  __renderButtons(index) {
    return (<button onClick={this.removeItem.bind(this, index)} className="btn btn-sm btn-danger"><i
      className="fa fa-trash-o"/></button>)
  }

  __renderItems() {
    this._refs = [];
    /**
     * @this {CustomList}
     */
    return this.state.items.map(function (item, index) {
      let ref = this._id + '_' + index;
      this._refs.push(ref);

      return (
        <div key={this._id + '_' + index}>
          <div className="row">
            <div className="col col-1">
              {this.__renderButtons(index)}
            </div>
            <div className="col col-11">
              <CustomListItem ref={ref} data={item}/>
            </div>
          </div>
        </div>
      );
    }, this);
  }

  get value() {
    return this._refs.map(function (ref, index) {
      return this.refs[ref].value;
    }, this)
  }

  render() {
    return (
      <div className="admin-custom-geo_list">
        {this.__renderItems()}
        <div>
          <button className="btn btn-secondary btn-sm" onClick={this.addItem.bind(this)}><i className="fa fa-plus"/><span>Добавить</span></button>
        </div>
      </div>
    );
  }
}

function coordination(coor) {
  coor = coor.replace(',', '.');
  coor = coor.replace(/[^0-9.-]/g, '');
  return coor;
}
/**
 *
 * @param props
 * @return {{name: string, coordinates: {}, label: (string|number), loading: boolean, trying: number, error: string}}
 */
function parsePropsItem(props) {
  /**
   * @type {Object}
   * @property {string|{lat:string|number,lng:string:number}} location
   * @property {{lat:string|number,lng:string:number}} coordinates
   * @property {string|number} label
   */
  let data = props.data || {};

  let title = (typeof data.location == 'string' ? data.location : '');
  let coordinates = {};

  if (data.coordinates && data.coordinates.lat && data.coordinates.lng)
    coordinates = data.coordinates;

  return {
    name        : title,
    coordinates : coordinates,
    label       : data.label,
    loading     : false,
    trying      : 0,
    error       : ''
  }
}

class CustomListItem extends Component {
  constructor(props) {
    super(props);
    this.state = parsePropsItem(props);
    this.state.google = false;
  }

  componentDidMount() {
    Updater.googleMapInit((function () {
      state(this, {google : true});
    }).bind(this));
  }

  componentWillReceiveProps(newProps) {
    state(this, parsePropsItem(newProps));
  }

  __onChangeName(event) {
    let val = event.target.value;

    state(this, {
      name        : val,
      coordinates : {},
      error       : ''
    });
  }

  __onChangeLat(event) {
    let val = event.target.value;

    state(this, {
      coordinates : {
        lat : coordination(val),
        lng : this.state.coordinates.lng
      }
    });
  }

  __onChangeLng(event) {
    let val = event.target.value;

    state(this, {
      coordinates : {
        lng : coordination(val),
        lat : this.state.coordinates.lat
      }
    });
  }

  __onChangeLabel(event) {
    let val = event.target.value;

    state(this, {
      label : val
    });
  }

  __find() {

    if (!this._geocoder)
      this._geocoder = new google.maps.Geocoder();

    if (this.state.name.length < 3 || !this.state.google || this.state.loading)
      return this;

    let d = this._d = Date.now();

    state(this, {
      loading : true
    });

    setTimeout((function () {
      if (this.state.loading && d == this._d) {
        state(this, {
          error   : 'Чет не пришел ответ от гугла',
          loading : false,
          trying  : 0
        });
      }
    }).bind(this), 5000);

    this._geocoder.geocode({'address' : this.state.name}, (function (results, status) {

      if (d !== this._d)
        return;

      if (status == google.maps.GeocoderStatus.OK) {
        let location = results[0].geometry.location;

        state(this, {
          coordinates : {
            lat : location.lat(),
            lng : location.lng()
          },
          loading     : false,
          error       : '',
          trying      : 0
        });

      } else if (status == google.maps.GeocoderStatus.OVER_QUERY_LIMIT) {
        if (this.state.trying >= 5) {
          state(this, {
            error   : `Превышено количество попыток`,
            loading : false,
            trying  : 0
          })
        } else {
          state(this, {
            error   : status + ` попытка - ${this.state.trying}`,
            loading : true,
            trying  : this.state.trying + 1
          });
          setTimeout(this.__find.bind(this, 'Повторка'), 1000);
        }
      } else {
        state(this, {
          error   : 'Geocode was not successful for the following reason: ' + status,
          loading : false,
          trying  : 0
        });
      }

    }).bind(this));
  }

  /**
   *
   * @return {{location: string, coordinates: *, label: (string|number|*)}}
   */
  get value() {
    return {
      location    : this.state.name,
      coordinates : this.state.coordinates,
      label       : this.state.label
    }
  }

  render() {
    return (
      <div className="admin-custom-geo_item">
        <div className={"input-group input-group-sm " + (this.state.error ? 'has-danger' : '')}>
          <input type="text" className="form-control"
                 value={this.state.name}
                 onChange={this.__onChangeName.bind(this)}
                 placeholder="Название"/>
          <span className="input-group-addon">lat:</span>
          <span className="input-group-addon" style={{padding : '0 0px'}}>
            <input type="text" style={{
              width        : 100,
              borderRadius : 0,
              border       : 0
            }}
                   className="form-control form-control-sm"
                   value={this.state.coordinates.lat || ''}
                   onChange={this.__onChangeLat.bind(this)}
                   placeholder="Lat"/>
            </span>
          <span className="input-group-addon">lng:</span>
          <span className="input-group-addon" style={{padding : '0 0px'}}>

            <input type="text" style={{
              width        : 100,
              borderRadius : 0,
              border       : 0
            }}
                   className="form-control form-control-sm"
                   value={this.state.coordinates.lng || ''}
                   onChange={this.__onChangeLng.bind(this)}
                   placeholder="Lng"/>
          </span>
          <span className="input-group-addon">label:</span>
          <span className="input-group-addon" style={{padding : '0 0px'}}>
            <input type="text" style={{
              width        : 100,
              borderRadius : 0,
              border       : 0
            }}
                   className="form-control form-control-sm"
                   value={this.state.label || ''}
                   onChange={this.__onChangeLabel.bind(this)}
                   placeholder="Label"/>
          </span>
          <span className="input-group-btn">
            <button className={"btn " + (this.state.google && !this.state.loading ? 'btn-primary' : 'btn-secondary')}
                    onClick={this.__find.bind(this)}
                    disabled={!this.state.google && this.state.loading} type="button"><i
              className="fa fa-google"/></button>
          </span>
        </div>
        {(() => {
          if (this.state.loading || this.state.error)
            return (
              <div>
                <small className="text-info">{this.state.loading ? 'loading...' : ''}</small>
                <small className="text-danger">{this.state.error}</small>
              </div>);
          return (
            <div><small>---</small></div>
          );
        })()}


      </div>
    )
  }
}

export default CustomList;
