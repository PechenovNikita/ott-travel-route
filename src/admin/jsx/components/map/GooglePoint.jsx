'use strict';

import {Component} from 'react';
import ReactDOM from 'react-dom';

import IconSelect from './IconSelect';
import state from './../../../js/mixins/state';
import Updater from './../../../../js/updater';
import {lat, lng} from './../../../js/mixins/Format';

function parseProps(props) {
  let data = props.data || [];
  return {
    lat   : lat(data[0]) || null,
    lng   : lng(data[1]) || null,
    icon  : data[2] || null,
    title : data[3] || null,
  }
}

class GooglePoint extends Component {

  constructor(props) {
    super(props);

    this.state = parseProps(props);

    this.state.google = false;
    this.state.error = false;
    this.state.loading = false;
    this.state.trying = false;
  }

  componentWillMount() {}

  componentDidMount() {
    Updater.googleMapInit(function () {
      state(this, {google : true});
    }.bind(this))
  }

  componentWillUnmount() {}

  componentDidUpdate(prevProps, prevState) {}

  componentWillReceiveProps(newProps) {
    state(this, newProps);
  }

  __onChangeLatLng(refName) {
    // let val;

    let val = this.refs[refName].value.replace(/[^-,.0-9]/g, '')
      , vals = val.split(',');

    if (vals.length == 2 && refName == 'lat') {
      state(this, {
        lat : lat(vals[0]),
        lng : lng(vals[1]) || this.state.lng
      });
      this.refs.lng.focus();
    } else {
      let temp = {}, t;
      if (refName == "lat")
        t = lat(val);
      else
        t = lng(val);

      if (val.indexOf('.') == val.length - 1)
        temp[refName] = t ? `${t}.` : '';
      else if (val === '-')
        temp[refName] = val;
      else
        temp[refName] = t || '';
      state(this, temp);
    }
    // if (vals.length == 2) {
    //   if (vals[0].indexOf('.') > -1 || vals[1].indexOf('.') > -1) {
    //     if (refName == 'lat')
    //       this.refs['lng'].focus();
    //
    //     state(this, {
    //       lat : lat(vals[0]) || '',
    //       lng : lng(vals[1]) || ''
    //     });
    //
    //   } else {
    //     let temp = {};
    //     if (refName == 'lat')
    //       temp[refName] = lat(vals.join('.'));
    //     else
    //       temp[refName] = lng(vals.join('.'));
    //
    //     state(this, temp);
    //   }
    //
    // } else if (vals.length == 4) {
    //   state(this, {
    //     lat : lat(`${vals[0]}.${vals[1]}`) || '',
    //     lng : lng(`${vals[2]}.${vals[3]}`) || '',
    //   });
    // } else {
    //   console.log(val);
    //   let temp = {};
    //   let t = parseFloat(val);
    //   if (val.indexOf('.') == val.length - 1)
    //     temp[refName] = t ? `${t}.` : '';
    //   else if (val === '-')
    //     temp[refName] = val;
    //   else
    //     temp[refName] = t || '';
    //   console.log(temp);
    //   state(this, temp);
    // }

  }

  __onChange(refName) {
    let temp = {
      error   : '',
      loading : false,
      trying  : 0
    };
    this._d = Date.now();
    if (['lat', 'lng'].indexOf(refName) > -1)
      return this.__onChangeLatLng(refName);

    temp[refName] = this.refs[refName].value;
    state(this, temp);
  }

  __find() {

    if (!this.state.google || this.state.title.length < 3)
      return;

    if (!this._geocoder)
      this._geocoder = new google.maps.Geocoder();

    let d = this._d = Date.now();
    state(this, {
      loading : true
    });

    setTimeout((function () {
      if (this.state.loading && d == this._d) {
        state(this, {
          error   : 'Чет не пришел ответ от гугла',
          loading : false,
          trying  : 0
        });
      }
    }).bind(this), 5000);

    this._geocoder.geocode({'address' : this.state.title}, (function (results, status) {

      if (d !== this._d)
        return;

      if (status == google.maps.GeocoderStatus.OK) {
        let location = results[0].geometry.location;

        state(this, {
          lat     : location.lat(),
          lng     : location.lng(),
          loading : false,
          error   : '',
          trying  : 0
        });

      } else if (status == google.maps.GeocoderStatus.OVER_QUERY_LIMIT) {
        if (this.state.trying >= 5) {
          state(this, {
            error   : `Превышено количество попыток`,
            loading : false,
            trying  : 0
          })
        } else {
          state(this, {
            error   : `${status}, попытка ${this.state.trying}`,
            loading : true,
            trying  : this.state.trying + 1
          });
          setTimeout(::this.__find, 1000);
        }
      } else {
        state(this, {
          error   : 'Geocode was not successful for the following reason: ' + status,
          loading : false,
          trying  : 0
        });
      }

    }).bind(this));
  }

  render() {
    return (
      <div className="admin-google-point">
        <div className={`input-group ${this.state.error ? 'has-danger' : ''}`}>
          <input type="text"
                 ref="title" value={this.state.title}
                 onChange={this.__onChange.bind(this, 'title')}
                 className="form-control"
                 placeholder="Заголовок/Название"/>
          <span className="input-group-btn">
            <button className="btn btn-primary"
                    onClick={this.__find.bind(this)} type="button"><i
              className="fa fa-google"/></button>
          </span>
          <input type="text"
                 style={{flex : '0 1 150px'}}
                 ref="lat" value={this.state.lat}
                 onChange={this.__onChange.bind(this, 'lat')}
                 className="form-control" placeholder="lat"/>
          <span className="input-group-addon">/</span>
          <input type="text"
                 style={{flex : '0 1 150px'}}
                 ref="lng" value={this.state.lng}
                 onChange={this.__onChange.bind(this, 'lng')}
                 className="form-control" placeholder="lng"/>
          <IconSelect icon={this.state.icon}/>
        </div>
        {(() => {
          if (this.state.loading || this.state.error)
            return (
              <div>
                <small className="text-info">{this.state.loading ? 'loading...' : ''}</small>
                <small className="text-danger">{this.state.error}</small>
              </div>);
          return (
            <div>
              <small>---</small>
            </div>
          );
        })()}
      </div>
    );
  }

  get value(){
    return [

    ]
  }
}

export default GooglePoint;