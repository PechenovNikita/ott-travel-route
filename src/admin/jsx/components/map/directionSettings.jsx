'use strict';

import React from 'react';
import Input from '../forms/input';
import TypeInput from '../forms/typeInput';

import state from './../../../js/mixins/state';

class DirectionSettings extends React.Component {

  constructor(props) {
    super(props);

    let data = props.data || {};

    this.state = {
      origin      : data.origin || '',
      destination : data.destination || '',
      waypoints   : data.waypoints || []
    };

    this._id = 'directionSettings_' + Date.now();
  }

  componentDidUpdate() {
    if (this._changed) {
      this._changed = false;

      if (this.props.onChange)
        this.props.onChange();
    }
  }

  componentWillReceiveProps(newProps) {
    let data = newProps.data || {};

    state(this, {
      origin      : data.origin || '',
      destination : data.destination || '',
      waypoints   : data.waypoints || []
    });
  }

  getValue() {
    return {
      origin      : this.refs.origin.getValue(),
      destination : this.refs.destination.getValue(),
      waypoints   : this.refs.waypoints.getValue().map(function (waypoint) {
        return {location : waypoint}
      })
    };

  }

  get value() {
    return this.getValue();
  }

  whenSomeThingChange(stateItem, ref) {
    if (this.refs[ref]) {
      this._changed = true;
      let st = this.state;
      st[stateItem] = this.refs[ref].value;
      this.setState(st);
    }
  }

  whenWayPointsChanged() {
    this._changed = true;
    let waypoints = this.refs.waypoints.getValue().map(function (waypoint) {
      return {location : waypoint}
    });
    state(this, {waypoints : waypoints});
  }

  render() {
    let waypoints = this.state.waypoints.map(function (waypoint) {
      return waypoint.location;
    }, this);

    return (
      <div className="admin-map-direction">
        <div className="row">
          <div className="col-md-6">

            <Input ref="origin" title="Стартовый город"
                   placeholder="Название города"
                   value={this.state.origin}
                   onChange={this.whenSomeThingChange.bind(this, 'origin', 'origin')}
                   desc="Название может быть на русском или на английском языке, чем больше уточнений (страна/конкретный адрес) тем лучше."/>

          </div>
          <div className="col-md-6">

            <Input ref="destination" title="Конечный пункт маршрута"
                   placeholder="Название города"
                   value={this.state.destination}
                   onChange={this.whenSomeThingChange.bind(this, 'destination', 'destination')}
                   desc="Название может быть на русском или на английском языке, чем больше уточнений (страна/конкретный адрес) тем лучше."/>

          </div>
        </div>

        <TypeInput ref="waypoints"
                   onChange={this.whenWayPointsChanged.bind(this)}
                   title="Промежуточные города" items={waypoints}/>

      </div>
    );
  }
}

export default DirectionSettings;