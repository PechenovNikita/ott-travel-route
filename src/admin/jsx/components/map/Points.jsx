'use strict';

import {Component} from 'react';
import ReactDOM from 'react-dom';

import GooglePoint from './GooglePoint';

class Points extends Component {

  constructor(props) {
    super(props);
  }

  componentWillMount() {}

  componentDidMount() {}

  componentWillUnmount() {}

  componentDidUpdate(prevProps, prevState) {}

  componentWillReceiveProps(newProps) {}

  render() {
    return (
      <div className="admin-points">
        <h4>Достопримечательности или кастомные точки</h4>
        <GooglePoint data={[100, 100, 'stadium', 'tenerife', 'text']}/>
      </div>
    );
  }
}

export default Points;