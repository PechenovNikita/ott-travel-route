'use strict';

'use strict';

import React, {Component} from 'react';

/**
 * props
 *
 * icon
 */
class PoiSightItem extends Component {

  click = (event) => {
    event.stopPropagation();
    event.preventDefault();

    this.props.onClick(this.props.icon);
  };

  render() {
    return (
      <a className="dropdown-item" href="javascript:void(0)" onClick={this.click}>
        <img src={`/public/images/map/png/m-${this.props.icon}.png`}
             className="mr-3"
             width={25} height={25}/>
        <span>{this.props.icon}</span>
      </a>
    );
  }
}

export default PoiSightItem;
