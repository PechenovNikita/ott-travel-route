export function findGeoLocation(location, formated = false) {
  const geocoder = new google.maps.Geocoder();
  return new Promise((resolve, reject) => {
    geocoder.geocode({'address' : location}, (results, status) => {

      if (status === google.maps.GeocoderStatus.OK) {
        if (formated) {
          resolve(results.map((result) => {
            return {
              address : result.formatted_address,
              lat     : result.geometry.location.lat(),
              lng     : result.geometry.location.lng(),
            };
          }));
        } else {
          resolve(results);
        }
      } else if (status === google.maps.GeocoderStatus.OVER_QUERY_LIMIT) {
        reject(status);
      } else {
        reject(status);
      }

    });
  })
}
