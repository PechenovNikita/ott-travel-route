'use strict';

'use strict';

import React, {Component} from 'react';

class PoiHotelItem extends Component {

  remove = (event) => {
    event.stopPropagation();
    event.preventDefault();

    this.props.onRemove(this.props.data.get('id'));
  };

  render() {
    return (
      <div style={{width : '100%'}} key={this.props.data.get('id')}>
        <div className="row">
          <div className="col-3 col-md-2">
            <img className="img-thumbnail" src={this.props.data.get('img', '')} alt=""/>
          </div>
          <div className="col-7 col-md-8">
            <div className="text-left">{this.props.data.get('name', '---')}</div>
            <div className="text-left">({this.props.data.get('city', '---')})</div>
          </div>
          <div className="col-2 text-right">
            <button className="btn btn-danger" onClick={this.remove}>Удалить</button>
          </div>
        </div>
      </div>
    );
  }
}

export default PoiHotelItem;
