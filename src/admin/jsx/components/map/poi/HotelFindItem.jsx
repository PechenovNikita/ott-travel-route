'use strict';

import React, {Component} from 'react';

class HotelFindItem extends Component {

  onClick = (event) => {
    event.stopPropagation();
    event.preventDefault();

    if (this.props.disabled)
      return;
    this.props.onClick(this.props.data);
  };

  render() {
    return (
      <div
        className={`btn btn-sm ${this.props.disabled ? 'btn-success' : 'btn-secondary'} mb-1 ${this.props.disabled ? 'disabled' : ''}`}
        style={{width : '100%'}}
        onClick={this.onClick}>
        <div className="row">
          <div className="col-3 col-md-2">
            <img className="img-thumbnail" src={this.props.data.img} alt=""/>
          </div>
          <div className="col-9 col-md-10 text-left">
            {this.props.disabled ? <div><span>выбрано</span></div> : ''}
            <div>{this.props.data.name}</div>
            <div>({this.props.data.city})</div>
          </div>
        </div>
      </div>
    );
  }
}

export default HotelFindItem;
