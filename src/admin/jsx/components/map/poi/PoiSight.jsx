'use strict';

import {Map, List} from 'immutable';
import React, {Component} from 'react';
import Updater from '../../../../../js/updater';
import {POIIcons} from '../../../../../js/map/icons';
import {findGeoLocation} from './api';
import PoiSightItem from './PoiSightItem';

// props
// data List(number, number, string, string)
// onChange (id, data)
// onRemove (id)

const icoReplace = {
  tree     : 'children',
  civic    : 'sign',
  school   : 'children',
  business : 'sign',
  medical  : 'children',
  golf     : 'sign'
};

const numberRegExp = /[,.]+/g;
const notNumberRegExp = /[^\d-.]/g;

export function fixNumberInput(value, oldValue) {
  value = value.toString().replace(numberRegExp, '.').replace(notNumberRegExp, '');
  if (value === '.')
    value = '0.';

  const parts = value.split('.');

  if (parts.length > 2) {
    const lastParts = (oldValue || '').toString().split('.');
    let dot = false;
    value = '';
    parts.forEach((part, index) => {
      if (dot) {
        value = value + part;
      } else {
        if (part === lastParts[index]) {
          value = value + part;
        } else {
          dot = true;
          value = value + part + '.';
        }
      }
    });
  }

  return value;
}

class PoiSight extends Component {

  state = {
    data : Map({
      icon       : false,
      icons      : List(),
      selectIcon : false,

      googling : false,

      error   : false,
      results : List()
    })
  };

  componentDidMount() {
    Updater.icons(this.loadIcons)
  }

  loadIcons = () => {
    this.setState(({data}) => ({
      data : data
        .set('icon', true)
        .set('icons', List(Object.keys(POIIcons.items)))
    }));

    if (Object.keys(icoReplace).indexOf(this.props.data.get(2)) > -1) {
      this.props.onChange(this.props.id, this.props.data.set(2, icoReplace[this.props.data.get(2)]));
    }
  };

  handlerClick = () => {
    if (this.state.data.get('selectIcon')) {
      document.addEventListener('click', this.handleOutsideClick, false);
    } else {
      document.removeEventListener('click', this.handleOutsideClick, false);
    }
  };

  handleOutsideClick = (event) => {
    if (!this._drop || this._drop.contains(event.target)) {
      return;
    }
    event.stopPropagation();
    event.preventDefault();

    this.setState(({data}) => ({data : data.set('selectIcon', false)})
      , this.handlerClick);
  };

  toggleIcon = (event) => {
    console.log('toggleIcon');
    event.stopPropagation();
    event.preventDefault();

    if (this.state.data.get('icon')) {
      this.setState(
        ({data}) => ({data : data.set('selectIcon', !data.get('selectIcon', false))})
        , this.handlerClick)
    }
  };

  changeInput = (event) => {
    let value = event.currentTarget.value;
    const id = event.currentTarget.getAttribute('data-id');

    if ([0, 1].indexOf(parseInt(id, 10)) > -1){
      value = fixNumberInput(value, this.props.data.get(id));
    }
    this.props.onChange(this.props.id, this.props.data.set(id, value));
  };

  findGoogle = (event) => {
    event.stopPropagation();
    event.preventDefault();

    this.setState(({data}) => ({
      data : data.set('googling', true)
    }), () => {
      findGeoLocation(this.props.data.get(3), true)
        .then(result => {
          this.setState(({data}) => ({data : data.set('error', false).set('googling', false).set('results', List(result))}));
        })
        .catch(err => {
          this.setState(({data}) => ({data : data.set('results', List()).set('googling', false).set('error', err)}));
        });
    });

  };

  remove = (event) => {
    event.stopPropagation();
    event.preventDefault();

    this.props.onRemove(this.props.id);
  };

  render() {
    const data = this.props.data;
    const show = (this.state.data.get('icon') && this.state.data.get('selectIcon'));
    return (
      <div className="mb-1">
        <div className="input-group input-group-sm">
          <div className={`input-group-btn ${show ? 'show' : ''}`}>
            <button type="button"
                    onClick={this.toggleIcon}
                    className="btn btn-light dropdown-toggle" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false">
              <img src={`//travelexpert.onetwotrip.com/public/images/map/png/m-${data.get(2)}.png`}
                   className="mr-2"
                   width={15} height={15}/>
            </button>
            {this.renderIcons()}
          </div>
          <input type="text" data-id={3} className="form-control"
                 onChange={this.changeInput} value={data.get(3)} placeholder={'title'}/>
          <span className="input-group-addon">(</span>
          <input type="text" data-id={0} className="form-control"
                 onChange={this.changeInput} value={data.get(0)} placeholder={'lat'}/>
          <span className="input-group-addon">,</span>
          <input type="text" data-id={1} className="form-control"
                 onChange={this.changeInput} value={data.get(1)} placeholder={'lng'}/>
          <span className="input-group-addon">)</span>
          <div className="input-group-btn">
            <button className="btn btn-primary" disabled={this.state.data.get('googling', false)} type="button"
                    onClick={this.findGoogle}><span className="fa fa-google"/></button>
          </div>
          <div className="input-group-btn">
            <button className="btn btn-danger" type="button" onClick={this.remove}><span
              className="fa fa-trash"/></button>
          </div>
        </div>
        {this.renderUnder()}
      </div>
    );
  }

  resultSelect = (event) => {
    const index = event.currentTarget.getAttribute('data-result-index');
    const result = this.state.data.getIn(['results', index], false);
    if (result) {
      let name = this.props.data.get(3, '');
      if (!name)
        name = result.address;

      this.setState(({data}) => ({data : data.set('results', List())}), () => {
        this.props.onChange(this.props.id, this.props.data
                                               .set(0, result.lat)
                                               .set(1, result.lng)
                                               .set(3, name)
        )
      });
      console.log(result);
      console.log(this.props.data.toJS());
    }

  };

  renderUnder() {
    if (this.state.data.get('error', false)) {
      return <div className="text-error">{this.state.data.get('error')}</div>
    } else if (this.state.data.get('results', List()).size > 0) {
      return this.state.data.get('results', List()).map((item, index) => {
        return <div key={index}>
          <button className="btn btn-sm btn-link" data-result-index={index} onClick={this.resultSelect}>
            <span className="fa fa-map-marker mr-3"/>
            {item.address} ( {item.lat} / {item.lng} )
          </button>
        </div>
      }).toArray();
    }
    return null;
  }

  /**
   *
   * @param {string} icon
   */
  selectIcon = (icon) => {
    this.setState(({data}) => ({
      data : data.set('selectIcon', false)
    }), () => {
      this.props.onChange(this.props.id, this.props.data.set(2, icon));
    });
  };

  renderIcons() {
    if (this.state.data.get('icon') && this.state.data.get('selectIcon')) {
      return (
        <div className="dropdown-menu" style={{
          maxHeight : 300,
          overflowY : 'scroll'
        }} ref={node => this._drop = node}>
          {this.state.data.get('icons').map((icon, index) => {
            return (
              <PoiSightItem key={icon} icon={icon} onClick={this.selectIcon}/>
            )
          }).toArray()}
        </div>
      );
    }
    return null;
  }
}

export default PoiSight;
