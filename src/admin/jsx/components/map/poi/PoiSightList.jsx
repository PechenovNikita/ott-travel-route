'use strict';

'use strict';

import {List, Map} from 'immutable';

import React, {Component} from 'react';
import PoiSight from './PoiSight';

class PoiSightList extends Component {

  state = {
    data : Map({
      hide : true
    })
  };

  toggle = (event) => {
    event.stopPropagation();
    event.preventDefault();

    this.setState(({data}) => ({data : data.set('hide', !data.get('hide', false))}));
  };

  add = (event) => {
    event.stopPropagation();
    event.preventDefault();

    const temp = List([0, 0, '', '']);
    const added = this.props.items.push(temp);

    this.props.onChange(added);
  };

  render() {
    const count = this.props.items ? this.props.items.size : 0;
    const icon = this.state.data.get('hide', false)
      ? (<span className="fa fa-caret-down"/>)
      : (<span className="fa fa-caret-up"/>);
    return (
      <div>
        <h5>
          <span style={{cursor : 'pointer'}} onClick={this.toggle}>Достопримечательности ({count}){icon}</span>
        </h5>
        {this.__renderList()}
      </div>
    );
  }

  __renderList() {
    if (this.state.data.get('hide', false)) {
      return null;
    }
    return (
      <div>
        {this.props.items.map(this.renderItem).toArray()}
        <div>
          <button onClick={this.add} className="btn btn-primary btn-sm mr-2">Добавить</button>
        </div>
      </div>
    );
  }

  changeItem = (id, data) => {
    this.props.onChange(this.props.items.set(id, data));
  };

  removeItem = (id) => {
    console.log('remove', id);
    this.props.onChange(this.props.items.delete(id))
  };

  renderItem = (item, index) => {
    if (this.state.data)
      return <PoiSight id={index} key={index}
                       onChange={this.changeItem}
                       onRemove={this.removeItem}
                       data={item}/>
  };
}

export default PoiSightList;
