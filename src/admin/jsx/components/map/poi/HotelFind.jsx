// @flow
'use strict';

import React, {Component} from 'react';
import {Map, List} from 'immutable';
import JSONP from '../../../../js/component/extendJSONP';
import HotelFindItem from './HotelFindItem';

class HotelFind extends Component {

  state = {
    data : Map({
      city       : '',
      searchCity : false,
      cities     : List(),
      selectCity : '',

      searchHotels   : false,
      hotels         : List(),
      hotelFilter    : '',
      hotelsFiltered : List(),

      error        : false,
      errorMessage : '',
    })
  };

  render() {
    return (
      <div>
        <h6>Поиск Отелей по базе OTT</h6>
        <div className="row">
          <div className="col-12 col-sm-4">
            {this.__renderCityName()}
          </div>
          <div className="col-12 col-sm-8">
            {this.__renderCitySelect()}
          </div>
        </div>
        {this.__renderError()}
        {this.__renderHotels()}
      </div>
    );
  }

  timestamp;

  changeFilter = (event) => {
    const value = event.currentTarget.value;
    const time = this.timestamp = Date.now();

    this.setState(({data}) => ({
      data : data.set('hotelFilter', value)
    }), () => {
      if (time !== this.timestamp) {
        return
      }
      const hotelFiltered = this.filterHotel(value);
      this.setState(({data}) => ({
        data : data.set('hotelsFiltered', hotelFiltered)
      }));
    });

  };

  filterHotel(filter) {
    const hotels = this.state.data.get('hotels', List());

    if (filter) {
      const filterLow = filter.toLowerCase();
      return hotels.filter((hotel) => hotel.name.toLocaleLowerCase().indexOf(filterLow) > -1);
    }
    return hotels;
  }

  __renderHotels() {
    const hotels = this.state.data.get('hotels', List());
    const hotelsFiltered = this.state.data.get('hotelsFiltered', List());

    const hotelsMax = hotelsFiltered.slice(0, 10);
    if (hotels.size > 0) {
      return (
        <div>
          <div className="mb-2">
            <input type="text"
                   value={this.state.data.get('hotelFilter')}
                   onChange={this.changeFilter}
                   placeholder="Фильтр"
                   className="form-control"/>
          </div>
          <div>{hotelsMax.size === 0
            ? <div>Ничего не найдено</div>
            : hotelsMax.map((hotel) => {
              return (
                <HotelFindItem key={hotel.id}
                               disabled={this.props.selected.indexOf(hotel.id) > -1}
                               data={hotel}
                               onClick={this.selectHotel}/>
              )
            }).toArray()}</div>
          {hotelsFiltered.size > hotelsMax.size ?
            <div>Найдено еще {hotelsFiltered.size - hotelsMax.size} отеля</div> : null}
        </div>
      );
    }
    return null;
  }

  selectHotel = (hotel) => {
    this.props.addHotel(hotel);
  };

  __renderError() {
    if (this.state.data.get('error', false)) {
      return <div style={{color : 'red'}}>Error: {this.state.data.get('errorMessage', '')}</div>;
    }
    return null;
  }

  __renderCitySelect() {
    const cities = this.state.data.get('cities', List());
    if (cities.size === 0) {
      return (
        <div className="form-group">
          <label>Список городов пуст</label>
          <div>
            {this.state.data.get('searchCity', false) ? 'Поиск...' : ''}
          </div>
        </div>
      );
    }
    return (
      <div className="form-group">
        <label htmlFor="city_select">Выберите город</label>
        <div className="input-group input-group-sm">
          <select id="city_select"
                  className="custom-select"
                  onChange={this.changeSelectCity}
                  value={this.state.data.get('selectCity', '')}>
            <option value="">Не выбрано</option>
            {cities.map((city, index) => {
              return <option key={city.id} value={city.id}>{city.name}/{city.country}</option>;
            }).toArray()}
          </select>
          <div className="input-group-btn">

            <button className="btn btn-primary"
                    disabled={!this.state.data.get('selectCity', '')}
                    onClick={this.selectCity}
                    type="button">Выбрать
            </button>
          </div>
        </div>
      </div>
    );
  }

  changeSelectCity = (event) => {
    const cityId = event.currentTarget.value;
    this.setState(({data}) => ({data : data.set('selectCity', cityId)}));
  };

  selectCity = (event) => {
    event.stopPropagation();
    event.preventDefault();
    console.log('selectCity', this.state.data.get('selectCity'));

    this.setState(({data}) => ({
      data : data.set('searchHotels', true)
    }), () => {
      JSONP.admin.getCityData(undefined, undefined, this.state.data.get('selectCity'), this.parseCityPoi);
    });
  };

  parseCityPoi = (response) => {
    console.log('parseCityPoi', response);
    if (response.error) {
      //response.error.msg
      this.setState(({data}) => ({data : data.set('error', true).set('errorMessage', response.error && response.error.msg ? response.error.msg : 'Something went wrong')}))
    } else {
      //response.result.hotels
      const hotels = List(response.result.hotels);
      this.setState(({data}) => ({
        data : data
          .set('error', false)
          .set('errorMessage', '')
          .set('hotels', hotels)
          .set('hotelFilter', '')
          .set('hotelsFiltered', hotels)
      }))

    }
  };

  __renderCityName() {
    const city = this.state.data.get('city', '');
    return (
      <div className="form-group">
        <label htmlFor="city_name">Название города</label>

        <div className="input-group">
          <input type="text" className="form-control"
                 disabled={this.state.data.get('searchCity')}
                 id="city_name"
                 value={city}
                 onChange={this.changeCity}
                 placeholder="Город для поиска отеля"/>
          <span className="input-group-btn">
                <button className="btn btn-primary" disabled={!city || this.state.data.get('searchCity')}
                        onClick={this.searchCity}
                        type="button"><span className="fa fa-search"/></button>
          </span>
        </div>
      </div>
    );
  }
  changeCity = (event) => {
    const value = event.currentTarget.value;
    this.setState(({data}) => ({data : data.set('city', value)}));
  };

  searchCity = (event) => {
    event.stopPropagation();
    event.preventDefault();

    const name = this.state.data.get('city');

    if (name) {
      this.setState(({data}) => ({
        data : data
          .set('error', false)
          .set('searchCity', true)
      }), () => {
        JSONP.admin.getCityId(this.state.data.get('city'), this.parseCity);
      });
    }
  };

  parseCity = (response) => {
    if (response.error) {
      this.setState(({data}) => ({
        data : data
          .set('searchCity', false)
          .set('error', true)
          .set('errorMessage', response.error && response.error.msg ? response.error.msg : 'Something went wrong')
          .set('cities', List())
      }));
    } else {
      this.setState(({data}) => ({
        data : data
          .set('searchCity', false)
          .set('cities', List(response.result))
      }))
    }
  };
}

export default HotelFind;
