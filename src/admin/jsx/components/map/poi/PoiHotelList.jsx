'use strict';

'use strict';

import {List, Map, fromJS} from 'immutable';

import React, {Component} from 'react';
import HotelFind from './HotelFind';
import PoiHotelItem from './PoiHotelItem';

class PoiHotelList extends Component {

  state = {
    data : Map({
      hide : true,
      add  : true,
    })
  };

  toggle = (event) => {
    event.stopPropagation();
    event.preventDefault();

    this.setState(({data}) => ({data : data.set('hide', !data.get('hide', false))}));
  };

  add = (event) => {
    event.stopPropagation();
    event.preventDefault();
  };

  render() {

    return (
      <div>
        <h5>
        <span style={{cursor : 'pointer'}}
              onClick={this.toggle}>Отели ({this.props.items ? this.props.items.size : 0}) {this.state.data.get('hide', false) ? (
          <span className="fa fa-caret-down"/>) : (
          <span className="fa fa-caret-up"/>)}</span>
        </h5>
        {this.__renderList()}
        {this.__renderAddForm()}
      </div>
    );
  }

  addHotel = (hotel) => {
    this.props.onChange(this.props.items.push(fromJS(hotel)));
  };

  removeHotel = (hotelId) => {
    this.props.onChange(this.props.items.filter((item) => hotelId !== item.get('id')));
  };

  __renderList() {
    if (this.state.data.get('hide', false)) {
      return null;
    }

    return this.props.items.map((hotel) => {
      return (
        <div className="mb-1" style={{width : '100%'}} key={hotel.get('id')}>
          <PoiHotelItem data={hotel} onRemove={this.removeHotel}/>
        </div>
      );
    }).toArray();
  }

  __renderAddForm() {
    if (this.state.data.get('hide', false)) {
      return null;
    }

    const ids = this.props.items.map((hotel) => hotel.get('id'));
    return (
      <HotelFind addHotel={this.addHotel} selected={ids}/>
    )
  }

}

export default PoiHotelList;
