'use strict';

import React from 'react';
import state from './../../../js/mixins/state';
import InputPoint from './../forms/inputPoint';
import Input from './../forms/input';

function parseProps(props) {

  let location = (props.data ? props.data.location : []);
  location = (Array.isArray(location) ? location : [props.data.location]);

  return {
    location : location,
    zoom     : (props.data ? (props.data.zoom || 7) : 7),
  };

}

class PointsSettings extends React.Component {

  constructor(props) {
    super(props);

    this.state = parseProps(props);

    this._id = 'pointsSettings_' + Date.now();
    this._refs = [];
  }

  // componentWillMount() {}

  // componentDidMount() {}

  // componentWillUnmount() {}

  // componentDidUpdate(prevProps, prevState) {}

  componentWillReceiveProps(newProps) {
    state(this, parseProps(newProps));
  }

  __getLocations() {
    return this._refs.map(function (ref) {
      return this.refs[ref].getValue();
    }, this);
  }

  getValue() {
    let location = this.__getLocations();
    return {
      location : location.filter(function (loc) {return !!loc;}),
      zoom     : parseInt(this.refs.zoom.getValue(), 10)
    }
  }

  addPoint() {
    let loc = this.__getLocations();
    loc.push('');
    state(this, {
      location : loc
    });
  }

  deletePoint(index) {
    let loc = this.__getLocations();
    loc.splice(index, 1);
    state(this, {
      location : loc
    });
  }

  render() {

    this._refs = [];
    let points = this.state.location.map(function (location, index) {
      let ref = Date.now() + '_' + index;
      this._refs.push(ref);
      return (<div key={index}>
        <div className="row">
          <div className="col col-10">
            <InputPoint ref={ref} title="Маркет на карте" location={location}/>
          </div>
          <div className="col col-1">
            <button className="btn btn-danger" onClick={this.deletePoint.bind(this, index)}><i
              className="fa fa-trash-o"/></button>
          </div>
        </div>
      </div>)
    }, this);

    return (
      <div className="admin-map-points">
        <div className="row">
          <div className="col col-10">
            {points}
            <div>
              <button className="btn btn-secondary" onClick={this.addPoint.bind(this)}><i className="fa fa-plus"/>
                <span>Добавить точку</span></button>
            </div>
          </div>
          <div className="col col-2">
            <Input ref="zoom" title="Масштаб если надо отдалить"
                   desc="Этот масштаб будет применен к карте при суловии нахождения всех точек в поле зрения"
                   placeholder="масштаб" value={this.state.zoom}/>
          </div>
        </div>
      </div>
    );
  }
}

export default PointsSettings;
