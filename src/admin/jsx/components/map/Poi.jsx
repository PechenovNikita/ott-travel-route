'use strict';

import {Map, List} from 'immutable';
import React, {Component} from 'react';
import PoiSightList from './poi/PoiSightList';
import PoiHotelList from './poi/PoiHotelList';

class Poi extends Component {

  changeSight = (sights) => {
    this.props.onChange(this.props.data.set('sight', sights));
  };

  changeHotel = (hotels) => {
    this.props.onChange(this.props.data.set('hotels', hotels));
  };

  render() {
    return (
      <div>
        <PoiSightList onChange={this.changeSight} items={this.props.data.get('sight', List())}/>
        <PoiHotelList onChange={this.changeHotel} items={this.props.data.get('hotels', List())}/>
      </div>
    );
  }
}

export default Poi;
