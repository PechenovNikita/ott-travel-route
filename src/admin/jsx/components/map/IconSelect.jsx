'use strict';

import {Component} from 'react';
import ReactDOM from 'react-dom';

import state from './../../../js/mixins/state';
import {names as IconsName, POIIcons, googleIcon} from './../../../../js/map/icons';
import Updater from './../../../../js/updater';

class IconSelect extends Component {

  constructor(props) {
    super(props);

    this.state = {
      loaded      : false,
      open        : false,
      isLabelIcon : !!props.label,
      label       : props.label || '',
      icon        : props.icon || null
    }
  }

  componentWillMount() {
    Updater.icons(function () {
      state(this, {loaded : true});
    }.bind(this));
  }

  componentDidMount() {}

  componentWillUnmount() {}

  componentDidUpdate(prevProps, prevState) {}

  componentWillReceiveProps(newProps) {}

  __renderList() {
    /**
     * @this IconSelect
     */
    let icons = IconsName.map(function (icon_name, index) {
      let icon = '..';
      if (this.state.loaded) {
        icon = <img src={POIIcons.items[icon_name].url} width={30} height={30} alt={icon_name}/>
      }
      return <a onClick={this.__selectIcon.bind(this, icon_name)}
                href="javascript:void(0)"
                className="dropdown-item"
                key={`icon_${index}`}>{icon} - <span>{icon_name}</span></a>
    }, this);

    return (
      <div className="dropdown-menu dropdown-menu-right" style={{
        maxHeight : 200,
        overflow  : 'auto'
      }}>
        <a onClick={this.__selectIcon.bind(this, null)}
           className="dropdown-item"
           href="javascript:void(0)">None</a>
        <a onClick={this.__selectIcon.bind(this, 'google-green')}
           className="dropdown-item"
           href="javascript:void(0)"><img src={googleIcon(true, '?')} width={22 * 30 / 40} height={30}/> - Google Label</a>
        <a onClick={this.__selectIcon.bind(this, 'google-red')}
           className="dropdown-item"
           href="javascript:void(0)"><img src={googleIcon(false, '?')} width={22 * 30 / 40} height={30}/> - Google Label</a>
        <div role="separator" className="dropdown-divider"></div>
        {icons}
      </div>
    );
  }

  __clickBtn(event) {
    event.preventDefault();
    event.stopPropagation();

    state(this, {open : !this.state.open});
  }

  __selectIcon(icon_name) {
    // event.preventDefault();
    // event.stopPropagation();
    let isLabel = false;

    switch (icon_name) {
      case "google-red":
      case "google-green":
        isLabel = true;
        break;
    }

    state(this, {
      open        : false,
      isLabelIcon : isLabel,
      label       : this.state.label,
      icon        : icon_name
    });
  }

  get icon() {
    return this.state.icon;
  }

  get label() {
    return this.state.label;
  }

  __onChangeLabel(event) {
    let target = event.target;

    state(this, {
      label : target.value
    });

  }

  __renderIconAddon() {
    let google = (['google-green', 'google-red'].indexOf(this.state.icon) > -1)
      , green = (this.state.icon === 'google-green');
    return (
      <span className="input-group-addon" style={{
        borderRadius : 0,
        borderLeft   : 0
      }}><img src={google ? googleIcon(green, this.state.label) : POIIcons.items[this.state.icon].url}
              width={google ? '' : 20}
              height={20}
              alt={this.state.icon}/></span>);
  }

  render() {
    return (
      <div className={`admin-icon-select input-group-btn ${this.state.open ? 'show' : ''}`}>

        {(() => {
          let icon = null, label = null;
          if (this.state.loaded && this.state.icon && POIIcons.items[this.state.icon])
            icon = this.__renderIconAddon();
          else if (this.state.isLabelIcon) {
            icon = this.__renderIconAddon();
            label = (
              <input type="text"
                     style={{
                       width        : 40,
                       paddingLeft  : 2,
                       paddingRight : 2,
                       textAlign    : "center"
                     }}
                     placeholder="?" className="form-control" value={this.state.label}
                     onChange={::this.__onChangeLabel}/>)
          }
          return (
            <div className="input-group">
              {icon}
              {label}
              <div className="input-group-btn">
                <button onClick={::this.__clickBtn} type="button" className="btn btn-secondary"><i
                  className="fa fa-chevron-down"/></button>
              </div>
            </div>
          );
        })()}


        {this.__renderList()}
      </div>
    );
  }
}

export default IconSelect;