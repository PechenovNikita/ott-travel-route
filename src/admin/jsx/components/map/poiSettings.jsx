'use strict';

import React from 'react';
import state from './../../../js/mixins/state';
import JSONP from './../../../js/component/extendJSONP';
import Select from './../forms/select';
import SelectMulti from './../forms/selectMulti';
import TypeInput from './../forms/typeInput';
import CustomList from './CustomList';
import FormCheck from './../standart/form-check';

import Points from './Points';

/**
 * @typedef {Array.<number,number,string,string>} OttPOIResult
 */


function filterSight(items, val) {
  return items.filter(function (item) {
    return item[3].toLowerCase().indexOf(val.toLocaleString()) >= 0;
  });
}

/**
 *
 * @param {Array.<number,number, string, string>} item
 * @return {string}
 */
function displaySight(item) {
  return item[3];
}

function markersProps(data, currentState, nameShowParam, nameDataParam){
  return (currentState && !currentState[nameShowParam] && (!data[nameDataParam] || data[nameDataParam].length == 0) ? (currentState[nameDataParam] || []) : (data[nameDataParam] || []));
}

function parseProps(props, currentState) {
  let data = props.data || {};
  console.log(currentState);

  return {
    hotels  : markersProps(data,currentState, 'showHotels', 'hotels'),
    sight   : markersProps(data,currentState, 'showSight', 'sight'),
    markers   : markersProps(data,currentState, 'showMarkers', 'markers'),
    // markers : (currentState && !currentState.showMarkers && (!data.markers || data.markers.length == 0) ? (currentState.markers || []) : (data.markers || [])),

    city_id : data.city_id || '',
    name    : data.name || props.location || '',

    disabled : false,

    ottResultFromSearch : (currentState ? currentState.ottResultFromSearch : []),
    ottResultFromSight  : (currentState ? currentState.ottResultFromSight : []),
    ottResultFromHotels : (currentState ? currentState.ottResultFromHotels : []),

    loadingCities : false,
    loadingSight  : false,
    loadingHotels : false,

    hideSight   : currentState ? !!currentState.hideSight : true,
    hideHotels  : currentState ? !!currentState.hideHotels : true,
    hideMarkers : currentState ? !!currentState.hideMarkers : true,

    showSight   : (currentState ? !!currentState.showSight : !!data.sight),
    showHotels  : (currentState ? !!currentState.showHotels : !!data.hotels),
    showMarkers : (currentState ? !!currentState.showMarkers : !!data.markers),
  };

}

class PoiSettings extends React.Component {

  constructor(props) {
    super(props);

    this.state = parseProps(props);
    this._defaultPOI = [];
    this._id = 'poiSettings_' + Date.now();
  }

  componentDidMount() {
    this.__getDataFromCityId();
  }

  componentWillReceiveProps(newProps) {
    state(this, parseProps(newProps, this.state));
    this._defaultPOI = [];
  }

  /**
   *
   * @param {Event} event
   */
  onNameInput(event) {
    state(this, {name : event.target.value});
  }

  /**
   *
   * @param {Event} event
   */
  onCitySearch(event) {
    event.stopPropagation();
    event.preventDefault();

    state(this, {
      disabled      : true,
      loadingCities : true
    });
    JSONP.admin.getCityId(this.state.name, this.onOttGetValue.bind(this));
  }

  onOttGetValue(response) {

    let results = (!response.error ? response.result : []);

    results = results.filter(function (data, index) {
      return (data.type == 'geo');
    });

    let city_id = (results.length > 0 ? results[0].city_id : '');

    state(this, {
      disabled : false,
      city_id  : city_id,

      ottResultFromSearch : results,
      ottResultFromHotels : [],
      ottResultFromSight  : [],

      loadingCities : false,
      loadingSight  : true,
      loadingHotels : true
    });

    this.__getDataFromCityId(city_id);
  }

  setDefaultSight(poi) {

    if (this.state.sight.length > 0) {
      let temp = poi.filter(function (newPoi, index) {
        return this.state.sight[0][0] == newPoi[0] && this.state.sight[0][1] == newPoi[1];
      }, this);
      if (temp.length > 0) {
        console.log(temp[0]);
        return;
      }
    }

    if (!this._defaultPOI || this._defaultPOI.length == 0 || !poi || poi.length == 0) {
      state(this, {sight : []});
      return;
    }
    console.log('setDefaultSight', this._defaultPOI[0], poi[0]);

    state(this, {
      sight : poi.filter(function (newPoi, index) {
        return this._defaultPOI.some(function (defPoi) {
          return defPoi.lat == newPoi[0] && defPoi.lng == newPoi[1];
        }, this);
      }, this)
    });

  }

  onOttGetSight(response) {

    console.log('onOttGetSight', response, this._defaultPOI, this.state.sight);

    if (response.error) {
      console.log('Error', response.error);
      return;
    }

    let results = (!response.error ? response.result : []);

    this.setDefaultSight(results);

    state(this, {
      ottResultFromSight : results || [],
      loadingSight       : false
    });

  }

  onOttGetGeo(response) {

    console.log('onOttGetGeo', response);

    if (response.error)
      return;

    this._defaultPOI = response.result.poi;

    let results = (!response.error ? response.result.hotels : []);

    this.setDefaultSight(this.state.ottResultFromSight);

    let hotels = this.state.hotels;
    if (hotels.length > 0) {
      let temp = results.filter(function (hotel) {
        return hotel.id == hotels[0].id;
      }, this);
      if (temp.length == 0) {
        hotels = [];
      }
    }

    state(this, {
      hotels              : hotels,
      ottResultFromHotels : results || [],
      loadingHotels       : false
    });

  }

  __getDataFromCityId(city_id = this.state.city_id) {
    if (city_id) {
      state(this, {
        loadingHotels : true,
        loadingSight  : true
      });
      JSONP.admin.getPOI(city_id, this.onOttGetSight.bind(this));
      JSONP.admin.getCityData(undefined, undefined, city_id, this.onOttGetGeo.bind(this))
    }
  }

  selectOttCityId(city_id) {

    state(this, {
      city_id            : city_id,
      ottResultFromSight : []
    });

    this.__getDataFromCityId(city_id);
  }

  getValue() {

    return {
      name    : this.state.name,
      city_id : this.state.city_id,
      sight   : this.state.showSight ? (this.refs.sight ? this.refs.sight.getValue() : this.state.sight) : [],
      hotels  : this.state.showHotels ? (this.refs.hotels ? this.refs.hotels.getValue() : this.state.hotels) : [],
      markers : this.state.showMarkers ? (this.refs.markers ? this.refs.markers.value : this.state.markers) : []
    }
  }

  /**
   *
   * @return {Object}
   * @private
   */
  __renderSight() {
    if (this.state.ottResultFromSight.length > 0 || this.state.sight.length > 0) {
      return (
        <div className="admin-map-poi__sight">
          <div className="form-group form-inline">
            <h5>
              <FormCheck onChange={this.toggle.bind(this, 'showSight')} checked={this.state.showSight}
                         label="Достопримечательности"/>
              &nbsp;
              <button
                className="btn btn-sm btn-secondary"
                onClick={this.toggle.bind(this, 'hideSight')}>{this.state.hideSight ? 'Показать список' : 'Скрыть список'}</button>
              &nbsp;
              <small>{this.state.loadingSight ? 'loading...' : ''}</small>
            </h5>
            <div className={this.state.hideSight ? 'hide' : ''}>
              <SelectMulti reg="sight" filterFunc={filterSight} displayFunc={displaySight} selected={this.state.sight}
                           items={this.state.ottResultFromSight}/>
            </div>
          </div>
        </div>
      );
    }
    return (this.state.loadingSight ? 'Поиск достопримечательностей' : '');
  }

  toggle(param) {
    let st = this.state;
    st[param] = !st[param];
    state(this, st);
  }

  /**
   *
   * @return {Object}
   * @private
   */
  __renderHotel() {
    if (this.state.ottResultFromHotels.length > 0 || this.state.hotels.length > 0) {
      return (
        <div className="admin-map-poi__sight">

          <div className="form-group form-inline">
            <h5>
              <FormCheck onChange={this.toggle.bind(this, 'showHotels')} checked={this.state.showHotels} label="Отели"/>
              &nbsp;
              <button
                className="btn btn-sm btn-secondary"
                onClick={this.toggle.bind(this, 'hideHotels')}>{this.state.hideHotels ? 'Показать список отелей' : 'Скрыть список отелей'}</button>
              &nbsp;
              <small>{this.state.loadingHotels ? 'loading...' : ''}</small>
            </h5>
            <div className={this.state.hideHotels ? 'hide' : ''}>

              <SelectMulti reg="hotels" filterFunc={function (items, val) {
                val = val.toLowerCase();
                return items.filter(function (item) {
                  let ret = (item.translated_city && item.translated_city.toLowerCase().indexOf(val) >= 0);
                  ret = ret || (item.translated_name && item.translated_name.toLowerCase().indexOf(val) >= 0);
                  ret = ret || (item.name && item.name.toLowerCase().indexOf(val) >= 0);
                  ret = ret || (item.address && item.address.toLowerCase().indexOf(val) >= 0);

                  return ret;
                })
              }} displayFunc={function (item) {
                return item.name;
              }} selected={this.state.hotels} items={this.state.ottResultFromHotels}/>
            </div>
          </div>
        </div>
      );
    }
    return (this.state.loadingHotels ? 'Поиск отелей' : '');
  }

  __renderPlaces() {
    return (
      <div>
        <div className="form-group form-inline">
          <h5>
            <FormCheck onChange={this.toggle.bind(this, 'showMarkers')} checked={this.state.showMarkers}
                       label="Места"/>
            &nbsp;
            <button
              className="btn btn-sm btn-secondary"
              onClick={this.toggle.bind(this, 'hideMarkers')}>{this.state.hideMarkers ? 'Показать список мест' : 'Скрыть список мест'}</button>
            &nbsp;</h5>
          <div className={this.state.hideMarkers ? 'hide' : ''}>
            <CustomList ref="markers" value={this.state.markers}/>
          </div>
        </div>
      </div>
    )
  }

  render() {
    let searchResult = [];

    if (this.state.ottResultFromSearch) {
      searchResult = this.state.ottResultFromSearch.map(function (city) {
        return {
          value : city.city_id,
          name  : city.name + ', ' + city.country + ' - ' + city.city_id
        }
      }, this);
    }

    if (searchResult.length == 0) {
      if (this.state.city_id) {
        searchResult = [{
          name  : this.state.name + ' - ' + this.state.city_id,
          value : this.state.city_id
        }]
      }
    }

    let select = 'Нет данных';

    if (searchResult.length > 0) {
      select = (<Select ref="city_id" onSelect={this.selectOttCityId.bind(this)}
                        title="Выдача OneTwoTrip"
                        items={searchResult}
                        value={this.state.city_id}/>);
    } else if (this.state.loadingCities)
      select = 'Поиск данных в OneTwoTrip...';

    let name = this.state.name || '';

    return (
      <div className="admin-map-poi">

        <div className="row">
          <div className="col-lg-4">

            <div className="form-group">
              <label htmlFor={this._id + '_search'}>Поиск города</label>
              <div className="input-group">
                <input type="text" id={this._id + '_search'}
                       value={name}
                       onChange={this.onNameInput.bind(this)}
                       className="form-control"
                       placeholder="Название"/>

                <span className="input-group-btn">
                  <button className="btn btn-secondary"
                          disabled={this.state.disabled || name.length < 2}
                          onClick={this.onCitySearch.bind(this)}
                          type="button">Найти</button>
                </span>
              </div>
            </div>

          </div>
          <div className="col-lg-5">
            {select}
          </div>
        </div>
        {/*<Points/>*/}
        {this.__renderSight()}
        {this.__renderHotel()}
        {this.__renderPlaces()}
      </div>
    );
  }
}

export default PoiSettings;