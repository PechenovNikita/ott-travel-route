'use strict';

import {fromJS, isCollection, List, Map} from 'immutable';
import React, {Component} from 'react';
import JSONP from '../../../js/JSONP';

import styled from 'styled-components';
import Preview from './customPhoto/Preview';
import Search from './customPhoto/Search';
import Grid from './customPhoto/Grid';

export const $GalleryWrapper = styled.div`
  display: flex;
  flex-flow: row wrap;
  margin-bottom: 1em;
`;

export const $GalleryItem = styled.div`
  flex: 0 0 ${100 / 10}%;
  height:100px;
  background-color: #D8D8D8;
  background-position: center center;
  background-repeat: no-repeat;
  background-size: cover;
  box-shadow: inset 0 0 0 4px ${props => props.selected ? 'blue' : 'transparent'};
  
  &:hover{
    box-shadow: inset 0 0 0 4px ${props => props.selected ? 'red' : 'green'};
  }
`;

// https://farm{farm-id}.staticflickr.com/{server-id}/{id}_{secret}.jpg
// https://farm{farm-id}.staticflickr.com/{server-id}/{id}_{secret}_[mstzb].jpg
// https://farm{farm-id}.staticflickr.com/{server-id}/{id}_{o-secret}_o.(jpg|gif|png)

export function generateImageUrlFromData(photoData) {
  if (photoData)
    return 'https://farm' + photoData.farm + '.staticflickr.com/' + photoData.server + '/' + photoData.id + '_' + photoData.secret + '.jpg';
  return null;
}

function parseOptions(props) {
  if (props.options) {
    return Map({
      ids   : List(props.options.ids || []),
      items : List(props.options.items || []),
      // grid  : fromJS(props.options.grid || [1, 1, 1, 1]),
    })
  }

  return Map({
    ids   : List(),
    items : List(),
    // grid  : List([1, 1, 1, 1]),
  });
}

class CustomPhoto extends Component {
  constructor(props) {
    super();

    this.state = {
      data : Map({
        loading  : false,
        items    : List(),
        preview  : Map({
          current : 0,
          perPage : 40,
        }),
        selected : parseOptions(props)
      })
    };

    this.addItem = this.addItem.bind(this);
    this.removeItem = this.removeItem.bind(this);
    this.onDrag = this.onDrag.bind(this);
    this.onSearch = this.onSearch.bind(this);
  }

  onChange(event) {
    const target = event.currentTarget;

    this.setState(({data}) => ({
      data : data.set('search', target.value)
    }));
  }

  onSearch(id, items) {
    if (this.state.data.get('loading') !== id)
      return;
    this.setState(({data}) => ({
      data : data.set('items', List(items))
                 .set('loading', false)
    }));
  }

  render() {
    return (
      <div className="">
        <Search onSearch={this.onSearch} onLoading={(id) => {
          this.setState(({data}) => ({data : data.set('loading', id)}))
        }}/>

        <Grid items={this.state.data.get('items', List())}
              selectedIds={this.state.data.getIn(['selected', 'ids'], List())}
              loading={!!this.state.data.get('loading')}
              onAdd={this.addItem}
              onRemove={this.removeItem}/>

        <h4>Результат</h4>
        <Preview grid={this.props.options && this.props.options.grid ? this.props.options.grid : [1, 1, 1, 1]}
                 items={this.state.data.getIn(['selected', 'items'], List())}
                 onRemove={this.removeItem}
                 onToggle={this.onDrag}
                 ref={grid => this._grid = grid}/>
      </div>
    );
  }

  addItem(item) {
    this.setState(({data}) => ({
        data : data
          .updateIn(['selected', 'items'], items => {
            return items.push(item);
          })
          .updateIn(['selected', 'ids'], ids => {
            return ids.push(item.id);
          })
      }
    ))
  }

  removeItem(item) {
    this.setState(({data}) => ({
      data : data
        .updateIn(['selected', 'items'], items => {
          return items.filter(it => it.id !== item.id);
        })
        .updateIn(['selected', 'ids'], ids => {
          return ids.filter(id => id !== item.id);
        })
    }))
  }

  toggle(item, selected) {
    if (selected) {
      this.removeItem(item);
    } else {
      this.addItem(item);
    }
  }

  onDrag(from, to) {
    this.setState(({data}) => ({
      data : data
        .updateIn(['selected', 'items'], items => {
          const itemFrom = items.get(from);
          const itemTo = items.get(to);
          return items
            .splice(from, 1, itemTo)
            .splice(to, 1, itemFrom);
        })
    }));
  }

  getValue() {
    const grid = this._grid ? this._grid.$value : [1, 1, 1, 1];
    const items = this.state.data.get('selected').toJS();

    const urls = items.items.map(item => generateImageUrlFromData(item));

    return {
      ...items,
      urls,
      grid
    }
  }
}

export default CustomPhoto;
