'use strict';

import React from 'react';
import state from './../../../js/mixins/state';

import Item from './waypoint/item';
import ItemMap from './waypoint/itemMap';
import ItemCards from './waypoint/itemCards';
import ItemText from './waypoint/itemText';
import ItemPhoto from './waypoint/itemPhoto';
import ItemTitle from './waypoint/itemTitle';
import ItemDropDown from './waypoint/itemDropDown';
import ItemCustomPhoto from './waypoint/itemCustomPhoto';
import ItemWeather from './waypoint/itemWeather';

import InputText from './../forms/inputTextArea'

class WaypointListItem extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      data : props.data
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (this._needSendDataToParent) {
      this._needSendDataToParent = false;
      this.__sendDataToParent();
    }
  }

  componentWillReceiveProps(newProps) {
    state(this, {
      data : newProps.data
    });
  }

  __sendDataToParent() {
    if (this.props.onChange)
      this.props.onChange(this.getValue(), this.props.index);
  }

  getDataFromChild(data, index) {
    this._needSendDataToParent = true;
    state(this, {data : data});
  }

  getValue() {
    return this.refs.item.getValue();
  }

  onDelete() {
    if (this.props.onDelete) {
      this.props.onDelete()
    }
  }

  sort(delta) {
    if (delta < 0 && this.props.onUp)
      this.props.onUp();
    else if (delta > 0 && this.props.onDown)
      this.props.onDown();
  }

  edit() {
    this.refs.item.edit();
  }

  render() {

    let inner, canUp = this.props.canUp, canDown = this.props.canDown;
    if (typeof this.state.data !== 'string') {
      switch (this.state.data.type) {
        case 'map':
        case 'maps':
        case 'm':
          inner = <ItemMap ref="item"
                           onChange={this.getDataFromChild.bind(this)} data={this.state.data}/>;
          break;
        case 'card':
        case 'cards':
        case 'c':
          inner =
            <ItemCards ref="item"
                       onChange={this.getDataFromChild.bind(this)} data={this.state.data}/>;
          break;
        case 'photo':
        case 'photos':
        case 'ph':
          inner =
            <ItemPhoto ref="item"
                       onChange={this.getDataFromChild.bind(this)} data={this.state.data}/>;
          break;
        case 'custom-photo':
        case 'custom-photos':
        case 'cp':
          inner = <ItemCustomPhoto ref="item"
                                   onChange={this.getDataFromChild.bind(this)} data={this.state.data}/>;
          break;
        case 'text':
          inner =
            <ItemText ref="item"
                      onChange={this.getDataFromChild.bind(this)}
                      data={this.state.data}/>;
          break;
        case 'weather':
          inner =
            <ItemWeather ref="item"
                         onChange={this.getDataFromChild.bind(this)}
                         data={this.state.data}/>;
          break;
        case 'drop-down':
        case 'dd':
          inner =
            <ItemDropDown ref="item"
                          onChange={this.getDataFromChild.bind(this)}
                          data={this.state.data}/>;
          break;
        case 'title':
        case 't':
          inner =
            <ItemTitle ref="item"
                       onChange={this.getDataFromChild.bind(this)}
                       data={this.state.data}/>;
          break;
        default:
          inner = <Item ref="item"
                        onChange={this.getDataFromChild.bind(this)}
                        data={this.state.data}/>;

      }
    } else {
      inner = <ItemText ref="item"
                        onChange={this.getDataFromChild.bind(this)}
                        data={{
                          type    : 'text',
                          options : this.state.data
                        }}/>
    }
    return (
      <div className="admin-waypoint-list_item">
        {inner}
      </div>
    );
  }
}

export default WaypointListItem;