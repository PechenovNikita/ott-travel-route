'use strict';

import React, {Component} from 'react';
import {Map} from 'immutable';

import styled from 'styled-components';

const $Row = styled.div`
  display: flex;
  flex-flow: row wrap;
  justify-content: stretch;
  align-items: stretch;
  
  > div{
    flex: 0 1 50%;
    box-sizing: border-box;
    padding: 5px;
    
    @media(min-width: 768px){
      flex: 0 1 33.33%;
    }
    
    @media(min-width: 1024px){
      flex: 0 1 25%;
    }
  }
`;

let items = [
  {
    name  : 'Выборочные фотографии',
    value : 'custom-photo'
  },
  {
    name  : 'Карта',
    value : 'map'
  },
  {
    name  : 'Текст',
    value : 'text'
  },
  {
    name  : 'Заголовок',
    value : 'title'
  },
  {
    name  : 'Карточки',
    value : 'cards'
  },
  {
    name  : 'Погода',
    value : 'weather'
  },
  {
    name  : 'Выпадающий список',
    value : 'drop-down'
  },
  {
    name  : 'Фотографии',
    value : 'photo'
  }
];

class WayPointSelectType extends Component {

  state = {
    data : Map({
      selected : null,
      count    : 1,
    })
  };

  add = (event) => {
    event.stopPropagation();
    event.preventDefault();

    const type = this.state.data.get('selected', null);
    let count = this.state.data.get('count', 1);

    if (!type)
      return;
    if (count < 1) {
      count = 1;
    }

    this.props.onAdd({
      type,
      count
    });
  };

  select = (type) => {
    this.setState(({data}) => ({data : data.set('selected', type)}));
  };

  render() {
    const selected = this.state.data.get('selected', null);
    return (
      <div className="admin-waypoint-list-modal">
        <h4>Добавить элемент</h4>
        <div className="admin-waypoint-list-modal__add">
          <$Row>

            {items.map((item) => (
              <div key={item.value}>
                <div className={`card ${item.value === selected ? 'card-success card-inverse' : ''}`}
                     onClick={() => this.select(item.value)}>
                  <div className="card-block">
                    <h6 className="mb-0">{item.name}</h6>
                  </div>
                </div>
              </div>
            ))}

          </$Row>
        </div>
        <div className="admin-waypoint-list-modal__save">
          <div className="input-group">
            <input type="number" className="form-control"
                   placeholder="Количество полей"
                   onChange={(event) => {
                     const value = event.currentTarget.value;
                     this.setState(({data}) => ({data : data.set('count', value)}))
                   }}
                   value={this.state.data.get('count', 1)}/>
            <div className="input-group-btn">
              <button className="btn btn-secondary"
                      disabled={!this.state.data.get('selected', null)}
                      onClick={this.add}><i
                className="fa fa-plus"/><span>Добавить</span>
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default WayPointSelectType;
