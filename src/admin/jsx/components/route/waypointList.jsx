'use strict';

import React from 'react';
import state from './../../../js/mixins/state';
import WaypointListItem from './waypointListItem';
import NewModal from './../../blocks/newModal';

import Select from '../forms/select';
import WayPointSelectType from './WayPointSelectType';

class WaypointList extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      items  : props.items,
      create : false
    };

    this._refs = [];

    this._id = 'routeDescList_' + Date.now();
  }

  componentWillReceiveProps(newProps) {
    state(this, {
      items  : newProps.items,
      create : false
    });
  }

  getValue() {
    return this._refs.map(function (ref) {
      return this.refs[ref].getValue();
    }, this);
  }

  del(index) {
    if (confirm('Вы уверены что хотите удалить данные?')) {
      let data = this.getValue();
      data.splice(index, 1);
      state(this, {items : data});
    }
  }

  sort(index, delta) {
    let data = this.getValue();
    let item = data.splice(index, 1);
    data.splice(index + delta, 0, item[0]);
    state(this, {items : data});
  }

  __onEditItem(ref) {
    this.refs[ref].edit();
  }

  add() {
    state(this, {
      create : true,
      items  : this.getValue()
    });
  }

  __renderItemButtons(index, ref, disableUp, disableDown) {
    return (<div className="btn-group btn-group-sm btn-group-vertical">


      <button className={'btn' + (!disableUp ? ' btn-info' : ' btn-secondary')}
              disabled={disableUp}
              onClick={disableUp ? () => {} : this.sort.bind(this, index, -1)}><i
        className="fa fa-chevron-up"/></button>

      <button className={'btn' + (!disableDown ? ' btn-info' : ' btn-secondary')}
              disabled={disableDown}
              onClick={disableDown ? () => {} : this.sort.bind(this, index, 1)}><i
        className="fa fa-chevron-down"/></button>

    </div>);
  }

  __renderItem(item, index, all) {
    let ref = 'desc_' + this._id + '_' + index;
    this._refs.push(ref);

    return (
      <div key={this._id + '_' + index} className="admin-desc-list__item">
        <div className="admin-desc-list_item">
          <table className="admin-desc-list_item__table">
            <tbody>
            <tr>
              <td className="admin-desc-list_item__table__sort">
                {this.__renderItemButtons(index, ref, !(index > 0), !(index < all.length - 1))}
              </td>
              <td className="admin-desc-list_item__table__edit">
                <button className="btn btn-primary btn-lg" onClick={this.__onEditItem.bind(this, ref)}><i
                  className="fa fa-edit"/></button>
              </td>
              <td className="admin-desc-list_item__table__view">
                <WaypointListItem ref={ref} data={item}/>
              </td>
              <td className="admin-desc-list_item__table__remove">
                <button className="btn btn-danger" onClick={this.del.bind(this, index)}><i
                  className="fa fa-trash-o"/></button>
              </td>
            </tr>
            </tbody>
          </table>
        </div>
      </div>
    )
  }

  __renderModalContent() {
    return (
      <WayPointSelectType key={this._id + '_edit'} onAdd={this.addNewType}/>
      // <div key={this._id + '_edit'} className="admin-waypoint-list-modal">
      //   <div className="admin-waypoint-list-modal__add">
      //     {/*<Select ref="select_new_type" items={items} value="map"/>*/}
      //     {items.forEach((item)=>(
      //       <button className="btn btn-lg btn-secondary" key={item.value}>{item.name}</button>
      //     ))}
      //   </div>
      //   <div className="admin-waypoint-list-modal__save">
      //     <button className="btn btn-secondary"
      //             onClick={this.selectNewType.bind(this)}><i
      //       className="fa fa-plus"/><span>Добавить</span>
      //     </button>
      //   </div>
      // </div>
    );
  }

  addNewType = ({type, count}) => {
    let items = this.getValue();

    while (count > 0) {
      items.push({
        type    : type,
        options : {}
      });
      count = count - 1;
    }

    state(this, {
      items  : items,
      create : false
    });
  };

  closeModal() {
    state(this, {create : false});
  }

  render() {

    this._refs = [];
    let items = this.state.items.map(this.__renderItem.bind(this), this);

    let create = (this.state.create
      ? (
        <NewModal ref="modal" title={this._title} onClose={this.closeModal.bind(this)}>
          {this.__renderModalContent()}
        </NewModal>)
      : '');

    return (
      <div className="admin-waypoint-list">
        <h5>Элементы</h5>
        <div className="admin-waypoint-list__inner">
          {items}
        </div>
        {create}
        <div className="admin-waypoint-list__footer">
          <button className="btn btn-secondary btn-lg" onClick={this.add.bind(this)}><i
            className="fa fa-plus"/><span>Добавить</span>
          </button>
        </div>
      </div>
    );
  }
}

export default WaypointList;
