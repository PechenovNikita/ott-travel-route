'use strict';

import React from 'react';
import Item from './item';
import state from './../../../../js/mixins/state';
import textCount from './../../../../js/mixins/textCount';
import WaypointList from './../waypointList';
import InputText from './../../forms/input';

function getNames(type) {
  switch (type) {
    case 'map':
    case 'maps':
    case 'm':
      return 'карта';
    case 'card':
    case 'cards':
    case 'c':
      return 'карточки';
    case 'photo':
    case 'photos':
    case 'ph':
      return 'фотографии';
    case 'custom-photo':
    case 'custom-photos':
    case 'cp':
      return 'выборочные фотографии';
    case 'drop-down':
    case 'dd':
      return 'выпадающий список';
    case 'title':
    case 't':
      return 'заголовок';
    case 'text':
    default:
      return 'текст';
  }
}

class WaipointItemDropDown extends Item {

  constructor(props) {
    super(props);
    let data = props.data;
    this.state.title = (data.title || '');

    let opt = this.state.options;

    if (!Array.isArray(opt))
      this.state.options = [];

    this._title = 'Выпадающий список';
  }

  __onTitleChange(){
    // console.l
  }

  __renderEditorContent() {
    return (
      <div key={this._id + '_edit_content'} className="admin-waypoint-item-modal__edit">
        <InputText ref="title" value={this.state.title} placeholder="Заголовок"/>
        <WaypointList ref="options" items={this.state.options}/>
      </div>
    );
  }

  save() {
    let opt = this.refs.options.getValue()
      , title = this.refs.title.value;
    state(this, {
      data    : {
        type    : 'drop-down',
        title   : title,
        options : opt,
      },
      title   : title,
      options : opt
    });
  }

  componentWillReceiveProps(newProps) {
    let data = newProps.data || {};
    state(this, {
      data    : data,
      type    : data.type,
      title   : data.title || "",
      options : (Array.isArray(data.options) ? data.options : [])
    });
  }

  renderTitle() {
    return <span><i className="fa fa-list"/> {this._title} : <strong>{this.state.title}</strong></span>
  }

  renderDesc() {

    let types = this.state.options.map(function (item) {
      return getNames(item.type);
    });

    return (
      <span>содержит {this.state.options.length} {textCount(this.state.options.length, 'вложение', 'вложения', 'вложений')}
        ({types.join(', ')})</span>);
  }
}

export default WaipointItemDropDown;