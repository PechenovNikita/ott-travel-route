'use strict';

import React from 'react';
import Item from './item';
import state from '../../../../js/mixins/state';
import Weather from '../../Weather';

class WaypointItemWeather extends Item {

  constructor(props) {
    super(props);
    this._title = 'Погода';
  }

  __renderEditorContent() {
    return (
      <div key={this._id + '_edit_content'} className="admin-waypoint-item-modal__edit">
        <Weather ref="weather" options={this.state.options}/>
      </div>
    );
  }

  save() {
    let opt = this.refs.weather.getValue();
    state(this, {
      data    : {
        type    : 'weather',
        options : opt
      },
      options : opt
    });
  }

  renderTitle() {
    return <span><i className="fa fa-cloud"/> {this._title}</span>;
  }

  renderDesc() {
    let str = [<span key="0">Погода </span>], options = this.state.options;

    if (options.text) {
      str.push([<span key="1">{options.text}</span>])
    }

    return str;
  }
}

export default WaypointItemWeather;