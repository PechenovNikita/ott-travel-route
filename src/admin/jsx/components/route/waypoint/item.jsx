'use strict';

import React from 'react';
import state from './../../../../js/mixins/state';

import Modal from './../../../blocks/modal';
import NewModal from './../../../blocks/newModal';
import JsonArea from './../../../components/forms/jsonArea';

/**
 * @class WaypointItem
 * @property {{type: string, options: *}} state
 * @property {{onChange: function=, type: string, options: *}} props
 */
class WaypointItem extends React.Component {

  constructor(props) {
    super(props);

    this._title = 'unknown';

    let data = props.data || {};

    this.state = {
      data    : data,
      type    : data.type,
      options : data.options || {},
      modal   : false,
      update  : false
    };

    this._id = 'waypointItem_' + Date.now();

    // this.renderEditor();

  }

  // componentDidUpdate() {
  //   // if (this._needSendDataToParent) {
  //   //   this._needSendDataToParent = false;
  //   //   this.__sendDataToParent();
  //   // }
  // }

  componentWillReceiveProps(newProps) {
    let data = newProps.data || {};
    state(this, {
      modal   : false,
      data    : data,
      type    : data.type,
      options : data.options || {}
    });
  }

  /**
   *
   * @returns {{type: *, options: *}}
   */
  getValue() {
    return this.state.data;
  }

  __renderEditorContent() {
    return (
      <div key={this._id + '_edit_content'} className="admin-waypoint-item-modal__edit">
        <JsonArea ref="form" onChange={this.onEditorChanged.bind(this)} value={this.state.data}/>
      </div>
    );
  }

  onEditorChanged(){

  }

  __renderModalContent() {
    return (
      <div key={this._id + '_edit'} className="admin-waypoint-item-modal">
        {this.__renderEditorContent()}
        <div className="admin-waypoint-item-modal__save">
          <button className="btn btn-secondary"
                  onClick={this.beforeSave.bind(this)}><i className="fa fa-save"/><span>Сохранить</span>
          </button>

          <button className="btn btn-secondary"
                  onClick={this.beforeSaveAndClose.bind(this)}><i
            className="fa fa-hdd-o"/><span>Сохранить и закрыть</span>
          </button>
        </div>
      </div>
    );
  }

  edit() {
    state(this, {
      modal  : true,
      update : true
    });
  }

  closeModal() {
    state(this, {
      modal  : false,
      update : true
    });
  }

  beforeSave() {
    this.save();
  }

  beforeSaveAndClose() {
    this.save();
    if(this.refs.modal)
      this.refs.modal.close();
  }

  save() {
    let val = this.refs.form.getValue();
    state(this, {
      type    : val.type,
      options : val.options
    });
  }

  renderTitle() {
    return <span><i className="fa fa-question"/> Неизвестный тип данных: <storng> {this.state.type}</storng></span>
  }

  renderDesc() {
    return this._title + ' json data';
  }

  render() {

    // if (this._needUpdateEditor)
    //   this.renderEditor();

    let modal = (this.state.modal
      ? (
      <NewModal ref="modal" title={this._title} onClose={this.closeModal.bind(this)}>
        {this.__renderModalContent()}
      </NewModal>)
      : '');

    return (
      <div className="admin-waypoint-item">
        <div className="blockquote mb-0" style={{fontSize : '.9em'}}>
          <p className="mb-0">{this.renderTitle()}</p>
          <p className="blockquote-footer mb-0">{this.renderDesc()}</p>
        </div>
        {modal}
      </div>
    );
  }
}

export default WaypointItem;