'use strict';

import React from 'react';
import Item from './item';
import state from './../../../../js/mixins/state';
import textCount from './../../../../js/mixins/textCount';
import CardsList from './cards/cardsList';

class WaypointItemCards extends Item {

  constructor(props) {
    super(props);
    this._title = 'Карточки'
  }


  save() {
    let opt = this.refs.cards.getValue();
    state(this, {
      data    : {
        type    : 'cards',
        options : opt,
      },
      options : opt
    });
  }

  __renderEditorContent() {
    return (
      <div key={this._id + '_edit_content'} className="admin-waypoint-item-modal__edit">
        <CardsList ref="cards" items={this.state.options}/>
      </div>
    );
  }

  renderTitle() {
    return <span><i className="fa fa-id-card-o"/> {this._title}</span>
  }

  renderDesc() {
    return (
      <span>содержит {this.state.options.length} {textCount(this.state.options.length, 'карточку', 'карточки', 'карточек')}</span>);
  }

}

export default WaypointItemCards;