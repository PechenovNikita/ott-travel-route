'use strict';

import React from 'react';
import Item from './item';
import Modal from './../../../blocks/modal';
import MapBox from './../../map';
import state from './../../../../js/mixins/state';

class WaypointItemMap extends Item {
  constructor(props) {
    super(props);
    this._title = 'Карта';
  }

  __renderEditorContent() {
    console.log('__renderEditorContent', this.state.options);
    return (
      <div key={this._id + '_edit_content'} className="admin-waypoint-item-modal__edit">
        <MapBox ref="map" height="200"
                direction={this.state.options.direction}
                poi={this.state.options.poi}
                point={this.state.options.point}/>
      </div>
    );
  }

  save() {
    let opt = this.refs.map.getValue();
    state(this, {
      data    : {
        type    : 'map',
        options : opt
      },
      options : opt
    });
  }

  renderTitle() {
    return <span><i className="fa fa-globe"/> {this._title}</span>;
  }

  renderDesc() {
    let str = '';

    if (this.state.options.direction) {

      let locationOrigin = this.state.options.direction.origin
        , locationDestination = this.state.options.direction.destination;
      str =
        <span>маршрут от <strong>
          {locationOrigin && typeof locationOrigin == 'string' ? locationOrigin : JSON.stringify(locationOrigin)}
        </strong> до <strong>
          {locationDestination && typeof locationDestination == 'string' ? locationDestination : JSON.stringify(locationDestination)}
        </strong></span>

    } else if (this.state.options.point) {
      let location = this.state.options.point.location;
      if (typeof location === 'string') {
        location = (<strong>{location}</strong>);
      } else if (location.lat && location.lng) {
        location = (<strong>({location.lat}, {location.lng})</strong>);
      } else if (Array.isArray(location)) {
        location = (<strong>{location.map(function (point) {
          if (typeof point === 'string')
            return point;
          else if (point.lat && point.lng) {
            return '(' + point.lat + ',' + point.lng + ')';
          }
        }).join(', ')}</strong>);
      }
      str = <span>точка на карте - {location}</span>
    } else if (this.state.options.poi) {
      console.log(this.state.options.poi);
      let sight = this.state.options.poi.sight ? this.state.options.poi.sight.length : 0;
      let hotels = this.state.options.poi.hotels ? this.state.options.poi.hotels.length : 0;
      return <span>Достопримечательности: (мест {sight}, отелей {hotels})</span>
    }

    return str;
  }

}

export default WaypointItemMap;
