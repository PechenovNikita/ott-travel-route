'use strict';

import React from 'react';

import Item from './item';
import state from './../../../../js/mixins/state';
import InputTextArea from '../../forms/inputTextArea';

class WaypointItemText extends Item {

  constructor(props) {
    super(props);
    this._title = 'Текст';
  }

  __renderEditorContent() {
    return (
      <div key={this._id + '_edit_content'} className="admin-waypoint-item-modal__edit">
        <InputTextArea ref="area" onChange={this.onEditorChanged.bind(this)} rows="10" value={this.state.options}/>
      </div>
    );
  }

  save() {
    let val = this.refs.area.getValue();
    state(this, {
      data    : {
        type    : 'text',
        options : val || ''
      },
      options : val || ''
    });
  }

  renderTitle() {
    return <span><i className="fa fa-paragraph"/> {this._title}</span>;
  }

  renderDesc() {
    if(typeof this.state.options !== 'string')
      return <span>Пока пусто</span>;
    return <span>{this.state.options}</span>;
  }

}

export default WaypointItemText;