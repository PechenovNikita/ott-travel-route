'use strict';

import {Component} from 'react';
import CardViewOther from '../view/cardOther';
import CardEdit from './card';

class CardEditOther extends CardEdit {

  __renderView() {
    return (
      <div style={{width : 600}} className="list-group">
        <div className="list-group-item">
          <CardViewOther ref="form" data={this.state.data}/>
        </div>
      </div>
    );
  }

}

export default CardEditOther;