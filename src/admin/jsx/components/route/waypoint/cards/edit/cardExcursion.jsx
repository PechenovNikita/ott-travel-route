'use strict';

import {Component} from 'react';
import CardViewExcursion from '../view/cardExcursion';
import CardEdit from './card';

class CardEditExcursion extends CardEdit {

  __renderView() {
    return (
      <div style={{width : 600}} className="list-group">
        <div className="list-group-item">
          <CardViewExcursion ref="form" data={this.state.data}/>
        </div>
      </div>
    );
  }

}

export default CardEditExcursion;