'use strict';

import {Component} from 'react';
import CardViewCar from '../view/cardCar';
import CardEdit from './card';

class CardEditCar extends CardEdit {

  __renderView() {
    return (
      <div style={{width : 600}} className="list-group">
        <div className="list-group-item">
          <CardViewCar ref="form" data={this.state.data}/>
        </div>
      </div>
    );
  }

}

export default CardEditCar;