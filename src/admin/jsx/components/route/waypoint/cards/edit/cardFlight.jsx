'use strict';

import {Component} from 'react';
import CardEdit from './card';
import CardViewFlight from './../view/cardFlight';

import OttFormFlight from './../../../../forms/ott/formFlight';
import NewModal from './../../../../../blocks/newModal';

import state from './../../../../../../js/mixins/state';

class CardEditFlight extends CardEdit {

  constructor(props) {
    super(props);

    this.state.create = false;
  }

  componentDidUpdate(prevProps, prevState) {
    // if (this._needUpdate) {
    //   this._needUpdate = false;
    //   state(this, {
    //     data : this.getValue()
    //   });
    // }
  }

  getValue() {
    return this.refs.form.getValue();
  }

  onEditorChanged(val) {

    state(this, {
      data : val
    });

  }

  // __renderEdit() {
  //   return (
  //     <JsonArea ref="form" onChange={this.onEditorChanged.bind(this)} value={this.state.data}/>
  //   );
  // }

  search() {
    state(this, {create : true});
  }

  __renderModalContent() {
    return (
      <div className="admin-cards-list-modal">
        <div className="admin-cards-list-modal__add">
          <OttFormFlight ref="search_form"/>
        </div>
        <div className="admin-cards-list-modal__save">
          <button className="btn btn-secondary"
                  onClick={this.selectSearch.bind(this)}><i
            className="fa fa-plus"/><span>Добавить</span>
          </button>
        </div>
      </div>
    )
  }

  selectSearch() {
    let val = (this.refs.search_form.getValue());
    this.refs.modal.close();
    val.title = val.title || 'Авиаперелет';
    state(this, {
      data : val
    });
  }

  closeModal() {
    state(this, {
      create : false
    });
  }

  __renderView() {

    let create = (this.state.create
      ? (
      <NewModal ref="modal" title="Поиск маршрута" onClose={this.closeModal.bind(this)}>
        {this.__renderModalContent()}
      </NewModal>)
      : '');

    return (
      <div className="row">
        <div className="col col-9">
          <div style={{width : 600}} className="list-group">
            <div className="list-group-item">
              <CardViewFlight ref="form" data={this.state.data}/>
            </div>
          </div>
        </div>
        <div className="col col-3">
          <button className="btn btn-secondary" onClick={this.search.bind(this)}>Найти маршрут</button>
          {create}
        </div>
      </div>
    );
  }
}

export default CardEditFlight;
