'use strict';

import {Component} from 'react';
import CardEdit from './card';
import CardViewHotel from './../view/cardHotel';

import OttFormHotel from './../../../../forms/ott/formHotel';
import NewModal from './../../../../../blocks/newModal';

import state from './../../../../../../js/mixins/state';

class CardEditHotel extends CardEdit {

  constructor(props) {
    super(props);

    this.state.create = false;
  }

  componentDidUpdate(prevProps, prevState) {
  }

  getValue() {
    return this.refs.form.getValue();
  }

  onEditorChanged(val) {

    state(this, {
      data : val
    });

  }

  // __renderEdit() {
  //   return (
  //     <JsonArea ref="form" onChange={this.onEditorChanged.bind(this)} value={this.state.data}/>
  //   );
  // }

  search() {
    state(this, {create : true});
  }

  __renderModalContent() {
    return (
      <div className="admin-cards-list-modal">
        <div className="admin-cards-list-modal__add">
          <OttFormHotel ref="search_form"/>
        </div>
        <div className="admin-cards-list-modal__save">
          <button className="btn btn-secondary"
                  onClick={this.selectSearch.bind(this)}><i
            className="fa fa-plus"/><span>Добавить</span>
          </button>
        </div>
      </div>
    )
  }

  selectSearch() {
    let val = (this.refs.search_form.getValue());
    this.refs.modal.close();
    state(this, {
      data : val
    });
  }

  closeModal() {
    state(this, {
      create : false
    });
  }

  __renderView() {

    let create = (this.state.create
      ? (
      <NewModal ref="modal" title="Поиск отеля" onClose={this.closeModal.bind(this)}>
        {this.__renderModalContent()}
      </NewModal>)
      : '');

    return (
      <div className="row">
        <div className="col col-9">
          <div style={{width : 600}} className="list-group">
            <div className="list-group-item">
              <CardViewHotel ref="form" data={this.state.data}/>
            </div>
          </div>
        </div>
        <div className="col col-3">
          <button className="btn btn-secondary" onClick={this.search.bind(this)}>Найти отель</button>
          {create}
        </div>
      </div>
    );
  }
}

export default CardEditHotel;
