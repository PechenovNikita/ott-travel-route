'use strict';

import {Component} from 'react';
import JsonArea from './../../../../forms/jsonArea';
import state from './../../../../../../js/mixins/state';
import CardView from '../view/card';

class CardEdit extends Component {

  constructor(props) {
    super(props);

    this.state = {
      data : props.data
    }
  }

  // componentWillMount() {}

  // componentDidMount() {}

  // componentWillUnmount() {}

  // componentDidUpdate(prevProps, prevState) {}

  componentWillReceiveProps(newProps) {
    state(this, {
      data : newProps.data
    });
  }

  getValue() {
    return this.refs.form.getValue();
  }

  onEditorChanged(val) {
    state(this, {
      data : val
    });
  }

  __renderEdit() {
    return (<JsonArea ref="form" onRefresh={this.onEditorChanged.bind(this)} onChange={this.onEditorChanged.bind(this)}
                      value={this.state.data}/>);
  }

  __renderView() {
    return (
      <div style={{width : 600}} className="list-group">
        <div className="list-group-item">
          <CardView ref="form" data={this.state.data}/>
        </div>
      </div>
    );
  }

  render() {
    return (
      <div className="admin-card-edit">
        <div className="admin-card-edit__form">
          <div className="form-group">
            {this.__renderEdit()}
          </div>
        </div>
        <div className="form-group">
          <div className="admin-card-edit__view">
            {this.__renderView()}
          </div>
        </div>
      </div>
    );
  }
}

export default CardEdit;