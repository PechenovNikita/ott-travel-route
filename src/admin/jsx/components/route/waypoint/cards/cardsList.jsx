'use strict';

import React from 'react';
import CardsListItem from './cardsListItem';
import state from './../../../../../js/mixins/state';
import Select from './../../../forms/select';
import NewModal from './../../../../blocks/newModal';
import OttFormHotel from '../../../forms/ott/formHotel';

const default_excursion_Type = {
  type  : 'excursion',
  image : {
    title       : 'Экскурсия ...',
    description : '...',
    src         : 'http://...'
  },
  price : {
    title : [
      '...',
      '...'
    ],
    btn   : {
      href  : 'http://...',
      title : '12 342 %rub%'
    }
  }
};

const default_car_Type = {
  'type'  : 'car',
  'image' : {
    'title' : '...',
    'src'   : 'http://...'
  },
  'price' : {
    'title' : [
      '...',
      '...'
    ],
    'btn'   : {
      'href'  : 'http://...',
      'title' : '1 234 %rub%'
    }
  }
};

const default_other_Type = {
  'type'  : 'other',
  'other' : {
    'title'     : 'Поезд ...',
    'direction' : '... — ...',
    'detail'    : [
      {
        'title' : '...',
        'time'  : '...'
      },
      {
        'title' : '...',
        'time'  : '...'
      }
    ]
  },
  'price' : {
    'title' : '...',
    'btn'   : {
      'href'  : 'http://...',
      'title' : '49$/чел'
    }
  },
  'back'  : {
    'title'      : '...',
    'items'      : [
      '... - ...'
    ],
    'close_text' : 'Др. сторона'
  }
};

class CardsList extends React.Component {

  constructor(props) {
    super(props);

    let items = props.items;
    items = (Array.isArray(items) ? items : []);

    this.state = {
      items     : items,
      create    : false,
      addHotels : false,
    };

    this._refs = [];
  }

  componentWillReceiveProps(newProps) {
    let items = newProps.items;
    items = (Array.isArray(items) ? items : []);
    state(this, {
      items  : items,
      create : false
    });
  }

  del(index) {
    if (confirm('Вы уверены что хотите удалить данные?')) {
      let data = this.getValue();
      data.splice(index, 1);
      state(this, {items : data});
    }
  }

  sort(index, delta) {
    let data = this.getValue();
    let item = data.splice(index, 1);
    data.splice(index + delta, 0, item[0]);
    state(this, {items : data});
  }

  __onEditItem(ref) {
    if (this.refs[ref]) {
      this.refs[ref].edit();
    }
  }

  __renderItemButtons(index, ref, disableUp, disableDown) {
    return (<div className="btn-group btn-group-sm">
      <button className="btn btn-primary btn-lg" onClick={this.__onEditItem.bind(this, ref)}><i
        className="fa fa-edit"/></button>

      <button className={'btn' + (!disableUp ? ' btn-info' : ' btn-secondary')}
              disabled={disableUp}
              onClick={disableUp ? () => {} : this.sort.bind(this, index, -1)}><i
        className="fa fa-chevron-up"/></button>

      <button className={'btn' + (!disableDown ? ' btn-info' : ' btn-secondary')}
              disabled={disableDown}
              onClick={disableDown ? () => {} : this.sort.bind(this, index, 1)}><i
        className="fa fa-chevron-down"/></button>
      <button className="btn btn-danger" onClick={this.del.bind(this, index)}><i
        className="fa fa-trash-o"/></button>
    </div>);
  }

  getValue() {
    return this._refs.map(function (ref) {
      return this.refs[ref].getValue();
    }, this);
  }

  __renderModalContent() {
    let items = [
      {
        name  : 'Перелет',
        value : 'flight'
      },
      {
        name  : 'Отель',
        value : 'hotel'
      },
      {
        name  : 'Экскурсия',
        value : 'excursion'
      },
      {
        name  : 'Машина на прокат',
        value : 'car'
      },
      {
        name  : 'Другое',
        value : 'other'
      }
    ];
    return (
      <div key={this._id + '_edit'}
           className="admin-cards-list-modal">
        <div className="admin-cards-list-modal__add">
          <Select ref="select_new_type" items={items} value="flight"/>
        </div>
        <div className="admin-cards-list-modal__save">
          <button className="btn btn-secondary"
                  onClick={this.selectNewType.bind(this)}><i
            className="fa fa-plus"/><span>Добавить</span>
          </button>
        </div>
      </div>
    );
  }

  addArrayHotels() {
    state(this, {
      addHotels : true
    });
  }

  selectNewType() {
    if (this.refs.select_new_type) {
      let type = this.refs.select_new_type.getValue();
      let items = this.getValue();

      if (type === 'excursion')
        items.push(default_excursion_Type);
      else if (type === 'car')
        items.push(default_car_Type);
      else if (type === 'other')
        items.push(default_other_Type);
      else
        items.push({
          type : type,
        });

      state(this, {
        items  : items,
        create : false
      });
    }
  }

  closeModal() {
    state(this, {
      create    : false,
      addHotels : false
    });
  }

  add() {
    state(this, {create : true});
  }

  selectSearch() {
    let items = this.getValue();
    if (this.refs.search_form && Array.isArray(items)) {
      console.log('select search', this.refs.search_form.getValue());

      items = items.concat(this.refs.search_form.getValue());

      state(this, {
        items     : items,
        create    : false,
        addHotels : false
      });
    } else {
      this.closeModal();
    }

  }

  render() {
    this._refs = [];
    let items = this.state.items.map(function (item, index, all) {
      let ref = 'item_' + index;
      this._refs.push(ref);
      return (
        <div key={index} className="col-lg-6 admin-cards-list__item">
          <div className="admin-cards-list__item__over">
            <div className="admin-cards-list__demo">
              <CardsListItem ref={ref} data={item}/>
            </div>

            <div className="admin-cards-list__edit">
              {this.__renderItemButtons(index, ref, !(index > 0), !(index < all.length - 1))}
            </div>
          </div>
        </div>
      );
    }, this);

    let create = (this.state.create
      ? (
        <NewModal ref="modal"
                  title={this._title}
                  onClose={this.closeModal.bind(this)}>
          {this.__renderModalContent()}
        </NewModal>)
      : null);

    let addHotels = (this.state.addHotels)
      ? (
        <NewModal
          title={'Добавить несколько отелей'}
          onClose={this.closeModal.bind(this)}>
          <div className="admin-cards-list-modal">
            <div className="admin-cards-list-modal__add">
              <OttFormHotel multi={true} ref="search_form"/>
            </div>
            <div className="admin-cards-list-modal__save">
              <button className="btn btn-secondary"
                      onClick={this.selectSearch.bind(this)}><i
                className="fa fa-plus"/><span>Добавить</span>
              </button>
            </div>
          </div>
        </NewModal>
      ) : null;

    return (
      <div className="admin-cards-list">
        <div className="row">
          {items}
        </div>
        {create}
        {addHotels}
        <div className="admin-cards-list__add">
          <button className="btn btn-secondary" onClick={this.add.bind(this)}><i
            className="fa fa-plus"/><span>Добавить</span></button>
          <button className="btn btn-secondary"
                  onClick={this.addArrayHotels.bind(this)}><i
            className="fa fa-plus"/><span>Добавить несколько отелей</span>
          </button>
        </div>
      </div>
    );
  }
}

export default CardsList;