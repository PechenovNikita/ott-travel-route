'use strict';

import React from 'react';
import state from './../../../../../js/mixins/state';
import CardView from './view/card';
import CardViewFlight from './view/cardFlight';
import CardViewHotel from './view/cardHotel';
import CardViewExcursion from './view/cardExcursion';
import CardViewCar from './view/cardCar';
import CardViewOther from './view/cardOther';


class CardsListItem extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      data : props.data || {}
    }
  }

  // componentWillMount() {}
  //
  // componentDidMount() {}
  //
  // componentWillUnmount() {}
  //
  // componentDidUpdate(prevProps, prevState) {}

  componentWillReceiveProps(newProps) {
    state(this, {
      data : newProps.data || []
    });
  }

  getValue(){
    return this.refs.card.getValue();
  }
  edit(){
    this.refs.card.edit();
  }

  render() {

    let cardData = this.state.data
      , cardView;

    switch (cardData.type) {
      case 'flight':
        if(!cardData.title){
          cardData.title = 'Авиаперелет';
        }
        cardView = <CardViewFlight ref="card" data={cardData}/>;
        break;
      case 'hotel':
        cardView = <CardViewHotel ref="card" data={cardData}/>;
        break;
      case 'excursion':
        cardView = <CardViewExcursion ref="card" data={cardData}/>;
        break;
      case 'car':
        cardView = <CardViewCar ref="card" data={cardData}/>;
        break;
      case 'other':
        cardView = <CardViewOther ref="card" data={cardData}/>;
        break;
      default:
        cardView = <CardView ref="card" data={cardData}/>
    }

    return (
      <div className="admin-cards-list-item">
        <small className="text-xs-center">Внешний вид отличается от того что будет на сайте</small>
        {cardView}
      </div>
    );
  }
}

export default CardsListItem;