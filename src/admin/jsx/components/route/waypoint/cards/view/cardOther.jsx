'use strict';

import CardView from './card';
import {CardLine} from './card';
import CardEditOther from './../edit/cardOther';

class CardViewOther extends CardView {

  __renderEditorContent() {
    return (
      <div key={this._id + '_edit_content'} className="admin-waypoint-item-modal__edit">
        <CardEditOther ref="form" data={this.state.data} onChange={this.onEditorChanged.bind(this)}/>
      </div>
    );
  }

  __renderFrontSide() {
    let other = this.state.data.other || {};

    let direction = other.direction || ''
      , title = other.title || ''
      , detail = other.detail || [];

    let det = detail.map(function (d, i) {
      return (<CardLine key={"detail_" + i} left={d.title} right={d.time} grey={true}/>)
    }, this);

    return (
      <div className="block-card__other">
        <div className="block-card__other__from-to">
          <div className="block-card__other__from-to__title">
            <span className="block-card__other__from-to__title__inner">{title}</span>
          </div>
          <div className="block-card__other__from-to__direction">
            <div className="block-card__other__from-to__direction__inner">{direction}</div>
          </div>
        </div>
        <div className="block-card__other__detail">
          {det}
        </div>
      </div>
    )
  }

}

export default CardViewOther;