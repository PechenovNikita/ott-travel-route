'use strict';

import CardView from './card';
import CardEditCar from './../edit/cardCar';

class CardViewCar extends CardView {

  __renderEditorContent() {
    return (
      <div key={this._id + '_edit_content'} className="admin-waypoint-item-modal__edit">
        <CardEditCar ref="form" data={this.state.data} onChange={this.onEditorChanged.bind(this)}/>
      </div>
    );
  }

  __renderFrontSide() {
    let image = this.state.data.image || {};

    let src = image.src || ''
      , title = image.title || ''
      , href = (this.state.data.price && this.state.data.price.btn ? this.state.data.price.btn.href : '');

    return (
      <div className="block-card__car">
        <div className="block-card__car__text">
          <div className="block-card__car__text__title">
            <span className="block-card__car__text__title__inner">{title}</span>
          </div>
          <div className="block-card__car__text__image">
            <a href={href} target="_blank">
              <img src={src} className="block-card__car__text__image__img"/>
            </a>
          </div>
        </div>
      </div>
    )
  }

}

export default CardViewCar;