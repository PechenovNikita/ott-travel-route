'use strict';

import CardView from './card';
import {CardLine} from './card';
import CardEditFlight from './../edit/cardFlight';

class CardViewFlight extends CardView {

  // constructor(props) {
  //   super(props);
  // }

  // componentWillMount() {}
  //
  // componentDidMount() {}
  //
  // componentWillUnmount() {}
  //
  // componentDidUpdate(prevProps, prevState) {}

  __renderEditorContent() {
    return (
      <div key={this._id + '_edit_content'} className="admin-waypoint-item-modal__edit">
        <CardEditFlight ref="form" data={this.state.data} onChange={this.onEditorChanged.bind(this)}/>
      </div>
    );
  }

  __renderFrontSide() {

    let from = this.state.data.from || {}
      , to = this.state.data.to || {}
      , across = this.state.data.across || {};

    let title = this.state.data.title || 'Авиаперелет';

    let acrossDiv = (across ? (
      <div className="block-card__flight__across">
        <CardLine left={across.title} right={across.time} grey={true}/>
      </div>
    ) : (
      <div className="block-card__flight__across empty">
        <CardLine left="" right="" grey={true}/>
      </div>
    ));

    return (
      <div className="block-card__flight">
        <div className="block-card__flight__from-to">
          <div className="block-card__flight__from-to__title">
            <span className="block-card__flight__from-to__title__inner">{title}</span>
          </div>
          <CardLine left={from.title} right={from.time}/>
          <CardLine left={to.title} right={to.time}/>
        </div>
        {acrossDiv}
      </div>
    )
  }

}

export default CardViewFlight;