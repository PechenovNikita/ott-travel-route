'use strict';

import CardViewImage from './cardImage';
import CardEditHotel from './../edit/cardHotel';



class CardViewHotel extends CardViewImage{
  __renderEditorContent() {
    return (
      <div key={this._id + '_edit_content'} className="admin-waypoint-item-modal__edit">
        <CardEditHotel ref="form" data={this.state.data} onChange={this.onEditorChanged.bind(this)}/>
      </div>
    );
  }
}

export default CardViewHotel;