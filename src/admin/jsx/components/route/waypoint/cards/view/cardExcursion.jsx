'use strict';

import CardViewImage from './cardImage';
import CardEditExcursion from './../edit/cardExcursion';


class CardViewExcursion extends CardViewImage{

  __renderEditorContent() {
    return (
      <div key={this._id + '_edit_content'} className="admin-waypoint-item-modal__edit">
        <CardEditExcursion ref="form" data={this.state.data} onChange={this.onEditorChanged.bind(this)}/>
      </div>
    );
  }
}

export default CardViewExcursion;