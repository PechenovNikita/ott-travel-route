'use strict';

import CardView from './card';
import {CardLine} from './card';

class CardViewImage extends CardView {

  // constructor(props) {
  //   super(props);
  // }

  // componentWillMount() {}
  //
  // componentDidMount() {}
  //
  // componentWillUnmount() {}
  //
  // componentDidUpdate(prevProps, prevState) {}

  __renderFrontSide() {

    let image = this.state.data.image || {};

    let src = image.src || ''
      , title = image.title || ''
      , description = image.description
      , href = (this.state.data.price && this.state.data.price.btn ? this.state.data.price.btn.href : '');

    let desc = (description ? (
      <div className="block-card__image__text__desc">
        <span className="block-card__image__text__desc__inner">{description}</span>
      </div>
    ) : null);

    return (
      <a href={href} target='_blank' className="block-card__image">
        <span className="block-card__image__back">
          <span className="block-card__image__back__overlay"/>
          <div style={{backgroundImage : 'url(' + src + ')'}} className="block-card__image__back__img"/>
        </span>
        <div className="block-card__image__text">
          <div className="block-card__image__text__title">
            <span className="block-card__image__text__title__inner">{title}</span>
          </div>
          {desc}
        </div>
      </a>
    )
  }

}

export default CardViewImage;