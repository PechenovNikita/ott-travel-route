'use strict';

import {Component} from 'react';
import state from './../../../../../../js/mixins/state';

import NewModal from './../../../../../blocks/newModal';
import CardEdit from './../edit/card';

export class CardLine extends Component {

  render() {
    let left = '', leftData = this.props.left;
    if (typeof leftData == 'string') {
      left = (<span className="block-card__line__left__inner">{leftData}</span>);
    } else if (leftData && leftData.href && leftData.title) {
      left = (
        <a href={leftData.href} target='_blank' className="block-card__line__left__link">
          <span className="block-card__line__left__link__inner">{leftData.title}</span>
        </a>);
    } else if (Array.isArray(leftData)) {
      left = leftData.map(function (text, index) {
        return (
          <span key={index} className="block-card__line__left__inner">{text}</span>
        )
      }, this);
    }

    let right = '', rightData = this.props.right;

    if (!this.props.btn) {
      right = (<span className="block-card__line__right__inner">{rightData}</span>);
    } else {
      right = (
        <a href={this.props.btn.href} target='_blank' className="btn btn-card">
          <span className="btn__inner">{this.props.btn.cost}</span>
        </a>
      );
    }
    return (
      <div className={'block-card__line' + (this.props.grey ? ' grey' : '') + (this.props.btn ? ' with-btn' : '')}>
        <div className="block-card__line__left">
          {left}
        </div>
        <div className="block-card__line__right">
          {right}
        </div>
      </div>
    )
  }
}

export class CardLineV2 extends Component {
  render() {
    let left = '', leftData = this.props.left;
    if (typeof leftData == 'string') {
      left = (<span className="block-card__line__left__inner">{leftData}</span>);
    } else if (leftData && leftData.href && leftData.title) {
      left = (
        <a href={leftData.href} target='_blank' className="block-card__line__left__link">
          <span className="block-card__line__left__link__inner">{leftData.title}</span>
        </a>);
    } else if (Array.isArray(leftData)) {
      left = leftData.map(function (text, index) {
        return (
          <span key={index} className="block-card__line__left__inner">{text}</span>
        )
      }, this);
    }

    return (
      <div className={'block-card__line v2 ' + (this.props.grey ? ' grey' : '') + (this.props.btn ? ' with-btn' : '')}>
        <div className="block-card__line__left">
          {left}
        </div>
        <div className="block-card__line__right">
          <span className="block-card__line__right__inner">{this.props.right}</span>
        </div>
      </div>
    )
  }
}

class CardView extends Component {

  constructor(props) {
    super(props);

    this.state = {
      data : props.data || {},
      edit : false
    };
  }

  componentWillMount() {}

  componentDidMount() {}

  componentWillUnmount() {}

  componentDidUpdate(prevProps, prevState) {}

  componentWillReceiveProps(newProps) {
    state(this, {
      data : newProps.data || {},
      edit : false,
    });
  }

  edit() {
    state(this, {edit : true});
  }

  __renderBackSide() {

    if (this.state.data.back) {
      let backData = this.state.data.back;
      let title = backData.title || ''
        , items = backData.items || []
        , across = backData.across
        , btn = {
        cost : ((this.state.data.price && this.state.data.price.btn) ? this.state.data.price.btn.title : ''),
        href : ((this.state.data.price && this.state.data.price.btn) ? this.state.data.price.btn.href : '')
      }, close_text = (backData.close_text || 'Закрыть');

      let list = items.map(function (item, index) {
        if (typeof item == 'string')
          return (<div key={'back_list_' + index} className="block-card__multi__list__item">{item}</div>);
        else if (item.title && item.time)
          return (<CardLineV2 key={'back_list_' + index} left={item.title} right={item.time}/>);
        return null;
      }, this);

      let acrossLine = (across ? (
        <div className="block-card__flight__across">
          <CardLine left={across.title} right={across.time} grey={true}/>
        </div>
      ) : null);

      return (
        <div className="block-card__backside">
          <div className={'block-card__multi ' + (across ? 'with-across' : '')}>
            <div className="block-card__multi__title">
              <span className="block-card__multi__title__inner">{title}</span>
            </div>
            <div className="block-card__multi__list">
              {list}
            </div>
          </div>
          {acrossLine}
          <div
            className='block-card__price'>
            <CardLine left={{
              href  : '#front-side-card',
              title : close_text
            }} right="" grey={true} btn={btn}/>
          </div>
        </div>
      );
    }
    return null;
  }

  __renderFrontSide() {
    return (
      <div className="block-card__none" style={{overflow : 'hidden'}}>Тут что-то новенькое</div>
    )
  }

  __preRenderFrontSide() {

    let priceData = this.state.data.price || {}
      , price = null;

    if (this.state.data.price) {
      let btn = {
        cost : priceData.btn.title,
        href : priceData.btn.href
      };
      let left = (this.state.data.back ? {
        title : priceData.title,
        href  : '#back-side-card'
      } : priceData.title);

      let type = this.state.data.type;
      let priceClass = 'block-card__price '
        + (['hotel', 'excursion'].indexOf(type) >= 0 ? 'bordered ' : '')
        + (type == 'excursion' ? 'no-top' : '');

      price = (
        <div className={priceClass}>
          <CardLine left={left} right="" grey={true} btn={btn}/>
        </div>
      );
    }

    return (
      <div className="block-card__frontside">
        {this.__renderFrontSide()}
        {price}
      </div>
    )
  }

  getValue() {
    return this.state.data;
  }

  __renderEditorContent() {
    return (
      <div key={this._id + '_edit_content'} className="admin-waypoint-item-modal__edit">
        <CardEdit ref="form" data={this.state.data} onChange={this.onEditorChanged.bind(this)}/>
      </div>
    );
  }

  __renderModalContent() {
    return (
      <div key={this._id + '_edit'} className="admin-waypoint-item-modal">
        {this.__renderEditorContent()}
        <div className="admin-waypoint-item-modal__save">

          <button className="btn btn-secondary"
                  onClick={this.beforeSave.bind(this)}><i className="fa fa-save"/><span>Сохранить</span>
          </button>
          &nbsp;
          <button className="btn btn-secondary"
                  onClick={this.beforeSaveAndClose.bind(this)}><i
            className="fa fa-hdd-o"/><span>Сохранить и закрыть</span>
          </button>
        </div>
      </div>
    );
  }

  onEditorChanged(val) {
    // state(this, {data : val});
  }

  beforeSaveAndClose() {
    this.save();
    if (this.refs.modal)
      this.refs.modal.close();
  }

  beforeSave() {
    this.save();
  }

  save() {
    if (this.refs.form) {
      let val = this.refs.form.getValue();
      state(this, {
        data : val
      });
    }
  }

  closeModal() {
    state(this, {
      edit : false
    });
  }

  render() {
    let back = (this.state.data.back ? (
      <div className={'block-card backside ' + this.state.data.type}>
        {this.__renderBackSide()}
      </div>
    ) : 'Без обратной стороны');

    let edit = (this.state.edit ? (
      <NewModal ref="modal" title={this._title} onClose={this.closeModal.bind(this)}>
        {this.__renderModalContent()}
      </NewModal>
    ) : null);

    return (
      <div className="admin-view-card" style={{width : '100%'}} onClick={(event) => {
        event.stopPropagation();
        event.preventDefault();
        return false;
      }}>
        {edit}
        <div className="row">
          <div className="col col-6">
            <div className={'block-card ' + this.state.data.type}>
              {this.__preRenderFrontSide()}
            </div>
          </div>
          <div className="col col-6">
            {back}
          </div>
        </div>
      </div>
    );
  }
}

export default CardView;
