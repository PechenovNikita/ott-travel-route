'use strict';

import React from 'react';
import Item from './item';
import Photo from './../../photo';
import Modal from './../../../blocks/modal';

class WaypointItemPhoto extends Item {

  constructor(props) {
    super(props);
    this._title = "Фотографии";
  }

  __renderEditorContent() {
    return (
      <div key={this._id + '_edit_content'} className="admin-waypoint-item-modal__edit">
        <Photo rel="photo" data={this.state.options}/>
      </div>
    );
  }

  save() {

  }

  renderTitle() {
    return <span><i className="fa fa-photo"/> {this._title}</span>;
  }

  renderDesc() {
    let str = [<span key="0">Фотографии </span>], options = this.state.options;

    if (options.location && options.location.lat && options.location.lng) {
      str.push(
        <span
          key="1">в радиусе <strong>{options.location.radius ? options.location.radius : 5}км </strong> от точки <strong>({options.location.lat}, {options.location.lng})</strong></span>);
    }

    if (options.text)
      str.push(<span key="3"> содержит в тексте: <strong>#{options.text}</strong></span>);
    else {
      if (options.tags)
        if (Array.isArray(options.tags))
          str.push(<span key="2"> с тэгами: <strong>#{options.tags.join(', #')}</strong></span>);
        else if (typeof options.tags == 'string')
          str.push(<span key="2"> с тэгом: <strong>#{options.tags}</strong></span>);
    }

    return str;
  }
}

export default WaypointItemPhoto;