'use strict';

import React from 'react';

import Item from './item';
import state from './../../../../js/mixins/state';
import Input from '../../forms/input';

class WaypointItemText extends Item {

  constructor(props) {
    super(props);
    this._title = 'Заголовок';
  }

  __renderEditorContent() {
    return (
      <div key={this._id + '_edit_content'} className="admin-waypoint-item-modal__edit">
        <Input ref="input" onChange={this.onEditorChanged.bind(this)} value={this.state.options}/>
      </div>
    );
  }

  save() {
    let val = this.refs.input.getValue();
    state(this, {
      data    : {
        type    : 'title',
        options : val
      },
      options : val
    });
  }

  renderTitle() {
    return <span><i className="fa fa-header"/> {this._title}</span>;
  }

  renderDesc() {
    if (typeof this.state.options !== 'string')
      return <span>Пока пусто</span>;
    return <span>{this.state.options}</span>
  }

}

export default WaypointItemText;