'use strict';

import React from 'react';
import Item from './item';
import Photo from './../../photo';
import Modal from './../../../blocks/modal';
import CustomPhoto from '../../CustomPhoto';
import state from '../../../../js/mixins/state';

class WaypointItemCustomPhoto extends Item {

  constructor(props) {
    super(props);
    this._title = 'Выборочные фотографии';
  }

  __renderEditorContent() {
    return (
      <div key={this._id + '_edit_content'} className="admin-waypoint-item-modal__edit">
        <CustomPhoto ref="photo" options={this.state.options}/>
      </div>
    );
  }

  save() {
    let opt = this.refs.photo.getValue();
    state(this, {
      data    : {
        type    : 'custom-photos',
        options : opt
      },
      options : opt
    });
  }

  renderTitle() {
    return <span><i className="fa fa-photo"/> {this._title}</span>;
  }

  renderDesc() {
    let str = [<span key="0">Фотографии </span>], options = this.state.options;

    if (options.items && options.items.length && options.grid) {
      str.push(<span key="1">Есть фотки</span>)
    } else {
      str.push(<span key="2">Нет фотки</span>)
    }

    return str;
  }
}

export default WaypointItemCustomPhoto;