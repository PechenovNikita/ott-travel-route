'use strict';

import React from 'react';
import state from './../../../js/mixins/state';
import Input from './../forms/input';
import WaypointList from './waypointList';

function parseProps(props, collapse) {
  let map = props.data.map
    , cards = props.data.cards;

  let description = (typeof props.data.description === 'string' ? [props.data.description] : props.data.description);
  description = (Array.isArray(description) ? description : []);

  if (map)
    description.push({
      type    : 'map',
      options : map
    });

  if (cards)
    description.push({
      type    : 'cards',
      options : cards
    });

  let ret = {
    title       : props.data.title || [],
    description : description
  };

  if (collapse)
    ret.collapse = true;
  if (props.data.focus)
    ret.collapse = false;
  return ret;
}

class Waypoint extends React.Component {

  constructor(props) {
    super(props);
    this.state = parseProps(props, true)
  }

  componentWillReceiveProps(newProps) {
    state(this, parseProps(newProps))
  }

  toggle() {
    state(this, {
      collapse    : !this.state.collapse,
      description : this.refs.description.getValue()
    });
  }

  changeTitle() {
    state(this, {
      title       : this.refs.title.value,
      description : this.refs.description.getValue()
    });
  }

  getValue() {
    console.log('Depricated getValue Waypoint');
    return {
      title       : this.refs.title.value,
      description : this.refs.description.getValue()
    }
  }

  get value(){
    return {
      title       : this.refs.title.value,
      description : this.refs.description.getValue()
    }
  }

  render() {
    return (
      <div className="admin-waypoint admin-box__border admin-box__pad-min">
        <div className="admin-box__pad admin-box__back"
             onClick={this.toggle.bind(this)}>
          {this.state.title}
          <button className="btn btn-sm btn-link float-xs-right"><i
            className={"fa fa-caret-" + (this.state.collapse ? 'down' : 'up')}/></button>
        </div>
        {/*<div className={"admin-box__inner admin-box__pad-min collapse" + (this.state.collapse ? '' : ' in')}>*/}
        <div className={"admin-box__inner admin-box__pad-min collapse" + (this.state.collapse ? '' : ' show in')}>
          <Input onChange={this.changeTitle.bind(this)} ref="title" value={this.state.title} title="Заголовок"/>
          <WaypointList ref="description" items={this.state.description}/>
        </div>
      </div>
    );
  }
}

export default Waypoint;