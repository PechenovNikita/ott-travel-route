'use strict';

import React from 'react';
import Updater from './../../../js/updater';
import state from '../../js/mixins/state';

import DirectionSetting from './map/directionSettings';
import PointSetting from './map/pointSettings';
import PointsSetting from './map/pointsSettings';
import PoiSettings from './map/poiSettings';

import {fromJS, Map as ImmutableMap} from 'immutable';
import Map from '../../js/component/extendMap';
import BtnUpdate from './forms/btnUpdate';
import Poi from './map/Poi';

/**
 *
 * @type {{name:string,value:string}[]}
 */
const mapTypes = [{
  name  : 'Без карты',
  value : 'none'
}, {
  name  : 'Карта направления',
  value : 'direction'
}, {
  name  : 'Карта точки',
  value : 'point'
}, {
  name  : 'Маркеры',
  value : 'poi'
}];

/**
 *
 * @param {Object} props
 * @property {*} [direction]
 * @property {*} [point]
 * @property {*} [poi]
 * @returns {{google, type: string, direction: {}, point: {}, points: {}, poi: (*|PointSettings.poi|data.poi|{}|testDataMap.point.poi|{name, city_id, sight, hotels})}}
 */
function parseProps(props) {
  let type = 'none';

  let dataPoint = props.point || {}
    , poi = props.poi || {};

  let direction = {}, point = {};

  if (props.direction) {
    type = 'direction';
    direction = props.direction;

  }
  if (props.point) {

    type = 'point';
    point = dataPoint;

  }
  if (dataPoint.poi || props.poi) {
    type = 'poi';
    poi = dataPoint.poi || props.poi;
  }

  return {
    google : Updater.googleMapLoaded(),
    type   : type,

    direction : direction,
    point     : point,
    poi       : poi
  }

}

class MapBox extends React.Component {

  constructor(props) {
    super();

    this.state = parseProps(props);

    this._google = Updater.googleMapLoaded();
    this._id = 'mapBox_' + Date.now();

  }

  componentDidMount() {
    if (this.state.google)
      this.googleInit();
    else
      Updater.googleMapInit(this.googleInit.bind(this));
  }

  componentDidUpdate(prevProps, prevState) {

    if (!this._google) {
      this._google = true;
      Updater.do();
    }

    if (this._needUpdateMap) {
      this._needUpdateMap = false;
      this._map.update();
    }

  }

  googleInit() {
    if ($(this.refs.map).is(':visible')) {
      this._map = new Map(this.refs.map);
      return true;
    }

    return false;
  }

  onTypeChange(event) {
    state(this, {type : event.target.value});
  }

  update() {

    if (!this._map) {
      if (!this.googleInit()) {
        console.log('Нету карты');
        return;
      }
    }

    state(this, {
      direction : this.refs.directionSettings.getValue(),
      point     : this.refs.pointSettings.getValue(),
      poi       : this.state.poi,
    });

    this._needUpdateMap = true;
  }

  getValue() {

    let value = {};

    switch (this.state.type) {
      case 'direction':
        value.direction = this.refs.directionSettings.getValue();
        break;
      case 'point':
        value.point = this.refs.pointSettings.getValue();
        break;
      case 'poi':
        value.poi = this.state.poi.hashCode ? this.state.poi.toJS() : this.state.poi;
        break;
    }

    return value;

  }

  getMapData() {

    if (this._map) {
      let location = this._map.getCenter();
      if (!location)
        return null;

      return {
        zoom     : this._map.getZoom(),
        location : {
          lat : location.lat(),
          lng : location.lng()
        },
      }
    }

    return null;
  }

  whenSomeThingChange(stateItem, ref) {
    if (this.refs[ref]) {
      let st = this.state;
      st[stateItem] = this.refs[ref].getValue();
      this.setState(st);
    }
  }

  render() {

    let typeSelect = mapTypes.map(function (type, index) {
      return (
        <div key={'type_' + index} className="form-chech form-check-inline">
          <label className="form-check-label">
            <input type="radio" className="form-check-input"
                   name={this._id + '_type'}
                   onChange={this.onTypeChange.bind(this)}
                   value={type.value} checked={this.state.type == type.value}/> {type.name}</label>
        </div>
      );
    }, this);

    let isDirection = (this.state.type === 'direction')
      , isPoint = (this.state.type === 'point')
      , isPoi = (this.state.type === 'poi');

    return (
      <div className="admin-map">
        <div className="admin-map__data admin-box__pad admin-box__back">
          <div className="admin-map__data__type form-check">
            {typeSelect}
          </div>
        </div>
        <div className={'admin-map__data admin-box__pad' + (this.state.type === 'none' ? ' hide' : '')}>

          <div className={'admin-map__data__direction' + (isDirection ? '' : ' hide')}>
            <DirectionSetting ref="directionSettings"
                              onChange={this.whenSomeThingChange.bind(this, 'direction', 'directionSettings')}
                              data={this.state.direction}/>
          </div>

          <div className={'admin-map__data__point' + (isPoint ? '' : ' hide')}>
            <PointSetting ref="pointSettings"
                          onChange={this.whenSomeThingChange.bind(this, 'point', 'pointSettings')}
                          fromMap={this.getMapData.bind(this)}
                          data={this.state.point}/>
          </div>

          {this.renderPoi()}


        </div>


        <div className={'admin-map__demo' + (this.state.type === 'none' ? ' hide' : '')}>
          <div className="admin-map__demo__btn">
            <BtnUpdate prop="map" onClick={this.update.bind(this)}/>
          </div>
          <div ref="map" style={{height : (isPoi ? 400 : 200)}}
               className="block-map mapped"
               data-direction={(isDirection ? JSON.stringify(this.state.direction) : false)}
               data-point={(isPoint ? JSON.stringify(this.state.point) : false)}
               data-poi={(isPoi ? JSON.stringify(this.state.poi) : false)}/>
        </div>
      </div>
    );
  }

  changePoi = (poi) => {
    state(this, {poi})
  };

  renderPoi() {
    return (
      <div className={'admin-map__data__poi' + (this.state.type === 'poi' ? '' : ' hide')}>
        <Poi data={fromJS(this.state.poi)} onChange={this.changePoi}/>
      </div>
    );
  }

}

export default MapBox;
