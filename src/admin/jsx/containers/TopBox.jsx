'use strict';

import React, {Component} from 'react';
import {Info} from '../blocks/Info';

/**
 * image : string;
 * text : string;
 * topText : string[];
 * under: string;
 **/

class TopBox extends Component {

  render() {
    return (
      <div className="">
        <div className="top-preview mb-3">
          {this.__renderImage()}
          <div className="top-preview__text">
            <div>
              {this.__renderTopText()}
              {this.__renderText()}
              {this.__renderUnder()}
            </div>
          </div>
        </div>
        {this.__inputImage()}
        {this.__inputTopText()}
        {this.__inputText()}
        {this.__inputUnder()}
      </div>
    );
  }

  __renderImage() {
    const image = this.props.image || 'top/back00.png';
    return <div className="top-preview__back" style={{backgroundImage : `url(/public/images/${image})`}}/>;
  }

  __renderTopText() {
    if (this.props.topText) {
      if (Array.isArray(this.props.topText)) {
        return (
          <div key="title-text" className="top-preview-title">
            {this.props.topText.map((text, index) => <div key={index}>{text}</div>)}
          </div>
        );
      } else {
        return (
          <div key="title-text" className="top-preview-title">
            <span className="block-top__forground__title__text__inner">{this.props.topText}</span>
          </div>
        )
      }
    } else {
      return ([
        <div key="title-image" className="top-preview-icon">
          <img className="top-preview-icon__img"
               src="/public/images/top/title.png"/>
        </div>,
        <div key="title-text" className="top-preview-title">
          <span className="block-top__forground__title__text__inner">Маршрут путешествия</span>
        </div>
      ]);
    }
  }

  __renderText() {
    return (<div className="top-preview-text">{this.props.text}</div>)
  }

  __renderUnder() {
    if (this.props.under) {
      return (<div className="top-preview-under">{this.props.under}</div>)
    }
    return null;
  }

  changeImage = (event) => {
    const value = event.currentTarget.value;
    this.props.onChange('image', value);
  };

  clearImage = () => {
    this.props.onChange('image', '');
  };

  __inputImage() {
    return (
      <div className="mb-2">
        <label htmlFor="top-image">Картинка</label>
        <div className="input-group mb-1">
          <span className="input-group-btn">
            <button onClick={this.clearImage}
                    className="btn btn-secondary"
                    type="button">Ст. картинка</button>
          </span>
          <span className="input-group-addon">/src/images/</span>
          <input type="text" id="top-image" className="form-control"
                 onChange={this.changeImage}
                 value={this.props.image}/>
        </div>
        <Info>
          Чтобы добавить новое изображение: необходимо поместить его в
          папку <strong>src/images/</strong> прописать путь до него в поле сверху и запушить в репозиторий.
          (например
          загрузим изображание в папку <strong>src/images/top/baku.jpg</strong> путь для поля сверху будет
          следующим <strong>top/baku.jpg</strong>).
          <br/>
          Если оставить поле пустым - будет стандартное изображение с картой
        </Info>
      </div>
    )
  }

  changeTopText = (event) => {
    const value = event.currentTarget.value;

    if (!value) {
      this.props.onChange('topText', '');
    } else {
      this.props.onChange('topText', value.split('<br/>'));
    }
  };

  __inputTopText() {
    let value = '';
    if (this.props.topText) {
      if (typeof this.props.topText === 'string') {
        value = this.props.topText;
      } else {
        value = this.props.topText.join('<br/>');
      }
    }
    return (
      <div className="mb-2">
        <label htmlFor="top-topText">Заголовок (крупный текст)</label>
        <div className="form-group mb-1">
          <input type="text" id="top-topText" className="form-control" placeholder="заголовок"
                 value={value} onChange={this.changeTopText}/>
        </div>
        <Info>
          Основной заголовок на картинке. <br/>
          Чтобы разбить заголовок на абзацы, между абзацами вставте <code>{'<br/>'}</code><br/>
          Если оставить пустым будет стандартное <strong>"Маш персональный маршрут путешествия"</strong>.
        </Info>
      </div>
    );
  }

  changeText = (event) => {
    const value = event.currentTarget.value;
    this.props.onChange('text', value);
  };

  __inputText() {
    return (
      <div>
        <label htmlFor="top-text">Текст под заголовком (Средний размер) </label>
        <div className="form-group mb-2">
          <input type="text"
                 id="top-text"
                 className="form-control"
                 placeholder="Текс под заголовком"
                 value={this.props.text}
                 onChange={this.changeText}/>
        </div>
        <Info>
          Дополнительный текст под заголовком. (средний размер шрифта)
        </Info>
      </div>
    );
  }

  changeUnder = (event) => {
    const value = event.currentTarget.value;
    this.props.onChange('under', value);
  };

  __inputUnder() {
    return (
      <div>
        <label htmlFor="top-text">Доп. описание (Маленький размер). Даты например </label>
        <div className="form-group mb-2">
          <input type="text"
                 id="top-text"
                 className="form-control"
                 placeholder="Нижний текст"
                 value={this.props.under}
                 onChange={this.changeUnder}/>
        </div>
        <Info>
          Дополнительный текст внизу блока. (маленький размер шрифта)
        </Info>
      </div>
    );
  }
}

export default TopBox;
