'use strict';

import React, {Component} from 'react';
import {Info} from '../blocks/Info';

// Props
//  "hideContacts": true,
//  "adviceTitle": "Полезная информация",
//  "blogFooter": true,

class Design extends Component {

  changeHideContacts = (event) => {
    const checked = event.currentTarget.checked;
    this.props.onChanges({
      hideContacts : checked,
    });
  };

  changeAdviceTitle = (event) => {
    const value = event.currentTarget.value;
    this.props.onChanges({
      adviceTitle : value,
    });
  };

  changeBlogFooter = (event) => {
    const checked = event.currentTarget.checked;
    this.props.onChanges({
      blogFooter : checked,
    });
  };

  makeNewDesign = (event) => {
    event.stopPropagation();
    event.preventDefault();

    this.props.onChanges({
      hideContacts : true,
      adviceTitle  : 'Полезная информация',
      blogFooter   : true,
    });

  };

  makeOldDesign = (event) => {
    event.stopPropagation();
    event.preventDefault();

    this.props.onChanges({
      hideContacts : false,
      adviceTitle  : '',
      blogFooter   : false,
    });

  };

  render() {
    return (
      <div className="">
        <div className="row">
          <div className="col-12 col-md-6">

            <div className="form-check">
              <label className="form-check-label">
                <input type="checkbox"
                       checked={this.props.hideContacts}
                       onChange={this.changeHideContacts} className="form-check-input"/> Скрыть контакты (новый дизайн)</label>
            </div>
          </div>
          <div className="col-12 col-md-6">
            <div className="form-check">
              <label className="form-check-label">
                <input type="checkbox"
                       checked={this.props.blogFooter}
                       onChange={this.changeBlogFooter}
                       className="form-check-input"/> Новый подвал (новый дизайн)</label>
            </div>
          </div>
        </div>
        <Info>
          Чтобы создать страницу в новом дизайне необходимо включить настройка <strong>Скрыть
          контакты</strong> и <string>Новый подвал</string>,
          также можно переименовать заголовок <strong>Советы эксперта</strong> в <strong>Полезная информация</strong>
        </Info>
        <div>
          <label htmlFor="top-text">Назване блока "Советы эксперта"</label>
          <div className="form-group mb-2">
            <input type="text"
                   id="top-text"
                   className="form-control"
                   placeholder="Заголовок"
                   value={this.props.adviceTitle}
                   onChange={this.changeAdviceTitle}/>
          </div>
        </div>
        <button onClick={this.makeNewDesign}>Новый дизайн</button>
        <button onClick={this.makeOldDesign}>Старый дизайн</button>
      </div>
    );
  }
}

export default Design;
