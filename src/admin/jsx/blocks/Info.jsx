'use strict';

'use strict';

import React, {Component} from 'react';

export const Info = (props) => (
  <div className="alert alert-info">
    <div style={{
      display  : 'flex',
      flexFlow : 'row nowrap'
    }}>
      <div style={{
        flex        : '0 0 auto',
        marginRight : '1em'
      }}><span className="fa fa-info-circle"/></div>
      <div style={{flex : '1 1 auto'}} className="small">
        {props.children}
      </div>
    </div>
  </div>
);
