'use strict';

import React from 'react';
import state from './../../js/mixins/state';

class PageNav extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      navItems   : props.nav || [],
      rightItems : props.right || []
    };
  }

  componentWillReceiveProps(newProps) {
    state(this, {
      navItems   : newProps.nav || [],
      rightItems : newProps.right || []
    });
  }

  render() {
    let nav = this.state.navItems.map(function (item, index) {
      return (
        <li key={'nav_' + index} className="nav-item">
          {item}
        </li>
      );
    }, this);

    let right = this.state.rightItems.map(function (item, index) {
      return (
        <span key={'right_' + index}>{item}</span>
      );
    });
    return (
      <nav className="navbar navbar-light bg-faded">
        <div className="container">
          <div style={{display : 'flex'}}>
            <ul className="navbar-nav mr-auto">
              {nav}
              {/*<li className="nav-item">*/}
              {/*<a className="nav-link" href="javascript:void(0)"*/}
              {/*onClick={this.props.onBack}>&lt; Назад к списку</a>*/}
              {/*</li>*/}
            </ul>

            <div className="form-inline">
              {right}
              {/*<button onClick={this.props.onSave}*/}
              {/*className="btn btn-outline-success" type="submit">Сохранить*/}
              {/*</button>*/}
            </div>
          </div>
        </div>
      </nav>
    );
  }
}

export default PageNav;
