'use strict';

import React from 'react';
import state from './../../js/mixins/state';
import Popup from './modal';

/**
 * @class
 */
class NewModal extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      title : props.title,
      open  : true
    };

    this._id = 'newModal_' + Date.now();
  }

  getID() {
    return this._id;
  }

  componentDidMount() {
    Popup.addModal(this);
  }

  // componentDidMount() {}

  componentWillUnmount() {
    Popup.removeModal(this);
  }

  componentDidUpdate(prevProps, prevState) {
  }

  componentWillReceiveProps(newProps) {
    state(this, {
      title : newProps.title,
      open  : true
    });
  }

  close() {
    state(this, {open : false});
    if (this.props.onClose) {
      this.props.onClose();
    }
  }

  renderModal() {
    return (
      <div ref="modal" className="modal fade show in" style={{
        zIndex  : 1000,
        display : this.state.open ? 'block' : 'none'
      }}>
      {/*<div ref="modal" className="modal fade in" style={{
        zIndex  : 1000,
        display : this.state.open ? 'block' : 'none'
      }}>*/}
        <div className="modal-dialog modal-lg" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <button type="button" className="close"
                      onClick={this.close.bind(this)}
                      data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <h4 className="modal-title">{this.state.title}</h4>
            </div>
            <div className="modal-body">
              {this.props.children}
            </div>
          </div>
        </div>
      </div>
    );
  }

  render() {
    return null;
  }

}

export default NewModal;