"use strict";

import React from 'react';
import state from './../../js/mixins/state';

export class PageBoxInner extends React.Component {
  render() {
    return (
      <div className="admin-box__inner">{this.props.children}</div>
    )
  }
}

export class PageBoxBody extends React.Component {
  render() {
    return (
      <div className="admin-box__body">{this.props.children}</div>
    )
  }
}

class PageBox extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      collapse : true
    };

    this._id = 'pageBox_' + Date.now();
  }

  onClick() {
    state(this, {collapse : !this.state.collapse});
  }

  render() {
    let title = (this.props.title ?
      <div className={"admin-box__title" + (this.state.collapse ? ' darken' : ' ')} onClick={this.onClick.bind(this)}>
        {this.props.title}
        <button className="btn btn-sm btn-link float-xs-right"><i
          className={"fa fa-caret-" + (this.state.collapse ? 'down' : 'up')}/></button>
      </div> : '');

    return (
      <div className={"admin-box" + (this.state.collapse ? '' : ' open')}>
        {title}
        {/*<div className={"admin-box__collapse collapse" + (this.state.collapse ? '' : ' in')}>*/}
        <div className={"admin-box__collapse collapse" + (this.state.collapse ? '' : ' show in')}>
          {this.props.children}
        </div>
        {/*{body_inner}*/}
      </div>
    )
  }
}

export default PageBox;