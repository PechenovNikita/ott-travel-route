'use strict';

import React from 'react';
import state from '../../js/mixins/state';
import {clone} from '../../js/mixins/state';

/**
 * @type {Popup}
 */
let pop;

class Popup extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      modals : []
    };

    pop = this;
  }

  /**
   *
   * @param {NewModal} modal
   */
  static addModal(modal) {
    pop.addModalTo(modal);
  }

  /**
   *
   * @param {NewModal} modal
   */
  static removeModal(modal) {
    pop.removeModalFrom(modal)
  }

  /**
   *
   * @param {NewModal} modal
   */
  addModalTo(modal) {
    this.state.modals.push(modal);
    state(this, {
      modals : this.state.modals
    })
  }

  /**
   *
   * @param {NewModal} modal
   */
  removeModalFrom(modal) {
    let modals = this.state.modals.filter(function (m) {
      return m.getID() !== modal.getID()
    });
    state(this, {
      modals : modals
    })
  }

  componentDidMount() {
    window.addEventListener('keydown', this.onKeyDown.bind(this));
  }

  onKeyDown(event) {
    if (this.state.modals.length > 0 && event.keyCode == 27) {
      this.state.modals[this.state.modals.length - 1].close();
    }
  }


  render() {

    let modals = this.state.modals.map(function (modal, index) {
      return (
        <div key={index}>{modal.renderModal()}</div>
      )
    });

    return (
      <div>
        {modals}
      </div>
    );
  }
}

export default Popup;