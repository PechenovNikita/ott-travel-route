'use strict';

import {Component} from 'react';

import OttSelectCity from './components/forms/ott/selectCity';
import FormHotel from './components/forms/ott/formHotel';
import FormFlight from './components/forms/ott/formFlight';
import Map from './components/map';
import Routes from './components/routes';
import CustomPhoto from './components/CustomPhoto';
import Weather from './components/Weather';

let testDataMap = {
  "poi": {
    "sight": [
      [
        52.501095,
        13.442324,
        "fastfood",
        "Burgermeister"
      ],
      [
        52.538174,
        13.410180,
        "food",
        "Schädels"
      ],
      [
        52.526439,
        13.387847,
        "food",
        "PHO - Noodlebar"
      ],
      [
        52.528016,
        13.408608,
        "food",
        "Zeit fur Brot"
      ],
      [
        52.501686,
        13.419387,
        "food",
        "Wonder Waffel"
      ],
      [
        52.528510,
        13.387413,
        "food",
        "Schnitzelei"
      ],
      [
        52.503341,
        13.317444,
        "food",
        "Marjellchen"
      ],
      [
        52.513549,
        13.455237,
        "cocktail",
        "McLarens"
      ],
      [
        52.502015,
        13.432434,
        "cocktail",
        "Vögelchen"
      ],
      [
        52.489931,
        13.425547,
        "cocktail",
        "Galatea Wine and Music"
      ],
      [
        52.479067,
        13.438732,
        "cocktail",
        "Prachtwerk"
      ],
      [
        52.510637,
        13.442213,
        "cocktail",
        "Berghain/Panorama Bar"
      ],
      [
        52.482080,
        13.431374,
        "cocktail",
        "Klunkerkranich"
      ],
      [
        52.495979,
        13.387726,
        "cocktail",
        "Gretchen"
      ],
      [
        52.523633,
        13.406941,
        "cocktail",
        "B-Flat Jazz Club"
      ],
      [
        52.512944,
        13.320144,
        "food",
        "Cafeteria Skyline TU"
      ],
      [
        52.516278,
        13.377688,
        "eye",
        "Бранденбургские ворота"
      ],
      [
        52.518632,
        13.376185,
        "monument",
        "Рейхстаг"
      ],
      [
        52.519080,
        13.401078,
        "church",
        "Берлинский собор"
      ],
      [
        52.520828,
        13.409423,
        "eye",
        "Телевизионная башня"
      ],
      [
        52.520971,
        13.295584,
        "monument",
        "Дворец Шарлоттенбург"
      ],
      [
        52.524003,
        13.402327,
        "children",
        "Hackesche Höfe"
      ],
      [
        52.505075,
        13.439707,
        "children",
        "East Side Gallery"
      ],
      [
        52.488647,
        13.469702,
        "children",
        "Трептов-парк"
      ],
      [
        52.528460,
        13.372132,
        "monument",
        "Hamburger Bahnhof"
      ],
      [
        52.520642,
        13.397691,
        "eye",
        "Музейный остров"
      ],
      [
        52.527036,
        13.396878,
        "children",
        "Auguststrasse"
      ],
      [
        52.506643,
        13.382185,
        "eye",
        "Martin-Gropius-Bau"
      ],
      [
        52.487558,
        13.388464,
        "theater",
        "English Theatre Berlin"
      ],
      [
        52.509002,
        13.379275,
        "monument",
        "Музей шпионажа"
      ],
      [
        52.505039,
        13.278190,
        "eye",
        "Радиобашня"
      ],
      [
        52.513964,
        13.378710,
        "eye",
        "Мемориал жертвам Холокоста"
      ]
    ],
    "hotels": [
      {
        "id": 307520,
        "type": 14,
        "city": "Berlin",
        "city_iata": "BER",
        "translated_city": "Берлин",
        "city_name_translated": "Берлин",
        "translated_name": "Select Hotel Berlin Checkpoint Charlie",
        "city_id": 2950159,
        "img": "https://1b4da894acfad722fb46-08ede53a5c8850764384bc99fd6145bb.ssl.cf3.rackcdn.com/307520/32032905_t.jpg",
        "img_s": "https://1b4da894acfad722fb46-08ede53a5c8850764384bc99fd6145bb.ssl.cf3.rackcdn.com/307520/32032905_t.jpg",
        "img_size": "640x384",
        "img_set": null,
        "img_set_s": null,
        "name": "Select Hotel Berlin Checkpoint Charlie",
        "address": "Hedemannstr. 11-12",
        "zip": "10969",
        "country_code": "DE",
        "stars": 4,
        "district_id": 6147550,
        "lat": 52.50317383,
        "lng": 13.38956642,
        "construction": 1926,
        "renovated": 2006,
        "total_rooms": 112,
        "num_floors": 4,
        "facilities": [
          1,
          6,
          10,
          18,
          37,
          40,
          43,
          46,
          50,
          65,
          68,
          70,
          72,
          74,
          80,
          81,
          88,
          97,
          112,
          138,
          142,
          163
        ],
        "facilitiesByRating": {
          "top": [
            [
              1,
              0
            ],
            [
              6,
              0
            ],
            [
              10,
              0
            ],
            [
              18,
              0
            ],
            [
              37,
              0
            ],
            [
              40,
              0
            ],
            [
              43,
              0
            ],
            [
              46,
              0
            ],
            [
              50,
              0
            ]
          ],
          "all": [
            [
              1,
              0
            ],
            [
              6,
              0
            ],
            [
              10,
              0
            ],
            [
              18,
              0
            ],
            [
              37,
              0
            ],
            [
              40,
              0
            ],
            [
              43,
              0
            ],
            [
              46,
              0
            ],
            [
              50,
              0
            ],
            [
              65,
              0
            ],
            [
              68,
              0
            ],
            [
              70,
              0
            ],
            [
              72,
              0
            ],
            [
              74,
              0
            ],
            [
              80,
              0
            ],
            [
              81,
              0
            ],
            [
              88,
              0
            ],
            [
              97,
              0
            ],
            [
              112,
              0
            ],
            [
              138,
              0
            ],
            [
              142,
              0
            ],
            [
              163,
              0
            ]
          ]
        },
        "facility": {
          "weight": 290,
          "top": [
            74,
            50,
            65
          ]
        },
        "amenities": {},
        "nearest": 0,
        "is_recommend": 0,
        "checkin": "15:00",
        "checkout": "11:00",
        "roomtypes": [
          "Business room",
          "Standard room",
          "Superior room"
        ],
        "chains": [
          "Winters Hotel Company"
        ],
        "themes": [
          5,
          7,
          8,
          9,
          10,
          14,
          15,
          19
        ],
        "tags": [],
        "ott_rank": 0.005,
        "ott_rating": 328,
        "ta_rating": 4,
        "ta_reviews": 1222,
        "ta": [
          4,
          1222
        ],
        "ty_reviews": 2153,
        "ty_sources": 19,
        "ty_rating": 82,
        "ty": [
          2153,
          19,
          82,
          "Very Good"
        ],
        "url": "https://www.onetwotrip.com/ru/hotels/hotel/select-hotel-berlin-checkpoint-charlie-307520?date_start=2018-04-04&date_end=2018-05-04&adults=1&children=0&children_ages=&s=true&to=Berlin&persons=1",
        "distance": 2.751,
        "dist_mc": 2.751,
        "seq": 238232
      },
      {
        "id": 305010,
        "type": 14,
        "city": "Berlin",
        "city_iata": "BER",
        "translated_city": "Берлин",
        "city_name_translated": "Берлин",
        "translated_name": "Derag Livinghotel Grosser Kurfürst",
        "city_id": 2950159,
        "img": "https://1b4da894acfad722fb46-08ede53a5c8850764384bc99fd6145bb.ssl.cf3.rackcdn.com/305010/32030131_t.jpg",
        "img_s": "https://1b4da894acfad722fb46-08ede53a5c8850764384bc99fd6145bb.ssl.cf3.rackcdn.com/305010/32030131_t.jpg",
        "img_size": "640x384",
        "img_set": null,
        "img_set_s": null,
        "name": "Derag Livinghotel Grosser Kurfürst",
        "address": "Neue Rossstr. 11-12",
        "zip": "10179",
        "country_code": "DE",
        "stars": 4,
        "district_id": 178779,
        "lat": 52.5121994,
        "lng": 13.40864182,
        "construction": 1996,
        "renovated": 2012,
        "total_rooms": 154,
        "num_floors": 7,
        "facilities": [
          1,
          2,
          6,
          8,
          10,
          14,
          18,
          21,
          30,
          32,
          40,
          46,
          48,
          50,
          68,
          70,
          72,
          81,
          87,
          93,
          96,
          97,
          98,
          112,
          117,
          127,
          142,
          143,
          163,
          181,
          183,
          203,
          204,
          205,
          206,
          207,
          241,
          242,
          253,
          257,
          305
        ],
        "facilitiesByRating": {
          "top": [
            [
              1,
              0
            ],
            [
              2,
              0
            ],
            [
              6,
              0
            ],
            [
              8,
              0
            ],
            [
              10,
              0
            ],
            [
              14,
              0
            ],
            [
              18,
              0
            ],
            [
              21,
              0
            ],
            [
              30,
              0
            ]
          ],
          "all": [
            [
              1,
              0
            ],
            [
              2,
              0
            ],
            [
              6,
              0
            ],
            [
              8,
              0
            ],
            [
              10,
              0
            ],
            [
              14,
              0
            ],
            [
              18,
              0
            ],
            [
              21,
              0
            ],
            [
              30,
              0
            ],
            [
              32,
              0
            ],
            [
              40,
              0
            ],
            [
              46,
              0
            ],
            [
              48,
              0
            ],
            [
              49,
              0
            ],
            [
              50,
              0
            ],
            [
              68,
              0
            ],
            [
              70,
              0
            ],
            [
              72,
              0
            ],
            [
              81,
              0
            ],
            [
              87,
              0
            ],
            [
              93,
              0
            ],
            [
              96,
              0
            ],
            [
              97,
              0
            ],
            [
              98,
              0
            ],
            [
              112,
              0
            ],
            [
              117,
              0
            ],
            [
              127,
              0
            ],
            [
              142,
              0
            ],
            [
              143,
              0
            ],
            [
              163,
              0
            ],
            [
              181,
              0
            ],
            [
              183,
              0
            ],
            [
              203,
              0
            ],
            [
              204,
              0
            ],
            [
              205,
              0
            ],
            [
              206,
              0
            ],
            [
              207,
              0
            ],
            [
              241,
              0
            ],
            [
              242,
              0
            ],
            [
              253,
              0
            ],
            [
              257,
              0
            ],
            [
              305,
              0
            ]
          ]
        },
        "facility": {
          "weight": 619,
          "top": [
            117,
            93,
            50,
            48
          ]
        },
        "amenities": {},
        "nearest": 0,
        "is_recommend": 0,
        "checkin": "14:00",
        "checkout": "11:00",
        "roomtypes": [
          "Apartment",
          "Business room",
          "Economy room",
          "Standard room"
        ],
        "chains": [
          "Derag Livinghotel"
        ],
        "themes": [
          5,
          8,
          9,
          14,
          15,
          19
        ],
        "tags": [],
        "ott_rank": 0.075,
        "ott_rating": 344,
        "ta_rating": 4.5,
        "ta_reviews": 1047,
        "ta": [
          4.5,
          1047
        ],
        "ty_reviews": 4106,
        "ty_sources": 28,
        "ty_rating": 86,
        "ty": [
          4106,
          28,
          86,
          "Excellent"
        ],
        "url": "https://www.onetwotrip.com/ru/hotels/hotel/derag-livinghotel-grosser-kurfuerst-305010?date_start=2018-04-04&date_end=2018-05-04&adults=1&children=0&children_ages=&s=true&to=Berlin&persons=1",
        "distance": 1.359,
        "dist_mc": 1.359,
        "seq": 240254
      },
      {
        "id": 307445,
        "type": 14,
        "city": "Berlin",
        "city_iata": "BER",
        "translated_city": "Берлин",
        "city_name_translated": "Берлин",
        "translated_name": "Park Inn by Radisson Berlin Alexanderplatz",
        "city_id": 2950159,
        "img": "https://1b4da894acfad722fb46-08ede53a5c8850764384bc99fd6145bb.ssl.cf3.rackcdn.com/307445/32032819_t.jpg",
        "img_s": "https://1b4da894acfad722fb46-08ede53a5c8850764384bc99fd6145bb.ssl.cf3.rackcdn.com/307445/32032819_t.jpg",
        "img_size": "640x384",
        "img_set": null,
        "img_set_s": null,
        "name": "Park Inn by Radisson Berlin Alexanderplatz",
        "address": "Alexanderplatz 7",
        "zip": "10178",
        "country_code": "DE",
        "stars": 4,
        "district_id": 178779,
        "lat": 52.52301025,
        "lng": 13.41292286,
        "construction": 1970,
        "renovated": null,
        "total_rooms": 1012,
        "num_floors": 37,
        "facilities": [
          1,
          2,
          6,
          8,
          10,
          11,
          14,
          18,
          21,
          23,
          30,
          32,
          40,
          43,
          44,
          46,
          48,
          50,
          68,
          70,
          72,
          73,
          74,
          80,
          81,
          87,
          88,
          93,
          96,
          97,
          98,
          112,
          115,
          117,
          120,
          138,
          143,
          163,
          181,
          182,
          183,
          184,
          203,
          207,
          219,
          240,
          242,
          244,
          253,
          305
        ],
        "facilitiesByRating": {
          "top": [
            [
              1,
              0
            ],
            [
              2,
              0
            ],
            [
              6,
              0
            ],
            [
              8,
              0
            ],
            [
              10,
              0
            ],
            [
              11,
              0
            ],
            [
              14,
              0
            ],
            [
              18,
              0
            ],
            [
              21,
              0
            ]
          ],
          "all": [
            [
              1,
              0
            ],
            [
              2,
              0
            ],
            [
              6,
              0
            ],
            [
              8,
              0
            ],
            [
              10,
              0
            ],
            [
              11,
              0
            ],
            [
              14,
              0
            ],
            [
              18,
              0
            ],
            [
              21,
              0
            ],
            [
              23,
              0
            ],
            [
              30,
              0
            ],
            [
              32,
              0
            ],
            [
              40,
              0
            ],
            [
              43,
              0
            ],
            [
              44,
              0
            ],
            [
              46,
              0
            ],
            [
              48,
              0
            ],
            [
              50,
              0
            ],
            [
              68,
              0
            ],
            [
              70,
              0
            ],
            [
              72,
              0
            ],
            [
              73,
              0
            ],
            [
              74,
              0
            ],
            [
              80,
              0
            ],
            [
              81,
              0
            ],
            [
              87,
              0
            ],
            [
              88,
              0
            ],
            [
              93,
              0
            ],
            [
              96,
              0
            ],
            [
              97,
              0
            ],
            [
              98,
              0
            ],
            [
              112,
              0
            ],
            [
              115,
              0
            ],
            [
              117,
              0
            ],
            [
              120,
              0
            ],
            [
              138,
              0
            ],
            [
              143,
              0
            ],
            [
              163,
              0
            ],
            [
              181,
              0
            ],
            [
              182,
              0
            ],
            [
              183,
              0
            ],
            [
              184,
              0
            ],
            [
              203,
              0
            ],
            [
              207,
              0
            ],
            [
              219,
              0
            ],
            [
              240,
              0
            ],
            [
              242,
              0
            ],
            [
              244,
              0
            ],
            [
              253,
              0
            ],
            [
              305,
              0
            ]
          ]
        },
        "facility": {
          "weight": 818,
          "top": [
            117,
            74,
            93,
            50
          ]
        },
        "amenities": {},
        "nearest": 0,
        "is_recommend": 0,
        "checkin": "15:00",
        "checkout": "12:00",
        "roomtypes": [
          "Business room",
          "Deluxe",
          "Economy room",
          "Standard room",
          "Suite",
          "Superior room"
        ],
        "chains": [
          "Park Inn by Radisson"
        ],
        "themes": [
          8,
          9,
          14,
          15,
          19
        ],
        "tags": [],
        "ott_rank": 0.207576,
        "ott_rating": 312,
        "ta_rating": 3.5,
        "ta_reviews": 8146,
        "ta": [
          3.5,
          8146
        ],
        "ty_reviews": 15876,
        "ty_sources": 43,
        "ty_rating": 78,
        "ty": [
          15876,
          43,
          78,
          "Good"
        ],
        "url": "https://www.onetwotrip.com/ru/hotels/hotel/park-inn-by-radisson-berlin-alexanderplatz-307445?date_start=2018-04-04&date_end=2018-05-04&adults=1&children=0&children_ages=&s=true&to=Berlin&persons=1",
        "distance": 0.222,
        "dist_mc": 0.222,
        "seq": 238254
      }
    ]
  }
};

const testDataCustomPhoto = {
  'ids'   : ['36960581303', '36920912564', '37372402510', '37372402330', '37598889832', '37582464616', '36920752564'],
  'items' : [{
    'server'   : '4510',
    'isfriend' : 0,
    'isfamily' : 0,
    'secret'   : '32609715f6',
    'ispublic' : 1,
    'owner'    : '67095818@N05',
    'farm'     : 5,
    'title'    : '',
    'id'       : '36960581303'
  }, {
    'server'   : '4489',
    'isfriend' : 0,
    'isfamily' : 0,
    'secret'   : 'a4296d9c11',
    'ispublic' : 1,
    'owner'    : '7676405@N04',
    'farm'     : 5,
    'title'    : 'CW Cluster',
    'id'       : '36920912564'
  }, {
    'server'   : '4445',
    'isfriend' : 0,
    'isfamily' : 0,
    'secret'   : '64d77b4068',
    'ispublic' : 1,
    'owner'    : '94887983@N05',
    'farm'     : 5,
    'title'    : 'DSC03172_1500x1000',
    'id'       : '37372402510'
  }, {
    'server'   : '4507',
    'isfriend' : 0,
    'isfamily' : 0,
    'secret'   : '03f147eaf0',
    'ispublic' : 1,
    'owner'    : '94887983@N05',
    'farm'     : 5,
    'title'    : 'DSC03207_1000x1500',
    'id'       : '37372402330'
  }, {
    'server'   : '4494',
    'isfriend' : 0,
    'isfamily' : 0,
    'secret'   : 'df183dab9f',
    'ispublic' : 1,
    'owner'    : '94887983@N05',
    'farm'     : 5,
    'title'    : 'DSC03182_1500x869',
    'id'       : '37598889832'
  }, {
    'server'   : '4474',
    'isfriend' : 0,
    'isfamily' : 0,
    'secret'   : '3feda7c233',
    'ispublic' : 1,
    'owner'    : '94887983@N05',
    'farm'     : 5,
    'title'    : 'DSC03113_1500x920',
    'id'       : '37582464616'
  }, {
    'server'   : '4476',
    'isfriend' : 0,
    'isfamily' : 0,
    'secret'   : '19a01f222b',
    'ispublic' : 1,
    'owner'    : '94887983@N05',
    'farm'     : 5,
    'title'    : 'DSC03145_1500x1000',
    'id'       : '36920752564'
  }],
  'grid'  : [1, [1, 1, 1, 1], 1, 1]
};

class Test extends Component {

  constructor(props) {
    super(props);

    this.state = {test : ''}
  }

  showResult() {
    console.clear();
    console.log('showResults (data):');
    console.log(this.refs.form.getValue());
    console.log('showResults (JSON):');
    console.log(JSON.stringify(this.refs.form.getValue(), null, 2));

    this.setState({test : JSON.stringify(this.refs.form.getValue(), null, 4)});
  }

  render() {
    let hash = location.hash;
    // console.log(location);
    return (
      <div className="admin-test">
        <div className="container">

          {(() => {
            if (hash === '#map')
              return <Map ref="form"/>;
            if (hash === '#map_direction')
              return <Map ref="form" direction={testDataMap.direction}/>;
            if (hash === '#map_point')
              return <Map ref="form" point={testDataMap.point}/>;
            if (hash === '#map_poi')
              return <Map ref="form" poi={testDataMap.poi}/>;
            if (hash === '#hotel')
              return <Map ref="form" point={testDataMap.point} poi={testDataMap.poi}/>;
            if (hash === '#waypoints_routes')
              return <Routes ref="form"/>;
            if (hash === '#weather')
              return <Weather ref="form"/>;
            if (hash === '#weather-with-data')
              return <Weather ref="form" options={{
                text : 'Москва, Россия',
                lat  : 55.755826,
                lng  : 37.617299900000035
              }}/>;
            if (hash === '#custom-photo')
              return <CustomPhoto ref="form"/>;
            if (hash === '#custom-photo-with-data')
              return <CustomPhoto ref="form" options={testDataCustomPhoto}/>;
            else
              return <FormFlight ref="form"/>
          })()}
        </div>
        <div className="container">
          <div>
            <button onClick={this.showResult.bind(this)}
                    className="btn btn-lg btn-primary">Показать результат
            </button>
          </div>
          <div className="form-group">
            <label htmlFor="testTextarea">Вывод JSON</label>
            <textarea name=""
                      className="form-control"
                      id="testTextarea" cols="30" rows="10" value={this.state.test} readOnly="true"/>
          </div>
        </div>

      </div>
    );
  }
}

export default Test;
