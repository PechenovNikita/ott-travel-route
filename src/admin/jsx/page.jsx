'use strict';

import React from 'react';
import state from '../js/mixins/state';
import Input from './components/forms/input';
import About from './components/about';
import Advices from './components/advices';
import Routes from './components/routes';
import AfterAdvices from './components/afterAdvice';
import Nav from './blocks/pageNav';
import Box from './blocks/pageBox';
import {PageBoxBody} from './blocks/pageBox';
import {getByKey} from './../js/mixins/data'
import TopBox from './containers/TopBox';
import Design from './containers/Design';
import {fromJS} from 'immutable';

class Page extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      data : props.data
    };

    try {
      this.makeFile = !!new Blob();
    } catch (e) {
      this.makeFile = false;
    }
  }

  getValue() {
    return fromJS({
      title : this.refs.title.getValue(),
      top   : this.state.data.top,

      adviceTitle  : this.state.data.adviceTitle,
      hideContacts : !!this.state.data.hideContacts,
      blogFooter   : !!this.state.data.blogFooter,
      about        : this.refs.about.getValue(),
      route        : this.refs.route.getValue(),
      advices      : this.refs.advices.getValue(),
      afterAdvices : this.refs.afterAdvices.getValue()
    }).toJS()
  }

  save() {
    let val = this.getValue();
    console.log(val);
    if (this.makeFile) {
      const blob = new Blob([JSON.stringify(val, null, 2)], {type : 'text/plain;charset=utf-8'});
      saveAs(blob, 'data.json');
    } else {
      console.log(JSON.stringify(val));
    }
  }

  render() {

    let nav = [(
      <a key="back" className="nav-link" href="javascript:void(0)"
         onClick={this.props.onBack}>&lt; Назад к списку</a>
    )];

    let right = [(
      <button key="save" onClick={this.save.bind(this)}
              className="btn btn-outline-success" type="submit">
        <i className="fa fa-save"/><span>Сохранить</span>
      </button>
    )];

    return (
      <div className="admin-page">
        <div className="admin-page__header">
          <Nav nav={nav} right={right}/>
        </div>
        <div className="admin-page__body">
          <div className="container">

            <Box title="Основные">
              <PageBoxBody>{this.__renderTopBlock()}</PageBoxBody>
            </Box>

            <Box title="Дизайн страницы">
              <PageBoxBody>{this.__renderDesign()}</PageBoxBody>
            </Box>

            {/*<Box title="OpenGraph для соцсетей">
              <PageBoxBody>{this.__renderOpenGraph()}</PageBoxBody>
            </Box>*/}

            <About ref="about" data={getByKey(this.state.data, ['about'], {})}/>

            <Routes ref="route" value={getByKey(this.state.data, ['route'], [])}/>

            <Advices ref="advices" advices={getByKey(this.state.data, ['advices'], [])}/>
            <AfterAdvices ref="afterAdvices" title={getByKey(this.state.data, ['afterAdvices', 'title'], '')}
                          text={getByKey(this.state.data, ['afterAdvices', 'text'], '')}/>
          </div>
        </div>
      </div>
    )
  }

  changeTop = (key, value) => {
    const temp = {};
    temp[key] = value;
    this.setState(({data}) => ({
      data : {
        ...data,
        top : {...data.top, ...temp}
      }
    }), () => {console.log(this.state.data.top)});
  };

  __renderTopBlock() {
    return (
      <div>

        <Input ref="title" title="Заголовок страницы" placeholder="Заголовок"
               value={getByKey(this.state.data, ['title'], 'Тревел-эксперт OneTwoTrip')}/>

        <TopBox {...this.state.data.top} onChange={this.changeTop}/>

      </div>
    );
  }

  __renderOpenGraph() {
    return (
      <div>
        OpenGraph
      </div>
    );
  }

  changeDesign = (design) => {
    this.setState(({data}) => ({data : {...data, ...design}}));
  };

  __renderDesign() {
    return (
      <div>
        <Design onChanges={this.changeDesign}
                hideContacts={this.state.data.hideContacts}
                adviceTitle={this.state.data.adviceTitle}
                blogFooter={this.state.data.blogFooter}/>
      </div>
    );
  }
}

export default Page;
