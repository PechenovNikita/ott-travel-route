'use strict';

import React from 'react';
import state from '../js/mixins/state';
import Nav from './blocks/pageNav';

class JSONList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      items : props.items,
      value : '',
    };
  }

  componentWillReceiveProps(newProps) {
    this.setState((state) => ({
      ...state,
      items : newProps.items
    }));
    // state(this, {items : newProps.items});
  }

  select(fileName) {
    if (this.props.onSelect)
      this.props.onSelect(fileName)
  }

  changeValue = (event) => {
    const value = event.currentTarget.value;
    this.setState((state) => ({
      ...state,
      value
    }));
  };

  render() {
    let right = [(
      <button key='create'
              onClick={this.props.onCreate}
              className="btn btn-outline-success" type="submit">
        <i className="fa fa-plus-circle"/> <span>Создать новый</span>
      </button>
    )];

    return (
      <div className="admin-page">

        <div className="admin-page__header">
          <Nav right={right}/>
        </div>


        <div className="admin-page__body">
          <div className="container">
            <div className="input-group">
                <span className="input-group-addon">
                    <i className="fa fa-search"/>
                </span>
              <input className="form-control" type="text"
                     value={this.state.value}
                     placeholder={'Поиск'}
                     onChange={this.changeValue}/>
            </div>

            <ul className="nav nav-pills flex-column nav-stacked">
              {this.__renderList()}
            </ul>
          </div>
        </div>
      </div>
    )
  }

  __renderList() {
    let items = this.state.items;
    if (this.state.value)
      items = items.filter((item) => item.toLowerCase().indexOf(this.state.value.toLowerCase()) > -1);
    return items.map(function (fileName, index) {
      return (
        <li key={index} className="nav-item">
          <a href="javascript:void(0)"
             onClick={this.select.bind(this, fileName)}
             className="nav-link admin-file-json">{fileName}</a>
        </li>);
    }, this);
  }
}

export default JSONList;
