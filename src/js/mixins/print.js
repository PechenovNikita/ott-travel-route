"use strict";

let beforePrintFunctions = []
  , afterPrintFunctions = [];


const beforePrint = function () {
  beforePrintFunctions.forEach(function(func){
    return func();
  });
};
const afterPrint = function () {
  afterPrintFunctions.forEach(function(func){
    return func();
  });
};

(function () {

  if (window.matchMedia) {
    let mediaQueryList = window.matchMedia('print');
    mediaQueryList.addListener(function (mql) {
      if (mql.matches) {
        beforePrint();
      } else {
        afterPrint();
      }
    });
  }

  window.onbeforeprint = beforePrint;
  window.onafterprint = afterPrint;

}());



export function whenPrint(beforeCallback, affterCallbak){
  beforePrintFunctions.push(beforeCallback);
  afterPrintFunctions.push(affterCallbak);
}