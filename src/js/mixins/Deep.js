"use strict";

import extend from './deep/extend';

export const deepExtend = extend;

export default {
  extend : extend
};
