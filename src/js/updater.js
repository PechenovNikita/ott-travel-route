'use strict';

/**
 * @typedef {Object} Updater
 * @prop {function} add
 * @prop {function} resize
 * @prop {function} do
 * @prop {function} icons
 * @prop {function} googleMapInit
 * @prop {function} beforePrint
 * @prop {function} beforePrintDo
 * @prop {function()=>boolean} googleMapLoaded
 */

/**
 *
 * @type {Array<function>}
 */
let updateFunctions = []
  , resizeFunctions = []
  , mapFunctions = []
  , iconsFunctions = []
  , beforePrintFunctions = []
  , afterPrintFunctions = [];

let googleMapLoad = false, iconsLoaded = false;

window.initMap = function () {
  googleMapLoad = true;
  whenMapInit();
};

/**
 *
 */
function whenMapInit() {
  mapFunctions = mapFunctions.filter(function (callback) {
    callback();
    return false;
  });
}

function whenResize() {
  resizeFunctions.forEach(function (callback) {
    callback();
  });
}

/**
 *
 * @type {Updater}
 */
let Updater = {

  init() {
    $(window).on('resize', whenResize);
  },
  /**
   *
   * @param {function} callback
   * @returns {Updater}
   */
  add(callback) {
    updateFunctions.push(callback);
    return Updater;
  },

  icons(callback) {
    if (iconsLoaded) {
      setTimeout(callback, 10);
    } else {
      iconsFunctions.push(callback);
    }
    return Updater;
  },

  iconsLoad() {
    iconsLoaded = true;
    iconsFunctions.forEach(function (callback) {
      callback();
    });
    return Updater;
  },
  /**
   *
   * @param {function} callback
   * @returns {Updater}
   */
  googleMapInit(callback) {
    if (googleMapLoad)
      callback();
    else
      mapFunctions.push(callback);
    return Updater;
  },
  /**
   *
   * @returns {boolean}
   */
  googleMapLoaded() {
    return googleMapLoad;
  },
  /**
   *
   * @param {function} callback
   * @returns {Updater}
   */
  resize(callback) {
    resizeFunctions.push(callback);
    return Updater;

  },
  /**
   *
   * @returns {Updater}
   */
  do() {

    updateFunctions.forEach(function (callback) {
      callback();
    });
    return Updater;
  },
  beforePrint(callback) {
    beforePrintFunctions.push(callback);
  },
  afterPrint(callback) {
    afterPrintFunctions.push(callback);
  },

  beforePrintDo() {

    beforePrintFunctions.forEach(function (callback) {
      callback();
    });
    return Updater;
  }
};

Updater.init();

export default Updater
