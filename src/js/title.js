// import {whenPrint} from './mixins/print';

(function () {
  "use strict";

  jQuery.fn.reverseEach = (function () {
    var list = jQuery([1]);
    return function (c) {
      var el, i = this.length;
      try {
        while (i-- > 0 && (el = list[0] = this[i]) && c.call(list, i, el) !== false);
      }
      catch (e) {
        delete list[0];
        throw e;
      }
      delete list[0];
      return this;
    };
  }());

  var $title = $('.block-title__top__route')
    , $inners = $title.find('.block-title__top__route__inner');

  $title.removeClass('no-js');

  function resize() {
    var height = $title.height(), noAfter = false;

    $inners.removeClass('no-after').reverseEach(function (index, el) {
      if (index == $inners.length - 1) {
        $(el).addClass('no-after');
      }
      if (noAfter)
        $(el).addClass('no-after');
      noAfter = false;
      $(el).remove();
      $title.height('');
      if (height > $title.height() && (height - $title.height()) > 5)
        noAfter = true;
      height = $title.height();
    });

    $title.append($inners);
  }

  resize();

  // whenPrint(resize, resize);

  $(window).resize(resize);
}());