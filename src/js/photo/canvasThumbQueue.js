"use strict";

import ImageQueue from './imageQueue';

class CanvasThumbQueue {
  /**
   *
   * @param {number} width
   * @param {number} height
   * @param {number=} border
   * @param {function=} callback
   */
  constructor(width, height, border = 0, callback = ()=> {}) {

    this._border = border;
    /**
     *
     * @type {Element|HTMLCanvasElement}
     */
    this._canvas = document.createElement('canvas');
    /**
     *
     * @type {CanvasRenderingContext2D}
     */
    this._context = this._canvas.getContext('2d');

    this._whenDone = callback;

    this._canvas.width = width;
    this._canvas.height = height;

    this._count = 0;
    this._queue = [];

    this._images = [];

  }

  /**
   *
   * @param {string} src
   * @param {number} sx - The x coordinate where to start clipping
   * @param {number} sy - The y coordinate where to start clipping
   * @param {number} swidth - The width of the clipped image
   * @param {number} sheight - The height of the clipped image
   * @param {number} x - The x coordinate where to place the image on the canvas
   * @param {number} y - The y coordinate where to place the image on the canvas
   * @param {number} width - The width of the image to use (stretch or reduce the image)
   * @param {number} height - The height of the image to use (stretch or reduce the image)
   *
   * @returns {CanvasThumbQueue}
   */
  addImage(src, sx, sy, swidth, sheight, x, y, width, height) {

    let self = this;

    ImageQueue.load(src, function (err, image) {
      self.onLoad(image, sx, sy, swidth, sheight, x, y, width, height);
    }, 1000);

    // if (this._count >= 4) {
    //   this._queue.push([src, sx, sy, swidth, sheight, x, y, width, height]);
    //   return this;
    // }
    // this._count++;
    //
    // let image = new Image()
    //   , self = this;
    //
    // image.crossOrigin = true;
    //
    // image.onload = function () {
    //   self.onLoad(this, sx, sy, swidth, sheight, x, y, width, height);
    // };
    //
    // image.onerror = this.next.bind(this);
    //
    // image.src = src;
    // return this;
  }

  // next() {
  //   this._count--;
  //   if (this._count < 4 && this._queue.length > 0) {
  //     this.addImage.apply(this, this._queue.shift());
  //   } else if (this._count <= 0)
  //     this.onDone();
  // }

  /**
   *
   * @param {HTMLImageElement} img
   * @param {number} sx - The x coordinate where to start clipping
   * @param {number} sy - The y coordinate where to start clipping
   * @param {number} swidth - The width of the clipped image
   * @param {number} sheight - The height of the clipped image
   * @param {number} x - The x coordinate where to place the image on the canvas
   * @param {number} y - The y coordinate where to place the image on the canvas
   * @param {number} width - The width of the image to use (stretch or reduce the image)
   * @param {number} height - The height of the image to use (stretch or reduce the image)
   */
  onLoad(img, sx, sy, swidth, sheight, x, y, width, height) {

    this._images.push({
      src     : img,
      sx      : sx,
      sy      : sy,
      swidth  : swidth,
      sheight : sheight,
      x       : x,
      y       : y,
      width   : width,
      height  : height,
      hover   : false
    });

    this._context.drawImage(img, sx, sy, swidth, sheight, x + this._border / 2, y + this._border / 2, width - this._border, height - this._border);
    // this.next();
  }

  onHover(x, y) {
    this._images.forEach(function () {

    });
  }

  /**
   *
   */
  onDone() {
    if (this._whenDone)
      this._whenDone(this);
  }

  /**
   *
   * @returns {Element|HTMLCanvasElement}
   */
  getNode() {
    return this._canvas;
  }

  /**
   *
   * @returns {string}
   */
  getDataUrl() {
    return this._canvas.toDataURL("image/png");
  }

}

export default CanvasThumbQueue;