'use strict';
let $mNode, $mBody, $mWindow, $mWindowInner, $mShadow, $mNavigation, $mWindowDescription;
let open = false;
let nextFunc, prevFunc;

(function () {
  $mNode = $('<div class="modal"><div class="modal__back"></div></div>');
  $mBody = $('<div class="modal__body"></div>');
  $mWindow = $('<div class="modal__window"></div>');
  $mWindowInner = $('<div class="modal__window__inner"></div>');
  $mWindowDescription = $(`<div class="modal__window__description"></div>`);
  $mShadow = $('<div class="modal__shadow"></div>');

  $mNavigation = $('<div class="modal__navigation"><div class="modal__navigation__prev"><img src="%gulp-imagePath%photos/prev.svg" alt="Назад"></div><div class="modal__navigation__next"><span><img src="%gulp-imagePath%photos/next.svg" alt="Вперед"></span></div></div>');

  $mNode.append($mBody.append($mShadow).append($mWindow.append($mWindowInner).append($mWindowDescription).append($mNavigation)));

  $('body').append($mNode);

  $(window).on('keydown', function (event) {
    if (!open)
      return true;

    switch (event.keyCode) {
      //esc
      case 27:
        open = false;
        $mNode.removeClass('open');
        break;
      // <-
      case 37:
        if (prevFunc)
          prevFunc();
        break;
      // ->
      case 39:
        if (nextFunc)
          nextFunc();
        break;
    }
  });

  $mNode.on('click', function (event) {
    let target = event.target;

    while (target !== this) {
      if ($(target).hasClass('modal__window'))
        return;
      target = target.parentNode;
    }

    open = false;
    $mNode.removeClass('open');

  });

  $mNavigation.on('click', '.modal__navigation__prev', function (event) {
    if (prevFunc)
      prevFunc();
  });
  $mNavigation.on('click', '.modal__navigation__next', function (event) {
    if (nextFunc)
      nextFunc();
  });

}());

class Modal {

  constructor(nextCallback, prevCallback) {
    this._next = nextCallback;
    this._prev = prevCallback;
  }

  static show(imageUrl, settings) {
    if (settings.description) {
      $mWindowDescription.html(settings.description);
    } else {
      $mWindowDescription.html('');
    }

    $mWindowInner.css('background-image', 'url(\'' + imageUrl + '\')');

    if (!settings.prev)
      $mNavigation.find('.modal__navigation__prev').addClass('hide');
    else
      $mNavigation.find('.modal__navigation__prev').removeClass('hide');

    if (!settings.next)
      $mNavigation.find('.modal__navigation__next').addClass('hide');
    else
      $mNavigation.find('.modal__navigation__next').removeClass('hide');
  }

  next() {
    this._next();
  }

  prev() {
    this._prev();
  }

  open($div, imageUrl, settings) {

    nextFunc = this.next.bind(this);
    prevFunc = this.prev.bind(this);

    $mWindowInner.css('background-image', '');

    let offset = $div.offset()
      , w = $div.width()
      , h = w
      , t = offset.top - $(window).scrollTop()
      , l = offset.left - $(window).scrollLeft();

    $mWindow.css({
      position : 'absolute',
      left     : l,
      top      : t,
      right    : $(window).width() - l - w,
      bottom   : $(window).height() - t - h,
      width    : w,
      opacity  : 0,
      height   : h
    });

    $mShadow.css({
      position           : 'absolute',
      left               : l,
      top                : t,
      right              : $(window).width() - l - w,
      bottom             : $(window).height() - t - h,
      width              : w,
      height             : h,
      opacity            : 1,
      'background-image' : $div.css('background-image')
    });

    $mNode.addClass('open');
    open = true;
    Modal.show(imageUrl, settings);

    setTimeout(function () {

      $mWindow.css({
        position : 'absolute',
        left     : 0,
        top      : 0,
        right    : 0,
        bottom   : 0,
        opacity  : 1,
        width    : '',
        height   : ''
      });

      $mShadow.css({
        position : 'absolute',
        left     : 0,
        top      : 0,
        right    : 0,
        bottom   : 0,
        opacity  : 1,
        width    : '',
        height   : ''
      });
    }, 10)
  }
}

export default Modal;
