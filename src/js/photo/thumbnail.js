"use strict";

// import CanvasThumbQueue from './canvasThumbQueue';
import DivThumbQueue from './divThumbQueue';
import Updater from './../updater';
// s	small square 75x75
// q	large square 150x150
// t	thumbnail, 100 on longest side
// m	small, 240 on longest side
// n	small, 320 on longest side
// -	medium, 500 on longest side
// z	medium 640, 640 on longest side
// c	medium 800, 800 on longest side†
// b	large, 1024 on longest side*
// h	large 1600, 1600 on longest side†
// k	large 2048, 2048 on longest side†
/**
 *
 * @param photoData
 * @returns {string}
 */
function getUrl(photoData) {
  if (photoData)
    return 'https://farm' + photoData.farm + '.staticflickr.com/' + photoData.server + '/' + photoData.id + '_' + photoData.secret + '_q.jpg';
  return null;
}

/**
 *
 * @param photo
 * @param size
 * @returns {*|jQuery|HTMLElement}
 */
  // function createThumbnail(photo, size) {
  //
  //   if (!photo)
  //     return;
  //
  //   var $thumb = $('<div class="block-photo-back__thumb"></div>');
  //   $thumb.css({
  //     width              : size,
  //     height             : size,
  //     'background-image' : 'url(' + getUrl(photo) + ')'
  //   });
  //   return $thumb;
  // }
  //
  // function createBoxThumbnail(size) {
  //   let $box = $('<div class="block-photo-back__box"></div>');
  //
  //   $box.css({
  //     width  : size,
  //     height : size
  //   });
  //
  //   return $box
  // }

class CardThumbnail {

  /**
   *
   * @param $el
   * @param data
   * @param big
   */
  constructor($el, data, big) {
    this._big = !!big;
    this._data = data.photo;
    this._count = (data.total > 100 ? 100 : data.total);
    this._$el = $el;
    this._size = 0;

    this._scale = 0;
    /**
     *
     * @type {Object.<number, CanvasThumbQueue>}
     * @private
     */
    this._backs = {};

    this.__setScale().init();

    Updater.resize(this.resize.bind(this));
    let self = this;
    this._$el.on('click', '.block-photos-cell', function(){
      self.onClick($(this).data('data'), $(this));
    });
    // $(window).on('resize', this.resize.bind(this));

  }

  whenClick(callback) {
    this._click = callback;
  }

  /**
   *
   * @returns {number}
   */
  static getBigCount() {
    let w = $(window).width();
    /*if (w >= 560 && w < 768) {
     return 2;
     } else */
    if (w >= 768 && w < 1024) {
      return 3;
    } else if (w >= 1024) {
      return 4;
    }
    return 2;
  }

  /**
   *
   * @returns {number}
   */
  static getBigSize() {
    let w = $(window).width();
    if (w >= 560 && w < 768) {
      return 480;
    } else if (w >= 768 && w < 1024) {
      return 700;
    } else if (w >= 1024) {
      return 940;
    }
    return 240;
  }

  /**
   *
   * @returns {number}
   */
  static getSmallSize() {
    let w = $(window).width();
    if (w >= 560 && w < 768) {
      return 230;
    } else if (w >= 768 && w < 1024) {
      return 220;
    } else if (w >= 1024) {
      return 220;
    }
    return 240;
  }

  /**
   *
   * @returns {number}
   */
  static getScale() {
    let w = $(window).width();
    if (w >= 560 && w < 768) {
      return 1;
    } else if (w >= 768 && w < 1024) {
      return 2;
    } else if (w >= 1024) {
      return 3;
    }
    return 0;
  }

  /**
   *
   * @param {number=} value
   * @returns {CardThumbnail}
   * @private
   */
  __setScale(value = CardThumbnail.getScale()) {
    this._scale = value;
    return this;
  }

  /**
   *
   * @param {number=} iteration
   * @returns {number}
   */
  static genInRow(iteration) {
    let inner = 1;
    if (iteration > 0) {
      let rand = Math.random();

      if (rand > (iteration > 1 ? .3 : .7)) {

        // if ((rand > (iteration > 2 ? .7 : .9)) && iteration > 2) {
        // if (rand > .95 && iteration > 2) {
        //   inner = 16;
        // } else {
        //   inner = 9;
        // }
        // } else {
        inner = 4;
        // }

      }

    }

    return inner;
  }

  /**
   *
   * @param {jQuery} $container
   * @param {number} width
   * @param {number} scale
   * @param {number=} iteration
   */
  // generateThumbBox($container, width, scale, iteration = 3) {
  //
  //   /**
  //    *
  //    * @type {number}
  //    */
  //   let inner = CardThumbnail.genInRow(iteration);
  //
  //   if (inner > 1) {
  //
  //     let $box = createBoxThumbnail(width / scale);
  //
  //     $container.append($box);
  //     $container = $box;
  //
  //     for (let i = 0; i < inner; i++) {
  //       this.generateThumbBox($container, width, scale * (Math.pow(inner, .5)), iteration - 1);
  //       // this._index++;
  //     }
  //   } else {
  //     $container.append(createThumbnail(this._data[this._index], width / scale));
  //     this._index++;
  //   }
  //
  // }

  /**
   *
   * @param {DivThumbQueue} canvasQueue
   * @param {number} x
   * @param {number} y
   * @param {number} size
   * @param {number} scale
   * @param {number} iteration
   */
  generateThumbCanvas(canvasQueue, x, y, size, scale, iteration = 3) {

    /**
     *
     * @type {number}
     */
    let inner = CardThumbnail.genInRow(iteration);

    if (inner > 1) {

      /**
       *
       * @type {number}
       */
      let sqrt = Math.pow(inner, .5)
        /**
         *
         * @type {number}
         */
        , newSize = size / (scale * sqrt);
      for (let dy = 0; dy < sqrt; dy++) {
        for (let dx = 0; dx < sqrt; dx++) {
          this.generateThumbCanvas(canvasQueue, x + dx * newSize, y + dy * newSize, size, scale * sqrt, iteration - 1);
        }
      }

    } else {
      let data = this._data[this._index];
      /**
       *
       * @type {string}
       */
      let url = getUrl(data);
      if (url)
        canvasQueue.addImage(url, 0, 0, 150, 150, x, y, (size / scale), (size / scale), data);

      this._index++;
    }

  }

  onClick(data, el) {
    if (this._click)
      this._click(data, el);
  }

  /**
   *
   */
  init() {

    let inRow = (this._big ? CardThumbnail.getBigCount() : 2)
      , w = (this._big ? CardThumbnail.getBigSize() : CardThumbnail.getSmallSize())
      , h = (this._big ? (w / (inRow)) : 160)
      , inColumn = 160 / (w / inRow);

    this._$el.closest('.block-photos').height(h);

    if (this._backs[w]) {
      this._$el.html('');
      this._$el.append(this._backs[w].getNode());
      return;
    }

    // let canvas = new CanvasThumbQueue(w, h, 1, function (can) {
    //   // console.log('done');
    //   // $el.html('');
    //   // var eurl = canvas.getDataUrl();
    //
    //   // console.log(eurl);
    //   // $el.css('background-image', 'url(' + canvas.getDataUrl() + ')');
    //   // console.log(can, this, canvas);
    // });
    /**
     *
     * @type {DivThumbQueue}
     */
    let canvas = new DivThumbQueue(w, h, 1, function (can) {
      // console.log('done');
      // $el.html('');
      // var eurl = canvas.getDataUrl();

      // console.log(eurl);
      // $el.css('background-image', 'url(' + canvas.getDataUrl() + ')');
      // console.log(can, this, canvas);
    });

    this._$el.html('').append(canvas.getNode());
    this._index = 0;
    this._count = Math.ceil(inRow * inColumn);
    this._backs[w] = canvas;

    if (this._big) {
      inColumn = 1;
      this._count = inRow;
    }

    this._$el.closest('.block-photos').height(h);

    let size = w / inRow;

    for (let dy = 0; dy < inColumn; dy++) {
      for (let dx = 0; dx < inRow; dx++) {
        this.generateThumbCanvas(canvas, dx * size, dy * size, w, inRow, this._big ? 2 : 3);
      }
    }

    // this._$el.append(fragment);

    // this._size = w;

  }

  /**
   *
   */
  resize() {
    let scale = CardThumbnail.getScale();
    if (this._scale != scale) {
      this.__setScale(scale).init();
    }
  }
}

export default CardThumbnail;
