"use strict";

function random(min, max) {
  let num = min + Math.round(Math.random() * (max - min));

  return num < 10 ? '0' + num.toString() : num.toString();
}

import ImageQueue from './imageQueue';
/**
 *
 * @param {number} x
 * @param {number} y
 * @param {number} w
 * @param {number} h
 * @param {ResponseFlickrPhoto} data
 * @returns {jQuery}
 */
function createImageCell(x, y, w, h, data) {
  let cell = $('<div class="block-photos-cell" data-id="' + data.id + '"></div>');
  cell.css({
    width              : w,
    height             : h,
    top                : y,
    left               : x,
    'background-color' : 'rgba(' + random(0, 255) + ',' + random(0, 255) + ',' + random(0, 255) + ', .3)'
  });

  cell.data('data', data);

  return cell;
}

class DivThumbQueue {
  /**
   *
   * @param {number} width
   * @param {number} height
   * @param {number=} border
   * @param {function=} callback
   */
  constructor(width, height, border = 0, callback = ()=> {}) {

    this._$wrapper = $('<div></div>');
    this._$wrapper.css({
      position : 'relative',
      width    : width,
      height   : height
    });

    this._border = border;

  }

  /**
   *
   * @param {string} src
   * @param {number} sx - The x coordinate where to start clipping
   * @param {number} sy - The y coordinate where to start clipping
   * @param {number} swidth - The width of the clipped image
   * @param {number} sheight - The height of the clipped image
   * @param {number} x - The x coordinate where to place the image on the canvas
   * @param {number} y - The y coordinate where to place the image on the canvas
   * @param {number} width - The width of the image to use (stretch or reduce the image)
   * @param {number} height - The height of the image to use (stretch or reduce the image)
   * @param {ResponseFlickrPhoto} data
   *
   * @returns {DivThumbQueue}
   */
  addImage(src, sx, sy, swidth, sheight, x, y, width, height, data) {

    let $wrap = createImageCell(x + this._border / 2, y + this._border / 2, width - this._border, height - this._border, data);

    this._$wrapper.append($wrap);

    let self = this;

    ImageQueue.load(src, function (err, image) {
      if (err.success)
        self.onLoad($wrap, image, sx, sy, swidth, sheight, x, y, width, height);
    }, 10000);

    return this;

  }

  /**
   *
   * @param {jQuery} $wrap
   * @param {HTMLImageElement} img
   * @param {number} sx - The x coordinate where to start clipping
   * @param {number} sy - The y coordinate where to start clipping
   * @param {number} swidth - The width of the clipped image
   * @param {number} sheight - The height of the clipped image
   * @param {number} x - The x coordinate where to place the image on the canvas
   * @param {number} y - The y coordinate where to place the image on the canvas
   * @param {number} width - The width of the image to use (stretch or reduce the image)
   * @param {number} height - The height of the image to use (stretch or reduce the image)
   */
  onLoad($wrap, img, sx, sy, swidth, sheight, x, y, width, height) {
    $wrap.css({'background-image' : "url('" + img.src + "')"})
  }

  onHover(x, y) {

  }

  /**
   *
   */
  onDone() {

  }

  /**
   *
   * @returns {Element}
   */
  getNode() {
    return this._$wrapper;
  }

  /**
   *
   * @returns {string}
   */
  getDataUrl() {
  }

}

export default DivThumbQueue;