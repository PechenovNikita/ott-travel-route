'use strict';
import Thumb from './thumbnail';
import JSONP from './../JSONP';

import Updater from '../updater';

import Modal from './modal';

/**
 * @typedef {Object} ResponseFlickrPhoto
 * @property {number} farm
 * @property {string|number} id
 * @property {number|boolean} isfamily
 * @property {number|boolean} isfriend
 * @property {number|boolean} ispublic
 * @property {string} owner
 * @property {string} secret
 * @property {string|number} server
 * @property {string} title
 */

/**
 * @typedef {Object} ResponseFlickrPhotos
 * @property {number} page
 * @property {number} pages
 * @property {number} perpage
 * @property {Array<ResponseFlickrPhoto>} photo
 * @property {string|number} total
 */

// https://farm{farm-id}.staticflickr.com/{server-id}/{id}_{secret}.jpg
// https://farm{farm-id}.staticflickr.com/{server-id}/{id}_{secret}_[mstzb].jpg
// https://farm{farm-id}.staticflickr.com/{server-id}/{id}_{o-secret}_o.(jpg|gif|png)

function generateImageUrlFromData(photoData) {
  if (photoData)
    return 'https://farm' + photoData.farm + '.staticflickr.com/' + photoData.server + '/' + photoData.id + '_' + photoData.secret + '.jpg';
  return null;
}

class Flickr {
  constructor($el) {
    this._$el = $el;
    $el.data('driver', this);
    this.init();

    this._modal = new Modal(this.showNext.bind(this), this.showPrev.bind(this));
  }

  init() {

    let $el = this._$el;
    this._tags = $el.data('tags');
    this._options = $el.data('options');
    this._text = $el.data('text');
    this._location = $el.data('location');
    this._thumb = null;

    /**
     *
     * @type {ResponseFlickrPhoto[]}
     * @private
     */
    this._photos = [];

    JSONP.getPhotos(this._tags, this._options, this._location, this._text, this.__onLoad.bind(this));
  }

  /**
   *
   * @param {{stat:string,photos:ResponseFlickrPhotos}} response
   * @param id
   * @private
   */
  __onLoad(response, id) {
    if (response.stat === 'ok') {
      if (!this._thumb) {

        this._total = parseInt(response.photos.total, 10);
        this._currentPage = parseInt(response.photos.page, 10);

        this._thumb = new Thumb(this._$el.find('.block-photo-back').first(), response.photos, this._$el.hasClass('block-photos'));

        this._thumb.whenClick(this.onClick.bind(this));

        this._photos = response.photos.photo;
      } else {
        this._photos.push(response.photos.photo);
        this._currentPage = parseInt(response.photos.page, 10);
      }
      Updater.do();
    } else {
      console.error('Error load flickr data');
    }
  }

  __getCurrentIndex(data) {
    for (let i = 0; i < this._photos.length; i++) {
      if (data.id == this._photos[i].id) {
        this._index = i;
        break
      }
    }

    this.__genNextPrev();
  }

  __getCurrentIndexById(id) {
    this._index = 0;

    for (let i = 0; i < this._photos.length; i++) {
      if (id == this._photos[i].id) {
        this._index = i;
        break
      }
    }

    this.__genNextPrev();

  }

  __genNextPrev() {
    this._prev = this._index > 0;
    this._next = this._index < this._photos.length - 1;
  }

  /**
   *
   * @param {ResponseFlickrPhoto} data
   * @param {HTMLElement} el
   */
  onClick(data, el) {
    // this.__getCurrentIndex(data);
    this.__getCurrentIndexById($(el).data('id'));
    this._modal.open($(el), generateImageUrlFromData(this._photos[this._index]), {
      prev : this._prev,
      next : this._next
    });
  }

  showPrev() {
    if (this._index == 0) {
      return;
    }

    this._index--;
    this.__genNextPrev();

    Modal.show(generateImageUrlFromData(this._photos[this._index]), {
      prev : this._prev,
      next : this._next
    });
  }

  showNext() {

    // if (this._index >= this._photos.length - 10 && this._total > this._photos.length) {
    //   this.getMore();
    // }

    if (this._index >= this._photos.length - 1) {
      return;
    }

    this._index++;
    this.__genNextPrev();

    Modal.show(generateImageUrlFromData(this._photos[this._index]), {
      prev : this._prev,
      next : this._next
    });

  }

  getMore() {
    JSONP.getPhotos(this._tags, this._options, this._location, this.__onLoad.bind(this), this._currentPage + 1);
  }
}

export default Flickr

// $(document).ready(function () {
//   $('.custom-photo').each(function(index, item){
//     console.log(item);
//     //.custom-photo-preview
//   })
// });