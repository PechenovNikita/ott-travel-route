'use strict';
import Thumb from './thumbnail';
import JSONP from './../JSONP';

import Updater from '../updater';

import Modal from './modal';
//
// /**
//  * @typedef {Object} ResponseFlickrPhoto
//  * @property {number} farm
//  * @property {string|number} id
//  * @property {number|boolean} isfamily
//  * @property {number|boolean} isfriend
//  * @property {number|boolean} ispublic
//  * @property {string} owner
//  * @property {string} secret
//  * @property {string|number} server
//  * @property {string} title
//  */
//
// /**
//  * @typedef {Object} ResponseFlickrPhotos
//  * @property {number} page
//  * @property {number} pages
//  * @property {number} perpage
//  * @property {Array<ResponseFlickrPhoto>} photo
//  * @property {string|number} total
//  */
//
// // https://farm{farm-id}.staticflickr.com/{server-id}/{id}_{secret}.jpg
// // https://farm{farm-id}.staticflickr.com/{server-id}/{id}_{secret}_[mstzb].jpg
// // https://farm{farm-id}.staticflickr.com/{server-id}/{id}_{o-secret}_o.(jpg|gif|png)
//
function generateImageUrlFromData(photoData) {
  if (photoData)
    return 'https://farm' + photoData.farm + '.staticflickr.com/' + photoData.server + '/' + photoData.id + '_' + photoData.secret + '.jpg';
  return null;
}

class CustomFlickr {
  constructor($el, items) {
    this._$el = $el;
    this._items = items;
    this._photos = items.map((item) => generateImageUrlFromData(item));
    this._modal = new Modal(this.showNext.bind(this), this.showPrev.bind(this));

    this.init();
  }

  init() {
    this._$el.on('click', '.custom-photo-preview', (ev) => {
      const $el = $(ev.target);
      let url = $(ev.target).data('url');

      this._index = this._photos.indexOf(url);

      this.__genNextPrev();

      this._modal.open($el, url, {
        prev        : this._prev,
        next        : this._next,
        description : `${this._index + 1}/${this._photos.length}`,
      });
    });

    this._$el.on('click', '.custom-photo-more', (ev) => {
      const $el = $(ev.target);
      this._index = this._$el.find('.custom-photo-preview').length;
      this.__genNextPrev();
      this._modal.open($el, this._photos[this._index], {
        prev : this._prev,
        next : this._next,
        description : `${this._index + 1}/${this._photos.length}`,
      });
    });
  }

  __getCurrentIndexById(id) {
    this._index = 0;

    for (let i = 0; i < this._photos.length; i++) {
      if (id === this._photos[i].id) {
        this._index = i;
        break
      }
    }

    this.__genNextPrev();

  }

  __genNextPrev() {
    this._prev = this._index > 0;
    this._next = this._index < this._photos.length - 1;
  }

  showPrev() {
    if (this._index <= 0) {
      return;
    }

    this._index--;
    this.__genNextPrev();

    Modal.show(this._photos[this._index], {
      prev : this._prev,
      next : this._next,
      description : `${this._index + 1}/${this._photos.length}`,
    });
  }

  showNext() {

    if (this._index >= this._photos.length - 1) {
      return;
    }

    this._index++;
    this.__genNextPrev();

    Modal.show(this._photos[this._index], {
      prev : this._prev,
      next : this._next,
      description : `${this._index + 1}/${this._photos.length}`,
    });

  }

  getMore() {
    JSONP.getPhotos(this._tags, this._options, this._location, this.__onLoad.bind(this), this._currentPage + 1);
  }
}

$(document).ready(function () {
  $('.custom-photo').each(function (index, item) {
    const $item = $(item);
    const items = $item.data('items');

    new CustomFlickr($item, items);
  })
});
