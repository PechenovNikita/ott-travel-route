"use strict";

/**
 * @typedef {Object} ImageQueue
 * @property {function} load
 * @property {function} __loadImage
 * @property {function} next
 */

/**
 *
 * @type {Array<Image>}
 */
let images = [];
/**
 *
 * @type {Array<Array<string,function, number>>}
 */
let queue = [];
/**
 *
 * @type {number}
 */
let count = 0;

/**
 *
 * @type {*[]}
 */
const errors = [{
  success : true,
  error   : false
}, {
  success : false,
  error   : true,
  message : 'Load error'
}, {
  success : false,
  error   : true,
  message : 'Time over'
}];
/**
 *
 * @type {number}
 */
const maxLoaded = 4;

/**
 *
 * @type {ImageQueue}
 */
var ImageQueue = {};

ImageQueue = {
  /**
   *
   * @param {string} src
   * @param {function(error:Object,image:Image)} callback
   * @param {number=} timeLimit
   * @returns {ImageQueue}
   */
  load(src, callback, timeLimit){

    if (images[src]) {
      callback(errors[0], images[src]);
      return ImageQueue;
    }

    if (count >= maxLoaded) {
      queue.push([src, callback, timeLimit]);
      return ImageQueue;
    }

    return ImageQueue.__loadImage(src, callback, timeLimit);
  },
  /**
   *
   * @param {string} src
   * @param {function(error:Object,image:Image)} callback
   * @param {number} timeLimit
   * @returns {ImageQueue}
   * @private
   */
  __loadImage(src, callback, timeLimit){
    count++;

    let image = new Image();

    image.crossOrigin = true;

    image.onload = function () {

      if (this.stop)
        return;

      callback(errors[0], this);

      ImageQueue.next()
    };

    image.onerror = function () {
      if (this.stop)
        return;

      callback(errors[1], this);

      ImageQueue.next()
    };

    if (timeLimit) {
      setTimeout(function () {
        image.stop = true;

        callback(errors[2], this);

        ImageQueue.next();
      }, timeLimit)
    }

    image.src = src;
    return ImageQueue;
  },
  /**
   *
   */
  next(){
    count--;

    if (count < maxLoaded && queue.length > 0) {
      ImageQueue.__loadImage.apply(null, queue.shift());
    }
  }
};

export default ImageQueue