// import JSONP from './JSONP';
// import Thumb from './photo/thumbnail';
import Updater from './updater';
import Flickr from './photo/flickr'
import './photo/CustomPhoto'

function init() {
  $('.block-card__photo:visible, .block-photos:visible').each(function (index, el) {
    if (!$(el).data('driver'))
      new Flickr($(el))
  });
}

$(document).ready(init);

Updater.add(init);
