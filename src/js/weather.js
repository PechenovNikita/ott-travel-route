'use strict';

import JSONP from './JSONP';

const api = '59cc8ee54bc95bc1b1bb6e79a26db2fc';

function getWeather(lat, lng, callback) {
  const url = `http://api.openweathermap.org/data/2.5/forecast?lat=${lat}&lon=${lng}&lang=ru&units=metric&APPID=${api}`;
  JSONP(url, callback);
}

function parseDate(dateStr) {
  let date = new Date(dateStr);
  if (!!(date.getTime()))
    return date;
  const temp = dateStr.split(/[-:\s]+/g);
  return new Date(temp[0], temp[1] - 1, temp[2], temp[3], temp[4], temp[5]);
}

$(document).ready(function () {
  $('.weather').each((idex, node) => {
    const $node = $(node);
    const data = $node.data('options');
    new Weather($node, data);
  })
});

class Weather {
  constructor($node, data) {
    this.$node = $node;
    this.data = data;

    this.__init();
  }

  __init() {
    getWeather(this.data.lat, this.data.lng, this.__parseData.bind(this))
  }

  __parseData(response) {

    if (response.list) {

      let weather = [];
      let lastDay = 0;

      response.list.forEach((item) => {
        const date = (parseDate(item.dt_txt));
        if (lastDay !== date.getDate()) {
          weather.push([
            item
          ]);
        } else {
          weather[weather.length - 1].push(item);
        }
        lastDay = date.getDate();
      });

      weather = weather.map((day) => {
        if (day.length === 8) {
          return [day[3], day[7]]
        } else if (day.length > 2) {
          return [day[0], day[day.length - 1]];
        }
        return day;
      }).filter((item, index) => index < 4);

      this.weather = weather;
      this.render();

    } else {
      this.$node.html('<div class="alert">Ошибка при загрузке погоды!</div>')
    }
  }

  render() {
    this.$node.addClass('done').html(this.weather.reduce((str, item, index) => str + this.__renderItemDN(item, index), ''));
  }

  __renderItemDN(day, index) {
    const d = day[0];
    const n = day[1] || null;
    if (index === 0) {
      return (`<div class="weather__current-item">${this.__renderInner(d, n, index)}</div>`);
    }
    return (`<div class="weather__item">${this.__renderInner(d, n, index)}</div>`)
  }

  __renderInner(day, night, index) {
    const date = parseDate(day.dt_txt);
    return `<table class="weather__table">
      <tbody>
      <tr>
        <td class="weather__date" colSpan="${night ? 2 : 1}">
          <div>${index === 0 ? 'Сегодня' : dateFormat(date)}</div>
        </td>
      </tr>
      <tr>
        <td style="text-align: center; vertical-align: bottom">
          <img class="weather__icon--big" src="${`/public/images/weather/${day.weather[0].icon}.svg`}"/>
        </td>
        ${(() => {
      if (night) {
        return (`<td style="text-align: left; vertical-align: bottom"><img class="weather__icon--medium" src="${`/public/images/weather/${night.weather[0].icon}.svg`}" alt=""/></td>`);
      }
      return ''
    })()}

      </tr>
      <tr>
        <td style="vertical-align: top; text-align: center" >
          <div class="weather__temp--big">${day.main.temp | 0}&deg;
            <span class="weather__cel">С</span>
          </div>
        </td>
        ${(() => {
      if (night) {
        return (
          `<td style="vertical-align: top; text-align: left" >
                <div class="weather__temp--medium">${night.main.temp | 0}&deg;
                  <span class="weather__cell">С</span>
                </div>
              </td>`
        );
      }
      return ``
    })()}

      </tr>
      <tr>
        <td class="weather__description" colSpan="${night ? 2 : 1}">
          <div>${day.weather[0].description}</div>
        </td>
      </tr>
      </tbody>
    </table>`
  }
}

const months = ['янв', 'фев', 'март', 'апр', 'май', 'июнь', 'июль', 'авг', 'сен', 'окт', 'ноя', 'дек']

function dateFormat(date) {
  return date.getDate() + ' ' + months[date.getMonth()];
}
