import Hotels from './api/Hotels';
import Flights from './api/Flights';
import Auto from './api/Auto';

import './api/Price';
if (typeof apiStore !== "undefined") {
  if (apiStore.length > 0) {
    apiStore.forEach(function (item) {
      Object.keys(item).forEach(function (key) {
        switch (key) {
          case "hotels":
          case "hotel":
            item[key].forEach((option) => {
              new Hotels(option);
            });
            break;
          case "flight":
          case "flights":
            item[key].forEach((option) => {
              new Flights(option);
            });
            break;
          case "car":
          case "cars":
          case "auto":
          case "autos":
            item[key].forEach((option) => {
              new Auto(option);
            });
            break;
        }
      })
    });

  }
}