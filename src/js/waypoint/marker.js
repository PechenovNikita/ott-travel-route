"use strict";
import Updater from './../updater';

var $window = $(window);

var $markers = $('.block-waypoint__marker');
var line = $('<div class="block-waypoint__line"></div>');
var line_active = $('<div class="block-waypoint__line block-waypoint__line__active"></div>');
$markers.first().before(line).before(line_active);

var firstTop, lastTop, offsetTop;
var height;

/**
 * @typedef {Object} Marker
 * @prop {function} init
 * @prop {function} initVariables
 * @prop {function} initEvents
 * @prop {function} scrolling
 * @prop {function} update
 */

/**
 *
 * @type {Marker}
 */
var Marker = {};

Marker.init = () => {
  Marker.initVariables().initEvents().scrolling();
};

/**
 *
 * @returns {Marker}
 */
Marker.initVariables = () => {
  firstTop = $markers.first().offset().top;
  lastTop = $markers.last().offset().top;
  offsetTop = $window.height() * .4;

  height = lastTop - firstTop;
  line.height(height);

  return Marker;
};

/**
 *
 * @returns {Marker}
 */
Marker.scrolling = () => {
  var scrollTop = $window.scrollTop();
  if (firstTop > scrollTop + offsetTop) {
    $markers.removeClass('active');
    line_active.height(0);

  } else if (lastTop < scrollTop + offsetTop) {
    $markers.addClass('active');
    line_active.height(height);
  } else {
    $markers.each(function (index, el) {
      var $el = $(el);
      if ($el.offset().top <= scrollTop + offsetTop) {
        $el.addClass('active');
      } else {
        $el.removeClass('active');
      }
    });
    line_active.height(scrollTop - firstTop + offsetTop);
  }

  return Marker;
};

/**
 *
 * @returns {Marker}
 */
Marker.update = () => {
  return Marker.initVariables().scrolling();
};

/**
 *
 * @returns {Marker}
 */
Marker.initEvents = () => {

  $window.on('scroll', Marker.scrolling);
  Updater
    .resize(Marker.update);
  Updater
    .add(Marker.update);
  return Marker;

};

$(document).ready(() => {
  Marker.init();
});

export default Marker;





