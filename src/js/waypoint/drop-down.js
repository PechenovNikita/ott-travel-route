"use strict";

import Updater from './../updater';
import {whenPrint} from './../mixins/print';

let $drop = $('.block-waypoint__drop');

$drop.on('click', '.block-waypoint__drop__title', function (event) {
  event.stopPropagation();
  event.preventDefault();
  let $thisDrop = $(this).parent();
  $thisDrop.toggleClass('opened');

  $thisDrop.find('.block-waypoint__drop__body').first().slideToggle(300, function () {
    console.log('Update.do');
    Updater.do();
  });
});

Updater.beforePrint(function () {
  $drop.each(function (index, item) {
    let $thisDrop = $(item);

    $thisDrop.data('beforePrint', $thisDrop.hasClass('opened')).addClass('opened');

    $thisDrop.find('.block-waypoint__drop__body').first().css('display', 'block');
    Updater.do();
  });
});

whenPrint(function () {
  // $drop.each(function (index, item) {
  //   let $thisDrop = $(item);
  //
  //   $thisDrop.data('beforePrint', $thisDrop.hasClass('opened')).addClass('opened');
  //
  //   $thisDrop.find('.block-waypoint__drop__body').first().css('display', 'block');
  //   Updater.do();
  // });
}, function () {

  $drop.each(function (index, item) {
    let $thisDrop = $(item);
    let opened = $thisDrop.data('beforePrint');


    if (opened) {
      $thisDrop.addClass('opened');
      $thisDrop.find('.block-waypoint__drop__body').first().css('display', 'block');
    } else {

      $thisDrop.removeClass('opened');
      $thisDrop.find('.block-waypoint__drop__body').first().css('display', 'none');
    }

  });
});