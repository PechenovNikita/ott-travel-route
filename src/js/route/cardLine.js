/**
 *
 * @param {string|{href:string,title:string}|string[]} leftContent
 * @param {string|{href:string,title:string}} rightContent
 * @param {{grey:boolean,btn:boolean}=} options
 * @returns {jQuery}
 */
var TRCardLine = (function () {
  "use strict";

  /**
   *
   * @param {string|{href:string,title:string}|string[]} content
   * @returns {string}
   */
  function createLeft(content) {
    var left = '<div class="block-card__line__left">';
    if (typeof content == 'string') {
      left += '<span class="block-card__line__left__inner">' + content + '</span>';
    } else if (content.href && content.title) {
      left += '<a href="' + content.href + '" class="block-card__line__left__link"><span class="block-card__line__left__link__inner">' + content.title + '</span></a>';
    } else {
      for (var i = 0; i < content.length; i++) {
        left += '<span class="block-card__line__left__inner">' + content[i] + '</span>';
      }
    }
    left += '</div>';

    return left;
  }

  /**
   *
   * @param {string} str
   * @returns {string}
   */
  function rubCreate(str) {
    return str.replace('%rub%', '<span class="icon-rub regular"></span>');
  }

  /**
   *
   * @param {string|{href:string,title:string}} content
   * @returns {string}
   */
  function createRight(content) {
    var right = '<div class="block-card__line__right">';

    if (typeof content == 'string') {
      right += '<span class="block-card__line__right__inner">' + content + '</span>'
    } else if (content.href && content.title) {
      right += '<a class="btn btn-card" href="' + content.href + '"><span class="btn__inner">' + rubCreate(content.title) + '</span></a>'
    }
    return right;
  }

  /**
   *
   * @param {string|{href:string,title:string}|string[]} leftContent
   * @param {string|{href:string,title:string}} rightContent
   * @param {{grey:boolean,btn:boolean}=} options
   * @returns {jQuery}
   */
  function ret(leftContent, rightContent, options) {
    var $line = $('<div class="block-card__line"></div>');

    if (options) {
      if (options.grey)
        $line.addClass('grey');
      if (options.btn)
        $line.addClass('with-btn');
    }

    $line.html(createLeft(leftContent) + createRight(rightContent));
    return $line;
  }

  return ret;
}());