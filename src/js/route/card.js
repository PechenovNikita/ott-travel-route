var TRCard = (function () {
  "use strict";

  function Card(options) {
    this._options = options;
    this.init();
  }

  Card.prototype = {
    init            : function () {
      this._node = $('<div class="block-card"></div>');
      this._front = $('<div class="block-card__frontside"></div>');

      switch (this._options.type) {
        case"flight":
          this._node.addClass('flight');
          this.__initFlight();
          break;
        case "hotel":
          this._node.addClass('hotel');
          this.__initHotel();
          break;
        case "excursion":
          this._node.addClass('excursion');
          this.__initExcursion();
          break;
        case "car":
          this._node.addClass('car');
          this.__initCar();
          break;
        case "other":
          this._node.addClass('other');
          this.__initOther();
          break;
        default:
          return;
      }

      if (this._options.price)
        this.__initPrice(['hotel', 'excursion'].indexOf(this._options.type) >= 0, this._options.type == 'excursion');

      this._node.append(this._front);

      if (this._options.back) {
        this.__initBackSide();
        this._node.append(this._back);
      }
      return this;
    },
    __initFlight    : function () {

      var _$flight = $('<div class="block-card__flight"></div>');

      var $fromto = $('<div class="block-card__flight__from-to"></div>');
      $fromto.html('<div class="block-card__flight__from-to__title"><span class="block-card__flight__from-to__title__inner">Авиаперелет</span></div>')
        .append(TRCardLine(this._options.from.title, this._options.from.time))
        .append(TRCardLine(this._options.to.title, this._options.to.time));

      var $across = $('<div class="block-card__flight__across"></div>');
      $across.append(TRCardLine(this._options.across.title, this._options.across.time, {grey : true}));

      _$flight.append($fromto).append($across);

      this._front.append(_$flight);

      return this;
    },
    __initHotel     : function () {
      this._front.append(this.__initImage());
    },
    __initExcursion : function () {
      this._front.append(this.__initImage());
    },
    __initImage     : function () {

      var _$image = $('<div class="block-card__image"></div>');

      var html = '';

      html += '<div class="block-card__image__back">';
      html += ' <div class="block-card__image__back__overlay"></div>';
      html += ' <div style="background-image: url(' + this._options.image.src + ');" class="block-card__image__back__img"></div>';
      html += '</div>';
      html += '<div class="block-card__image__text">';

      if (this._options.image.title) {
        html += '  <div class="block-card__image__text__title">';
        html += '    <span class="block-card__image__text__title__inner">' + this._options.image.title + '</span>';
        html += '  </div>';
      }

      if (this._options.image.description) {
        html += '<div class="block-card__image__text__desc">';
        html += '  <span class="block-card__image__text__desc__inner">' + this._options.image.description + '</span>';
        html += '</div>';
      }

      html += '</div>';

      _$image.html(html);

      return _$image;
    },
    __initCar       : function () {
      var _$car = $('<div class="block-card__car"></div>');

      var html = '';
      html += '<div class="block-card__car__text">';
      html += '  <div class="block-card__car__text__title">';
      html += '    <span class="block-card__car__text__title__inner">' + this._options.image.title + '</span>';
      html += '  </div>';
      html += '  <div class="block-card__car__text__image">';
      html += '    <img src="' + this._options.image.src + '" alt="' + this._options.image.title + '" class="block-card__car__text__image__img">';
      html += '  </div>';
      html += '</div>';

      _$car.html(html);
      this._front.append(_$car);

      return this;
    },
    __initOther     : function () {

      var _$other = $('<div class="block-card__other"></div>');

      var html = '';
      html += '<div class="block-card__other__from-to">';
      html += '  <div class="block-card__other__from-to__title">';
      html += '    <span class="block-card__other__from-to__title__inner">' + this._options.other.title + '</span>';
      html += '  </div>';
      html += '  <div class="block-card__other__from-to__direction">';
      html += '    <span class="block-card__other__from-to__direction__inner">' + this._options.other.direction + '</span>';
      html += '  </div>';
      html += '</div>';

      var _$detail = $('<div class="block-card__other__detail"></div>');

      for (var i = 0; i < this._options.other.detail.length; i++) {
        _$detail.append(TRCardLine(this._options.other.detail[i].title, this._options.other.detail[i].time, {
          grey : true
        }));
      }

      _$other.html(html).append(_$detail);
      this._front.append(_$other);

      return this;
    },
    __initPrice     : function (bordered, noTop, back) {

      var _$price = $('<div class="block-card__price"></div>');

      var left = this._options.price.title;
      if (this._options.back) {
        left = {
          href  : "#back-side-card",
          title : left
        }
      }

      if (back) {
        left = {
          href  : "#front-side-card",
          title : 'Закрыть'
        }
      }

      _$price.append(TRCardLine(left, this._options.price.btn, {
        grey : true,
        btn  : true
      }));

      if (bordered)
        _$price.addClass('bordered');
      if (noTop)
        _$price.addClass('no-top');

      if (back)
        this._back.append(_$price);
      else
        this._front.append(_$price);

      return this;
    },

    __initBackSide : function () {

      this._back = $('<div class="block-card__backside"></div>');

      var html = '';
      html += '<div class="block-card__multi">';
      html += '  <div class="block-card__multi__title">';
      html += '    <span class="block-card__multi__title__inner">' + this._options.back.title + '</span>';
      html += '  </div>';
      html += '  <div class="block-card__multi__list">';
      for (var i = 0; i < this._options.back.items.length; i++) {
        html += '<div class="block-card__multi__list__item">' + this._options.back.items[i] + '</div>'
      }
      html += '  </div>';
      html += '</div>';

      this._back.html(html);

      this.__initPrice(false, false, true);
      return this;
    },
    getNode        : function () {
      return this._node;
    }
  };

  return Card;
}());