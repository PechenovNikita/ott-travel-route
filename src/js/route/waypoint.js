var TRWayPoint = (function () {
  "use strict";

  function WayPoint(options) {
    this._options = options;
    this.init();
  }

  WayPoint.prototype = {
    init    : function () {
      this._$section = $('<section class="block-waypoint"><div class="block-waypoint__marker"></div></section>');

      this._$text = $('<div class="block-waypoint__text"></div>');

      var text = '';
      if (this._options.title)
        text += '<div class="block-waypoint__title"><span class="block-waypoint__title__inner">' + this._options.title + '</span></div>';
      if (this._options.description)
        text += '<div class="block-waypoint__desc"><span class="block-waypoint__desc__inner">' + this._options.description + '</span></div>';

      this._$text.html(text);
      this._$cards = $('<div class="block-waypoint__cards"></div>');
      var cards = '';
      if (this._options.map) {
        cards += '<div class="layout-row"><div class="layout-row__cell"><div class="block-map">';
        if (this._options.map.point) {
          cards += "<div class='block-map__inner' data-point='" + JSON.stringify(this._options.map.point) + "'></div>"
        } else if (this._options.map.direction) {
          cards += "<div class='block-map__inner' data-direction='" + JSON.stringify(this._options.map.direction) + "'></div>"
        }
        cards += '</div></div></div>';
      }

      var cardFragment = document.createDocumentFragment();
      if (this._options.cards) {
        var $row = $('<div class="layout-row"></div>');
        $(this._options.cards).each(function (index, card) {
          var $temp = $('<div class="layout-row__cell layout-row__cell--1-4"></div>');

          $temp.append((new TRCard(card)).getNode());
          $row.append($temp);
        });
        cardFragment.appendChild($row[0]);
      }

      this._$cards.html(cards).append(cardFragment);
      this._$section.append(this._$text).append(this._$cards);
    },
    getNode : function () {
      return this._$section;
    }
  };

  return WayPoint;
}());