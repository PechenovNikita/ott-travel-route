"use strict";

import {priceFormat} from './mixin/Format';
import PriceGroup from './price/Group';
import PriceItem, {PriceItemStore} from './price/Item';
import CurrencyDict from './price/CurrencyDict';
import {svgString} from './view/Loader';

import Updater from '../updater';

const $priceNode = $('.block-title-prise__cost__inner');
const oldPrice = parseInt(($priceNode.text().replace(/[^\d]/g, '')), 10);

let loadCurrency = false, initCurrency = false;
let needUpdate = false;
/**
 *
 * @type {{dict}}
 */
export let CurrentCurrencyDict = {dict : null};
/**
 *
 * @type {Array.<PriceGroup>}
 */
const Groups = [];

if (typeof priceStore !== "undefined") {
  if (priceStore && priceStore.length > 0) {

    priceStore.forEach((price) => {new PriceItem(price);});

    priceGroups.forEach((group) => {
        if (group.groups) {
          group.groups.forEach((grp) => {
            Groups.push(new PriceGroup(grp));
          });
        }
      }
    );

    loadCurrency = true;
    $.ajax('https://hapi.onetwotrip.com/api/getCurrencies_v2').done((response) => {
      initCurrency = true;

      if (response.error) {
        if (console && console.error)
          console.error('Error::getCurrencies_v2');
        else if (console)
          console.log('Error::getCurrencies_v2');
      } else {
        CurrentCurrencyDict.dict = new CurrencyDict(response.result.dict, priceCurrency);
        if (!loading)
          __update();
      }
    });

  }
}

function __update() {
  needUpdate = false;
  if (!CurrentCurrencyDict.dict)
    return;

  const price = Groups.reduce((sum, group) => sum + group.$price, 0);

  switch (priceCurrency) {
    case'USD':
      $priceNode.html(`from ${priceFormat(price, CurrentCurrencyDict.dict.$currency, true)}`);
      break;
    default:
      $priceNode.html(`от ${priceFormat(price, CurrentCurrencyDict.dict.$currency, true)}`);
  }

  Updater.do();
}

let loading = false, loadCount = 0;

export function loadPrice(index = 1) {
  loadCount += index;
  if (loading)
    return;
  loading = true;
  let $loader = $('<div class="card-loader"></div>');
  $loader.html(svgString);
  switch (priceCurrency) {
    case'USD':
      $priceNode.html('from ...');
      break;
    default:
      $priceNode.html('от ...');
  }
  $priceNode.append($loader);
}

export function updatePrice(id, cost) {
  loadCount--;

  if (!loadCurrency)
    return;
  if (PriceItemStore[id]) {
    PriceItemStore[id].setPrice(cost.price, cost.currency);
  } else {
    return;
  }
  if (!initCurrency) {
    needUpdate = true;
    return;
  }

  if (loadCount > 0)
    return;

  loading = false;

  __update();
}

export function removePrice(id) {
  if (PriceItemStore[id])
    PriceItemStore[id].remove();
  loadCount--;
  if (loadCount > 0)
    return;
  loading = false;

  __update();
}