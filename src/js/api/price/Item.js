'use strict';
/**
 *
 * @type {Object.<string, PriceItem>}
 */
export const PriceItemStore = {};

import {CurrentCurrencyDict as CurDict} from '../Price';

export default class PriceItem {
  /**
   *
   * @param {{cost:{coefficient:number, currency:string, price:(string|number)}, id:string}} options
   */
  constructor(options) {
    this._id = options.id;
    this._options = options.cost;

    this._group = false;

    PriceItemStore[this._id] = this;
  }

  /**
   *
   * @return {number}
   */
  get $price() {
    return CurDict.dict.getPrice(parseInt(this._options.price, 10), this._options.currency) * this._options.coefficient;
  }

  /**
   *
   * @param {PriceGroup} group
   * @return {PriceItem}
   */
  setToGroup(group) {
    this._group = group;
    return this;
  }

  /**
   *
   * @return {boolean}
   */
  inGroup() {
    return !!this._group;
  }

  /**
   *
   * @param {number} price
   * @param {string} [currency]
   * @return {PriceItem}
   */
  setPrice(price, currency = this._currency) {
    this._options.price = price;
    this._options.currency = currency;
    return this;
  }

  remove(){
    delete PriceItemStore[this._id];
    this._group.remove(this)
  }
};
