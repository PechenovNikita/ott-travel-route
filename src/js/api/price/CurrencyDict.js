'use strict';

export default class currencyDict {
  /**
   *
   * @param {Object.<string,{code:string,name:string,nominal:number,value:number}>}dict
   * @param {string} currency
   */
  constructor(dict, currency) {
    /**
     *
     * @type {Object.<string, {code: string, name: string, nominal: number, value: number}>}
     * @private
     */
    this._dict = dict;
    /**
     *
     * @type {string}
     * @private
     */
    this._currency = currency;
  }

  /**
   *
   * @param {string} currency
   */
  set $currency(currency) {
    this._currency = currency;
  }

  /**
   *
   * @return {string}
   */
  get $currency(){
    return this._currency;
  }

  /**
   *
   * @param {number} price
   * @param {string} currency
   * @return {number}
   */
  getPrice(price, currency) {


    // КОСТЫЛЬ
    if(!currency){
      console.error(price, currency);
      currency = 'RUB';
      price = 0;
    }

    let inRUB = price * this._dict[currency].value / this._dict[currency].nominal;
    return inRUB / this._dict[this._currency].value * this._dict[this._currency].nominal;
  }
};
