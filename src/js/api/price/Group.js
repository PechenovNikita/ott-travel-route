'use strict';

import {PriceItemStore} from './Item';

export default class PriceGroup {
  constructor(ids) {
    this._items = [];
    ids.forEach((id) => {
      let priceItem = PriceItemStore[id];
      if (priceItem) {
        this._items.push(priceItem);
        priceItem.setToGroup(this);
      }
    });

  }

  /**
   *
   * @return {number}
   */
  get $price() {
    return this._items.reduce((sum, /*PriceItem*/item) => {
      if (sum == 0)
        return item.$price;
      return Math.min(sum, item.$price);
    }, 0);
  }

  /**
   *
   * @param {PriceItem} item
   */
  remove(item) {
    this._items = this._items.filter((it) => it !== item);
  }
};
