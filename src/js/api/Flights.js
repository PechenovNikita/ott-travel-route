'use strict';
import {deepExtend} from '../mixins/Deep';

import {svgString} from './view/Loader';
import FlightView from './view/Flight';

import {findCities} from '../libs/cities';

import {priceFormat, withZero, textCount} from './mixin/Format';
import {updatePrice, loadPrice} from './Price';

const defaultApiData = {
  ad     : 1,
  cn     : 0,
  'in'   : 0,
  cs     : 'E', // E/B
  marker : '1-1884-0-2',

  source    : '12trip_travel-expert_ru',
  srcmarker : 'travel_expert_page',
  ott4862   : true
};

/**
 *
 * @param dateString
 * @return {string}
 */
function getTime(dateString) {
  let date = new Date(dateString);
  return `${withZero(date.getUTCHours())}:${withZero(date.getUTCMinutes())}`;
}

function getTimeString(timeStringInMill) {
  let minutes = Math.ceil(timeStringInMill / 1000 / 60);
  let h = Math.floor(minutes / 60)
    , m = minutes % 60;

  if (m == 0) {
    return h + textCount(h, ' час', ' часа', ' часов');
  } else {
    if (h == 0)
      return m + textCount(m, ' минута', ' минуты', ' минут');
    return h + ' ч ' + m + ' мин';
  }
}

function parseTrip(trips, airports, cities, reverse = false) {

  // console.log(trips, airports, cities, reverse);
  // console.log(trips.length - 1, trips[trips.length - 1].to, airports[trips[trips.length - 1].to]);
  // console.log('from', trips[trips.length - 1].from, airports[trips[trips.length - 1].to]);
  // console.log('UYU airport', airports['UYU']);
  // @TODO разобраться

  let from, to;

  if (trips[0].from === 'UYU') {
    from = 'Уюни'
  } else {
    if (trips[0].from === 'MOW')
      from = (reverse ? cities[airports[trips[0].from].city].name : airports[trips[0].from].name);
    else
      from = (cities[airports[trips[0].from].city].name);
  }
  if (trips[trips.length - 1].to === 'UYU') {
    to = 'Уюни'
  } else {
    if (trips[trips.length - 1].to === 'MOW')
      to = (!reverse ? cities[airports[trips[trips.length - 1].to].city].name : airports[trips[trips.length - 1].to].name);
    else
      to = (cities[airports[trips[trips.length - 1].to].city].name);
  }

  let opt = {
    from   : {
      title : from,
      time  : getTime(trips[0].startDate)
    },
    to     : {
      title : to,
      time  : getTime(trips[0].endDate)
    },
    across : {
      title : 'Прямой',
      time  : ''
    }
  };

  if (trips.length > 1) {
    if (trips.length == 2) {
      let firstDate = new Date(trips[0].endDate)
        , lastDate = new Date(trips[1].startDate);

      let time = lastDate.getTime() - firstDate.getTime();

      opt.across.time = getTimeString(time);
      opt.across.title = `через ${cities[airports[trips[0].to].city].name}`;
    } else {
      let c = trips.length - 1;
      opt.across.title = c + textCount(c, ' переседка', ' пересадки', ' пересадок');
    }
  }

  return opt;
}

export default class Flights {

  constructor(options) {

    this._render = options.render || {};
    this._replaceURL = options.render ? options.render.url || {} : {};
    this._data = deepExtend({}, defaultApiData, options.options);
    this._filter = options.filter || {};

    this.__init();
  }

  __initURL() {
    let cities = findCities(this._data.route);
    let p = this._replaceURL.p || `${this._data.ad},${this._data.cn},${this._data.in}`;
    this._url = `https://www.onetwotrip.com/ru/aviabilety/${cities[0][1]}-${cities[1][1]}_${cities[0][0]}-${cities[1][0]}/?p=${p}&s=true&scp=14,12trip_travel-expert_ru,travel_expert_page#${this._data.route}`;
  }

  __init() {
    this.__initURL();

    this._v2 = true;
    $.ajax('https://partner.onetwotrip.com/_api/searching/startSync', {
      // $.ajax('https://www.onetwotrip.com/_api/searching/startSync2', {
      //   jsonp : 'callback',
      //
      //   // Tell jQuery we're expecting JSONP
      //   dataType : 'jsonp',
      dataType : 'json',
      data     : this._data,
      method   : 'get'
    }).done((response) => {
      if (response.status !== 'success') {
        // if (!response) {
        // @TODO тест ошибки
        console.log('ОШИБКА flight', response);
      } else {
        // this.__parseSearchData(response);
        this.__parseSearchData(response.data);
      }
    });

    if (this._render.cards) {
      loadPrice(this._render.cards.length);
      this._render.cards.forEach((cardID) => {
        let $loader = $('<div class="card-loader"></div>');
        $loader.html(svgString);
        $(`[data-id="${cardID}"]`).append($loader);
      });
    }

    return this;
  }

  __filterData(fares) {
    let filtered = fares;

    if (this._filter.includeLessTimeSpent && !this._v2) {
      let included = [], i = 0, notIncluded = [];

      while (included.length < this._filter.includeLessTimeSpent.count && i < fares.length) {

        if (fares[i].dirs.reduce((last, dir) => {
            const trips = dir.trips;
            if (trips.length < 2)
              return true;
            if (trips.length > 2)
              return false;

            let firstDate = new Date(trips[0].endDate)
              , lastDate = new Date(trips[1].startDate);

            let time = lastDate.getTime() - firstDate.getTime();
            let minutes = time / 1000 / 60;

            return last && (minutes <= this._filter.includeLessTimeSpent.time);

          }, true)) {
          included.push(fares[i]);
        } else {
          notIncluded.push(fares[i]);
        }
        i++;
      }
      fares.splice(0, i);
      included = included.concat(notIncluded).concat(fares);
      filtered = included;
    }

    filtered = filtered.filter(fare => {
      let filter = true;

      if (filter && this._filter.direct) {
        filter = fare.dirs.every((dir) => {
          if (dir.trps) {
            return dir.trps.length === 1;
          }
          if (dir.trips) {
            return dir.trips.length === 1;
          }
          return false;
        });
      }

      return filter;
    });

    return filtered.length > 0 ? filtered : fares;

  }

  __parseSearchData2(results) {

    const fares = this.__filterData(results.frs);

    let i = 0;
    while (i < this._render.cards.length) {
      if (fares[i]) {

        let fare = fares[i];
        let options = {type : 'flight'}
          , price = priceFormat(fare.price, fare.currency);

        options.price = {
          title : '',
          btn   : {
            href  : this._url,
            title : price
          }
        };

        deepExtend(options, parseTrip(fare.dirs[0].trips, results.airports, results.cities));

        if (fare.dirs.length > 1) {
          let backOptions = parseTrip(fare.dirs[1].trips, results.airports, results.cities, true);
          options.back = {
            title      : 'Наборный перелёт',
            close_text : 'Развернуть',
            items      : [backOptions.from, backOptions.to],
            across     : backOptions.across
          };

          options.price.title = 'Сложный маршрут';
        }

        this.__renderResults(options, this._render.cards[i], {
          price    : fare.price,
          currency : fare.currency
        });
      } else {
        return this;
      }
      i++;
    }
    return this;
  }

  /**
   *
   * @param results
   * @private
   */
  __parseSearchData(results) {

    const fares = this.__filterData(results.frs);

    let i = 0;
    while (i < this._render.cards.length) {
      if (fares[i]) {

        const coefficient = this._render.coefficient || 1;

        let fare = fares[i];
        let options = {type : 'flight'}
          , price = priceFormat(fare.price * coefficient, fare.currency);

        options.price = {
          title : '',
          btn   : {
            href  : this._url,
            title : price
          }
        };

        deepExtend(options, parseTrip(fare.dirs[0].trips, results.airports, results.cities));

        if (fare.dirs.length > 1) {
          let backOptions = parseTrip(fare.dirs[1].trips, results.airports, results.cities, true);
          options.back = {
            title      : 'Наборный перелёт',
            close_text : 'Развернуть',
            items      : [backOptions.from, backOptions.to],
            across     : backOptions.across
          };

          options.price.title = 'Сложный маршрут';
        }

        this.__renderResults(options, this._render.cards[i], {
          price    : fare.price * coefficient,
          currency : fare.currency
        });
      } else {
        return this;
      }
      i++;
    }
    return this;
  }

  __renderResults(options, data_id, cost) {
    let el = $(`[data-id="${data_id}"]`);

    updatePrice(data_id, cost);

    options.price.title = el.find('.block-card__frontside .block-card__line__left__link__inner').text() || options.price.title;
    if (options.back) {
      options.back.title = el.find('.block-card__backside .block-card__multi__title__inner').text() || options.back.title;
      options.back.close_text = el.find('.block-card__backside .block-card__line__left__link__inner').text() || options.back.close_text;
    }
    let view = new FlightView(options);
    el.html(view.$html);
  }
};
