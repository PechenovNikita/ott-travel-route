"use strict";

/**
 *
 * @param {string} autoDateString - YYYYMMDD
 * @return {Date}
 */
export function autoToDate(autoDateString) {
  const match = autoDateString.match(/^(\d{4})(\d{2})(\d{2})$/);
  return new Date(parseInt(match[1], 10), parseInt(match[2], 10) - 1, parseInt(match[3], 10));
}

/**
 *
 * @param {Date} date1
 * @param {Date} date2
 * @return {number}
 */
export function days(date1, date2) {
  return Math.abs(Math.floor((date2.getTime() - date1.getTime()) / 1000 / 60 / 60 / 24));
}