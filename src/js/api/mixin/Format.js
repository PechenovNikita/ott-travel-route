"use strict";
/**
 *
 * @param {number|string} nStr
 * @return {string}
 */
function addSpaces(nStr) {
  nStr += '';
  let x = nStr.split('.');
  let x1 = x[0];
  let x2 = x.length > 1 ? '.' + x[1] : '';
  let rgx = /(\d+)(\d{3})/;
  while (rgx.test(x1)) {
    x1 = x1.replace(rgx, '$1' + ' ' + '$2');
  }
  return x1 + x2;
}

/**
 *
 * @param {string|number} price
 * @param {string} currency
 * @param {boolean} [bold=false]
 * @return {string}
 */
export function priceFormat(price, currency, bold = false) {
  let str = addSpaces(Math.round(price));
  switch (currency) {
    case "RUR":
    case "RUB":
      str += ` <span class="icon-rub ${bold ? "bold" : "regular"}"></span>`;
      break;
    case "USD":
      str += ` $`;
      break;
    default :
      str += " ?";
  }

  return str;
}
/**
 *
 * @param {number} num
 * @return {string}
 */
export function withZero(num) {
  return num > 9 ? '' + num : '0' + num;
}
/**
 *
 * @param {number} number
 * @param {string} one
 * @param {string} two
 * @param {string} five
 * @return {string}
 */
export function textCount(number, one, two, five) {
  number = Math.abs(number);
  number %= 100;
  if (number >= 5 && number <= 20) {
    return five;
  }
  number %= 10;
  if (number == 1) {
    return one;
  }
  if (number >= 2 && number <= 4) {
    return two;
  }
  return five;
};