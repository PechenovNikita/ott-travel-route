"use strict";

import Card from './Card';

function card_line(left, right, grey) {
  return `
    <div class="block-card__line ${grey ? 'grey' : ''}">
      <div class="block-card__line__left">
        <span class="block-card__line__left__inner">${left}</span>
      </div>
      <div class="block-card__line__right">
        <span class="block-card__line__right__inner">${right}</span>
      </div>
    </div>
  `;
}

export default class FlightView extends Card {
  constructor(options, url) {
    super(options);
  }

  __renderFrontSide() {

    return `
      <div class="block-card__flight">
        <div class="block-card__flight__from-to">
          <div class="block-card__flight__from-to__title">
            <span class="block-card__flight__from-to__title__inner">${this._options.title || 'Авиаперелет'}</span>
          </div>
          ${card_line(this._options.from.title, this._options.from.time)}
          ${card_line(this._options.to.title, this._options.to.time)}
        </div>
        ${(() => {
      if (this._options.across) {
        return `
          <div class="block-card__flight__across">
            ${card_line(this._options.across.title, this._options.across.time, true)}
          </div>`
      }
      return `<div class="block-card__flight__across empty">${card_line('', '', true)}</div>`;
    })()}
      </div>
    `;
  }

}