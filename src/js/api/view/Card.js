"use strict";

function line(left, right, grey, btn, size) {
  return `
    <div class="block-card__line ${(grey ? ' grey' : '') + (btn ? ' with-btn' : '')}">
      <div class="block-card__line__left" style="width : ${size ? size[0] : ''}">
        ${(() => {
    if (typeof left === 'string') {
      return `<span class="block-card__line__left__inner">${left}</span>`;
    } else if (left.href && left.title) {
      return `<a href="${left.href}" class="block-card__line__left__link"><span class="block-card__line__left__link__inner">${left.title}</span></a>`;
    } else {
      return left.reduce((last, current) => {
        if (typeof current === 'string')
          return last + `<span class="block-card__line__left__inner">${current}</span>`;
        return last + `<a href="${current.href}" class="block-card__line__left__link"><span class="block-card__line__left__link__inner">${current.title}</span></a>`;
      }, '');

    }
  })()}
      </div>
      <div class="block-card__line__right"  style="width : ${size ? size[1] : ''}">
        ${(() => {
    if (!btn) {
      return `<span class="block-card__line__right__inner">${right}</span>`;
    } else {
      return `<a href="${btn.href}" class="btn btn-card" target="_blank"><span class="btn__inner">${btn.cost}</span></a>`;
    }
  })()}
      </div>
    </div>`
}

function line_2(left, right, grey, btn) {
  return `
    <div class="block-card__line v2 ${(grey ? ' grey' : '') + (btn ? ' with-btn' : '')}">
    
      <div class="block-card__line__left">
        ${(() => {
    if (typeof left == 'string')
      return `<span class="block-card__line__left__inner">${left}</span>`;
    else if (left.href && left.title)
      return `<a href="${left.href}" class="block-card__line__left__link"><span class="block-card__line__left__link__inner">${left.title}</span></a>`;
    else
      return left.reduce((last, text) => last + `<span class="block-card__line__left__inner">${text}</span>`, '');
  })()}
</div>
      <div class="block-card__line__right">
        <span class="block-card__line__right__inner">${right}</span>
</div>
    </div>
  `
}

function back(title, items, across, btn, close_text) {
  return `
    <div class="block-card__backside">
      <div class="block-card__multi ${typeof across !== "undefined" ? 'with-across' : ''}">
        <div class="block-card__multi__title">
          <span class="block-card__multi__title__inner">${title}</span>
        </div>
        <div class="block-card__multi__list">
          ${items.reduce((last, item) => last + `${
    (() => {
      if (typeof item == 'string')
        return `<div class="block-card__multi__list__item">${item}</div>`;
      else if (item.title && item.time) {
        return line_2(item.title, item.time);
      } else {
        return ''
      }
    })()}`, '')}
        </div>
      </div>
      ${(() => {
    if (typeof across !== "undefined") {
      return `<div class="block-card__flight__across">${line(across.title, across.time, true)}</div>`;
    }
    return ''
  })()}
      <div class="block-card__price">
      ${line({
    href  : "#front-side-card",
    title : close_text
  }, '', true, btn)}
      </div>
    </div>
  `;
}

export default class CardView {
  /**
   *
   * @param {Object} options
   * @param {...any} args
   */
  constructor(options, ...args) {
    this._options = options;
    this.__initHTML();
  }

  __initHTML() {

    let opt = this._options;
    this._$node = $(`<div class="block-card ${opt.type}"></div>`);

    let innerHTML = `<div class="block-card__frontside">${this.__renderFrontSide()}${this.__renderPrice()}</div>`;
    if (opt.back) {
      innerHTML += this.__renderBackSide();
    }

    this._$node.html(innerHTML);
  }

  /**
   *
   * @return {*|{prices, currency, isPriceClear, isForNight}|{hotel}|jQuery}
   */
  get $html() {
    return $('<div>').append(this._$node.clone()).html();
  }

  __renderPrice() {
    return `
    <div class="block-card__price ${(['hotel', 'excursion'].indexOf(this._options.type) >= 0) ? 'bordered ' : ''} ${(this._options.type == 'excursion' ) ? 'no-top' : ''}">
      ${(() => {
      if (this._options.back) {
        return line({
          title : this._options.price.title,
          href  : '#back-side-card'
        }, '', true, {
          cost : this._options.price.btn.title,
          href : this._options.price.btn.href
        })
      } else {
        return line(this._options.price.title, '', true, {
          cost : this._options.price.btn.title,
          href : this._options.price.btn.href
        })
      }
    })()}
    </div>
`
  }

  /**
   *
   * @return {string}
   * @private
   */
  __renderBackSide() {
    return back(this._options.back.title, this._options.back.items, this._options.back.across, {
      cost : this._options.price.btn.title,
      href : this._options.price.btn.href
    }, (this._options.back.close_text ? this._options.back.close_text : 'Закрыть'));
  }

  /**
   *
   * @return {string}
   * @private
   */
  __renderFrontSide() {
    return `
        case config.type
          when 'photos'
          when 'photo'
            +block_card__photos(config.tags, config.location, config.description)
          when 'flight'
            +block_card__flight(config.from, config.to, config.across, config.title)
          when 'hotel'
          when 'excursion'
            +block_card__image(config.image.src, config.image.title, config.image.description, config.price.btn.href)
          when 'car'
            +block_card__car(config.image.src, config.image.title, config.price.btn.href)
          when 'other'
            +block_card__other(config.other.title, config.other.direction, config.other.detail)
        if(config.price)
          .block-card__price(class=((['hotel', 'excursion'].indexOf(config.type) >= 0) ? 'bordered ' : '') + ( (config.type == 'excursion' ) ? 'no-top' : ''))
            if(config.back)
              +block_card_line({
                title : config.price.title,
                href  : '#back-side-card'
              }, '', true, {
                cost : config.price.btn.title,
                href : config.price.btn.href
              })
            else
              +block_card_line(config.price.title, '', true, {
                cost : config.price.btn.title,
                href : config.price.btn.href
              })`;
  }
};
