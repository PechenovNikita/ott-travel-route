"use strict";

import Card from './Card';


export default class AutoView extends Card{
  constructor(options){
    super(options);
  }

  __renderFrontSide(){
   return `
      <div class="block-card__car">
        <div class="block-card__car__text">
          <div class="block-card__car__text__title"><span class="block-card__car__text__title__inner">${this._options.image.title}</span></div>
          <div class="block-card__car__text__image"><a href="${this._options.price.btn.href}" target="_blank"><img class="block-card__car__text__image__img" src=${this._options.image.src}></a></div>
        </div>
      </div>`
  }
}