"use strict";

import {FACILITIES_NAMES} from '../Hotels';
import {deepExtend} from '../../mixins/Deep';

function priceFormat(price) {
  return addSpaces(Math.ceil(price));
}

function addSpaces(nStr) {
  nStr += '';
  let x = nStr.split('.');
  let x1 = x[0];
  let x2 = x.length > 1 ? '.' + x[1] : '';
  let rgx = /(\d+)(\d{3})/;
  while (rgx.test(x1)) {
    x1 = x1.replace(rgx, '$1' + ' ' + '$2');
  }
  return x1 + x2;
}

export default class HotelView {
  constructor(hotel, offer, descriptions, renderOptions, facilities, rating, url, render_url, currency = 'RUB') {

    this._hotel = hotel;
    this._offer = offer;
    this._currency = currency;

    this._render = renderOptions;

    this._rating = rating;
    this._facilities = facilities;
    this._descriptions = descriptions;

    this.__parseURL(url, render_url);
    this.__initHTML();
  }

  __parseURL(/*string*/url, render_url = {}) {
    let temp = url.split(/\?|&/g);

    let src = temp.splice(0, 1)[0];

    let params = temp.reduce((arg, param) => {
      let t = param.split('=');
      switch (t[0]) {
        case 'persons':
          let counts = t[1].split(',');
          if (counts[0])
            arg['adults'] = counts[0];
          arg['children'] = 0;
          break;
        default:
          arg[t[0]] = t[1];

      }
      return arg;
    }, {});

    params = deepExtend({
      utm_source   : 'travel_expert_page',
      utm_medium   : 'travel-expert',
      utm_campaign : 'travel_expert_page',
      srcmarker2   : 'travel_expert_page',
      scp          : '14,12trip_travel-expert_ru,travel_expert_page',

      s           : true,
      countryName : 'country',
      cityName    : 'city',

    }, params, render_url);

    params.to = this._hotel.city_name_translated;
    params.id = this._hotel.city_id;
    params.sc = this._hotel.city_id;

    let newUrl = src;

    Object.keys(params).forEach((key) => {
      if (newUrl.indexOf('?') < 0) {
        newUrl += `?${encodeURIComponent(key)}=${encodeURIComponent(params[key])}`
      } else {
        newUrl += `&${encodeURIComponent(key)}=${encodeURIComponent(params[key])}`
      }
    });

    this._url = encodeURI(newUrl);


  }

  __initHTML() {

    let stars = '';

    if (this._hotel.stars) {
      stars = '';
      for (let i = 0; i < this._hotel.stars; i++)
        stars += `<span class="star-icon"></span>`;
    }

    this._node = document.createElement('div');
    this._node.className = 'block-card hotel';

    let price = '';

    switch(this._currency){
      case 'USD':
        price = `${priceFormat(this._offer[2])} $`;
        break;
      default:
        price = `${priceFormat(this._offer[2])} <span class="icon-rub regular"></span>`;
    }

    this._node.innerHTML = `
      <div class="block-card__frontside">
        <a href="${this._url}" class="block-card__image" target="_blank">
          <div class="block-card__image__back">
            <div class="block-card__image__back__overlay"></div>
            <div style='background-image: url(${this._hotel.img})' class="block-card__image__back__img"></div>
            ${(this._render && this._render.subtitle)?`<div class="block-card__image__back__subtitle">${this._render.subtitle}</div>`:''}
          </div>
          <div class="block-card__image__text">
          
            <div class="block-card__image__text__title">
              <span class="block-card__image__text__title__inner">${this._hotel.name}</span>
            </div>
              <div class="block-card__image__text__desc">
                <span class="block-card__image__text__desc__inner">${stars}</span>
              </div>
          </div>
        </a>
          <div class="block-card__price bordered">
              <div class="block-card__line grey with-btn">
                <div class="block-card__line__left">
                  <span class="block-card__line__left__inner">
                    ${this._facilities.reduce((str, fac) => `${str}<span class="hotel-icon ${fac}" title="${FACILITIES_NAMES[fac]}"></span>`, '')}
                  </span>
                  ${(this._rating) ? (`<span class="block-card__line__left__inner">${this._rating / 10} / 10</span>`) : ('')}
                </div>
                <div class="block-card__line__right">
                    <a href="${this._url}" target="_blank" class="btn btn-card">
                      <span class="btn__inner">${price}</span>
                    </a>
                </div>
              </div>
          </div>
      </div>
    `;
  }

  toString() {
    return this._node.outerHTML;
  }
}