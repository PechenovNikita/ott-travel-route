'use strict';
import {updatePrice, removePrice, loadPrice} from './Price';

// prepay
// в офферах 16 значение массива это препей
// 0 - в отеле есть и препеи и постпеи
// 1 - в отеле только постпеи
// 2 - в отеле только препеи

// getTemes()
//https://hapi.onetwotrip.com/api/getThemes?lang=ru&locale=ru&hash=1488852566652&currency=RUB&utm_source=travel_expert_page&utm_medium=travel-expert&utm_campaign=travel_expert_page&source=dest_matrix&term=RU-Moscow&pos=1&ga_dimension=20160511_newBlue&ga_sortType=normalize
// {
//   "error": null,
//   "result": {
//   "hash": "49a9727c162d62d78b29a95b4944fb0d",
//     "lang": "ru",
//     "locale": "ru",
//     "dict": {
//     "1": "Пляжный отдых",
//       "2": "Лыжный/зимний отдых",
//       "3": "Спа/Оздоровление",
//       "4": "Гольф/Спорт",
//       "5": "Романтический отпуск/Медовый месяц",
//       "6": "Изысканная кухня",
//       "7": "Семейный отдых",
//       "8": "Дизайн-отель",
//       "9": "\"Люкс\"-отель",
//       "10": "Бюджетные/молодежные поездки",
//       "13": "Отдых в горах",
//       "14": "Шоппинг",
//       "15": "Экскурсии по городу",
//       "16": "Отдых за городом",
//       "18": "Дикая природа",
//       "19": "Деловая поездка",
//       "20": "Эко-отель",
//       "24": "Отели типа \"Все включено\""
//   }
// }
// }

// getTypes
//https://hapi.onetwotrip.com/api/getTypes?lang=ru&locale=ru&currency=RUB&utm_source=travel_expert_page&utm_medium=travel-expert&utm_campaign=travel_expert_page&source=dest_matrix&term=RU-Moscow&pos=1&ga_dimension=20160511_newBlue&ga_sortType=normalize
// {
//   "error": null,
//   "result": {
//   "2": "Апартаменты",
//     "3": "Гостевые дома",
//     "13": "Хостелы",
//     "14": "Гостиницы",
//     "19": "Мотели",
//     "21": "Курортные отели",
//     "23": "Резиденции",
//     "24": "Отели типа \"Постель и завтрак\"",
//     "25": "Риокан",
//     "10001": "Апартотели",
//     "10002": "Клубные отели"
// }
// }

// const THEMES = {
//   "1"  : "Пляжный отдых",
//   "2"  : "Лыжный/зимний отдых",
//   "3"  : "Спа/Оздоровление",
//   "4"  : "Гольф/Спорт",
//   "5"  : "Романтический отпуск/Медовый месяц",
//   "6"  : "Изысканная кухня",
//   "7"  : "Семейный отдых",
//   "8"  : "Дизайн-отель",
//   "9"  : "\"Люкс\"-отель",
//   "10" : "Бюджетные/молодежные поездки",
//   "13" : "Отдых в горах",
//   "14" : "Шоппинг",
//   "15" : "Экскурсии по городу",
//   "16" : "Отдых за городом",
//   "18" : "Дикая природа",
//   "19" : "Деловая поездка",
//   "20" : "Эко-отель",
//   "24" : "Отели типа \"Все включено\""
// };

const FACILITIES = {
  'internet' : [50, 65, 136],
  'parking'  : [49, 77, 82, 87, 91, 130],
  'fitness'  : [48],
  'child'    : [7, 9, 27, 28, 41, 51],
  'pool'     : [62, 63, 64, 83, 84, 85],
  'spa'      : [117]
};

const RATING = {
  'excellent' : [86, 101],
  'very_good' : [80, 86],
};

export const FACILITIES_NAMES = {
  'internet' : 'Интернет',
  'parking'  : 'Парковка',
  'fitness'  : 'Фитнес',
  'child'    : 'Для детей',
  'pool'     : 'Бассейн',
  'spa'      : 'СПА'
};

const THEMES_NAMES = {
  '1'  : 'Пляжный отдых',
  '2'  : 'Лыжный/зимний отдых',
  '3'  : 'Спа/Оздоровление',
  '4'  : 'Гольф/Спорт',
  '5'  : 'Романтический отпуск/Медовый месяц',
  '6'  : 'Изысканная кухня',
  '7'  : 'Семейный отдых',
  '8'  : 'Дизайн-отель',
  '9'  : '"Люкс"-отель',
  '10' : 'Бюджетные/молодежные поездки',
  '13' : 'Отдых в горах',
  '14' : 'Шоппинг',
  '15' : 'Экскурсии по городу',
  '16' : 'Отдых за городом',
  '18' : 'Дикая природа',
  '19' : 'Деловая поездка',
  '20' : 'Эко-отель',
  '24' : 'Отели типа "Все включено"'
};

import {deepExtend} from '../mixins/Deep';
import {MAP_STORE} from '../map';

import {svgString} from './view/Loader';
import HotelView from './view/Hotel';

const defData = {
  date_start   : 'XXXX-XX-XX',
  date_end     : 'XXXX-XX-XX',
  currency     : 'RUB',
  adults       : 1,
  children     : 0,
  object_id    : 'XXXXXXXX',
  object_type  : 'geo',
  user_auth    : false,
  lang         : 'ru',
  locale       : 'ru',
  utm_source   : 'travel_expert_page',
  utm_medium   : 'travel-expert',
  utm_campaign : 'travel_expert_page',
  term         : 'RU-Moscow',
  pos          : 1
};

/**
 *
 * @param {Array.<string>} facilities
 * @return {Array.<number>|boolean}
 */
function getFacilities(facilities) {
  if (!facilities || facilities.length < 1)
    return false;
  let ret = [];
  facilities.forEach((facility) => {
    if (FACILITIES[facility]) {
      ret.push(FACILITIES[facility])
    }
  });
  return ret.length > 0 ? ret : false;
}

export default class Hotels {
  constructor(options) {
    this._filter = options.filter || {};

    this._themes = options.filter.themes || false;
    this._rating = options.filter.rating || false;
    this._stars = options.filter.stars || false;
    this._districts = options.filter.districts || false;
    this._ids = options.filter.ids || false;
    this._pay_method = options.filter.pay_method || false;

    this._facilitiesNames = options.filter.facilities;
    this._facilities = getFacilities(options.filter.facilities);

    this._options = deepExtend({}, defData, options.options);
    this._render = options.render;

    this._hotelsIDS = [];
    this.__init();
  }

  /**
   *
   * @private
   */
  __init() {
    $.ajax('https://hapi.onetwotrip.com/api/searchRequest', {
      type   : 'json',
      method : 'get',
      data   : this._options
    }).done(function (response) {
      if (response.error) {
        // @TODO тест ошибки
        console.log('ОШИБКА Hotels', response)
      } else {
        this.__parseSearchData(response.result).__initPolling();
      }
    }.bind(this));

    if (this._render.cards) {
      loadPrice(this._render.cards.length);

      this._render.cards.forEach((cardID) => {
        let $loader = $('<div class="card-loader"></div>');
        $loader.html(svgString);
        $(`[data-id="${cardID}"]`).append($loader);
      })
    }
  }

  /**
   *
   * @param responseResult
   * @return {Hotels}
   * @private
   */
  __parseSearchData(responseResult) {

    this._polling = {
      id     : responseResult.request_id,
      status : 'progress',
      start  : Date.now(),
      count  : 0
    };

    if (this._filter.log) {
      console.log('отелей из api', responseResult.hotels);
    }

    this._hotels = responseResult.hotels.filter((hotel) => {
      let filtered = true;

      if (filtered && this._stars) {
        if (typeof this._stars === 'number') {
          filtered = hotel.stars >= this._stars;
        } else if (this._stars[0]) {
          filtered = this._stars.some(stars => hotel.stars === stars);
        }
      }

      if (filtered && this._filter.type) {
        filtered = this._filter.type.indexOf(hotel.type) > -1;
      }

      if (filtered && this._facilities) {
        filtered = this._facilities.every((facility) => {
          return facility.some((facilityID) => hotel.facilities.indexOf(facilityID) > -1);
        });
      }

      if (filtered && this._districts) {
        filtered = hotel.district_id && this._districts.indexOf(hotel.district_id) > -1;
        filtered = filtered || this._districts.indexOf(hotel.city_id) > -1;
        // if (filtered)
        //   console.log(this._districts, hotel.district_id, this._districts.indexOf(hotel.district_id) > -1, hotel.name);
      }

      if (filtered && this._ids) {
        filtered = this._ids.indexOf(hotel.id) > -1;
        // if (filtered)
        //   console.log(this._ids, hotel.id);
      }

      if (filtered && this._rating) {
        filtered = this._rating.some((rating_name) => {
          if (!RATING[rating_name])
            return true;
          return hotel.ty_rating >= RATING[rating_name][0] && hotel.ty_rating < RATING[rating_name][1];
        })
      }

      if (filtered && this._themes) {
        filtered = this._themes.some((themeID) => {
          if (Array.isArray(themeID))
            return themeID.some((thID) => hotel.themes.indexOf(thID) > -1);
          return hotel.themes.indexOf(themeID) > -1
        });
      }

      if (filtered) {
        this._hotelsIDS.push(hotel.id);
      }

      if (this._filter.log) {
        console.log(filtered, hotel);
      }

      return filtered;
    });

    if (this._filter.log) {
      console.log('отфильтровано отелей', this._hotels);
    }

    return this;
  }

  __initPolling() {

    this._polling.count++;
    $.ajax('https://hapi.onetwotrip.com/api/searchPolling', {
      type   : 'json',
      method : 'get',
      data   : {
        request_id : this._polling.id,
        currency   : this._options.currency || 'RUB',
        lang       : this._options.lang || 'ru',
        locale     : this._options.locale || 'ru'
      }
    }).done((response) => {
      if (response.error) {
        console.log('ОШИБКА Hotels', response);
      } else {
        this.__parsePolling(response.result);
      }
    });

  }

  __parsePolling(responseDate) {
    this._polling.status = responseDate.status;

    if (this._polling.status !== 'done') {
      setTimeout(this.__initPolling.bind(this), 3000);
    } else {
      this.__parseOffers(responseDate.offers).__renderResults();
    }
  }

  __parseOffers(offers) {
    if (this._filter.log) {
      console.log('всего предложеный', offers);
    }
    this._offers = offers.filter(offer => {
      let filtered = true;
      if (filtered && this._pay_method && this._pay_method.length > 0) {
        filtered = this._pay_method.indexOf(offer[16]) > -1;
      }

      if (filtered && this._filter.price) {

        if (filtered && this._filter.price.min) {

          filtered = offer[2] >= this._filter.price.min;
        }

        if (filtered && this._filter.price.max) {
          filtered = offer[2] <= this._filter.price.max;
        }
      }

      return filtered;
    }).filter((offer) => this._hotelsIDS.indexOf(offer[0]) > -1)
      .sort((a, b) => a[1] - b[1]);

    if (this._filter.log) {
      console.log('отфильтровано предложеный', this._offers)
    }

    this._offers = this._offers.filter((a, index) => index < this._render.cards.length);

    return this;
  }

  __renderResults() {
    const hotels = [];
    this._offers.forEach((offer, index) => {

      const hotel = this._hotels.find((hotel) => hotel.id === offer[0]);

      hotels.push(hotel);

      if (this._render.cards && this._render.cards[index]) {

        let facilities = [];

        hotel.facilities.forEach((index) => {
          Object.keys(FACILITIES).forEach((key) => {
            if (facilities.indexOf(key) >= 0)
              return;
            if (FACILITIES[key].indexOf(index) >= 0) {
              facilities.push(key);
            }
          })
        });

        let descriptions = [];
        if (this._facilitiesNames)
          descriptions = descriptions.concat(this._facilitiesNames.map((item) => FACILITIES_NAMES[item]));
        if (this._themes)
          descriptions = descriptions.concat(this._themes.map((item) => THEMES_NAMES[item]));

        let hotelView = new HotelView(hotel, offer, descriptions, this._render, facilities, hotel.ty_rating, hotel.url, this._render.url, this._options.currency);
        $(`[data-id=${this._render.cards[index]}]`).html(hotelView.toString());

        updatePrice(this._render.cards[index], {
          price    : offer[2],
          currency : this._options.currency || 'RUB'
        });
      }

    });

    for (let i = this._offers.length; i < this._render.cards.length; i++) {
      removePrice(this._render.cards[i]);
      $(`[data-id=${this._render.cards[i]}]`).parent().remove();
    }

    if (this._render.maps) {
      // const hotels = this._
      this._render.maps.forEach((mapID) => {
        /**
         * @type {MapG|undefined}
         */
        const map = MAP_STORE[mapID];
        if (map) {
          map.__getPOIHotels(hotels);
          map.bound();
        }
      });
    }

    return this;
  }

}
