'use strict';

import {deepExtend} from '../mixins/Deep';
import {svgString} from './view/Loader';

import {priceFormat, textCount} from './mixin/Format';
import {updatePrice, loadPrice} from './Price';

import AutoView from './view/Auto';

function url() {

}

const urlAdds = {
  ageRangeIdx : 0,
  startSearch : 'true',
  srcmarker2  : 'travel_expert_page',
  scp         : '14,12trip_travel-expert_ru,travel_expert_page'
};

export default class Auto {
  constructor(options = {}) {
    this._render = options.render || {};
    this._data = deepExtend({}, options.options);
    this._filters = options.filter || {};
    this.__init()
  }

  __initURL() {
    let url = 'https://www.onetwotrip.com/ru/cars/';

    Object.keys(this._data).forEach((key) => {
      if (url.indexOf('?') > 0) {
        url += `&${key}=${this._data[key]}`
      } else {
        url += `?${key}=${this._data[key]}`
      }
    });

    Object.keys(urlAdds).forEach((key) => {
      if (url.indexOf('?') > 0) {
        url += `&${key}=${urlAdds[key]}`
      } else {
        url += `?${key}=${urlAdds[key]}`
      }
    });

    this._url = url;
  }

  __init() {
    this.__initURL();

    $.ajax('https://www.onetwotrip.com/_api/cars/search', {
      dataType : 'json',
      data     : this._data,
      method   : 'get'
    }).done((response) => {
      if (response.status !== 'OK') {
        // @TODO тест ошибки
        console.log('ОШИБКА Cars', response);
      } else {
        this.__parseSearchData(response.data);
      }
    });

    if (this._render.cards) {
      loadPrice(this._render.cards.length);
      this._render.cards.forEach((cardID) => {
        let $loader = $('<div class="card-loader"></div>');
        $loader.html(svgString);
        $(`[data-id="${cardID}"]`).append($loader);
      });
    }
  }

  /**
   *
   * @param {{providers:{}, rates:{}, searchId:string}} data
   * @private
   */
  __parseSearchData(data) {

    if (data.rates && this._render.cards) {
      if (this._filters.log)
        console.log('Auto предложения', data.rates);
      let rates = data.rates.filter((rate, index) => {
        let ret = true;
        Object.keys(this._filters).forEach((key) => {
          switch (key) {
            case 'class':
            case 'size':
              if (this._filters.log)
                console.log('размер', rate.size);
              ret = ret && rate.size.some((cl) => {
                return this._filters[key].indexOf(cl) > -1
              });
              break;
            case 'door':
            case 'doors':
            case 'seat':
            case 'seats':
              if (!rate[key] || rate[key] < this._filters[key]) {
                ret = ret && false;
              }

              break;
            case 'not':
              ret = ret && this._filters.not.reduce((last, car) => {
                return last && rate.carModels.reduce((rateLast, rateCar) => {
                  return rateLast && !(car.brand == rateCar.brand && car.model == rateCar.model);
                }, true)
              }, true);
              break;
          }
        });
        return ret;
      });


      if (this._filters.log)
        console.log('Auto отфильтровано', rates);

      if (rates.length > 0) {

        let i = 0;
        while (i < this._render.cards.length) {
          // console.log(data.rates[i]);
          const rate = rates[i];

          let options = {type : 'car'};

          options.image = {
            title : `Аренда авто на ${rate.rentalDays} ${textCount(rate.rentalDays, 'день', 'дня', 'дней')}`,
            src   : rate.imageURL
          };

          options.price = {
            title : [],
            btn   : {
              href  : this._url,
              title : `от ${priceFormat(rate.totalAmount, rate.currency)}`
            }
          };

          if (rate.carModels && rate.carModels[0]) {
            options.price.title.push(`${rate.carModels[0].brand} ${rate.carModels[0].model}`);
          }

          if (this._data.transmission) {
            options.price.title.push((this._data.transmission == 'A' ? 'АКПП' : 'МКПП'))
          }

          this.__renderResult(options, this._render.cards[i], {
            price    : rate.totalAmount,
            currency : rate.currency
          });
          i++;
        }
      } else {
        updatePrice(this._render.cards[0], 0);
        $(`[data-id="${this._render.cards[0]}"]`).remove();
      }
    } else {
      updatePrice(this._render.cards[0], 0);
      $(`[data-id="${this._render.cards[0]}"]`).remove();
    }

  }

  __renderResult(options, data_id, cost) {

    let el = $(`[data-id="${data_id}"]`);

    updatePrice(data_id, cost);

    options.image.title = el.find('span.block-card__car__text__title__inner').text() || options.image.title;
    let view = new AutoView(options);
    el.html(view.$html);
  }
}
