"use strict";

$(document).on('click', 'a[href="#back-side-card"]', function (event) {
  event.stopPropagation();
  event.preventDefault();

  $(this).parents('.block-card').first().addClass('backside');
});

$(document).on('click', 'a[href="#front-side-card"]', function (event) {
  event.stopPropagation();
  event.preventDefault();

  $(this).parents('.block-card').first().removeClass('backside');
});

