"use strict";

class Point {
  /**
   *
   * @param {any} options
   * @param {google.maps.Map} defaultMap
   * @param {number} zIndex
   */
  constructor(options, defaultMap, zIndex) {
    /**
     *
     * @type {any}
     * @private
     */
    this._options = options;
    /**
     *
     * @type {google.maps.Map}
     * @private
     */
    this._defaultMap = defaultMap;
    /**
     *
     * @type {number}
     * @private
     */
    this._zIndex = zIndex;

    this._loaded = false;
    this._error = false;
  }

  /**
   *
   * @returns {Point}
   */
  init() {
    return this.initVars().initIcon();
  }

  /**
   *
   * @returns {Point}
   */
  initVars() {
    return this;
  }

  /**
   *
   * @returns {Point}
   */
  initIcon() {
    return this;
  }

  /**
   *
   * @param {function(boolean, Point)} callback
   * @returns {Point}
   */
  onLoad(callback) {
    /**
     *
     * @type {function(boolean, Point)}
     * @private
     */
    this._onLoad = callback;
    if (this._loaded)
      this.whenLoad();
    return this;
  }

  /**
   *
   * @param {boolean} [error]
   */
  whenLoad(error) {
    this._loaded = true;

    if (typeof error !== 'undefined')
      this._error = error;

    if (this._onLoad)
      this._onLoad(this._error, this);
  }
}