"use strict";

import {HotelIcons} from './icons';
// import MarkerCluster from './cluster';
import {byLat} from './sortMarker';
import Updater from './../updater';

let pinnedInfoWindow = undefined, selectedHotel = undefined;

export class HotelLayer {
  /**
   *
   * @param {google.maps.Map} map
   * @param {OTTResultHotelSearch[]} data
   */
  constructor(map, data) {
    this._map = map;
    this._hotels = data.sort(byLat);
    this._items = [];

    this.init();
  }

  init() {
    //   let markers = []
    //     , map = this._map;
    //   // hotelSearch = hotelSearch.sort(sortByLNG);
    //   this._hotels.forEach(function (data) {
    //     let hotel = new Hotel(map, data);
    //     markers.push(hotel.getMarker());
    //   });
    //
    //   new MarkerCluster(
    //     map,
    //     markers,
    //     {
    //       imagePath : '%gulp-imagePath%' + 'map/cluster/m'
    //     }
    //   );

    let map = this._map
      , items = [];

    this._hotels.forEach(function (data) {
      let hotel = new Hotel(data);
      items.push(hotel);
      hotel.setMap(map);
    });

    this._items = items;

  }


  destroy(){
    this._items.forEach(function(poi){
      poi.setMap(null);
    });
    this._items = [];
  }
}

function createBlockHotel(img, name, autopan = false) {
  return new google.maps.InfoWindow({
    disableAutoPan : !autopan,
    content        : '<div class="block-map-hotel">' +
    '  <div class="block-map-hotel__image" style="background-image: url(' + img + ')"></div>' +
    '  <div class="block-map-hotel__info">' +
    '    <div class="block-map-hotel__info__name">' + name + '</div>' +
    '  </div>' +
    '</div>'
  });
}

/**
 * @typedef {Object} OTTPoiData
 * @property {int} id
 * @property {number} lat
 * @property {number} lng
 * @property {string} name
 * @property {string} name_en
 * @property {string} type
 * @property {number} 0
 * @property {number} 1
 * @property {string} 2
 * @property {string} 3
 */
class Hotel {
  /**
   *
   * @param {OTTResultHotelSearch} data
   */
  constructor(data) {
    /**
     *
     * @type {OTTResultHotelSearch}
     * @private
     */
    this._data = data;
    /**
     *
     * @type {number}
     * @private
     */
    this._lat = data.lat;
    /**
     *
     * @type {string}
     * @private
     */
    this._lng = data.lng;
    /**
     *
     * @type {string}
     * @private
     */
    this._type = data.type;
    /**
     *
     * @type {string}
     * @private
     */
    this._name = data.name;

    this._icon = HotelIcons.def;

    // let $divContainer = $(this._map.getDiv());
    // if (!$divContainer.data('evented')) {
    //   $divContainer.on('mouseover', '.block-map-hotel', function () {
    //     windowHover = true;
    //   });
    //   $divContainer.on('mouseout', '.block-map-hotel', function () {
    //     console.log('out');
    //     windowHover = false;
    //     if (hotelHover)
    //       setTimeout(hotelHover.closeWindow.bind(hotelHover), 500);
    //   });
    //   $divContainer.data('evented', true);
    // }

    this.__createMarker();


  }

  __createMarker() {
    /**
     *
     * @type {google.maps.InfoWindow}
     * @private
     */
    this._infowindow = createBlockHotel(this._data.img, this._data.name, false);
    /**
     *
     * @type {google.maps.Marker}
     * @private
     */
    this._marker = new google.maps.Marker({
      // map      : this._map,
      position : {
        lat : this._lat,
        lng : this._lng
      },
      icon     : this._icon,
      title    : this._name,
      zIndex   : 5
    });

    google.maps.event.addListener(this._marker, 'mouseover', this.__mouseOver.bind(this));
    google.maps.event.addListener(this._marker, 'mouseout', this.__mouseOut.bind(this));
    google.maps.event.addListener(this._marker, 'click', this.__click.bind(this));


    Updater.icons(this.setDefaultIcon.bind(this));
  }

  __click() {
    if (pinnedInfoWindow) {
      selectedHotel.unSelect();
    }
    this.select();
  }

  unSelect() {
    pinnedInfoWindow.close();

    selectedHotel = undefined;
    pinnedInfoWindow = undefined;

    this.setUnHover();
    google.maps.event.removeListener(this.listener);
    this.listener = undefined;
  }

  select() {
    selectedHotel = this;

    pinnedInfoWindow = createBlockHotel(this._data.img, this._data.name, true);
    this.closeWindow();
    pinnedInfoWindow.open(this._map, this._marker);

    this.listener = google.maps.event.addListener(pinnedInfoWindow, 'closeclick', this.unSelect.bind(this));
    this.setSelected();

  }

  __mouseOver() {
    // this._in = true;
    if (pinnedInfoWindow) {
      return this.setHover();
    }
    this.setHover().showWindow();
  }

  __mouseOut() {
    // this._in = false;

    if (pinnedInfoWindow) {
      return this.setUnHover();
    }

    this.setUnHover();
    setTimeout(this.closeWindow.bind(this), 500);
    this.closeWindow();
  }

  setSelected() {
    this._marker.setIcon(HotelIcons.selected);
    this._marker.setZIndex(999);
    return this;
  }

  setHover() {
    if (selectedHotel == this)
      return;
    this._marker.setIcon(HotelIcons.hover);
    this._marker.setZIndex(99);
    return this;

  }

  setUnHover() {
    if (selectedHotel == this)
      return;
    this._marker.setIcon(HotelIcons.def);
    this._marker.setZIndex(5);
    return this;

  }

  setDefaultIcon(){
    this._marker.setIcon(HotelIcons.def);
    this._marker.setZIndex(5);
    return this;
  }

  showWindow() {
    // if (!hotelHover && !windowHover) {
    //   hotelHover = this;
    if (this._map)
      this._infowindow.open(this._map, this._marker);
    // }
  }

  setMap(map) {
    this._marker.setMap(map);
    this._map = map;
    return this;
  }

  closeWindow() {
    // if (this._in || (hotelHover !== this || windowHover))
    //   return;
    // hotelHover = null;
    this._infowindow.close();
  }

  getMarker() {
    return this._marker;
  }
}

export default Hotel;