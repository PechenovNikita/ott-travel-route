'use strict';

import {isFunction} from './../mixins/Function';

/**
 * @typedef {Object} DirectionOptions
 * @property {GooglePointOptions} origin
 * @property {GooglePointOptions} destination
 * @property {Array.<{location:GooglePointOptions}>} waypoints
 */

const labels = [
  'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
  'АA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN', 'AO', 'AP', 'AQ', 'AR', 'AS', 'AT', 'AU', 'AV', 'AW', 'AX', 'AY', 'AZ'
];

/**
 * @class
 */
class Direction {
  /**
   *
   * @param {DirectionOptions} [options={}]
   * @param {google.maps.Map} map
   */
  constructor(options = {}, map) {

    this._id = 'map-direction_' + Date.now();
    this._map = map;
    this._visible = false;

    options = (!options ? {} : options);

    /**
     *
     * @type {Array.<google.maps.Marker>}
     * @private
     */
    this._markers = [];

    this.__initVarsData(options);

    if (this._origin && this._destination)
      this.create();
  }

  onDone(callback) {
    this._onDone = callback;
    return this;
  }

  /**
   *
   * @param {DirectionOptions} options
   */
  __initVarsData(options) {
    /**
     * @type {GooglePointOptions}
     */
    this._origin = options.origin;
    /**
     * @type {GooglePointOptions}
     */
    this._destination = options.destination;

    return this.__initWaypoints(options);
  }

  /**
   *
   * @param {DirectionOptions} options
   */
  __initWaypoints(options) {
    /**
     *
     * @type {Array.<{location:GooglePointOptions}>}
     */
    let waypoints = options.waypoints || [];

    if (waypoints && waypoints.length > 0 && waypoints[0].location == options.origin)
      waypoints.shift();

    if (waypoints && waypoints.length > 0 && waypoints[waypoints.length - 1].location == options.destination)
      waypoints.pop();

    /**
     *
     * @type {Array.<{location:GooglePointOptions}>}
     * @private
     */
    this._waypoints = waypoints;

    return this;
  }

  create() {
    /**
     *
     * @type {{destination: GooglePointOptions, origin: GooglePointOptions, waypoints: Array.<{location: GooglePointOptions}>, provideRouteAlternatives: boolean, travelMode: (*)}}
     */
    let request = {
      destination              : this._destination,
      origin                   : this._origin,
      waypoints                : this._waypoints,
      provideRouteAlternatives : false,
      travelMode               : google.maps.TravelMode.DRIVING
    };

    /**
     *
     * @type {google.maps.DirectionsService}
     * @private
     */
    this._directionsService = new google.maps.DirectionsService();
    this._directionsService.route(request, this.__googleRouteDirectionService.bind(this));
  }

  /**
   *
   * @param {google.maps.DirectionsResult} response
   * @param {google.maps.DirectionsStatus} status
   * @private
   */
  __googleRouteDirectionService(response, status) {
    // console.log('Direction', '__googleRouteDirectionService()', response.routes[0].bounds);
    if (status == google.maps.DirectionsStatus.OK) {
      this.__googleDirectionResponse(response);
    } else {
      this.__manualDirection();
    }
  }

  /**
   *
   * @param {google.maps.DirectionsResult} response
   * @private
   */
  __googleDirectionResponse(response) {
    /**
     *
     * @type {google.maps.DirectionsRenderer}
     * @private
     */
    this._directionsDisplay = new google.maps.DirectionsRenderer();
    this._directionsDisplay.setDirections(response);

    this._bounds = response.routes[0].bounds;

    if (this._visible)
      this.show();
  }

  __manualDirection() {
    /**
     *
     * @type {Array.<{location:GooglePointOptions}>}
     */
    let waypoints = this._waypoints.slice(0);

    waypoints.splice(0, 0, {location : this._origin});
    waypoints.push({location : this._destination});
    // console.log('Direction', '__manualDirection()', waypoints);

    this._markerCount = waypoints.length;
    this._markers = [];

    waypoints.forEach(function (waypoint, i) {
      this.__preMarkerCreate(waypoint.location, labels[i], i, i == waypoints.length - 1);
    }, this);

  }

  /**
   *
   * @param {GooglePointOptions} location
   * @param {string} label
   * @param {number} index
   * @param {boolean} [last]
   * @private
   */
  __preMarkerCreate(location, label, index, last) {
    // console.log('Direction', '__preMarkerCreate()', arguments);
    // label = (!label && (location && location.label) ? location.label : label);
    if (!this._geocoder)
      this._geocoder = new google.maps.Geocoder();

    if (typeof location === 'string') {
      this.__preMarkerCreate_stringLocation(location, label, index, last);
    } else if (typeof location.lat !== 'undefined' && typeof location.lng !== 'undefined') {
      if (isFunction(location.lat) && isFunction(location.lng))
        this.__setMarker(location, label, index, last);
      else
        this.__setMarker(new google.maps.LatLng(location.lat, location.lng), label, index, last);
    }
  }

  /**
   *
   * @param {string} location
   * @param {string} label
   * @param {number} index
   * @param {boolean} [last]
   * @private
   */
  __preMarkerCreate_stringLocation(location, label, index, last) {
    // console.log('Direction', '__preMarkerCreate_stringLocation()', arguments);

    this._geocoder.geocode({'address' : location}, (function (results, status) {

      if (status == google.maps.GeocoderStatus.OK) {
        this.__setMarker(results[0].geometry.location, label, index, last);
      } else if (status == google.maps.GeocoderStatus.OVER_QUERY_LIMIT) {
        setTimeout(this.__preMarkerCreate_stringLocation.bind(this, location, label, index, last), 500);
      } else {
        console.log('Geocode was not successful for the following reason: ' + status);
      }

    }).bind(this));

  }

  /**
   *
   * @param {google.maps.LatLng} position
   * @param {?string} label
   * @param {?number} index
   * @param {boolean} [last]
   */
  __setMarker(position, label, index, last) {
    // console.log('Direction', '__setMarker()', arguments);
    /**
     *
     * @type {google.maps.Marker}
     */
    let marker = new google.maps.Marker({
      position : position,
      // label    : label
    });

    if (!last) {
      marker.setIcon(`https://mts.googleapis.com/maps/vt/icon/name=icons/spotlight/spotlight-waypoint-a.png&text=${label}&psize=16&font=fonts/Roboto-Regular.ttf&color=ff333333&ax=44&ay=48&scale=1`);
    } else {
      marker.setIcon(`https://mts.googleapis.com/maps/vt/icon/name=icons/spotlight/spotlight-waypoint-b.png&text=${label}&psize=16&font=fonts/Roboto-Regular.ttf&color=ff333333&ax=44&ay=48&scale=1`);
    }

    if (typeof index === 'undefined')
      this._markers.push(marker);
    else
      this._markers[index] = marker;

    this.__onMarkerAdd();
  }

  __onMarkerAdd() {
    this._markerCount--;
    // console.log('Direction', '__onMarkerAdd()', this._markerCount, this._markers);
    if (this._markerCount == 0)
      this.__onManualRouteDone();
  }

  __onManualRouteDone() {
    // console.log('Direction', '__onManualRouteDone()', this._markers);

    // this._markers = Array.prototype.slice.call(this._markers);

    let markers = [];

    for (let index in this._markers) {
      if (this._markers.hasOwnProperty(index)) {
        markers.push(this._markers[index]);
      }
    }

    this._markers = markers;

    this.__createCoordinates();
    this.__createPath();

    if (this._visible)
      this.show();

    if (this._onDone)
      this._onDone();
  }

  __createCoordinates() {
    /**
     *
     * @type {google.maps.LatLngBounds}
     * @private
     */
    this._bounds = new google.maps.LatLngBounds();
    /**
     *
     * @type {Array.<google.maps.LatLng>}
     */
    this._coordinates = this._markers.map(function (marker) {
      let tempLatLng = marker.getPosition();
      this._bounds.extend(tempLatLng);
      return tempLatLng;
    }, this);

    // for (let i in this._markers) {
    //   if (this._markers.hasOwnProperty(i)) {
    //     let tempLatLng = this._markers[i].getPosition();
    //     this._coordinates.push(tempLatLng);
    //     this._bounds.extend(tempLatLng);
    //   }
    // }
    return this;
  }

  __createPath() {
    /**
     *
     * @type {google.maps.Polyline}
     * @private
     */
    this._manualPath = new google.maps.Polyline({
      path          : this._coordinates,
      geodesic      : true,
      strokeColor   : '#5085f8',
      strokeOpacity : 0.7,
      strokeWeight  : 4
    });

    return this;
  }

  /**
   *
   * @param {google.maps.Map} map
   * @returns {Direction}
   * @private
   */
  __setMap(map) {
    this._markers.forEach(function (marker) {
      marker.setMap(map)
    });

    if (this._manualPath)
      this._manualPath.setMap(map);

    if (this._directionsDisplay)
      this._directionsDisplay.setMap(map);
    return this;
  }

  /**
   *
   * @returns {Direction}
   * @private
   */
  __remove() {
    this._markers = [];
    this._bounds = null;

    if (this._manualPath)
      this._manualPath = null;

    if (this._directionsDisplay)
      this._directionsDisplay = null;

    return this;
  }

  /**
   *
   * @returns {Direction}
   */
  clear() {
    // this._origin = '';
    // this._destination = '';
    // this._waypoints = [];

    return this.hide().__remove();
  }

  /**
   *
   * @returns {Direction}
   */
  show() {
    this._visible = true;
    return this.__setMap(this._map);
  }

  /**
   *
   * @returns {Direction}
   */
  hide() {
    this._visible = false;
    return this.__setMap(null);
  }

  /**
   *
   * @returns {Direction}
   */
  bound() {
    if (this._bounds && this._coordinates && this._coordinates.length > 0)
      this._map.fitBounds(this._bounds);
    return this;
  }

  /**
   *
   * @param {DirectionOptions} options
   */
  update(options) {
    this.clear().__initVarsData(options);

    if (this._origin && this._destination)
      this.create();
  }

  /**
   *
   * @returns {google.maps.LatLngBounds|google.maps.LatLngBounds|google.maps.LatLngBoundsLiteral|undefined|google.maps.LatLngBounds|google.maps.LatLngBoundsLiteral|Array|*}
   */
  getBounds() {
    if (this._bounds)
      return this._bounds;
    return null;
  }
}

export default Direction;
