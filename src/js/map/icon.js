"use strict";
/**
 * @class
 */
class MapIcon {
  /**
   * @param {string} src
   * @param {number} width
   * @param {number} height
   * @param {{src:string,width:number,height:number,position:{x:number,y:number}}=} add
   * @constructor
   */
  constructor(src, width, height, add) {
    this._src = src;
    this._width = width;
    this._height = height;

    this._loadedMain = false;
    if (add) {
      this._add = new MapIcon(add.src, add.width, add.height);
      this._add.onLoad(this.addLoaded.bind(this, add.position.x, add.position.y, add.width, add.height));
      this._addLoad = false;
    } else {
      this._addLoad = true;
    }

    this._loaded = false;

    this.init();
  }

  /**
   *
   * @returns {HTMLCanvasElement}
   */
  getCanvas() {
    return this._canvas;
  }

  /**
   *
   * @callback MapIconCallback
   * @param {MapIcon} responseIcon
   */

  /**
   *
   * @param {MapIconCallback} callback
   */
  onLoad(callback) {
    if (this._loaded)
      callback(this);
    else
      /**
       *
       * @type {MapIconCallback}
       * @private
       */
      this._onload = callback;
  }

  __whenLoad() {
    this._loaded = true;

    if (this._onload)
      this._onload(this);
  }

  addLoaded(x, y, width, height) {
    if (this._loadedMain)
      this.drawAdd(x, y, width, height);
    else
      this._localOnLoad = this.drawAdd.bind(this, x, y, width, height)
  }

  /**
   *
   * @param {number} x
   * @param {number} y
   * @param {number} width
   * @param {number} height
   */
  drawAdd(x, y, width, height) {
    let addCanvas = this._add.getCanvas();
    // this._context.rect(x,y,width,height);
    // this._context.stroke();
    this._context.drawImage(addCanvas, 0, 0, addCanvas.width, addCanvas.height, x, y, width, height);
    this.__whenLoad();
  }

  init() {
    /**
     *
     * @type {HTMLCanvasElement|Element}
     * @private
     */
    this._canvas = document.createElement('canvas');

    /**
     *
     * @type {CanvasRenderingContext2D}
     * @private
     */
    this._context = this._canvas.getContext('2d');

    this._canvas.width = this._width;
    this._canvas.height = this._height;

    this.__initImage();

  }

  __initImage() {
    let image = new Image();
    image.onload = this.__imageLoaded.bind(this, image);
    image.src = this._src;
  }

  __imageLoaded(image) {
    this._context.drawImage(image, 0, 0, image.width, image.height, 0, 0, this._width, this._height);
    this._loadedMain = true;

    if (this._add && this._localOnLoad)
      this._localOnLoad();
    else if (!this._add)
      this.__whenLoad();
    // if (mapInit) {
    //   HotelIcons.selected = createGoogleIcon(can.toDataURL('image/png'), HOTEL_WIDTH, HOTEL_HEIGHT, 0, 0, (HOTEL_WIDTH) / 2, HOTEL_HEIGHT - 2);
    // } else
    //   HotelIcons.selected = can.toDataURL('image/png');
  }
}

export default MapIcon;
