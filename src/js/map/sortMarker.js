"use strict";

/**
 *
 * @param {OTTResultHotelSearch|OTTResultPoiSearch} a
 * @param {OTTResultHotelSearch|OTTResultPoiSearch} b
 * @returns {number}
 */
export function byLat(a, b) {
  if (a.lat > b.lat || a[0] > b[0])
    return -1;
  if (a.lat < b.lat || a[0] < b[0])
    return 1;
  return 0;
}