"use strict";

import {POIIcons} from './icons';
// import LayerOverlay from './layerOverlay';
import {byLat} from './sortMarker'
import Updater from './../updater';

export const names = {
  tree     : 'children',
  civic    : 'sign',
  school   : 'children',
  business : 'sign',
  medical  : 'children',
  golf     : 'sign'
};

export class POILayer {
  /**
   *
   * @param {google.maps.Map} map
   * @param {OTTPoiData[]} data
   */
  constructor(map, data) {
    this._map = map;
    this._poi = data.sort(byLat);

    this._items = [];

    this.init();
  }

  init() {
    let map = this._map
      , items = [];

    this._poi.forEach(function (data) {
      let poi = new POI(data);
      items.push(poi);
      poi.setMap(map);
    });

    this._items = items;

    // layer.setMap(this._map);
  }

  destroy(){
    this._items.forEach(function(poi){
      poi.setMap(null);
    });
    this._items = [];
  }
}

/**
 * @typedef {Object} OTTPoiData
 * @property {int} id
 * @property {number} lat
 * @property {number} lng
 * @property {string} name
 * @property {string} name_en
 * @property {string} type
 * @property {number} 0
 * @property {number} 1
 * @property {string} 2
 * @property {string} 3
 */
class POI {
  /**
   *
   * @param {google.maps.Map} map
   * @param {OTTPoiData} data
   */
  constructor(data) {
    /**
     *
     * @type {number}
     * @private
     */
    this._lat = data[0] || data.lat;
    /**
     *
     * @type {string}
     * @private
     */
    this._lng = data[1] || data.lng;
    /**
     *
     * @type {string}
     * @private
     */
    this._type = data[2] || data.type;
    /**
     *
     * @type {string}
     * @private
     */
    this._name = data[3] || data.name;
    this._icon = (names[this._type] ? POIIcons.items[names[this._type]] : POIIcons.items[this._type]);

    this.__createMarker();

  }

  __createMarker() {
    /**
     *
     * @type {google.maps.InfoWindow}
     * @private
     */
    this._infowindow = new google.maps.InfoWindow({
      content : this._name
    });

    this._marker = new google.maps.Marker({
      position : {
        lat : this._lat,
        lng : this._lng
      },
      icon     : this._icon,
      title    : this._name,
      zIndex   : 10
    });

    google.maps.event.addListener(this._marker, 'mouseover', this.__mouseOver.bind(this));
    google.maps.event.addListener(this._marker, 'mouseout', this.__mouseOut.bind(this));
    google.maps.event.addListener(this._marker, 'click', this.__click.bind(this));


    Updater.icons(this.setDefaultIcon.bind(this));
  }

  setDefaultIcon(){
    this._icon = (names[this._type] ? POIIcons.items[names[this._type]] : POIIcons.items[this._type]);
    this._marker.setIcon(this._icon);
    return this;
  }

  __mouseOver() {
    if (this._map)
      this._infowindow.open(this._map, this._marker);
  }

  __mouseOut() {
    this._infowindow.close();
  }

  setMap(map) {
    this._marker.setMap(map);
    this._map = map;
    return this;
  }

  __click() {
    this._infowindow.close();
  }

  /**
   *
   * @returns {google.maps.Marker}
   */
  getMarker() {
    return this._marker;
  }
}

export default POI;
