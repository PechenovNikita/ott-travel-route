"use strict";

import {isFunction} from './../mixins/Function';

/**
 * @typedef {Object} PointOptions
 * @property {GooglePointOptions|GooglePointOptions[]} location
 * @property {boolean|undefined} [hidden]
 * @property {number} zoom
 */

export class PointsLayer {
  /**
   *
   * @param {PointOptions} [options={}]
   * @param {google.maps.Map} map
   */
  constructor(options = {}, map) {
    /**
     *
     * @type {google.maps.Map}
     * @private
     */
    this._map = map;
    this._id = 'points_' + Date.now();

    options = (!options ? {} : options);

    this._points = [];

    this._coordinates = [];

    this.__initVarsData(options);
  }

  onDone(callback) {
    this._onDone = callback;
    return this;
  }

  /**
   *
   * @param {PointOptions} options
   * @returns {PointsLayer}
   * @private
   */
  __initVarsData(options) {
    // console.log('Points', '__initVarsData()', arguments);
    /**
     *
     * @type {Array.<google.maps.LatLng>}
     * @private
     */
    this._coordinates = [];

    let hidden = !!options.hidden;

    if (Array.isArray(options.location)) {
      // console.log('Points', '__initVarsData()', 'Array.isArray(options.location)');
      this._count = options.location.length;
      /**
       *
       * @type {Array<Point>}
       * @private
       */
      this._points = options.location.map(function (locationOption, index) {
        return new Point(locationOption, hidden, this._map, this.whenMarkerCreated.bind(this));
      }, this);

    } else if (options.location) {
      // console.log('Points', '__initVarsData()', '!Array.isArray(options.location)');
      this._count = 1;
      /**
       *
       * @type {Array<Point>}
       * @private
       */
      this._points = [new Point(options.location, this._map, this.whenMarkerCreated.bind(this))];
    }
    return this;
  }

  /**
   *
   * @returns {PointsLayer}
   */
  show() {
    this._visible = true;
    this._points.forEach(function (point) {
      point.show();
    }, this);
    return this;
  }

  /**
   *
   * @returns {PointsLayer}
   */
  hide() {
    this._visible = false;
    this._points.forEach(function (point) {
      point.hide();
    }, this);
    return this;
  }

  /**
   *
   * @returns {PointsLayer}
   */
  clear() {
    this.hide();
    this._points = [];
    this._bounds = null;
    return this;
  }

  /**
   *
   * @param {PointOptions} options
   */
  update(options) {
    this.clear().__initVarsData(options);
  }

  /**
   *
   * @param {boolean} error
   * @param {Point} point
   */
  whenMarkerCreated(error, point) {
    // console.log('Points', 'whenMarkerCreated()', arguments);
    this._count--;

    if (!error)
      this.__createCoordinate(point);

    if (this._count == 0) {
      this.__done();
    }
  }

  /**
   *
   * @param {Point} point
   * @returns {PointsLayer}
   * @private
   */
  __createCoordinate(point) {

    // console.log('Point', '__createCoordinate()', point);
    if (!this._bounds)
      /**
       *
       * @type {google.maps.LatLngBounds}
       * @private
       */
      this._bounds = new google.maps.LatLngBounds();

    /**
     *
     * @type {google.maps.LatLng}
     */
    let pos = point.getLatLng();

    this._coordinates.push(pos);
    this._bounds.extend(pos);

    return this;
  }

  /**
   *
   * @returns {PointsLayer}
   * @private
   */
  __done() {

    // console.log('Point', '__done()', this._bounds.toJSON());

    if (this._visible)
      this.show();

    if (this._onDone)
      this._onDone();

    return this;

  }

  /**
   *
   * @returns {PointsLayer}
   */
  bound() {
    if (this._bounds && this._coordinates && this._coordinates.length > 0)
      this._map.fitBounds(this._bounds);
    return this;
  }

  /**
   *
   * @returns {google.maps.LatLngBounds}
   */
  getBounds() {
    if (!!this._bounds) {
      return this._bounds;
    }
    return null;
  }
}

class Point {

  /**
   *
   * @param {GooglePointOptions} location
   * @param {boolean} [hidden=false]
   * @param {google.maps.Map} map
   * @param {function(boolean, Point)} [whenDone = function () {}]
   */
  constructor(location, hidden = false, map, whenDone = function () {}) {
    // console.log('Point', 'constructor()', arguments);
    this._map = map;
    this._id = 'point_' + Date.now();
    this._marker = null;
    this._visible = false;
    this._hidden = hidden;

    this._onCreate = whenDone;

    this.__preMarkerCreate(location);
  }

  /**
   *
   * @param {GooglePointOptions} location
   * @param {string} [label]
   * @private
   */
  __preMarkerCreate(location, label) {
    // console.log('Point', '__preMarkerCreate()', arguments);
    // label = (!label && (location && location.label) ? location.label : label);
    if (!this._geocoder)
      this._geocoder = new google.maps.Geocoder();

    if (typeof location === "string") {
      this.__preMarkerCreate_stringLocation(location, label);
    } else if (typeof location.lat !== "undefined" && typeof location.lng !== "undefined") {
      if (isFunction(location.lat) && isFunction(location.lng))
        this.__setMarker(location, label);
      else
        this.__setMarker(new google.maps.LatLng(location.lat, location.lng), label);
    } else if (location.name && typeof location.name === 'string') {
      this.__preMarkerCreate_stringLocation(location, label);
    }
  }

  /**
   *
   * @param {string} location
   * @param {string} label
   * @private
   */
  __preMarkerCreate_stringLocation(location, label) {
    // console.log('Point', '__preMarkerCreate_stringLocation()', arguments);

    this._geocoder.geocode({'address' : location}, (function (results, status) {

      if (status === google.maps.GeocoderStatus.OK) {
        this.__setMarker(results[0].geometry.location, label);
      } else if (status === google.maps.GeocoderStatus.OVER_QUERY_LIMIT) {
        setTimeout(this.__preMarkerCreate_stringLocation.bind(this, location, label), 500);
      } else {
        this._onCreate(true, this);
        console.log(location, 'Geocode was not successful for the following reason: ' + status);
      }

    }).bind(this));

  }

  /**
   *
   * @param {google.maps.LatLng} position
   * @param {?string} label
   */
  __setMarker(position, label) {
    // console.log('Point', '__setMarker()', position.toJSON());
    /**
     *
     * @type {google.maps.Marker}
     */
    this._marker = new google.maps.Marker({
      position : position,
      label    : label,
      zIndex   : 0
    });

    this.__onMarkerAdd();
  }

  __onMarkerAdd() {
    if (this._visible)
      this.show();

    this._onCreate(false, this);
  }

  /**
   *
   * @param {?google.maps.Map} map
   * @returns {Point}
   */
  setMap(map) {
    if (!this._hidden && this._marker)
      this._marker.setMap(map);
    return this;
  }

  /**
   *
   * @returns {Point}
   */
  show() {
    this._visible = true;
    return this.setMap(this._map);
  }

  /**
   *
   * @returns {Point}
   */
  hide() {
    this._visible = false;
    return this.setMap(null);
  }

  /**
   *
   * @returns {google.maps.LatLng}
   */
  getLatLng() {
    if (this._marker)
      return this._marker.getPosition();
    return null;
  }

}

export default Point;
