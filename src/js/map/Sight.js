'use strict';

import {POIIcons, googleIcon} from './icons';

import {PointsLayer} from './Point';
import Point from './Point';

import {byLat} from './sortMarker'
import Updater from './../updater';

/**
 * @typedef {Object} OTTPoiData
 * @property {int} id
 * @property {number} lat
 * @property {number} lng
 * @property {string} name
 * @property {string} name_en
 * @property {string} type
 * @property {number} 0
 * @property {number} 1
 * @property {string} 2
 * @property {string} 3
 */

const names = {
  tree     : 'children',
  civic    : 'sign',
  school   : 'children',
  business : 'sign',
  medical  : 'children',
  golf     : 'sign'
};

export class SightLayer {

  /**
   *
   * @param {OTTPoiData[]} options
   * @param {google.maps.Map} map
   */
  constructor(options, map) {
    // console.log('SightLayer', 'constructor()', arguments);
    this._map = map;
    // this._options = options.sort(byLat);

    this._points = [];

    this._coordinates = [];

    this.__initVarsData(options);
  }

  onDone(callback) {
    this._onDone = callback;
    return this;
  }

  /**
   *
   * @param {OTTPoiData[]} options
   * @returns {SightLayer}
   * @private
   */
  __initVarsData(options) {
    // console.log('SightLayer', '__initVarsData()', arguments);
    /**
     *
     * @type {Array.<google.maps.LatLng>}
     * @private
     */
    this._coordinates = [];

    if (Array.isArray(options)) {

      /**
       *
       * @type {Array.<Sight>}
       * @this SightLayer
       * @private
       */
      this._points = options.sort(byLat).map(function (option, index) {
        let sight = new Sight(option, this._map, 50 + index);
        this.__createCoordinate(sight);
        return sight;
      }, this);
    }

    if (this._onDone)
      this._onDone();

    return this;

  }

  /**
   *
   * @returns {SightLayer}
   */
  show() {
    this._visible = true;
    this._points.forEach(function (point) {
      point.show();
    }, this);
    return this;
  }

  /**
   *
   * @returns {SightLayer}
   */
  hide() {
    this._visible = false;
    this._points.forEach(function (point) {
      point.hide();
    }, this);
    return this;
  }

  /**
   *
   * @returns {SightLayer}
   */
  clear() {
    this.hide();
    this._points = this._points.filter(function (sight) {
      sight.remove();
      return false;
    });
    this._bounds = null;
    return this;
  }

  /**
   *
   * @param {OTTPoiData[]} options
   */
  update(options) {
    this.clear().__initVarsData(options);
  }

  /**
   *
   * @param {Sight} point
   * @private
   */
  __createCoordinate(point) {
    // console.log('SightLayer', '__createCoordinate()', arguments);

    // console.log('Point', '__createCoordinate()', point);
    if (!this._bounds)
      /**
       *
       * @type {google.maps.LatLngBounds}
       * @private
       */
      this._bounds = new google.maps.LatLngBounds();

    /**
     *
     * @type {?google.maps.LatLng}
     */
    let pos = point.getLatLng();

    if (pos !== null) {
      this._coordinates.push(pos);
      this._bounds.extend(pos);
    }

    return this;
  }

  /**
   *
   * @returns {SightLayer}
   */
  bound() {
    if (this._bounds && this._coordinates && this._coordinates.length > 0)
      this._map.fitBounds(this._bounds);
    return this;
  }

  /**
   *
   * @returns {google.maps.LatLngBounds}
   */
  getBounds() {
    if (!!this._bounds) {
      return this._bounds;
    }
    return null;
  }
}

class Sight {

  /**
   *
   * @param {OTTPoiData} options
   * @param {google.maps.Map} map
   * @param {number} zIndex
   */
  constructor(options, map, zIndex) {
    /**
     *
     * @type {number}
     */
    this._zIndex = zIndex;
    /**
     *
     * @type {google.maps.Map}
     * @private
     */
    this._map = map;
    /**
     *
     * @type {number}
     * @private
     */
    this._lat = parseFloat(options[0]) || parseFloat(options.lat || 0);
    /**
     *
     * @type {number}
     * @private
     */
    this._lng = parseFloat(options[1]) || parseFloat(options.lng || 0);
    /**
     *
     * @type {string}
     * @private
     */
    this._type = options[2] || options.type || 'none';
    /**
     *
     * @type {string}
     * @private
     */
    this._label = options[4] || '';
    /**
     *
     * @type {string}
     * @private
     */
    this._name = options[3] || options.name;
    this._icon = (names[this._type] ? POIIcons.items[names[this._type]] : POIIcons.items[this._type]);

    this.__createMarker();

  }

  __createMarker() {
    /**
     *
     * @type {google.maps.InfoWindow}
     * @private
     */
    this._infowindow = new google.maps.InfoWindow({
      content : this._name
    });

    this._marker = new google.maps.Marker({
      position : {
        lat : this._lat,
        lng : this._lng
      },
      icon     : this._icon,
      zIndex   : this._zIndex,
      // animation: google.maps.Animation.DROP,
    });

    this._listenerOver = google.maps.event.addListener(this._marker, 'mouseover', this.__mouseOver.bind(this));
    this._listenerOut = google.maps.event.addListener(this._marker, 'mouseout', this.__mouseOut.bind(this));
    this._listenerClick = google.maps.event.addListener(this._marker, 'click', this.__click.bind(this));

    Updater.icons(this.setDefaultIcon.bind(this));
  }

  setDefaultIcon() {
    switch (this._type) {
      case 'google-red':
        this._icon = googleIcon(false, this._label);
        break;
      case 'google-green':
        this._icon = googleIcon(true, this._label);
        break;
      default:
        this._icon = (names[this._type] ? POIIcons.items[names[this._type]] : POIIcons.items[this._type]);
    }
    this._marker.setIcon(this._icon);
    return this;
  }

  __mouseOver() {
    if (this._map)
      this._infowindow.open(this._map, this._marker);
  }

  __mouseOut() {
    this._infowindow.close();
  }

  __click() {
    // console.log('Sight', '__click');
    // this._infowindow.close();
  }

  setMap(map) {
    if (this._marker) {
      this._marker.setMap(map);
    }
    return this;
  }

  show() {
    this.setMap(this._map);
    return this;
  }

  hide() {
    this.setMap(null);
    return this;
  }

  remove() {
    this.hide();

    google.maps.event.removeListener(this._listenerClick);
    google.maps.event.removeListener(this._listenerOver);
    google.maps.event.removeListener(this._listenerOut);

    return this;

  }

  /**
   *
   * @returns {google.maps.LatLng}
   */
  getLatLng() {
    if (this._marker)
      return this._marker.getPosition();
    return null;
  }
}

export default Sight;
