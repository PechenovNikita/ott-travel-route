"use strict";

/**
 * @typedef {Object} GoogleMapIcon
 * @property {string} url
 * @property {google.maps.Size} size
 * @property {google.maps.Point} origin
 * @property {google.maps.Point} anchor
 *
 */


import Updater from './../updater';
import Icon from './icon';

const POIImage = [
  'm-airport.png',
  'm-anchor.png',
  'm-beer.png',
  'm-bicycle.png',
  'm-budda.png',
  'm-boat.png',
  'm-border.png',
  'm-cabin.png',
  'm-castle.png',
  'm-cathedral.png',
  'm-children.png',
  'm-church.png',
  'm-coffeeshop.png',
  'm-cocktail.png',
  'm-eye.png',
  'm-fastfood.png',
  'm-food-tray.png',
  'm-food.png',
  'm-icecream.png',
  'm-info.png',
  'm-helicopter.png',
  'm-historic.png',
  'm-kiosk.png',
  'm-house.png',
  'm-hookah.png',
  'm-monument.png',
  'm-mountain.png',
  'm-museums.png',
  'm-parking.png',
  'm-place.png',
  'm-surfing.png',
  'm-sign.png',
  'm-skiing.png',
  'm-shopping.png',
  'm-stadium.png',
  'm-theater.png',
  'm-train.png',
  'm-waterfall.png',
  'm-wave.png',
  'm-volleyball.png',
  'm-vine.png',
];

export let names = (function () {
  return POIImage.map(function (icon_src) {
    return icon_src.replace('m-', '').replace('.png', '');
  }).sort();
})();

/**
 *
 * @type {{def: GoogleMapIcon|string|null, items: {}}}
 */
export let POIIcons = {
  def   : null,
  items : {}
};

const POI_WIDTH = 50, POI_HEIGHT = 50;
let mapInit = false;

let iconsCount = 0;

/**
 *
 * @param {string|GoogleMapIcon} url
 * @param {number} width
 * @param {number} height
 * @param {number} oWidth
 * @param {number} oHeight
 * @param {number} aWidth
 * @param {number} aHeight
 * @param {string} name
 * @returns {?GoogleMapIcon}
 */
function createGoogleIcon(url, width, height, oWidth, oHeight, aWidth, aHeight, name) {
  iconsCount--;
  if (typeof url == "string") {
    return {
      url    : url,
      size   : new google.maps.Size(width, height),
      origin : new google.maps.Point(oWidth, oHeight),
      anchor : new google.maps.Point(aWidth, aHeight)
    };
  } else if (url && url.url && url.size)
    return url;
  return null;
}

function Update() {
  if (iconsCount <= 0) {
    Updater.iconsLoad();
  }
}

Updater.googleMapInit(function () {
  mapInit = true;
  updatePOIIcons();
  updateHotelsIcons();
  Update();
});

function updatePOIIcons() {
  // POIIcons.def = createGoogleIcon(POIIcons.def, POI_WIDTH, POI_HEIGHT, 0, 0, POI_WIDTH / 2, POI_HEIGHT)
  for (let name in POIIcons.items) {
    if (POIIcons.items.hasOwnProperty(name)) {
      POIIcons.items[name] = createGoogleIcon(POIIcons.items[name], POI_WIDTH, POI_HEIGHT, 0, 0, POI_WIDTH / 2, POI_HEIGHT, 'poi - ' + name)
    }
  }
}

(function () {
  POIImage.forEach(function (ico) {
    iconsCount++;
    let name = ico.replace('m-', '').replace('.png', '');
    let add = {
      src      : '%gulp-imagePath%' + 'map/png/' + ico,
      width    : POI_WIDTH * .4,
      height   : POI_WIDTH * .4,
      position : {
        x : POI_WIDTH * .3,
        y : POI_HEIGHT / 6 + 1,
      }
    };

    let test = new Icon('%gulp-imagePath%' + 'map/png/m-marker.png', POI_WIDTH, POI_HEIGHT, add);

    test.onLoad(function (t) {

      let can = t.getCanvas();

      if (mapInit) {
        POIIcons.items[name] = createGoogleIcon(can.toDataURL('image/png'), POI_WIDTH, POI_HEIGHT, 0, 0, POI_WIDTH / 2, POI_HEIGHT, 'poi - ' + name);
        Update();
      } else
        POIIcons.items[name] = can.toDataURL('image/png');

    });
  });

  Update();
}());

/**
 *
 * @type {{def: null|GoogleMapIcon|string, hover: null|GoogleMapIcon|string, selected: null|GoogleMapIcon|string}}
 */
export var HotelIcons = {};

const HOTEL_WIDTH = 20, HOTEL_HEIGHT = 28;

let tempHotels = {
  def      : 'map/m-hotel.png',
  hover    : 'map/m-hotel--hover.png',
  selected : 'map/m-hotel--selected.png',
};

function updateHotelsIcons() {
  for (let prop in HotelIcons) {
    if (HotelIcons.hasOwnProperty(prop)) {
      HotelIcons[prop] = createGoogleIcon(HotelIcons[prop], HOTEL_WIDTH, HOTEL_HEIGHT, 0, 0, HOTEL_WIDTH / 2, HOTEL_HEIGHT - 1, 'hotels - ' + prop);
    }
  }
}

(function () {

  for (let prop in tempHotels) {
    if (tempHotels.hasOwnProperty(prop)) {
      iconsCount++;

      let temp = new Icon('%gulp-imagePath%' + tempHotels[prop], HOTEL_WIDTH, HOTEL_HEIGHT);
      temp.onLoad(function (t) {
        let can = t.getCanvas();

        if (mapInit) {
          HotelIcons[prop] = createGoogleIcon(can.toDataURL('image/png'), HOTEL_WIDTH, HOTEL_HEIGHT, 0, 0, HOTEL_WIDTH / 2, HOTEL_HEIGHT - 1, 'hotels - ' + prop);
          Update();
        } else
          HotelIcons[prop] = can.toDataURL('image/png');
      });
    }
  }

  Update();
}());

/**
 *
 * @param {boolean} green
 * @param {string} text
 * @param {number|undefined} [psize=16]
 * @param {string|undefined} [color='ff333333]
 * @param {number|undefined} [scale=1]
 * @returns {string}
 */
export function googleIcon(green = false, text = 'string', psize = 16, color = 'ff333333', scale = 1) {
  if (green)
    return `https://mts.googleapis.com/maps/vt/icon/name=icons/spotlight/spotlight-waypoint-a.png&text=${text}&psize=${psize}&font=fonts/Roboto-Regular.ttf&color=${color}&ax=44&ay=48&scale=${scale}`
  return `https://mts.googleapis.com/maps/vt/icon/name=icons/spotlight/spotlight-waypoint-b.png&text=${text}&psize=${psize}&font=fonts/Roboto-Regular.ttf&color=${color}&ax=44&ay=48&scale=${scale}`
}
