"use strict";

import {isFunction} from './../mixins/Function';
import {googleIcon} from './icons';

/**
 * @typedef {Object} CustomMarkerData
 * @property {GooglePointOptions} location
 * @property {{lat:string|number, lng:string|number}} [coordinates]
 * @property {string} label
 *
 */

export class CustomsLayer {
  /**
   *
   * @param {CustomMarkerData[]} options
   * @param {google.maps.Map} map
   */
  constructor(options, map) {
    this._map = map;
    this._points = [];

    this._coordinates = [];
    this.__initVarsData(options);
  }

  onDone(callback) {
    this._onDone = callback;
    return this;
  }

  /**
   *
   * @param {CustomMarkerData[]} options
   * @private
   */
  __initVarsData(options) {
    /**
     *
     * @type {Array.<google.maps.LatLng>}
     * @private
     */
    this._coordinates = [];

    if (Array.isArray(options)) {
      this._counts = options.length;
      /**
       *
       * @type {Array.<CustomPoint>}
       * @this CustomsLayer
       * @private
       */
      this._points = options.map(function (option, index) {
        let customPoint = new CustomPoint(option, this._map, 50 + index);
        customPoint.onLoad(this.whenPointLoaded.bind(this));
        return customPoint;
      }, this);
    }

    return this;

  }

  /**
   *
   * @param {boolean} error
   * @param {CustomPoint} point
   */
  whenPointLoaded(error, point) {
    this._counts--;
    if (!error) {
      this.__createCoordinate(point);
    }

    if (this._counts == 0) {

      if (this._visible)
        this.show();

      if (this._onDone)
        this._onDone();
    }
  }

  /**
   *
   * @returns {CustomsLayer}
   */
  show() {
    this._visible = true;
    this._points.forEach(function (point) {
      point.show();
    }, this);
    return this;
  }

  /**
   *
   * @returns {CustomsLayer}
   */
  hide() {
    this._visible = false;
    this._points.forEach(function (point) {
      point.hide();
    }, this);
    return this;
  }

  /**
   *
   * @returns {CustomsLayer}
   */
  clear() {
    this.hide();
    this._points = this._points.filter(function (point) {
      point.remove();
      return false;
    });
    this._bounds = null;
    return this;
  }

  /**
   *
   * @param {CustomMarkerData[]} options
   */
  update(options) {
    this.clear().__initVarsData(options);
  }

  /**
   *
   * @param {CustomPoint} point
   * @private
   */
  __createCoordinate(point) {
    // console.log('CustomsLayer', '__createCoordinate()', arguments);

    // console.log('Point', '__createCoordinate()', point);
    if (!this._bounds)
      /**
       *
       * @type {google.maps.LatLngBounds}
       * @private
       */
      this._bounds = new google.maps.LatLngBounds();

    /**
     *
     * @type {?google.maps.LatLng}
     */
    let pos = point.getLatLng();

    if (pos !== null) {
      this._coordinates.push(pos);
      this._bounds.extend(pos);
    }

    return this;
  }

  /**
   *
   * @returns {CustomsLayer}
   */
  bound() {
    if (this._bounds && this._coordinates && this._coordinates.length > 0)
      this._map.fitBounds(this._bounds);
    return this;
  }

  /**
   *
   * @returns {google.maps.LatLngBounds}
   */
  getBounds() {
    if (!!this._bounds) {
      return this._bounds;
    }
    return null;
  }
}

class CustomPoint {

  /**
   *
   * @param {CustomMarkerData} options
   * @param {google.maps.Map} map
   * @param {number} zIndex
   */
  constructor(options, map, zIndex) {

    /**
     *
     * @type {number}
     * @private
     */
    this._zIndex = zIndex;
    /**
     *
     * @type {google.maps.Map}
     * @private
     */
    this._map = map;

    this._loaded = false;

    this._error = true;

    this._icon = (options.label ? googleIcon(true, options.label, 12, 'ff444444', .9) : undefined);

    this.__preMarkerCreate(options.location, options.coordinates);

  }

  /**
   *
   * @param {function(boolean, CustomPoint)} callback
   * @returns {CustomPoint}
   */
  onLoad(callback) {
    /**
     *
     * @type {function(boolean, CustomPoint)}
     * @private
     */
    this._onLoad = callback;
    if (this._loaded)
      return this.whenLoad();
    return this;
  }

  /**
   *
   * @param {GooglePointOptions} location
   * @param {{lat:string|number, lng:string|number}} [coordinates]
   * @private
   */
  __preMarkerCreate(location, coordinates) {
    // console.log('CustomPoint', '__preMarkerCreate()', arguments);
    if (!this._geocoder)
      this._geocoder = new google.maps.Geocoder();

    if (coordinates && coordinates.lat && coordinates.lng) {
      this.__createMarker(new google.maps.LatLng(coordinates.lat, coordinates.lng));
    } else {
      if (typeof location === "string") {
        this.__preMarkerCreate_stringLocation(location);
      } else if (typeof location.lat !== "undefined" && typeof location.lng !== "undefined") {
        if (isFunction(location.lat) && isFunction(location.lng))
          this.__createMarker(location);
        else
          this.__createMarker(new google.maps.LatLng(location.lat, location.lng));
      }
    }
  }

  /**
   *
   * @param {string} location
   * @private
   */
  __preMarkerCreate_stringLocation(location) {
    // console.log('CustomPoint', '__preMarkerCreate_stringLocation()', arguments);
    /**
     *
     * @this CustomPoint
     */
    this._geocoder.geocode({'address' : location}, (function (results, status) {

      if (status == google.maps.GeocoderStatus.OK) {
        this.__createMarker(results[0].geometry.location);
      } else if (status == google.maps.GeocoderStatus.OVER_QUERY_LIMIT) {
        setTimeout(this.__preMarkerCreate_stringLocation.bind(this, location), 500);
      } else {
        this.whenLoad(true);
        console.log(location, 'Geocode was not successful for the following reason: ' + status);
      }

    }).bind(this));

  }

  /**
   *
   * @param {google.maps.LatLng} LatLng
   * @private
   */
  __createMarker(LatLng) {
    // console.log('CustomPoint', '__createMarker()', arguments);
    /**
     *
     * @type {google.maps.Marker}
     * @private
     */
    this._marker = new google.maps.Marker({
      position : LatLng,
      icon     : this._icon,
      zIndex   : this._zIndex
      // title : ()
      // animation: google.maps.Animation.DROP,
    });

    this.whenLoad(false);
  }

  whenLoad(error) {
    this._loaded = true;

    if (typeof error !== 'undefined')
      this._error = !!error;

    if (this._onLoad)
      this._onLoad(this._error, this);
  }

  /**
   *
   * @param {?google.maps.Map} map
   * @returns {CustomPoint}
   */
  setMap(map) {
    if (this._marker)
      this._marker.setMap(map);
    return this;
  }

  /**
   *
   * @returns {CustomPoint}
   */
  show() {
    this.setMap(this._map);
    return this;
  }

  /**
   *
   * @returns {CustomPoint}
   */
  hide() {
    this.setMap(null);
    return this;
  }

  /**
   *
   * @returns {CustomPoint}
   */
  remove() {
    this.hide();
    return this;
  }

  /**
   *
   * @returns {google.maps.LatLng}
   */
  getLatLng() {
    if (this._marker)
      return this._marker.getPosition();
    return null;
  }

}

export default CustomPoint;