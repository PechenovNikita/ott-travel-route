"use strict";

import {HotelIcons} from './icons';

import {byLat} from './sortMarker';
import Updater from './../updater';

let pinnedInfoWindow = undefined, selectedHotel = undefined;

export class HotelsLayer {
  /**
   *
   * @param {OTTResultHotelSearch[]} options
   * @param {google.maps.Map} map
   */
  constructor(options, map) {
    this._map = map;
    // this._hotels = data.sort(byLat);
    this._points = [];

    this._coordinates = [];
    this.__initVarsData(options);
  }

  onDone(callback) {
    this._onDone = callback;
    return this;
  }

  /**
   *
   * @param options
   * @private
   */
  __initVarsData(options) {
    //   let markers = []
    //     , map = this._map;
    //   // hotelSearch = hotelSearch.sort(sortByLNG);
    //   this._hotels.forEach(function (data) {
    //     let hotel = new Hotel(map, data);
    //     markers.push(hotel.getMarker());
    //   });
    //
    //   new MarkerCluster(
    //     map,
    //     markers,
    //     {
    //       imagePath : '%gulp-imagePath%' + 'map/cluster/m'
    //     }
    //   );

    // let map = this._map
    //   , items = [];
    //
    // this._hotels.forEach(function (data) {
    //   let hotel = new Hotel(data);
    //   items.push(hotel);
    //   hotel.setMap(map);
    // });
    //
    // this._items = items;

    /**
     *
     * @type {Array.<google.maps.LatLng>}
     * @private
     */
    this._coordinates = [];

    if (Array.isArray(options)) {

      /**
       *
       * @type {Array.<Hotel>}
       * @this HotelsLayer
       * @private
       */
      this._points = options.sort(byLat).map(function (option, index) {
        let hotel = new Hotel(option, this._map, 100 + index);
        this.__createCoordinate(hotel);
        return hotel;
      }, this);
    }

    if (this._onDone)
      this._onDone();

    return this;

  }

  /**
   *
   * @returns {HotelsLayer}
   */
  show() {
    this._visible = true;
    this._points.forEach(function (point) {
      point.show();
    }, this);
    return this;
  }

  /**
   *
   * @returns {HotelsLayer}
   */
  hide() {
    this._visible = false;
    this._points.forEach(function (point) {
      point.hide();
    }, this);
    return this;
  }

  /**
   *
   * @returns {HotelsLayer}
   */
  clear() {
    this.hide();
    this._points = this._points.filter(function (hotel) {
      hotel.remove();
      return false;
    });
    this._bounds = null;
    return this;
  }

  /**
   *
   * @param {OTTPoiData[]} options
   */
  update(options) {
    this.clear().__initVarsData(options);
  }

  /**
   *
   * @param {Hotel} point
   * @private
   */
  __createCoordinate(point) {
    // console.log('HotelsLayer', '__createCoordinate()', arguments);

    // console.log('Point', '__createCoordinate()', point);
    if (!this._bounds)
      /**
       *
       * @type {google.maps.LatLngBounds}
       * @private
       */
      this._bounds = new google.maps.LatLngBounds();

    /**
     *
     * @type {?google.maps.LatLng}
     */
    let pos = point.getLatLng();

    if (pos !== null) {
      this._coordinates.push(pos);
      this._bounds.extend(pos);
    }

    return this;
  }

  /**
   *
   * @returns {HotelsLayer}
   */
  bound() {
    if (this._bounds && this._coordinates && this._coordinates.length > 0)
      this._map.fitBounds(this._bounds);
    return this;
  }

  /**
   *
   * @returns {google.maps.LatLngBounds}
   */
  getBounds() {
    if (!!this._bounds) {
      return this._bounds;
    }
    return null;
  }
}

/**
 * @typedef {Object} OTTPoiData
 * @property {int} id
 * @property {number} lat
 * @property {number} lng
 * @property {string} name
 * @property {string} name_en
 * @property {string} type
 * @property {number} 0
 * @property {number} 1
 * @property {string} 2
 * @property {string} 3
 */
class Hotel {
  /**
   *
   * @param {string} img
   * @param {string} name
   * @param {boolean} [autopan=false]
   * @returns {google.maps.InfoWindow}
   */
  static createBlockHotel(img, name, autopan = false) {
    return new google.maps.InfoWindow({
      disableAutoPan : !autopan,
      content        : `
      <div class="block-map-hotel">
        <div class="block-map-hotel__image" style="background-image: url(${img})"></div>
        <div class="block-map-hotel__info">
          <div class="block-map-hotel__info__name">${name}</div>
        </div>
      </div>`
    });
  }

  /**
   *
   * @param {OTTResultHotelSearch} options
   * @param {google.maps.Map} map
   * @param {number} zIndex
   */
  constructor(options, map, zIndex) {

    /**
     *
     * @type {number}
     * @private
     */
    this._zIndex = zIndex;
    /**
     *
     * @type {google.maps.Map}
     * @private
     */
    this._map = map;
    /**
     *
     * @type {OTTResultHotelSearch}
     * @private
     */
    this._data = options;
    /**
     *
     * @type {number}
     * @private
     */
    this._lat = options.lat;
    /**
     *
     * @type {string}
     * @private
     */
    this._lng = options.lng;
    // /**
    //  *
    //  * @type {string}
    //  * @private
    //  */
    // this._type = options.type;
    /**
     *
     * @type {string}
     * @private
     */
    this._name = options.name;

    this._icon = HotelIcons.def;

    // let $divContainer = $(this._map.getDiv());
    // if (!$divContainer.data('evented')) {
    //   $divContainer.on('mouseover', '.block-map-hotel', function () {
    //     windowHover = true;
    //   });
    //   $divContainer.on('mouseout', '.block-map-hotel', function () {
    //     console.log('out');
    //     windowHover = false;
    //     if (hotelHover)
    //       setTimeout(hotelHover.closeWindow.bind(hotelHover), 500);
    //   });
    //   $divContainer.data('evented', true);
    // }

    this.__createMarker();

  }

  __createMarker() {
    /**
     *
     * @type {google.maps.InfoWindow}
     * @private
     */
    this._infowindow = Hotel.createBlockHotel(this._data.img, this._data.name, false);
    /**
     *
     * @type {google.maps.Marker}
     * @private
     */
    this._marker = new google.maps.Marker({
      position : {
        lat : this._lat,
        lng : this._lng
      },
      icon     : this._icon,
      title    : this._name,
      zIndex   : this._zIndex,
      // animation: google.maps.Animation.DROP,
    });

    this._listenerOver = google.maps.event.addListener(this._marker, 'mouseover', this.__mouseOver.bind(this));
    this._listenerOut = google.maps.event.addListener(this._marker, 'mouseout', this.__mouseOut.bind(this));
    this._listenerClick = google.maps.event.addListener(this._marker, 'click', this.__click.bind(this));

    Updater.icons(this.setDefaultIcon.bind(this));
  }

  __click() {
    if (pinnedInfoWindow) {
      selectedHotel.unSelect();
    }
    this.select();
  }

  unSelect() {
    pinnedInfoWindow.close();

    selectedHotel = undefined;
    pinnedInfoWindow = undefined;

    this.setUnHover();
    google.maps.event.removeListener(this.listener);
    this.listener = undefined;
  }

  select() {
    selectedHotel = this;

    pinnedInfoWindow = Hotel.createBlockHotel(this._data.img, this._data.name, true);
    this.closeWindow();
    pinnedInfoWindow.open(this._map, this._marker);

    this.listener = google.maps.event.addListener(pinnedInfoWindow, 'closeclick', this.unSelect.bind(this));
    this.setSelected();

  }

  __mouseOver() {
    // this._in = true;
    if (pinnedInfoWindow) {
      return this.setHover();
    }
    this.setHover().showWindow();
  }

  /**
   *
   * @returns {Hotel}
   * @private
   */
  __mouseOut() {
    // this._in = false;

    if (pinnedInfoWindow) {
      return this.setUnHover();
    }

    this.setUnHover();
    setTimeout(this.closeWindow.bind(this), 500);
    this.closeWindow();
    return this;
  }

  /**
   *
   * @returns {Hotel}
   */
  setSelected() {
    this._marker.setIcon(HotelIcons.selected);
    this._marker.setZIndex(999);
    return this;
  }

  /**
   *
   * @returns {Hotel}
   */
  setHover() {
    if (selectedHotel == this)
      return this;
    this._marker.setIcon(HotelIcons.hover);
    this._marker.setZIndex(998);
    return this;

  }

  /**
   *
   * @returns {Hotel}
   */
  setUnHover() {
    if (selectedHotel == this)
      return this;
    this._marker.setIcon(HotelIcons.def);
    this._marker.setZIndex(this._zIndex);
    return this;

  }

  /**
   *
   * @returns {Hotel}
   */
  setDefaultIcon() {
    this._marker.setIcon(HotelIcons.def);
    this._marker.setZIndex(this._zIndex);
    return this;
  }

  showWindow() {
    if (this._map)
      this._infowindow.open(this._map, this._marker);
    return this;
  }

  closeWindow() {
    this._infowindow.close();
    return this;
  }

  /**
   *
   * @param map
   * @returns {Hotel}
   */
  setMap(map) {
    if (this._marker)
      this._marker.setMap(map);
    return this;
  }

  /**
   *
   * @returns {Hotel}
   */
  show() {
    this.setMap(this._map);
    return this;
  }

  /**
   *
   * @returns {Hotel}
   */
  hide() {
    this.closeWindow().setMap(null);
    return this;
  }

  /**
   *
   * @returns {Hotel}
   */
  remove() {
    if (selectedHotel === this)
      selectedHotel.unSelect();

    this.hide();

    google.maps.event.removeListener(this.listener);

    google.maps.event.removeListener(this._listenerClick);
    google.maps.event.removeListener(this._listenerOver);
    google.maps.event.removeListener(this._listenerOut);

    return this;
  }

  /**
   *
   * @returns {google.maps.LatLng}
   */
  getLatLng() {
    if (this._marker)
      return this._marker.getPosition();
    return null;
  }

}

export default Hotel;