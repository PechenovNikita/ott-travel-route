// import OtherMap from './mapbox/mapbox.standalone.uncompressed';

import React from 'react';
import {render} from 'react-dom';

// console.log(OtherMap);

export default function Test(W, D, L, undefined) {
  'use strict';

  W = window;
  console.log('Test', W.L);

  var app = W.app || {
        vars       : {
          MAX_POLLING_COUNT    : 2,
          activatePopUnder     : true,
          dimension            : "20160511_listOrMap",
          emptySearch          : true,
          gaSortType           : "20160928_default",
          goPopUp              : false,
          isCallbackBannerShow : true,
          isHotelDiscount      : true,
          isHotelsProject      : true,
          isNewPage            : false,
          isPackagesTab        : true,
          isPartner            : false,
          isSEOCityPage        : false,
          isTouchScreen        : false,
          isTrackConversion    : true,
          isWithFlight         : true,
          langLocale           : "ru",
          listOrMap            : true,
          mile                 : 1.609344,
          newDealsLimit        : 18,
          pathData             : 'Object',
          plLocale             : false,
          sortType             : "default",
          time                 : 'Object',
          trLocale             : false,
          video                : false,
          wasOnAvia            : true
        },
        langLocale : {
          currency   : "RUB",
          lang       : "ru",
          langLocale : "ru",
          locale     : "ru",
          variants   : {
            az      : true,
            de      : true,
            "en-gb" : true,
            "en-ie" : true,
            "en-us" : true,
            es      : true,
            "kk-kz" : true,
            pl      : true,
            ru      : true,
            "ru-kz" : true,
            "ru-ua" : true,
            tr      : true,
            "uk-ua" : true
          }
        }
      },
    MapAPI = W.L,
    MapStorage = (function (self) {
      var isListOrMap = app.vars.listOrMap,
        LIST_OR_MAP_HEIGHT = 130,
        NEW_BLUE_HEIGHT = app.vars.isPartner ? 90 : 62,
        COOKIE_HEIGHT = 47,
        HEADER_HEIGHT = isListOrMap ? LIST_OR_MAP_HEIGHT : NEW_BLUE_HEIGHT,
        SCROLL_TOP_MAP = 48,
        COLUMN_WIDTH = isListOrMap ? 200 : 500,
        zIndex = 10000,
        MAX_COUNT_OF_PRICE_MARKERS = 10,
        DISTANCE_BETWEEN_MARKERS = 60,
        MAP_WIDTH = 330,
        lastHotelId;

      console.log(app);

      Backbone.on('cookie.show', function () {
        if (!isListOrMap) {
          HEADER_HEIGHT = NEW_BLUE_HEIGHT + COOKIE_HEIGHT;
          self.resizeMapDiv();
        }
      });

      Backbone.on('cookie.hide', function () {
        if (!isListOrMap) {
          HEADER_HEIGHT = NEW_BLUE_HEIGHT;
          self.resizeMapDiv();
        }
      });

      self.initMapScroll = function () {
        var oldIsFixed;
        // W.addEventListener('scroll', function () {
        //   var scrollTop = D.body.scrollTop || D.documentElement.scrollTop,
        //     isFixed = scrollTop > SCROLL_TOP_MAP;
        //   if (oldIsFixed !== isFixed) {
        //     Backbone.trigger('payload', {
        //       action : 'scrollMapPage',
        //       data   : {
        //         isFixed : isFixed
        //       }
        //     });
        //     oldIsFixed = isFixed;
        //   }
        // }, false)
      };

      self.resizeMapDiv = function () {
        var div = self.mapDiv,
          heightViewport,
          widthViewport,
          newHeight,
          newWidth,
          zoomDiv;
        if (div && div.parentNode) {
          heightViewport = D.documentElement.clientHeight || D.body.clientHeight;
          widthViewport = D.documentElement.clientWidth || D.body.clientWidth;
          newHeight = heightViewport - HEADER_HEIGHT;
          newWidth = widthViewport - COLUMN_WIDTH;
          if (app.vars.listOrMap) {
            if (self.isList) {
              newHeight = MAP_WIDTH * (self.isList ? 1 : 2) - 2;
            }
            newWidth = MAP_WIDTH * (self.isList ? 1 : 2) - 2;
          }
          div.parentNode.style.height = newHeight + 'px';
          div.parentNode.style.width = newWidth + 'px';
          div.style.height = newHeight + 'px';
          div.style.width = newWidth + 'px';
          zoomDiv = div.querySelector('.leaflet-control-zoom');
          if (zoomDiv) {
            zoomDiv.style.top = (newHeight / 2 - 26) + 'px';
            if (!app.vars.listOrMap) {
              zoomDiv.style.left = '14px';
            }
          }
          if (self.appMap) {
            self.appMap.invalidateSize();
          }
        }
      };

      self.initMap = function (opts) {
        var options = opts || {},
          div = options.div,
          city = self.currentCity || {},
          lat = city.lat || 0,
          lng = city.lng || 0,
          tileJSON = {
            "bounds"   : [-180, -85, 180, 85],
            "data"     : ["//a.tiles.mapbox.com/v4/madmaxx.jn6cha66/features.json?access_token=pk.eyJ1IjoibWFkbWF4eCIsImEiOiJJT0xBaTBBIn0.w7DkXt1g1v5cSNfBVwBIuA"],
            "dataset"  : "madmaxx.k597el7h",
            "id"       : "madmaxx.jn6cha66",
            "maxzoom"  : 18,
            "minzoom"  : 0,
            "name"     : "hotels",
            "private"  : true,
            "scheme"   : "xyz",
            "tilejson" : "2.0.0",
            "tiles"    : [
              "//a.tiles.mapbox.com/v4/madmaxx.jn6cha66/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFkbWF4eCIsImEiOiJJT0xBaTBBIn0.w7DkXt1g1v5cSNfBVwBIuA",
              "//b.tiles.mapbox.com/v4/madmaxx.jn6cha66/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFkbWF4eCIsImEiOiJJT0xBaTBBIn0.w7DkXt1g1v5cSNfBVwBIuA"
            ],
            "webpage"  : "//a.tiles.mapbox.com/v4/madmaxx.jn6cha66/page.html?access_token=pk.eyJ1IjoibWFkbWF4eCIsImEiOiJJT0xBaTBBIn0.w7DkXt1g1v5cSNfBVwBIuA"
          },
          map = MapAPI.mapbox.tileLayer(tileJSON, {
            detectRetina : true
          }),
          mapOptions = {
            dragging               : true,
            fadeAnimation          : false,
            zoomAnimation          : false,
            markerZoomAnimation    : false,
            minZoom                : 5,
            zoomAnimationThreshold : 10,
            zoomControl            : false,
            touchZoom              : false,
            scrollWheelZoom        : false
          };
        self.mapDiv = div;
        self.resizeMapDiv();
        self.appMap = MapAPI.map(div, mapOptions).setView([lat, lng], 12).addLayer(map);
        new MapAPI.Control.Zoom({
          position : !app.vars.listOrMap ? 'topleft' : 'topright'
        }).addTo(self.appMap);
        MapAPI.control.scale({
          position : 'bottomright'
        }).addTo(self.appMap);
        self.resizeMapDiv();

        self.appMap.on('zoomend', function () {
          Backbone.trigger('payload', {
            action : 'mapZoomChanged'
          });
        });

        self.appMap.on('mousemove', function (e) {
          Backbone.trigger('payload', {
            action : 'mapMouseMove',
            data   : {
              eva : e
            }
          });
        });

        self.appMap.on('moveend', function () {
          Backbone.trigger('payload', {
            action : 'mapMoveEnd'
          });
        });

        self.appMap.on('dragend', function (e) {
          self.handleMapEvent({
            type : 'MAP_MOVE_END',
            eva  : e
          });
        });

        self.appMap.on('click', function (e) {
          Backbone.trigger('payload', {
            action : 'clickMap',
            data   : {
              eva : e
            }
          });
        });

        var zoomInDiv = self.mapDiv.querySelector('.leaflet-control-zoom-in');
        var zoomOutDiv = self.mapDiv.querySelector('.leaflet-control-zoom-out');
        if (zoomInDiv) {
          zoomInDiv.addEventListener('click', function () {
            self.handleMapEvent({
              type : 'MAP_ZOOM_IN_CLICK'
            });
          }, false);
        }
        if (zoomOutDiv) {
          zoomOutDiv.addEventListener('click', function () {
            self.handleMapEvent({
              type : 'MAP_ZOOM_OUT_CLICK'
            });
          }, false);
        }
      };

      self.handleMapEvent = (function () {
        var hashOfEvents = {};
        var USE_MAP = 'USE_MAP';
        var MAP_INIT = 'MAP_INIT';
        var LOCK = false;
        return function (opts) {
          var options = opts || {};
          var eva = options.eva || {};
          var type = options.type || "";

          if (!hashOfEvents[type] && !LOCK) {
            hashOfEvents[type] = true;
            Backbone.trigger('analytics.event', {
              name : type
            });
          }

          if (type === 'MAP_MOVE_END') {
            LOCK = true;
            setTimeout(function () {
              LOCK = false;
            }, 100);
          }

          if (!hashOfEvents[USE_MAP] && type !== MAP_INIT) {
            Backbone.trigger('analytics.event', {
              name : USE_MAP
            });
            hashOfEvents[USE_MAP] = true;
          }
        }
      }());

      self.getMarkersData = function () {
        var hotels = self.hotels || [],
          markers = [],
          map = self.appMap,
          zoom = map.getZoom(),
          point,
          x,
          y,
          roundX,
          roundY,
          hashStr,
          step = 10,
          radius = 4,
          hash = {},
          showSold = self.showSold,
          markersCopyArr,
          markersCount;
        hotels.forEach(function (hotel) {
          if (hotel.show) {
            point = map.project([hotel.lat, hotel.lng], zoom);
            x = point.x;
            y = point.y;
            roundX = ~~(x / step);
            roundY = ~~(y / step);
            hashStr = roundX + '-' + roundY;
            if (!hash[hashStr] && (showSold ? true : hotel.hasPrice)) {
              markers.push({
                index    : hotel.orderSortMarkers,
                x        : x,
                y        : y,
                radius   : radius,
                lat      : hotel.lat,
                lng      : hotel.lng,
                hotelId  : hotel.id,
                hasPrice : hotel.hasPrice
              });
              hash[hashStr] = true;
            }
          }
        });
        markersCopyArr = markers.slice(0).filter(function (marker) {
          return marker.hasPrice;
        });
        markersCopyArr.sort(function (a, b) {
          return a.index - b.index;
        });
        markersCount = markersCopyArr.length;
        markersCopyArr.forEach(function (marker, index) {
          marker.coef = index / markersCount;
        });
        self.renderedMarkers = markers;
        return markers;
      };

      self.getColorForMarker = function (opts) {
        var self = this,
          options = opts || {},
          coef = options.coef;
        return self.getCoefColor({
          coef : coef
        });
      };

      self.getCoefColor = function (opts) {
        var options = opts || {},
          coef = options.coef,
          color = '',
          alpha = W.parseFloat(options.alpha) || 0,
          r,
          g,
          b;
        if (coef < 0.3) {
          color = '98c64d';
        } else if (coef < 0.7) {
          color = '82b21e';
        } else {
          color = '5c8a03';
        }
        if (alpha) {
          r = parseInt(color.slice(0, 2), 16);
          g = parseInt(color.slice(2, 4), 16);
          b = parseInt(color.slice(4, 6), 16);
          return 'rgba(' + r + ',' + g + ',' + b + ',' + alpha + ')';
        }
        return '#' + color;
      };

      self.renderMarkers = (function () {
        var canvasTiles,
          timeout;

        return function (opts) {
          clearTimeout(timeout);
          if (self.appMap) {
            timeout = setTimeout(function () {
              var map = self.appMap,
                options = opts || {},
                markers = self.getMarkersData(),
                scale = 2,
                tileSize = 256,
                lengthOfCircle = 2 * Math.PI,
                show = options.show;
              if (canvasTiles) {
                map.removeLayer(canvasTiles);
              }
              if (!show) {
                return;
              }
              canvasTiles = MapAPI.tileLayer.canvas();
              canvasTiles.drawTile = function (canvas, tilePoint) {
                canvas.width = canvas.width * scale;
                canvas.height = canvas.height * scale;
                var ctx = canvas.getContext('2d'),
                  left = tilePoint.x * tileSize,
                  top = tilePoint.y * tileSize,
                  right = left + tileSize,
                  bottom = top + tileSize,
                  marker,
                  len = markers.length,
                  i,
                  x,
                  y,
                  centerX,
                  centerY,
                  radius,
                  markerZone,
                  lineWidth = scale * 4,
                  color = '#199cdb';
                ctx.lineWidth = lineWidth;

                for (i = 0; i < len; i += 1) {
                  marker = markers[i];
                  x = marker.x;
                  y = marker.y;
                  centerX = x - left + 1;
                  centerY = y - top + 1;
                  radius = marker.radius;
                  markerZone = radius + radius;
                  color = self.getColorForMarker({
                    coef : marker.coef
                  });
                  ctx.fillStyle = color;
                  ctx.strokeStyle = color;
                  ctx.fillStyle = marker.hasPrice ? color : '#ffffff';
                  if (x > (left - markerZone) && x < (right + markerZone) && y > (top - markerZone) && y < (bottom + markerZone)) {
                    ctx.beginPath();
                    ctx.arc(centerX * scale, centerY * scale, radius * scale, -Math.PI * 2 / 3 - Math.PI / 2, Math.PI * 2 / 3 - Math.PI / 2, false);
                    ctx.lineTo(centerX * scale, centerY * scale + 6 * scale);
                    ctx.closePath();
                    ctx.stroke();
                    ctx.fill();
                  }
                }
                canvas.style.width = tileSize + 'px';
                canvas.style.height = tileSize + 'px';
              };
              map.addLayer(canvasTiles);
            }, 50);
          } else {
            clearTimeout(timeout);
            timeout = setTimeout(function () {
              self.renderMarkers(opts);
            }, 200);
          }
        }
      }());

      self.showPopup = function (opts) {
        var options = opts || {},
          hotelId = options.hotelId || 0,
          map = self.appMap,
          hotel;
        self.hotels.some(function (objHotel) {
          if (hotelId === objHotel.id) {
            hotel = objHotel;
            return true;
          }
        });
        if (hotel) {
          self.popupMarker = MapAPI.marker([hotel.lat, hotel.lng], {
            icon         : self.getPopupMarkerIcon({
              hotel : hotel
            }),
            zIndexOffset : 1000000
          });
          self.popupMarker.addTo(map);
          self.correctDesc();
        }
      };

      self.removePopup = function () {
        if (self.popupMarker && self.appMap) {
          self.appMap.removeLayer(self.popupMarker);
        }
      };

      self.getPopupMarkerContent = function (opts) {
        var options = opts || {},
          hotel = options.hotel || {},
          rating = hotel.ty.rating / 10,
          ratingValue = rating + l10n.hotels.ty.from,
          ratingElem,
          hotelName = (app.langLocale.lang === 'ru' ? (hotel.translatedName || hotel.name) : hotel.name),
          ratingClass = 'rating',
          starsClass = 'stars',
          stars = +hotel.stars || 0,
          starsList = [],
          hasPrice = hotel.hasPrice,
          className = options.isExtend ? 'lView extend' : 'lView';
        starsClass += ' s' + stars;
        ratingClass += rating >= 9 ? ' good' : '';
        ratingElem = rating ? '<div class="' + ratingClass + '">' + ratingValue + '</div>' : '';
        while (stars-- >= 0) {
          starsList.push('<div class="star"></div>');
        }
        return '<div class="' + className + '">' +
          '<div class="pContent">' +
          '<img class="pImg" src="' + hotel.image + '" />' +
          '<div class="pInfo">' +
          ratingElem +
          '<div class="line">' +
          '<div class="name">' + hotelName + '</div>' +
          '<div class="' + starsClass + '">' + starsList.join('') + '</div>' +
          '</div>' +
          '<div class="line">' +
          '<div class="desc">' + hotel.address + '</div>' +
          (hasPrice ? ('<div class="cost">' + self.getPrice({
            prices       : hotel.prices,
            currency     : self.currentCurrency,
            isPriceClear : self.isPriceClear,
            isForNight   : self.isForNight
          }) + '</div>') : '<div class="sold">' + l10n.hotels.sold + '</div>') +
          '</div>' +
          '</div>' +
          '</div>' +
          '</div>';
      };

      self.getPopupMarkerIcon = function (opts) {
        var options = opts || {},
          hotel = options.hotel || {};
        return MapAPI.divIcon({
          className : 'popupMarker',
          html      : self.getPopupMarkerContent({
            hotel : hotel
          }),
          iconSize  : [2, 2]
        });
      };

      self.getPrice = function (opts) {
        var options = opts || {},
          currency = options.currency || '',
          prices = options.prices || {},
          priceObj = prices[currency] || {},
          isPriceClear = options.isPriceClear,
          isForNight = options.isForNight,
          price = (isForNight ? (isPriceClear ? priceObj.priceClearDay : priceObj.priceDay) : (isPriceClear ? priceObj.priceClearPeriod : priceObj.pricePeriod)) || 0,
          isSignLeft = currency === 'USD' || currency === 'EUR' || currency === 'GBP',
          priceEnd = String(Math.ceil(price)).split('').reverse().map(function (sym, index) {
            return [2, 5, 8, 11].indexOf(index) === -1 ? sym : '\u2009' + sym;
          }).reverse().join('').trim(),
          currencySign = {
            USD : '$',
            EUR : '€',
            GBP : '£',
            UAH : '₴',
            TRY : '₺',
            KZT : '₸',
            PLN : 'zł'
          }[currency],
          currencyClassName = currencySign ? 'money-formatted' : ' ',
          currencyContent = currencySign || currency,
          RUBcurrency = app.tools.getRUBcurrency();

        if (price) {
          if (currency === 'RUB') {
            return '<span>' + priceEnd + ' <em class="' + RUBcurrency.className + '">' + RUBcurrency.sign + '</em></span>';
          } else if (isSignLeft) {
            return '<span><em class="' + currencyClassName + '">' + currencyContent + '</em> ' + priceEnd + '</span>';
          } else {
            return '<span>' + priceEnd + ' <em class="' + currencyClassName + '">' + currencyContent + '</em></span>';
          }
        }
        return '';
      };

      self.correctDesc = function () {
        var descs = [].slice.call(document.querySelectorAll('.popupMarker .desc'), 0);
        descs.forEach(function (desc) {
          var parent = desc.parentNode,
            parentHeight = parent.clientHeight,
            cost,
            costWidth,
            width;
          if (parentHeight > 25) {
            cost = parent.querySelector('.cost') || {};
            costWidth = cost.clientWidth || 0;
            width = 210 - costWidth - 15;
            desc.style.width = width + 'px';
          }
        });
      };

      self.getPlaceMarkerDiv = function (opts) {
        var options = opts || {},
          target = options.target || {};
        while (target) {
          if ((' ' + target.className + ' ').indexOf(' mPlace ') >= 0) {
            return target;
          }
          target = target.parentNode;
        }
        return false;
      };

      self.getPriceMarkerDiv = function (opts) {
        var options = opts || {},
          target = options.target || {};
        while (target) {
          if ((' ' + target.className + ' ').indexOf(' mPopup ') >= 0) {
            return target;
          }
          target = target.parentNode;
        }
        return false;
      };

      self.mapMouseMove = (function () {
        var timeout; // must be less then zIndexOffset
        return function (opts) {
          clearTimeout(timeout);
          timeout = setTimeout(function () {
            var options = opts || {},
              map = self.appMap,
              zoom = map.getZoom(),
              eva = options.eva || {},
              originalEva = eva.originalEvent || {},
              latLng = eva.latlng || {},
              mousePoint = map.project([latLng.lat, latLng.lng], zoom),
              x = ~~mousePoint.x,
              y = ~~mousePoint.y,
              markers = self.renderedMarkers || [],
              target = originalEva.target,
              placeMarker = self.getPlaceMarkerDiv({
                target : target
              }),
              priceMarker = self.getPriceMarkerDiv({
                target : target
              });
            target.style.cursor = 'default';
            if (self.hoverPlaceMarker) {
              self.hoverPlaceMarker.removeAttribute('name');
              self.hoverPlaceMarker = null;
            }
            self.removePopup();
            self.deselectPriceMarker();
            if (placeMarker) {
              if (app.vars.listOrMap) {
                Backbone.trigger('payload', {
                  action : 'hotelsListDeSelectHotel'
                });
              }
              self.hoverPlaceMarker = placeMarker;
              placeMarker.setAttribute('name', 'marker');
              if (placeMarker.parentNode) {
                placeMarker.parentNode.style.zIndex = zIndex;
              }
              zIndex += 1;
              target.style.cursor = 'pointer';
            } else if (priceMarker) {
              priceMarker.style.zIndex = zIndex;
              zIndex += 1;
              target.style.cursor = 'pointer';
            } else {
              Backbone.trigger('payload', {
                action : 'hotelsListDeSelectHotel'
              });
              markers.some(function (marker) {
                var radius = marker.radius,
                  markerZone = radius + 2;
                if ((x + markerZone) >= marker.x &&
                  (x - markerZone) <= marker.x &&
                  (y + markerZone) >= marker.y &&
                  (y - markerZone) <= marker.y) {
                  target.style.cursor = 'pointer';
                  if (!app.vars.listOrMap) {
                    Backbone.trigger('payload', {
                      action : 'showPopup',
                      data   : {
                        hotelId : marker.hotelId
                      }
                    });
                  } else {
                    Backbone.trigger('payload', {
                      action : 'hotelsListSelectHotel',
                      data   : {
                        hotelId : marker.hotelId
                      }
                    });
                  }
                  return true;
                }
              });
            }
          }, 10);
        }
      }());

      self.mapClick = function (opts) {
        var options = opts || {},
          map = self.appMap,
          zoom = map.getZoom(),
          eva = options.eva || {},
          latLng = eva.latlng || {},
          mousePoint = map.project([latLng.lat, latLng.lng], zoom),
          x = ~~mousePoint.x,
          y = ~~mousePoint.y,
          markers = self.renderedMarkers || [];
        self.removePopup();
        markers.some(function (marker) {
          var radius = marker.radius;
          if ((x + radius) >= marker.x &&
            (x - radius) <= marker.x &&
            (y + radius) >= marker.y &&
            (y - radius) <= marker.y) {
            Backbone.trigger('payload', {
              action : 'clickHotelCard',
              data   : {
                hotelId : marker.hotelId,
                type    : 'fromMap'
              }
            });
            return true;
          }
        });
      };

      self.setViewportCenter = (function () {
        var timeout;
        return function () {
          var bounds = [],
            allLatLng = self.hotels.map(function (hotel) {
              return {
                lat      : hotel.lat,
                lng      : hotel.lng,
                distance : hotel.distance
              };
            }),
            firstPartOfHotels = Math.ceil(allLatLng.length * 0.7);
          clearTimeout(timeout);
          if (self.appMap) {
            allLatLng.sort(function (a, b) {
              return a.distance - b.distance;
            }).slice(0, firstPartOfHotels).forEach(function (latLng) {
              bounds.push([latLng.lat, latLng.lng]);
            });
            if (bounds.length) {
              self.appMap.fitBounds(bounds);
            }
          } else {
            timeout = setTimeout(function () {
              self.setViewportCenter();
            }, 200);
          }
        };
      }());

      self.makePlaces = function (opts) {
        var options = opts || {},
          places = options.places || [],
          groupedPlaces = {},
          activePlaces = {
            //	anchor: true,
            //	business: true,
            casino   : true,
            //	civic: true,
            //	golf: true,
            historic : true,
            icecream : true,
            //	medical: true,
            monument : true,
            // museums  : true,
            //	school: true,
            shopping : true,
            sign     : true,
            //	skiing: true,
            //	stadium: true,
            //	sunglass: true,
            theater  : true,
            //	tree: true,
            winery   : true,
            airport  : true,
            railway  : true,
            bus      : true,
            metro    : true
          },
          hasPlaces = false,
          newPlaces = [],
          place,
          value,
          points,
          start = 0;
        places.filter(function (place) {
          return place && place[2] && activePlaces[place[2]];
        }).forEach(function (place) {
          var type = place[2],
            group = groupedPlaces[type] || {};
          group.points = group.points || [];
          group.points.push({
            lat  : place[0],
            lng  : place[1],
            type : type,
            name : place[3],
            id   : start + 12345
          });
          start += 1;
          hasPlaces = true;
          groupedPlaces[type] = group;
        });
        for (place in groupedPlaces) {
          value = groupedPlaces[place] || {};
          points = value.points || [];
          newPlaces.push({
            text   : l10n.hotels.places[place],
            type   : place,
            points : points,
            count  : points.length,
            active : false,
            hover  : false
          });
        }
        self.places = newPlaces;
        self.hasPlaces = hasPlaces;
        self.renderFirstPoints();
      };

      self.renderFirstPoints = (function () {
        var featureGroup,
          timeout;

        if (app.vars.listOrMap) {
          return function () {};
        } else {
          return function () {
            clearTimeout(timeout);
            var places = self.places || [],
              firstPoints = places.filter(function (place) {
                return place.type !== 'metro';
              }).map(function (place) {
                return place.points[0];
              }),
              markers = [],
              newFeatureGroup;
            if (self.appMap) {
              if (featureGroup) {
                featureGroup.off('click');
                self.appMap.removeLayer(featureGroup);
              }
              if (!self.removeAllPlaces) {
                firstPoints.forEach(function (point) {
                  var marker = MapAPI.marker([point.lat, point.lng], {
                    icon : self.getPlaceIcon({
                      point : point
                    })
                  });
                  markers.push(marker);
                });
                newFeatureGroup = MapAPI.featureGroup(markers);
                newFeatureGroup.on('click', self.clickMapPlace);
                newFeatureGroup.addTo(self.appMap);
                featureGroup = newFeatureGroup;
              }
            } else {
              timeout = setTimeout(function () {
                self.renderFirstPoints();
              }, 500);
            }
          };
        }
      }());

      self.updatePlace = function (opts) {
        var options = opts || {},
          type = options.type || '',
          placeType = options.placeType || '',
          places = self.places || [];
        if (type === 'click') {
          places.some(function (place) {
            if (place.type === placeType) {
              place.active = !place.active;
              return true;
            }
          });
        }
        if (type === 'hover') {
          places.forEach(function (place) {
            place.hover = place.type === placeType;
          });
        }
        if (type === 'leave') {
          places.forEach(function (place) {
            place.hover = false;
          });
        }
        self.renderPlaces();
      };

      self.getPlaceIcon = function (opts) {
        var options = opts || {},
          point = options.point || {},
          pointClass = 'mPlaceIcon';
        pointClass += ' ' + point.type;
        return MapAPI.divIcon({
          className : 'placeMarker',
          html      : '<div class="mPlace" data-type="' + point.id + '">' +
          '<div class="' + pointClass + '"></div>' +
          '<div class="mPlaceHover">' + point.name + '</div>' +
          '</div>',
          iconSize  : [2, 2]
        });
      };

      self.renderPlaces = (function () {
        var featureGroup;

        return function () {
          var places = self.places || [],
            placesToRender = places.filter(function (place) {
              return place.active || place.hover;
            }),
            markers = [],
            newFeatureGroup;
          if (featureGroup) {
            featureGroup.off('click');
            self.appMap.removeLayer(featureGroup);
          }
          if (!self.removeAllPlaces) {
            placesToRender.forEach(function (place) {
              var points = place.points || [];
              points.forEach(function (point) {
                var marker = MapAPI.marker([point.lat, point.lng], {
                  icon : self.getPlaceIcon({
                    point : point
                  })
                });
                markers.push(marker);
              });
            });
            newFeatureGroup = MapAPI.featureGroup(markers);
            newFeatureGroup.on('click', self.clickMapPlace);
            newFeatureGroup.addTo(self.appMap);
            featureGroup = newFeatureGroup;
          }
        }
      }());

      self.clickMapPlace = function (e) {
        var eva = e || {},
          originalEvent = eva.originalEvent || {},
          target = originalEvent.target || {},
          pointDiv = self.getPlaceMarkerDiv({
            target : target
          }),
          pointId = pointDiv && +pointDiv.getAttribute('data-type'),
          places = self.places || [];
        if (pointId) {
          places.some(function (place) {
            return place.points.some(function (point) {
              if (point.id === pointId && point.name) {
                self.handleMapEvent({
                  type : 'MAP_PLACE_CLICK'
                });
                W.open('//www.google.ru/search?q=' + (point.name || '').split(' ').join('+'));
                return true;
              }
            });
          });
        }
      };

      self.updateCurrentCurrency = function (opts) {
        var options = opts || {},
          type = options.type || '';
        if (type) {
          self.currentCurrency = type;
        }
      };

      self.isInMapBounds = function () {
        var bounds = self.appMap && self.appMap.getBounds();
        return function (opts) {
          var options = opts || {},
            hotel = options.hotel || {},
            lat = hotel.lat || 0,
            lng = hotel.lng || 0;
          if (bounds && bounds.contains) {
            return bounds.contains([lat, lng]);
          }
          return true; // map is not init!!!!
        };
      };

      self.getHotelHoverIcon = function () {
        return MapAPI.divIcon({
          className : 'hotelHoverPopup',
          html      : '<div class="hHotelPopup">' +
          '<div class="hHotelPopupInner"></div>' +
          '</div>',
          iconSize  : [2, 2]
        });
      };

      self.removeHotelHoverPopup = function () {
        if (self.hotelHoverPopup && self.appMap) {
          self.appMap.removeLayer(self.hotelHoverPopup);
          self.hotelHoverPopup = null;
        }
        self.deselectPriceMarker();
      };

      self.calculatePriceMarkers = (function () {
        var timeout;
        return function (opts) {
          var options = opts || {};
          clearTimeout(timeout);
          timeout = setTimeout(function () {
            self.renderPriceMarkers({
              hotels : options.hotels || []
            });
          }, 30);
        };
      }());

      self.renderPriceMarkers = function (opts) {
        var options = opts || {},
          hotel,
          hotels = options.hotels || [],
          count = 0,
          popups = [],
          popupsGroup,
          popupClass = 'mPopup green',
          popupClassExtend = app.vars.listOrMap && false ? 'mPopup green extend' : popupClass,
          bounds,
          otherPriceHotels = [],
          canAdd = false,
          points = [],
          projection,
          currentZoom,
          createPopup = function (hotel) {
            var extendedContent = app.vars.listOrMap && false ? '<div class="popupMarker">' + self.getPopupMarkerContent({
              hotel    : hotel,
              isExtend : true
            }) + '</div>' : '';
            return MapAPI.marker([hotel.lat, hotel.lng], {
              icon : MapAPI.divIcon({
                className : popupClassExtend,
                html      : extendedContent + '<div class="lView" data-hotelid="' + hotel.id + '"><div class="pContent">' + self.getPrice({
                  prices       : hotel.prices,
                  currency     : self.currentCurrency,
                  isPriceClear : self.isPriceClear,
                  isForNight   : self.isForNight
                }) + '</div><div class="after"></div></div>',
                iconSize  : [2, 2]
              })
            });
          };
        if (self.appMap) {
          bounds = self.appMap.getBounds();
          currentZoom = self.appMap.getZoom();
          if (self.renderMarkerPopupsHash) {
            self.renderMarkerPopupsHash.off('click');
            self.renderMarkerPopupsHash.off('mouseout');
            self.renderMarkerPopupsHash.off('mouseover');
            Backbone.trigger('payload', {
              action : 'hotelsListDeSelectHotel'
            });
            self.appMap.removeLayer(self.renderMarkerPopupsHash);
          }
          self.renderMarkerPopupsHash = null;
          hotels.forEach(function (hotel) {
            if (hotel.hasPrice) {
              projection = self.appMap.project([hotel.lat, hotel.lng], currentZoom);
              if (hotel.visible && hotel.show) {
                popups.push(createPopup(hotel));
                points.push({
                  x : projection.x,
                  y : projection.y
                });
              } else {
                if (bounds.contains([hotel.lat, hotel.lng])) {
                  otherPriceHotels.push(hotel);
                }
              }
            }
          });
          otherPriceHotels.sort(function (a, b) {
            return a.orderSortMarkers - b.orderSortMarkers;
          });
          while (MAX_COUNT_OF_PRICE_MARKERS > count) {
            hotel = otherPriceHotels.shift();
            if (hotel) {
              canAdd = true;
              projection = self.appMap.project([hotel.lat, hotel.lng], currentZoom);
              points.forEach(function (p) {
                var x = Math.abs(p.x - projection.x),
                  y = Math.abs(p.y - projection.y),
                  pixDistance = Math.sqrt(x * x + y * y);
                if (pixDistance < DISTANCE_BETWEEN_MARKERS) {
                  canAdd = false;
                }
              });
              if (canAdd && hotel.show) {
                popups.push(createPopup(hotel));
                points.push({
                  x : projection.x,
                  y : projection.y
                });
                count += 1;
              }
            } else {
              break;
            }
          }
          if (popups.length) {
            if (app.vars.listOrMap) {
              popups = popups.slice(0, 10)
            }
            popupsGroup = MapAPI.featureGroup(popups);
            popupsGroup.on('click', function (popup) {
              var originalEvent = popup.originalEvent,
                elemWithId = originalEvent.currentTarget.querySelector(['[data-hotelid]']),
                hotelId = +elemWithId.getAttribute('data-hotelid');
              Backbone.trigger('payload', {
                action : 'clickHotelCard',
                data   : {
                  hotelId : hotelId,
                  type    : 'fromMap'
                }
              });
            });
            popupsGroup.on('mouseover', function (popup) {
              var originalEvent = popup.originalEvent,
                $target = $((originalEvent || {}).target),
                $parent = $target.parents('.lView'),
                hotelId = +$parent.data('hotelid');
              Backbone.trigger('payload', {
                action : 'hotelsListSelectHotel',
                data   : {
                  hotelId : hotelId
                }
              });
              self.correctDesc();
            });
            /*
             popupsGroup.on('mouseout', function (popup) {
             Backbone.trigger('payload', {
             action: 'hotelsListDeSelectHotel'
             });
             });
             */
            self.renderMarkerPopupsHash = popupsGroup;
            popupsGroup.addTo(self.appMap);
            if (lastHotelId) {
              self.showHotelHoverPopup({
                hotelId : lastHotelId
              });
            }
          }
        }
      };

      self.showHotelHoverPopup = function (opts) {
        var options = opts || {},
          hotelId = +options.hotelId || 0,
          hotels = self.hotels || [],
          hotel;
        hotels.some(function (item) {
          if (item.id === hotelId) {
            hotel = item;
            return true;
          }
        });
        if (hotel && self.appMap) {
          if (hotel.hasPrice && !app.vars.listOrMap) {
            return self.selectPriceMarker({
              hotelId : hotelId
            });
          }
          if (self.hotelHoverPopup) {
            self.hotelHoverPopup.setLatLng([hotel.lat, hotel.lng]);
          } else {
            self.hotelHoverPopup = MapAPI.marker([hotel.lat, hotel.lng], {
              icon : self.getHotelHoverIcon({
                hotel : hotel
              })
            });
            self.hotelHoverPopup.addTo(self.appMap);
          }
          if (self.isMapUseAsFilter) {

          } else {
            self.appMap.setView([hotel.lat, hotel.lng]);
          }
        }
      };

      self.selectPriceMarker = function (opts) {
        var options = opts || {},
          hotelId = options.hotelId || 0,
          queryClass = '.mPopup.green .lView',
          markers = [].slice.call(D.querySelectorAll(queryClass), 0),
          hotels = self.hotels || [],
          hotel,
          bounds;
        hotels.some(function (item) {
          if (item.id === hotelId) {
            hotel = item;
            return true;
          }
        });
        markers.forEach(function (marker) {
          var dataId = +marker.getAttribute('data-hotelid');
          if (hotelId === dataId) {
            marker.setAttribute('name', 'selected');
            if (marker.parentNode) {
              marker.parentNode.style.zIndex = zIndex;
              zIndex += 1;
            }
          } else {
            marker.removeAttribute('name');
          }
        });
        if (hotel && self.appMap) {
          if (self.isMapUseAsFilter) {

          } else {
            bounds = self.appMap.getBounds();
            if (!bounds.contains([hotel.lat, hotel.lng])) {
              self.appMap.setView([hotel.lat, hotel.lng]);
            }
          }
        }
      };

      self.deselectPriceMarker = function () {
        var queryClass = '.mPopup.green .lView',
          markers = [].slice.call(D.querySelectorAll(queryClass), 0);
        markers.forEach(function (marker) {
          marker.removeAttribute('name');
        });
      };

      self.goToHotelOnMap = (function () {
        var timeout,
          isEnter = false;

        return function (opts) {
          var options = opts || {},
            type = options.type || '',
            hotelId = options.hotelId || 0;
          if (type === 'leave') {
            isEnter = false;
          }
          if (type === 'enter') {
            isEnter = true;
          }
          if (type === 'move') {
            clearTimeout(timeout);
            timeout = setTimeout(function () {
              if (isEnter) {
                lastHotelId = hotelId;
                self.showHotelHoverPopup({
                  hotelId : hotelId
                });
              } else {
                lastHotelId = null;
                self.removeHotelHoverPopup();
              }
            }, 100);
          }
        };
      }());

      self.mapClickShowSold = function () {
        self.showSold = !self.showSold;
        self.renderMarkers({
          show : true
        });
      };

      self.removeAllPlacesFromMap = function () {
        self.removeAllPlaces = !self.removeAllPlaces;
        self.renderFirstPoints();
        self.renderPlaces();
      };

      self.calculateDistrictsPolygons = (function () {
        var polygonsSelected = {},
          polygonsHovered = {},
          polygonsOnMap = [],
          renderPolygons = function () {
            var polygons = _.extend({}, polygonsHovered, polygonsSelected),
              id,
              value,
              polygon,
              map = self.appMap;
            if (map) {
              polygonsOnMap.forEach(function (polygon) {
                map.removeLayer(polygon);
              });
              for (id in polygons) {
                value = polygons[id];
                if (value) {
                  polygon = MapAPI.layerGroup();
                  polygon.addTo(map);
                  polygon.addLayer(MapAPI.polygon(value, {
                    fillColor : '#82b21e',
                    color     : '#5c8a03',
                    weight    : 1,
                    clickable : false
                  }));
                  polygonsOnMap.push(polygon);
                }
              }
            }
          };

        return function (opts) {
          var options = opts || {},
            eventType = options.eventType || '',
            type = +options.type || '',
            itemType = options.itemType || '',
            data = self.districtsCoods || [];
          if (itemType === 'area') {
            if (eventType === 'enter') {
              data.forEach(function (v) {
                if (v.type === type) {
                  polygonsHovered[type] = v.coords;
                }
              });
            }
            if (eventType === 'select') {
              data.forEach(function (v) {
                if (v.type === type) {
                  polygonsSelected[type] = v.coords;
                }
              });
            }
            if (eventType === 'deselect') {
              data.forEach(function (v) {
                if (v.type === type) {
                  delete polygonsSelected[type];
                }
              });
            }
            if (eventType === 'leave') {
              data.forEach(function (v) {
                if (v.type === type) {
                  delete polygonsHovered[type];
                }
              });
            }
            renderPolygons();
          }
          if (eventType === 'reset') {
            polygonsSelected = {};
            polygonsHovered = {};
            renderPolygons();
          }
        }
      }());

      self.setDistrictsCoords = function (opts) {
        var options = opts || {},
          districts = options.districts || [],
          coords = districts.map(function (item) {
            var c = (item.coordinates || []).map(function (v) {
              return [v[0], v[1]];
            });
            return {
              type   : item.id,
              coords : c
            };
          });
        self.districtsCoods = coords;
      };

      self.isCursorOnMap = function (opts) {
        var options = opts || {},
          target = options.target || {};
        while (target) {
          if (target.id === 'appMap') {
            return true;
          }
          target = target.parentNode;
        }
        return false;
      };

      self.mouseMoveDeSelectHotel = function (opts) {
        var options = opts || {},
          eva = options.eva || {},
          priceMarker = self.getPriceMarkerDiv({
            target : eva.target
          }),
          isCursorOnMap = self.isCursorOnMap({
            target : eva.target
          });
        if (!priceMarker && !isCursorOnMap) {
          Backbone.trigger('payload', {
            action : 'hotelsListDeSelectHotel'
          });
        }
      };

      Backbone.on('payload', function (opts) {
        var options = opts || {},
          action = options.action,
          data = options.data;
        switch (action) {
          case 'pollingDoneWithNoOffers':
            self.showSold = true;
            break;
          case 'clickOnMapDiv':
            self.handleMapEvent({
              type : 'MAP_CLICK'
            });
            return true;
          case 'mousemove':
            self.mouseMoveDeSelectHotel({
              eva : data.eva
            });
            return true;
            break;
          case 'scrollMapPage':
            self.isFixed = data.isFixed;
            break;
          case 'districtsInfoLoaded':
            self.setDistrictsCoords({
              districts : data.districts
            });
            return true;
            break;
          case 'districtsMouseEvent':
            self.calculateDistrictsPolygons(data);
            return true;
            break;
          case 'districtsReset':
            self.calculateDistrictsPolygons({
              eventType : 'reset'
            });
            return true;
            break;
          case 'listOrMapChoose':
            self.isList = data.isList;
            Backbone.trigger('payload', {
              action : 'updateFilters'
            });
            self.resizeMapDiv();
            self.setViewportCenter();
            break;
          case 'hotelsListAdditionalInfo':
            self.isForNight = data.isForNight;
            self.isPriceClear = data.isPriceClear;
            return true;
            break;
          case 'removeAllPlacesFromMap':
            self.handleMapEvent({
              type : 'MAP_PLACES_DISABLE'
            });
            self.removeAllPlacesFromMap();
            break;
          case 'mapClickShowSold':
            self.handleMapEvent({
              type : 'MAP_CLICK_SHOW_SOLD'
            });
            self.mapClickShowSold();
            break;
          case 'hotelsPriceMarkers':
            self.hotels = data.hotels;
            self.calculatePriceMarkers({
              hotels : data.hotels
            });
            return true;
            break;
          case 'hotelCardMouseEnter':
            self.goToHotelOnMap({
              hotelId : data.hotelId,
              type    : 'enter'
            });
            return true; // there is no reason to update View
            break;
          case 'hotelCardMouseLeave':
            self.goToHotelOnMap({
              hotelId : data.hotelId,
              type    : 'leave'
            });
            return true; // there is no reason to update View
            break;
          case 'hotelCardMouseMove':
            self.goToHotelOnMap({
              hotelId : data.hotelId,
              type    : 'move'
            });
            return true; // there is no reason to update View
            break;
          case 'mapMoveEnd':
            if (self.isMapUseAsFilter) {
              Backbone.trigger('payload', {
                action : 'updateFilters'
              });
            } else {
              self.calculatePriceMarkers({
                hotels : self.hotels
              });
              return true;
            }
            break;
          case 'useMapAsFilter':
            self.isMapUseAsFilter = !self.isMapUseAsFilter;
            self.handleMapEvent({
              type : 'MAP_USE_AS_FILTER'
            });
            Backbone.trigger('payload', {
              action : 'updateFilters'
            });
            break;
          case 'currencyChange':
            self.updateCurrentCurrency({
              type : data.type
            });
            break;
          case 'clickPlace':
            self.handleMapEvent({
              type : 'MAP_CLICK_PLACE_MENU'
            });
            if (!self.removeAllPlaces) {
              self.updatePlace({
                placeType : data.type,
                type      : 'click'
              });
            } else {
              return true;
            }
            break;
          case 'hoverPlace':
            self.updatePlace({
              placeType : data.type,
              type      : 'hover'
            });
            break;
          case 'leavePlace':
            self.updatePlace({
              placeType : data.type,
              type      : 'leave'
            });
            break;
          case 'places':
            self.makePlaces({
              places : data.places
            });
            break;
          case 'mapMouseMove':
            self.mapMouseMove({
              eva : data.eva
            });
            break;
          case 'showPopup':
            self.showPopup({
              hotelId : data.hotelId
            });
            break;
          case 'initMap':
            self.initMap({
              div : data.div
            });
            self.handleMapEvent({
              type : 'MAP_INIT'
            });
            break;
          case 'updateCity':
            self.lastCity = data.item;
            return true;
            break;
          case 'hotelsListUpdated':
            self.renderMarkers({
              show : true
            });
            break;
          case 'clickMap':
            self.mapClick({
              eva : data.eva
            });
            break;
          case 'updateHotelsInfo':
            self.hotels = data.hotels;
            self.setViewportCenter();
            break;
          case 'mapZoomChanged':
            self.renderMarkers({
              show : true
            });
            self.calculatePriceMarkers({
              hotels : self.hotels
            });
            break;
          default:
            return true;
        }
        self.trigger('update');
      });

      Backbone.on('search.action.submit', function () {
        self.currentCity = self.lastCity;
        self.trigger('update');
      });

      Backbone.on('getFilterFunctionForMapSync', function (opts) {
        var options = opts || {},
          callback = options.callback || function () {};
        callback(self.isMapUseAsFilter ? function (hotel) {
          return self.isInMapBounds()({
            hotel : hotel
          });
        } : false);
      });

      self.getState = function () {
        return {
          currentCurrency  : self.currentCurrency,
          places           : self.places,
          hasPlaces        : self.hasPlaces,
          isMapUseAsFilter : self.isMapUseAsFilter,
          showSold         : self.showSold,
          removeAllPlaces  : self.removeAllPlaces,
          isForNight       : self.isForNight,
          isPriceClear     : self.isPriceClear,
          isList           : self.isList,
          isFixed          : self.isFixed
        };
      };

      self.initMapScroll();

      return self;
    }(_.extend({}, Backbone.Events, {
      removeAllPlaces  : false,
      hotels           : [],
      appMap           : null,
      mapDiv           : null,
      currentCity      : {},
      renderedMarkers  : [],
      lastCity         : {},
      places           : [],
      hasPlaces        : false,
      currentCurrency  : app.langLocale.currency,
      isMapUseAsFilter : false,
      hotelHoverPopup  : null,
      showSold         : false,
      isForNight       : true,
      isPriceClear     : false,
      isList           : true,
      districtsCoods   : [],
      isFixed          : false
    }))),
    Map = React.createClass({
      shouldComponentUpdate : function () {
        return false;
      },
      componentDidMount     : function () {
        var self = this,
          map = ReactDOM.findDOMNode(self.refs.map);
        Backbone.trigger('payload', {
          action : 'initMap',
          data   : {
            div : map
          }
        });
      },
      clickMap              : function () {
        Backbone.trigger('payload', {
          action : 'clickOnMapDiv'
        });
      },
      render                : function () {
        var self = this;
        return <div ref="map" onClick={self.clickMap}></div>
      }
    }),
    UseAsFilter = React.createClass({
      clickUseAsFilter : function () {
        Backbone.trigger('payload', {
          action : 'useMapAsFilter'
        });
      },
      render           : function () {
        var self = this,
          data = self.props.data || {},
          bClass = data.isMapUseAsFilter ? 'active' : '',
          itemClass = 'item q';
        return <div className="useMapAsFilter">
          <div className={itemClass} onClick={self.clickUseAsFilter}>
            <b className={bClass}/>{l10n.hotels.checkboxMap}
          </div>
        </div>
      }
    }),
    MapContainer = React.createClass({
      oldStateHash         : null,
      update               : function () {
        var self = this,
          state = MapStorage.getState(),
          newStateHash = JSON.stringify(state);
        if (newStateHash !== self.oldStateHash) {
          self.setState(state);
          self.oldStateHash = newStateHash;
        }
      },
      componentWillMount   : function () {
        var self = this;
        MapStorage.on('update', self.update);
      },
      componentWillUnmount : function () {
        var self = this;
        MapStorage.off('update', self.update);
      },
      getInitialState      : function () {
        return MapStorage.getState();
      },
      clickPlaceItem       : function (e) {
        var currentTarget = e.currentTarget,
          type = currentTarget.getAttribute('data-type');
        Backbone.trigger('payload', {
          action : 'clickPlace',
          data   : {
            type : type
          }
        });
      },
      hoverPlaceItem       : function (e) {
        var currentTarget = e.currentTarget,
          type = currentTarget.getAttribute('data-type');
        Backbone.trigger('payload', {
          action : 'hoverPlace',
          data   : {
            type : type
          }
        });
      },
      leavePlaceItem       : function (e) {
        var currentTarget = e.currentTarget,
          type = currentTarget.getAttribute('data-type');
        Backbone.trigger('payload', {
          action : 'leavePlace',
          data   : {
            type : type
          }
        });
      },
      clickShowSold        : function () {
        Backbone.trigger('payload', {
          action : 'mapClickShowSold'
        });
      },
      removeAllPlaces      : function () {
        Backbone.trigger('payload', {
          action : 'removeAllPlacesFromMap'
        });
      },
      render               : (function () {

        if (app.vars.listOrMap) {
          return function () {
            var self = this,
              state = self.state,
              maxNumber = state.places.reduce(function (number, place) {
                if (place.count > number) {
                  number = place.count;
                }
                return number;
              }, 0),
              showSoldClass = state.showSold ? 'active' : '',
              removeAllPlaces = state.removeAllPlaces,
              places = state.places.map(function (type, index) {
                var bClass = type.active && !removeAllPlaces ? 'active' : '',
                  itemClass = 'item q',
                  text = type.text;
                if (removeAllPlaces) {
                  itemClass += ' disabled';
                }
                return <div key={index} className={itemClass} data-type={type.type}
                            onMouseEnter={self.hoverPlaceItem} onMouseLeave={self.leavePlaceItem}
                            onClick={self.clickPlaceItem}>
                  <b className={bClass}/>
                  {text}
                  <span className="count">{' ' + maxNumber}</span>
                  <div className="number">{type.count}</div>
                </div>
              }),
              mainClass = 'listOrMap',
              legendClass = 'mLegend mGreen';
            if (!state.isList) {
              mainClass += ' scroll';
            }
            if (state.isFixed) {
              mainClass += ' fixed';
            }
            if (app.vars.isSEOCityPage) {
              mainClass += ' high';
            }
            legendClass += ' listOrMap';
            places.push(<div key='removeAll' className="removeAll item q" onClick={self.removeAllPlaces}>
              <b className={removeAllPlaces ? 'active' : ''}/>
              {l10n.hotels.mapLegend.removeAll}
            </div>);

            return <div id="appMap" className={mainClass}>
              <Map />
              {!state.isList ? <div className={legendClass}>
                <div className="price">{l10n.hotels.mapLegend.price}</div>
                <div className="sold">{l10n.hotels.mapLegend.sold}</div>
                <div className="showSold q" onClick={self.clickShowSold}><b
                  className={showSoldClass}/>{l10n.hotels.mapLegend.showSold}</div>
              </div> : false}
              {places.length && !state.isList && state.hasPlaces ? <div className="sPlaces">
                <div className="sButton">
                  <div className="sIcon"></div>
                  <div className="sList">{places}</div>
                </div>
              </div> : false}
            </div>
          }
        } else {
          return function () {
            var self = this,
              state = self.state,
              maxNumber = state.places.reduce(function (number, place) {
                if (place.count > number) {
                  number = place.count;
                }
                return number;
              }, 0),
              showSoldClass = state.showSold ? 'active' : '',
              removeAllPlaces = state.removeAllPlaces,
              places = state.places.map(function (type, index) {
                var bClass = type.active && !removeAllPlaces ? 'active' : '',
                  itemClass = 'item q',
                  text = type.text;
                if (removeAllPlaces) {
                  itemClass += ' disabled';
                }
                return <div key={index} className={itemClass} data-type={type.type}
                            onMouseEnter={self.hoverPlaceItem} onMouseLeave={self.leavePlaceItem}
                            onClick={self.clickPlaceItem}>
                  <b className={bClass}/>
                  {text}
                  <span className="count">{' ' + maxNumber}</span>
                  <div className="number">{type.count}</div>
                </div>
              }),
              mainClass = app.vars.listOrMap ? 'listOrMap' : '',
              legendClass = 'mLegend mGreen';
            places.push(<div key='removeAll' className="removeAll item q" onClick={self.removeAllPlaces}>
              <b className={removeAllPlaces ? 'active' : ''}/>
              {l10n.hotels.mapLegend.removeAll}
            </div>);
            return <div id="appMap" className={mainClass}>
              <Map />
              <UseAsFilter data={state}/>
              <div className={legendClass}>
                <div className="price">{l10n.hotels.mapLegend.price}</div>
                <div className="sold">{l10n.hotels.mapLegend.sold}</div>
                <div className="showSold q" onClick={self.clickShowSold}><b
                  className={showSoldClass}/>{l10n.hotels.mapLegend.showSold}</div>
              </div>
              {places.length && state.hasPlaces ? <div className="sPlaces">
                <div className="sButton">
                  <div className="sIcon"></div>
                  <div className="sList">{places}</div>
                </div>
              </div> : false}
            </div>
          }
        }
      }())
    });

  app.appMap = MapContainer;

  render(<MapContainer/>, document.getElementById('wrapper__test'))

}

Test();
