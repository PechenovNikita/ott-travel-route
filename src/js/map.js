"use strict";

/**
 * @typedef {Object} AdminPoint
 * @property {string} [name]
 * @property {number|string} [lat]
 * @property {number|string} [lng]
 */

/**
 * @typedef {string|google.maps.LatLng|AdminPoint} GooglePointOptions
 */

import Updater from './updater';

import Direction from './map/Direction';
import {PointsLayer} from './map/Point';

import {HotelsLayer} from './map/Hotel';
import {SightLayer} from './map/Sight';
import {CustomsLayer} from './map/Custom';

function isEmpty(obj) {

  // null and undefined are "empty"
  if (obj == null) return true;

  // Assume if it has a length property with a non-zero value
  // that that property is correct.
  if (obj.length > 0)    return false;
  if (obj.length === 0)  return true;

  // If it isn't an object at this point
  // it is empty, but it can't be anything *but* empty
  // Is it empty?  Depends on your application.
  if (typeof obj !== "object") return true;

  // Otherwise, does it have any properties of its own?
  // Note that this doesn't handle
  // toString and valueOf enumeration bugs in IE < 9
  for (var key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key)) return false;
  }

  return true;
}
/**
 *
 * @const {(bounds:google.maps.LatLngBounds, mapDim:{height: number, width: number})=>number}
 */
const getBoundsZoomLevel = function () {

  const WORLD_DIM = {
    height : 256,
    width  : 256
  }, ZOOM_MAX = 21;

  function latRad(lat) {
    let sin = Math.sin(lat * Math.PI / 180);
    let radX2 = Math.log((1 + sin) / (1 - sin)) / 2;
    return Math.max(Math.min(radX2, Math.PI), -Math.PI) / 2;
  }

  function zoom(mapPx, worldPx, fraction) {
    return Math.floor(Math.log(mapPx / worldPx / fraction) / Math.LN2);
  }

  return function (bounds, mapDim) {

    let ne = bounds.getNorthEast()
      , sw = bounds.getSouthWest();

    let latFraction = (latRad(ne.lat()) - latRad(sw.lat())) / Math.PI;

    let lngDiff = ne.lng() - sw.lng()
      , lngFraction = ((lngDiff < 0) ? (lngDiff + 360) : lngDiff) / 360;

    let latZoom = zoom(mapDim.height, WORLD_DIM.height, latFraction)
      , lngZoom = zoom(mapDim.width, WORLD_DIM.width, lngFraction);

    return Math.min(latZoom, lngZoom, ZOOM_MAX);
  }
}();

/**
 * @const {(bounds:google.maps.LatLngBounds,pixelWidth:number)=>number}
 */
const getZoomBounds = (function () {
  const GLOBE_WIDTH = 256; // a constant in Google's map projection
  return function (bounds, pixelWidth) {

    let ne = bounds.getNorthEast()
      , sw = bounds.getSouthWest();

    let west = sw.lng()
      , east = ne.lng()
      , angle = east - west;
    if (angle < 0) {
      angle += 360;
    }

    return Math.round(Math.log(pixelWidth * 360 / angle / GLOBE_WIDTH) / Math.LN2);
  }
})();

Updater.googleMapInit(function () {
  MapG.build();
  Updater.add(MapG.build);
});

export const MAP_STORE = {};

class MapG {

  static build() {
    $('.block-map:visible').not('.mapped')/*.not('.not-mapped')*/.each(function (index, el) {
      new MapG(el);
    });
  }

  // controls() {
  //   let $div = $(
  //     `<div class="map-controls">
  //       <div class="map-controls__check"></div>
  //     </div>`);
  //
  // }

  /**
   *
   * @param {HTMLElement} el
   */
  constructor(el) {
    /**
     *
     * @type {HTMLElement}
     * @private
     */
    this._el = el;
    /**
     *
     * @type {jQuery}
     */
    this.$el = $(el);
    this.$el.addClass('mapped');

    /**
     *
     * @type {google.maps.Map|undefined}
     * @private
     */
    this._map = undefined;

    this._options = undefined;

    this._direction = undefined;
    this._point = undefined;
    this._poi = undefined;

    /**
     *
     * @type {google.maps.Marker[]}
     * @private
     */
    this._markers = [];

    this.init();
  }

  /**
   *
   */
  init() {
    this.initData()
      .initMap()
      .initDirection()
      .initPoint()
      .initPOI()
      .bound();

  }

  /**
   *
   * @returns {MapG}
   */
  initData() {
    // ---- direction
    this._direction = JSON.parse(this._el.getAttribute('data-direction')) || undefined;//this.$el.data('direction');

    // ---- point
    this._point = JSON.parse(this._el.getAttribute('data-point')) || undefined;//this.$el.data('point');

    // ---- poi
    this._poi = JSON.parse(this._el.getAttribute('data-poi')) || (this._point ? this._point.poi : undefined);

    // ---- options
    let options = JSON.parse(this._el.getAttribute('data-options')) || {};
    // console.log(options);
    let zoom = options.zoom || null;//|| (this._point ? (this._point.zoom || null) : null);

    this._options = {
      zoom : parseInt(options.zoom, 10)
    };

    if (options && options.id) {
      MAP_STORE[options.id] = this;
    }

    return this;
  }

  /**
   *
   * @returns {MapG}
   */
  initMap() {
    if (this._options.zoom)
      this._map = new google.maps.Map(this._el, {
        scrollwheel : false,
        zoom        : this._options.zoom
      });
    else
      this._map = new google.maps.Map(this._el, {
        scrollwheel : false
      });

    google.maps.event.addListenerOnce(this._map, 'idle', () => {
      this._loaded = true;
      this.__ready();
    });

    return this;
  }

  isReady() {
    return this._loaded;
  }

  whenReady(callback) {
    if (this.isReady())
      callback();
    else
      this._callbackReady = callback;
  }

  __ready() {
    if (this._callbackReady)
      this._callbackReady();
  }

  /**
   *
   * @returns {MapG}
   */
  initDirection() {
    if (isEmpty(this._direction))
      return this;

    if (this._layerDirection) {
      this._layerDirection.update(this._direction);
    } else {
      /**
       *
       * @type {Direction}
       * @private
       */
      this._layerDirection = new Direction(this._direction, this._map);
      this._layerDirection.onDone(this.bound.bind(this))
    }

    this._layerDirection.show();

    return this;
  }

  initPoint() {
    if (isEmpty(this._point))
      return this;

    if (this._point.location
      && Array.isArray(this._point.location)
      && this._point.location.length == 1) {
      if (this._point.location[0] == '') {
        return this;
      }
    }

    if (this._layerPoints)
      this._layerPoints.update(this._point);
    else {
      /**
       *
       * @type {PointsLayer}
       * @private
       */
      this._layerPoints = new PointsLayer(this._point, this._map);
      this._layerPoints.onDone(this.bound.bind(this));
    }
    this._layerPoints.show();

    return this;
  }

  initPOI() {
    if (isEmpty(this._poi))
      return this;

    let poi = this._poi || {};

    if (poi.sight) {
      this.__getPOISight(poi.sight);
    }
    if (poi.hotels) {
      this.__getPOIHotels(poi.hotels);
    }
    if (poi.markers) {
      this.__getPOIMarkers(poi.markers);
    }

    return this;
  }

  /**
   *
   * @param {CustomMarkerData[]} markers
   * @private
   */
  __getPOIMarkers(markers) {

    if (this._layerCustoms)
      this._layerCustoms.update(markers);
    else {

      /**
       *
       * @type {CustomsLayer}
       * @private
       */
      this._layerCustoms = new CustomsLayer(markers, this._map);
      this._layerCustoms.onDone(this.bound.bind(this));
    }

    this._layerCustoms.show();

  }

  /**
   *
   * @param {OTTResultHotelSearch[]} hotelSearch
   * @private
   */
  __getPOIHotels(hotelSearch) {

    if (this._layerHotels)
      this._layerHotels.update(hotelSearch);
    else {

      /**
       *
       * @type {HotelsLayer}
       * @private
       */
      this._layerHotels = new HotelsLayer(hotelSearch, this._map);
      this._layerHotels.onDone(this.bound.bind(this));
    }

    this._layerHotels.show();

  }

  /**
   *
   * @param {OTTResultPoiSearch[]} poiSearch
   * @private
   */
  __getPOISight(poiSearch) {
    if (this._layerSight)
      this._layerSight.update(poiSearch);
    else {
      /**
       *
       * @type {SightLayer}
       * @private
       */
      this._layerSight = new SightLayer(poiSearch, this._map);
      this._layerSight.onDone(this.bound.bind(this));
    }

    this._layerSight.show();
  }

  /**
   *
   * @param {string} layerName
   * @param {google.maps.LatLngBounds} bounds
   * @returns {MapG}
   * @private
   */
  __addBounds(layerName, bounds) {
    /**
     * @type {Direction|PointsLayer|SightLayer|HotelsLayer|CustomsLayer}
     */
    let layer = this[layerName];
    if (layer) {
      /**
       *
       * @type {?google.maps.LatLngBounds}
       */
      let b = layer.getBounds();
      if (b) {
        bounds.union(b);
      }
    }

    return this;
  }

  bound() {
    google.maps.event.trigger(this._map, 'resize');
    /**
     *
     * @type {google.maps.LatLngBounds}
     */
    let bounds = new google.maps.LatLngBounds();

    this.__addBounds('_layerDirection', bounds)
      .__addBounds('_layerPoints', bounds)
      .__addBounds('_layerSight', bounds)
      .__addBounds('_layerHotels', bounds)
      .__addBounds('_layerCustoms', bounds);

    this.doZoom(bounds);

  }

  /**
   *
   * @param {google.maps.LatLngBounds} bounds
   */
  doZoom(bounds) {

    this._map.setCenter(bounds.getCenter());

    let listener = google.maps.event.addListener(this._map
      , "idle"
      , (function () {

        let bZoom = getBoundsZoomLevel(bounds, {
          width  : this._el.offsetWidth,
          height : this._el.offsetHeight - 30
        });

        let mZoom = this._map.getZoom();
        let oZoom = this._options.zoom;

        // let zoom = this._map.getZoom();
        if (oZoom && mZoom >= oZoom) {
          this._map.setZoom(oZoom);
        } else if (bZoom && isFinite(bZoom)) {
          this._map.setZoom(bZoom);
        } else if (!mZoom) {
          this._map.setZoom(7);
        }

        google.maps.event.removeListener(listener);
      }).bind(this)
    );
  }

  clear() {
    if (this._layerDirection)
      this._layerDirection.clear();
    if (this._layerPoints)
      this._layerPoints.clear();
    if (this._layerHotels)
      this._layerHotels.clear();
    if (this._layerSight)
      this._layerSight.clear();
    if (this._layerCustoms)
      this._layerCustoms.clear();

    return this;
  }
}

export default MapG;

