"use strict";

//   // './src/js/menu.js',
//   './src/js/JSONP.js',
//   './src/js/title.js',
//   './src/js/parallax.js',
//   // './src/js/route/waypoint.js',
//   // './src/js/route/cardLine.js',
//   // './src/js/route/card.js',
//   './src/js/map.js',
//   './src/js/card.js',
//   './src/js/marker.js',
//   './src/js/photos.js',
//   './src/js/photo/data.js',
//   './src/js/photo/thumbnail.js',
//   // './src/js/data.js'

require('./mixins/Array');

require('./title');
require('./header');
require('./parallax');
require('./map');
require('./card');
require('./waypoint/marker');
require('./waypoint/drop-down');
require('./weather');
require('./photos');
require('./share');

require('./api');

import Updater from './updater';

window.printPage = function () {
  Updater.beforePrintDo();
  window.print();
};

$(document).on('mousewheel', '.block-card__multi', function (event) {

  let scrollTop = $(this).scrollTop();

  $(this).scrollTop(scrollTop + event.originalEvent.deltaY);

  if (this.scrollHeight > this.offsetHeight) {
    event.stopPropagation();
    event.preventDefault();
  }
  // console.log('scroll', event, $(this).scrollTop(), this.scrollHeight - this.offsetHeight);
});
