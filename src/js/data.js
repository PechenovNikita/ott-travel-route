"use strict";

function init(data) {
  if (data.route) {
    var fragment = document.createDocumentFragment();
    $(data.route).each(function (index, waypoint) {
      var temp = new TRWayPoint(waypoint);
      fragment.appendChild(temp.getNode()[0]);
    });

    $('#route_section').html('').append(fragment);

    setTimeout(function () {
      renderMaps();
      InitMarkers();
    }, 10);
  }
}

$.ajax('%gulp-mainPath%data/data.json', {
  cache    : false,
  dataType : 'json'
}).done(function (response) {
  // init(response);
}).fail(function () {
  console.log('Can not use this file data or it\'s emty');
});

