"use strict";
import {whenPrint} from './mixins/print';

var $top = $('.block-top').first()
  , $back = $top.find('.block-top__background__img').first()
  , $front = $top.find('.block-top__forground').first()
  , $scroll = $top.find('.block-top__scroll').first();

var $route = $top.find('.block-top__forground__route').first()
  , $icon = $top.find('.block-top__forground__title__icon').first()
  /*, $text = $top.find('.block-top__forground__title__text').first()*/;

// <<<<<<< HEAD
// var height = $top.height()
//   , top = $top.offset().top;
//
// $back.ontouchstart = function () {};
//
// window.addEventListener('scroll', function () {
//   var scrollTop = $(window).scrollTop();
// =======
$back.ontouchstart = function () {};

whenPrint(function(){
  $icon.css('opacity', 1);
  $front.css('marginTop', 0);
  $back.css('marginTop', 0);
  $scroll.css('bottom', 18);
  $route.css('opacity', 1);
}, parallax);

// window.onbeforeprint = function () {
//   console.log('onbeforeprint');
//   $front.css('marginTop', 0);
//   $back.css('marginTop', 0);
//   $scroll.css('bottom', 18);
// };
//
// window.onafterprint = function () {
//   parallax();
// };

function parallax() {

  let height = $top.height()
    , top = $top.offset().top;

  let scrollTop = $(window).scrollTop();
  // >>>>>>> master

  if (scrollTop > top + height) {
  } else if (scrollTop <= top) {
    $front.css('marginTop', 0);
    $back.css('marginTop', 0);
    $scroll.css('bottom', 18);

  } else {

    var delta = (scrollTop - top) / 2;
    $front.css('marginTop', delta);
    $back.css('marginTop', delta);
    $scroll.css('bottom', 18 - delta);

    if (delta > height / 5) {
      $icon.css('opacity', 1 - (delta - height / 5) / 90);
      $route.css('opacity', 1 - (delta - height / 5) / 90);
    } else if (delta > height / 5 + 90) {
      $icon.css('opacity', 0);
      $route.css('opacity', 0);
    } else {
      $icon.css('opacity', 1);
      $route.css('opacity', 1);
    }
  }
}

window.addEventListener('scroll', parallax);

// $(window).on('mousewheel', function () {
//
// });
