(function () {
  "use strict";

  var $header = $('.navbar-global-cell__menu')
    , $menu = $header.find('.navbar-global-menu');
  var $items = $menu.find('.navbar-global-menu__item');
  var $burger = $('.navbar-global-burger');
  var $more = $menu.find('.navbar-global-menu__item--more')
    , $sub = $menu.find('.navbar-global-sub_menu');

  var winW = 0, isMobile = false;

  // console.log($items);
  /**
   *
   * @param {jQuery[]} items
   */
  function subAdd(items) {

    items.forEach(function ($el) {
      $sub.append($el);

      var i = 0, w = $el.width();

      while ($el.height() > 50 && i < 100) {
        w += 10;
        $el.width(w);
        i++;
      }

    });
  }

  function subPosition(windowWidth) {
    var offsetLeft = $more.css('left', '').offset().left + 250/*$sub.width()*/;

    if (offsetLeft + 20 > windowWidth) {
      $sub.css('left', windowWidth - (offsetLeft + 20));
    }
  }

  function hideItem(items, $item, moreIsVisible) {
    if (!moreIsVisible)
      $more.show().css('display', '');
    items.push($item.clone());
    $item.hide();
    return true;
  }

  function setMenuList() {
    var i = ($more.length == 1 ? $items.length - 2 : $items.length - 1), items = [], moreIsVisible = false;
    while ($menu.height() > 30 && i >= 0 || $($items[i]).hasClass('item-hide')) {
      moreIsVisible = hideItem(items, $($items[i]), moreIsVisible);
      i--;
    }
    return items;
  }

  function mobile(windowWidth) {
    if (isMobile)
      return;
    $burger.removeClass('close');
    $header.css('display', 'none');
    $items.show();
    $more.hide();
    $sub.html('');

    winW = windowWidth;
    isMobile = true;
  }

  function preDesktop() {
    if (isMobile) {
      $items.show();
      $more.hide();
      $sub.html('');

      isMobile = false;
    }
  }

  function desktop(windowWidth) {
    $burger.removeClass('close');
    $header.css({
      display : '',
      height  : ''
    });

    if ($menu.height() > 30 || windowWidth > winW) {
      winW = windowWidth;

      $items.show();
      $more.hide();
      $sub.html('');

      var items = setMenuList();

      subAdd(items.reverse());
    } else {
      winW = windowWidth;
    }

    subPosition(windowWidth);
  }

  function resize() {
    $header.css('overflow', '');
    var windowWidth = $(window).width();

    if (windowWidth < 461) {
      mobile(windowWidth);
    } else {

      preDesktop();
      desktop(windowWidth);

    }
    $header.css('overflow', 'visible');
  }

  resize();

  $burger.on('click',function(){
    $(this).toggleClass('close');
    $header.stop().slideToggle(300);
  });

  window.addEventListener('resize', resize);

}());