/**
 *
 * @param url
 * @param done
 * @param callbackFuncName
 * @returns {number}
 */
function JSONP(url, done, callbackFuncName = 'callback') {
  let id = Date.now();

  // let functionName = "func_" + Date.now();
  //
  // while (window[functionName])
  //   functionName = functionName + '0';
  //
  // window[functionName] = function (result) {
  //   if (done)
  //     done(result, id);
  // };
  //
  // url = url + '&' + callbackFuncName + '=' + functionName;

  // JSONP.append(url);

  jQuery.ajax(url, {
    method   : 'get',
    dataType : 'json'
  }).done(function (result) {
    done(result, id);
  });

  return id;

}

JSONP.ajax = function (url, done) {
  let xmlhttp = new XMLHttpRequest();

  xmlhttp.onreadystatechange = function () {
    if (xmlhttp.readyState == XMLHttpRequest.DONE) {
      if (xmlhttp.status == 200) {
        done(xmlhttp.responseText);
      }
      else if (xmlhttp.status == 400) {
        console.log('There was an error 400');
      }
      else {
        console.log('something else other than 200 was returned');
      }
    }
  };

  url += (url.indexOf('?') >= 0) ? '&_=' + Date.now() : '?_=' + Date.now();

  xmlhttp.open('GET', url, true);
  xmlhttp.send();
};

JSONP.json = function (url, done) {
  JSONP.ajax(url, function (responseText) {
    done(JSON.parse(responseText));
  })
};

JSONP.append = function (url) {
  var script = document.createElement('script');
  script.src = url;

  document.head.appendChild(script);
};
/**
 *
 * @param {?string|?string[]} tags
 * @param {{}} options
 * @param {?{lat:string|number, lng:string|number, radius:string|number}} location
 * @param {?string} text
 * @param {function} done
 * @param {number=} page
 * @param {number=} per_page
 */
JSONP.getPhotos = function (tags, options = {}, location, text, done, page = 1, per_page = 100) {
  let method = 'flickr.photos.search';

  if (Array.isArray(tags))
    tags = tags.join(',');

  let args = (tags ? '&tag_mode=' + (options.tag_mode ? options.tag_mode : 'any') + '&tags=' + tags : '');
  if (text)
    args = '&text=' + text;
  if (location && location.lat && location.lng) {
    args += '&lat=' + location.lat;
    args += '&lon=' + location.lng;
    args += (location.radius ? '&radius=' + location.radius + '&radius_units=km' : '');

    // method = 'flickr.photos.geo.photosForLocation';
  }

  args += '&page=' + page;
  args += '&per_page=' + per_page;

  let url = 'https://api.flickr.com/services/rest/?method=' + method + '&format=json&api_key=2c7d52d0d378d2df47924bbeb0cb2ad8' + args;

  JSONP(url, done, 'jsoncallback');
};

/**
 * @typedef {Object} OTTResponse
 * @property {?boolean} error
 * @property {any} result
 */

/**
 * @typedef {Object} OTTResultPoiSearch
 * @property {int} id         : 6002068
 * @property {number} lat     : 40.68893
 * @property {number} lng     : -74.0441
 * @property {string} name    : "Статуя Свободы"
 * @property {string} name_en : "Statue of Liberty"
 * @property {string} type    : "monument"
 */

/**
 * @typedef {Object} OTTResultHotelSearch
 * address : "80 Rutland Road"
 * @property {Object} amenities             : Object
 * @property {Array} chains                 : Array[0]
 * @property {string} checkin               : "15:00"
 * @property {string} checkout              : "11:00"
 * @property {string} city                  : "Brooklyn"
 * @property {number} city_id               : 5110302
 * @property {string} city_name_translated  : "Бруклин"
 * @property {any} construction             : null
 * @property {string} country_code          : "US"
 * @property {number} district_id           : 6056496
 * @property {Array} facilities             : Array[5]
 * @property {Object} facilitiesByRating    : Object
 * @property {Object} facility              : Object
 * @property {number} id                    : 297947
 * @property {string} img                   : "http://t-ec.bstatic.com/images/hotel/max500/332/33228206.jpg"
 * @property {string} img_s                 : "https://t-ec.bstatic.com/images/hotel/max500/332/33228206.jpg"
 * @property {any} img_size                 : null
 * @property {number|boolean} is_recommend  : 0
 * @property {number} lat                   : 40.65894318
 * @property {number} lng                   : -73.95793152
 * @property {any} min_price                : null
 * @property {string} name                  : "Lefferts Manor"
 * @property {any} num_floors               : null
 * @property {any} ott_rank                 : null
 * @property {number} ott_rating            : 0
 * @property {any} renovated                : null
 * @property {Array} roomtypes              : Array[0]
 * @property {int} stars                    : 0
 * @property {any} ta                       : null
 * @property {any} ta_rating                : null
 * @property {any} ta_reviews               : null
 * @property {Array} tags                   : Array[0]
 * @property {Array} themes                 : Array[2]
 * @property {number} total_rooms           : 5
 * @property {string} translated_city       : "Бруклин"
 * @property {string} translated_name       : "Lefferts Manor"
 * @property {Array} ty                     : Array[4]
 * @property {number} ty_rating             : 82
 * @property {number} ty_reviews            : 193
 * @property {number} ty_sources            : 12
 * @property {number} type                  : 24
 * @property {string} url                   : "https://www.onetwotrip.com/ru/hotel/us/brooklyn/lefferts-manor-297947?date_start=2016-12-06&date_end=2016-12-07&persons=1"
 * @property {string} zip                   : "NY 11225"
 */

/**
 * @typedef {Object} OTTCityDataResult
 * @property {string} currency                    : "RUB"
 * @property {Array<OTTResultHotelSearch>} hotels : Array[1516]
 * @property {Array<OTTResultPoiSearch>} poi      : Array[5]
 * @property {string} request_id                  : "5f97c040-aafc-11e6-a9cb-c123fb11bbb7"
 * @property {string} status                      : "progress"
 */

/**
 * @typedef {Object} OTTResultCitySearch
 * @property {int} id
 * @property {int} city_id
 * @property {string} timezone
 * @property {int} timezone_gmt
 * @property {string} name
 * @property {string} name_orig
 * @property {string} name_ascii
 * @property {number} lat
 * @property {number} lng
 * @property {int} hotels
 * @property {int} population
 * @property {string} fcode
 * @property {string} type
 * @property {string} country_code
 * @property {number} rel
 * @property {number} rel1
 * @property {int} rel_name
 * @property {string} currency
 * @property {string} stars
 * @property {string} country
 * @property {*} admin1code
 * @property {*} admin2code
 * @property {number[]} map_view
 * @property {string} city_img
 */

/**
 * @typedef {OTTResponse} OTTResponseCitySearch
 * @property {OTTResultCitySearch[]} result
 */

/**
 * @typedef {OTTResponse} OTTResponseGeoByCityID
 * @property {Array<number,string>[]} result
 */

/**
 * @typedef {OTTResponse} OTTResponseDataByCityID
 * @property {?OTTCityDataResult} result
 */
let currentDate = new Date();

function numUpdate(num) {
  return (num < 10 ? '0' + num : num);
}

let defaultDateStart = (function () {
  return currentDate.getFullYear() + '-' + numUpdate(currentDate.getMonth() + 1) + '-' + numUpdate(currentDate.getDate());
}())
  , defaultDateEnd = (function () {
  currentDate.setDate(currentDate.getDate() + 30);
  return currentDate.getFullYear() + '-' + numUpdate(currentDate.getMonth() + 1) + '-' + numUpdate(currentDate.getDate());
}());

JSONP.ott = {
  getCityId(name, callback, limit = 100, type = 'geo') {
    let url = 'https://hapi.onetwotrip.com/api/suggestRequest?query=' + name + '&limit=' + limit + '&type=' + type;
    return JSONP(url, callback);
  },
  getPOI(city_id, callback) {
    let url = 'https://hapi.onetwotrip.com/api/getPoints?city_id=' + city_id + '\'&lang=ru&locale=ru';
    return JSONP(url, callback);
  },
  getCityData(dateStart = defaultDateStart, dateEnd = defaultDateEnd, city_id, callback) {
    let url = 'https://hapi.onetwotrip.com/api/searchRequest?stars=0,1,2,3,4,5&date_start=' + dateStart + '&date_end=' + dateEnd + '&adults=1&children=0&object_id=' + city_id + '&object_type=geo';
    return JSONP(url, callback);
  },
};

export default JSONP;
