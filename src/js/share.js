var Share = (function () {
  var share = {};

  var big_text = '';

  // var tw_text = '';
  // var tw_hashtags = '';

  var sample = {
    url   : window.location.toString(),
    text  : big_text,
    title : '',
    image : ''
  };

  share.vk = function (url) {

    url = (typeof url === 'undefined') ? sample.url : url;
    // text = (typeof text === 'undefined') ? big_text : text;
    // title = (typeof title === 'undefined') ? sample.title : title;
    // image = (typeof image === 'undefined') ? sample.image : image;

    popup('http://vk.com/share.php?url=' + url /*+*/
      /*'&description=' + text +
       '&title=' + title +
       '&image=' + image*/);
  };

  share.fb = function (url) {
    url = (typeof url === 'undefined') ? sample.url : url;
    popup('http://www.facebook.com/sharer.php?u=' + url);
  };

  // share.gp = function (url) {
  //   url = (typeof url === 'undefined') ? sample.url : url;
  //   popup('https://plus.google.com/share?url=' + url);
  // };
  //
  // share.tw = function (url, text, hashtags) {
  //   url = (typeof url === 'undefined') ? sample.url : url;
  //   text = (typeof text === 'undefined') ? tw_text : text;
  //   hashtags = (typeof hashtags === 'undefined') ? tw_hashtags : hashtags;
  //   popup('http://twitter.com/share?url=' + url +
  //   '&text=' + text +
  //   '&hashtags=' + hashtags);
  // };
  //
  //
  // share.ok = function (url, text) {
  //   url = (typeof url === 'undefined') ? sample.url : url;
  //   text = (typeof text === 'undefined') ? big_text : text;
  //
  //   popup('http://www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1' +
  //   '&st._surl=' + url +
  //   '&st.comments=' + text);
  // };

  var popup = function (url) {
    var left = (window.innerWidth - 626) / 2
      , top = (window.innerHeight - 436) / 2;
    left = (left < 0) ? 0 : left;
    top = (top < 0) ? 0 : top;
    window.open(url, '', 'toolbar=0,status=0,width=626,height=436,left=' + left + ',top=' + top);
  };

  // if(jQuery){
  $(document).on('click', '.social-share', function () {
    event.stopPropagation();
    event.preventDefault();

    var label = this.getAttribute('data-share');

    if (Share[label])
      Share[label]();
  });
  // } else {
  // document.addEventListener("DOMContentLoaded", function (event) {
  //   document.addEventListener('click', function (event) {
  //     var target = event.target
  //       , what = 0;
  //     while (what < 3 && !target.classList.contains('social-share')) {
  //       what++;
  //       target = target.parentNode;
  //     }
  //     if (target.classList.contains('social-share')) {
  //       event.stopPropagation();
  //       event.preventDefault();
  //
  //       var label = target.getAttribute('data-share');
  //
  //       console.log(label, Share[label]);
  //       if (Share[label])
  //         Share[label]();
  //     }
  //   }, true);
  // });

  // }

  return share;
}());